//  LeftSideMenuViewController.swift
//  ChainShopper
//  Created by macbook on 17/09/18.
//  Copyright © 2018 Flexbox Digital. All rights reserved.

import UIKit

class LeftSideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
// MARK: VIEW DELEGATE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }

    // MARK: TABLEVIEW DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftSideMenuCell", for: indexPath) as! LeftSideMenuCell
         let cellImage = tableView.dequeueReusableCell(withIdentifier: "LeftSideMenuImageCell", for: indexPath) as! LeftSideMenuImageCell
         cell.selectionStyle = .none
         cellImage.selectionStyle = .none
       
        //*** CHANGE WHEN USER LOGGED IN ****//
        if indexPath.row == 0{
            let btnClose = cellImage.viewWithTag(101) as! UIButton
            btnClose.addTarget(self, action: #selector(LeftSideMenuViewController.buttonClosePressed(sender:)) , for: .touchUpInside)
             if bIsUserLoggedIn() == true{
                let firstname = UserDefaults.standard.value(forKey: Constants.DEFAULT_SIDE_MENU_USER) as? String
                cellImage.lblSideMenuName.text = "Hi," + " " + (firstname?.trim().decodeChatString() ?? " ")
            }
            return cellImage
        }else{
             if bIsUserLoggedIn() == true{
                cell.lblTitle.text = self.arrSideMenuUser[indexPath.row]
             }else{
                cell.lblTitle.text = self.arrSideMenuGuest[indexPath.row]
            }
            return cell
        }
    }
    @objc func buttonClosePressed(sender:UIButton!){
        dismiss(animated: true, completion: nil)
    }

   // MARK: TABLEVIEW DELEGATE
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
                DispatchQueue.main.async {
                    SideMenuManager.default.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
//                    let storyboard = UIStoryboard(name: Constants.STORYBOARD_MAIN, bundle: nil)
//                    let objHomeView = storyboard.instantiateViewController(withIdentifier: Constants.STORYBOARD_HOME_VIEW_CONTROLLER) as! HomeViewController
//                    self.navigationController?.pushViewController(objHomeView, animated: true)
                    }
                
            }

      }
}

