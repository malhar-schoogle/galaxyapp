//  Constant.swift
//  Schoogle
//  Created by Malhar on 23/10/18.
//  Copyright © 2018 Malhar. All rights reserved.

import Foundation
import UIKit



class Constant{
    
    static let SHADOW_GRY : CGFloat = 120.0 / 255.0
    
    //APP VERSION
    static let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    static let APPDELEGATE = UIApplication.shared.delegate as? AppDelegate
    static var appOldVersion = "2.3.0"
    //MARK: VIEW CONTROLLER
    static let VIEWCONTROLLER_STANDARD = "StandardViewController"
    static let VIEWCONTROLLER_SUBJECT = "SubjectViewController"
    static let VIEWCONTROLLER_TOPICS = "TopicsViewController"

    static let notificationDB = "notificationDB"
    
    //MARK:- STANDARD VIEW CONTROLLER
    static let CELL_STANDARD = "StandardCell"
    
    //MARK:- SUBJECT VIEW CONTROLLER
    static let CELL_SUBJECT  = "SubjectCell"
    
    //MARK:- TOPICS VIEW CONTROLLER
    static let CELL_TOPICS = "TopicsCell"
    static var arrPendingStandard = [Standard]()
    //MARK:- POPOVER CONTROLLER
    static let CELL_POPOVER_MENU = "PopOverMenuCell"
    
    //MARK:- MENU CONTROLLER
    static let CELL_SIDE_MENU = "MenuCell"
    static let CELL_SIDE_MENU_STANDARD = "MenuStandardCell"

    //MARK:- SEARCH BAR CELL
    static let CELL_SEARCH = "SearchCell"

    //MARK:- TABLES
    static let SYNC  =  "Sync"
    static let SYNC_CASE  =  "SyncCase"
    static let HTML  =  "Html"
    static let BOARD  =  "Board"
    static let STANDARD  =  "Standard"
    static let SUBJECT  =  "Subject"
    static let CHAPTER  =  "Chapter"
    static let CHAPTER_PRE_READING  =  "ChapterPreReading"
    static let TOPIC  =  "Topic"
    static let TOPIC_PRE_READING  =  "TopicPreReading"
    static let CONTENT_TYPE  =  "ContentType"
    static let CONTENT_GROUP  =  "ContentGroup"
    static let CONTENT_GROUP_DETAIL  =  "ContentGroupDetail"
    static let CONTENT  =  "Content"
    static let MEDIA  =  "Media"
    static let CONTENT_MEDIA  =  "ContentMedia"
    static let MEDIA_TYPE  =  "MediaType"
    static let QUESTION_PATTERN  =  "QuestionPattern"
    static let QUESTION  =  "Question"
    static let QUESTION_OPTIONS  =  "QuestionOptions"
    static let PAPER_BLUE_PRINT  =  "PaperBluePrint"
    static let PAPER_BLUE_PRINT_DETAIL  =  "PaperBluePrintDetail"
    static let PAPER_BLUE_PRINT_GRADE  =  "PaperBluePrintGrade"
    static let SYNC_STANDARD_LIST  =  "SyncStandardList"
    static let QUESTION_MEDIA  =  "QuestionMedia"
    static let QUESTION_OPTION_MEDIA  =  "QuestionOptionMedia"
    static let SYNC_TABLE_DETAIL  =  "SyncTableDetail"
    static let SYNC_TABLE_MODE  =  "SyncTableMode"
    static let SYNC_CHAPTER_LIST  =  "SyncChapterList"
    static let USER_ALLOCATION  =  "UserAllocation"
    
//    static let ASSESSMENT  =  "Assessment"
//    static let ASSESSMENT_QUESTION  =  "AssessmentQuestion"
//    static let ASSESSMENT_QUESTION_DETAILS  =  "AssessmentQuestionDetails"
//
//    static let ASSESSMENT_TYPE  =  "Assessment_type"
//    static let ASSESSMENT_TIME  =  "Assessment_time"
//    static let ASSESSMENT_SCORE  =  "Assessment_Score"
//    static let ASSESSMENT_RESULT  =  "Assessment_result"
//    static let ASSESSMENT_RESULT_UPLOAD  =  "Assessment_result_upload"
    
    static let MESSAGE_CENTER  =  "message_center"
    static let WHITELIST_URLS  =  "white_list_urls"
    static let RECENTLY_VISITED  =  "RecentlyVisited"
    static let SYNC_DATA_URLS  =  "SyncUrls"
    static let VERSION_DATA  =  "Version"
    
    static let CREATE_TABLE_SYNC_TABLE_DETAIL  =  "CREATE TABLE "
        + SYNC_TABLE_DETAIL
        + " (TableId INTEGER PRIMARY KEY, TableName TEXT, SyncTableModeId INTEGER)"
    
    static let CREATE_TABLE_SYNC_TABLE_MODE  =  "CREATE TABLE "
        + SYNC_TABLE_MODE
        + " (SyncTableModeId INTEGER PRIMARY KEY, Mode TEXT)"
    
    static let CREATE_TABLE_SYNC  =  "CREATE TABLE "
        + SYNC
        + " (TableSyncId INTEGER PRIMARY KEY AUTOINCREMENT, TableName TEXT, LastSuccessfulSyncDate TEXT, LastAttemptDate TEXT)"
    
    static let CREATE_TABLE_SYNC_CASE  =  "CREATE TABLE "
        + SYNC_CASE
        + " (SyncCaseId INTEGER PRIMARY KEY AUTOINCREMENT, CaseName TEXT, LastSuccessfulSyncDate TEXT)"
    
    static let CREATE_TABLE_BOARD  =  "CREATE TABLE "
        + BOARD
        + " (BoardId INTEGER PRIMARY KEY, BoardName TEXT,BoardAlias TEXT, ShortDescription TEXT, LongDescription TEXT, BoardImage TEXT, Remarks TEXT, IsActive INTEGER, "
        + "CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId Integer, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_STANDARD  =  "CREATE TABLE "
        + STANDARD
        + " (StandardId INTEGER PRIMARY KEY, BoardId INTEGER, StandardName TEXT, StandardAlias TEXT, ShortDescription TEXT, LongDescription TEXT, "
        + "ListOrder INTEGER, StandardImage TEXT, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER,"
        + "IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT, VersionName TEXT, IsManual INTEGER)"
    
    static let CREATE_TABLE_SUBJECT  =  "CREATE TABLE "
        + SUBJECT
        + " (SubjectId INTEGER PRIMARY KEY, StandardId INTEGER, SubjectName TEXT, SubjectAlias TEXT, ShortDescription TEXT, LongDescription TEXT, "
        + "ListOrder INTEGER, SubjectImage TEXT, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER,"
        + "IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_CHAPTER  =  "CREATE TABLE "
        + CHAPTER
        + " (ChapterId INTEGER PRIMARY KEY, SubjectId INTEGER, ChapterName TEXT, ChapterAlias TEXT, ShortDescription TEXT, LongDescription TEXT, "
        + "Importance TEXT, HasPreREading INTEGER, ListOrder INTEGER, ChapterImage TEXT, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT,"
        + "CreatedUserId INTEGER, UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_TOPIC  =  "CREATE TABLE "
        + TOPIC
        + " (TopicId INTEGER PRIMARY KEY, ChapterId INTEGER, TopicName TEXT, TopicAlias TEXT, ShortDescription Text, LongDescription TEXT, "
        + "ListOrder INTEGER, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER, "
        + "IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_CONTENT_TYPE  =  "CREATE TABLE "
        + CONTENT_TYPE
        + " (ContentTypeId INTEGER PRIMARY KEY, ContentTypeName TEXT, ContentTypeAlias TEXT, StandardId INTEGER, ContentFiles TEXT, "
        + "ShortDescription TEXT, LongDescription TEXT, IsForTeacher INTEGER, IsForStudent INTEGER, ListOrder INTEGER, Remarks TEXT, "
        + "IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER, IsDeleted INTEGER, "
        + "DeletedUserId INTEGER, DeletedDate TEXT, SubjectId INTEGER)"
    
    static let CREATE_TABLE_CONTENT  =  "CREATE TABLE "
        + CONTENT
        + " (ContentId INTEGER PRIMARY KEY, TopicId INTEGER, ContentTypeId INTEGER, ContentName TEXT, ContentDescription TEXT, ContentSummary TEXT, ContentGroupId INTEGER, "
        + "IsPasswordProtected INTEGER, ContentPassword TEXT, PrePageContentId INTEGER, NextPageContentId INTEGER, ListOrder INTEGER, IsForTeacher INTEGER, "
        + "IsForStudent INTEGER, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER, "
        + "IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_MEDIA_TYPE  =  "CREATE TABLE "
        + MEDIA_TYPE
        + " (MediaTypeId INTEGER PRIMARY KEY, MediaType TEXT, FileExtnAllowed TEXT, ListOrder INTEGER, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, "
        + "UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_MEDIA  =  "CREATE TABLE "
        + MEDIA
        + " (MediaId INTEGER PRIMARY KEY, MediaTypeId INTEGER, MediaFile TEXT, ManualMedia INTEGER, Version INTEGER, IsImageInFolderYN INTEGER, FileSize INTEGER, IsActive INTEGER, "
        + "CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT, MediaFlag TEXT)"
    
    static let CREATE_TABLE_CONTENT_MEDIA  =  "CREATE TABLE "
        + CONTENT_MEDIA
        + " (ContentMediaId INTEGER PRIMARY KEY, ContentId INTEGER, MediaId INTEGER, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_QUESTION_PATTERN  =  "CREATE TABLE "
        + QUESTION_PATTERN
        + " (QuestionPatternId INTEGER PRIMARY KEY, QuestionPatternName TEXT, QuestionPatternAlias TEXT, ShortDescription TEXT, LongDescription TEXT, "
        + "ListOrder INTEGER, IsAutoEvaluation INTEGER, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_QUESTION  =  "CREATE TABLE "
        + QUESTION
        + " (QId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, QuestionId INTEGER, QuestionPatternId INTEGER,AssessmentTypeId INTEGER, TopicId INTEGER, QuestionName TEXT, AnswerText TEXT,SolutionText TEXT, DifficultyLevel TEXT, "
        + "SuggestedMarks INTEGER, Hint TEXT, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT, ListOrder INTEGER)"
    
    static let CREATE_TABLE_QUESTION_OPTIONS  =  "CREATE TABLE "
        + QUESTION_OPTIONS
        + " (QuestionOptionsId INTEGER PRIMARY KEY, QuestionId INTEGER, OptionSr TEXT, OptionText TEXT, ColumnSr TEXT, ColumnText TEXT, IsCorrectAnswer TEXT,"
        + "IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, "
        + "DeletedDate TEXT)"
    
    static let CREATE_TABLE_SYNC_STANDARD_LIST  =  "CREATE TABLE "
        + SYNC_STANDARD_LIST
        + " (StandardId INTEGER PRIMARY KEY, BoardId INTEGER, StandardName TEXT, StandardAlias TEXT, ShortDescription TEXT, LongDescription TEXT, "
        + "ListOrder INTEGER, StandardImage TEXT, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER,"
        + "IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT, LastSuccessfulSyncDate TEXT, LastAttemptDate TEXT)"
    
    static let CREATE_TABLE_QUESTION_MEDIA  =  "CREATE TABLE "
        + QUESTION_MEDIA
        + " (QuestionMediaId INTEGER PRIMARY KEY, QuestionId INTEGER, MediaId INTEGER, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_QUESTION_OPTION_MEDIA  =  "CREATE TABLE "
        + QUESTION_OPTION_MEDIA
        + " (QuestionOptionMediaId INTEGER PRIMARY KEY, QuestionOptionId INTEGER, MediaId INTEGER, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_CONTENT_GROUP  =  "CREATE TABLE "
        + CONTENT_GROUP
        + " (ContentGroupId INTEGER PRIMARY KEY, ContentGroupName TEXT, ContentGroupDescription TEXT, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_CONTENT_GROUP_DETAIL  =  "CREATE TABLE "
        + CONTENT_GROUP_DETAIL
        + " (ContentGroupDetailId INTEGER PRIMARY KEY, ContentGroupId INTEGER, ContentId INTEGER, PrevPageContentId INTEGER, NextPageContentId INTEGER, Remarks TEXT, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_SYNC_CHAPTER_LIST  =  "CREATE TABLE "
        + SYNC_CHAPTER_LIST
        + " (ChapterId INTEGER, LastSuccessfulSyncDate TEXT)"
    
    static let CREATE_TABLE_USER_ALLOCATION  =  "CREATE TABLE "
        + USER_ALLOCATION
        + " (UserAllocationId INTEGER PRIMARY KEY, UserId INTEGER, StandardId INTEGER, SubjectId INTEGER, ContentId INTEGER, ChapterId TEXT, BoardId INTEGER, IsActive INTEGER, CreatedDate TEXT, UpdatedDate TEXT, CreatedUserId INTEGER, "
        + "UpdatedUserId INTEGER, IsDeleted INTEGER, DeletedUserId INTEGER, DeletedDate TEXT)"
    
    static let CREATE_TABLE_RECENT_VISITED_PAGE  =  "CREATE TABLE " + RECENTLY_VISITED
        + "(ChapterId INTEGER, PageId INTEGER)"
    
    static let CREATE_TABLE_SYNC_DATA_URL  =  "CREATE TABLE " + SYNC_DATA_URLS + "(Url TEXT PRIMARY KEY)"
    
    static let CREATE_TABLE_VERSION_DATA  =  "CREATE TABLE " + VERSION_DATA
        + "(StandardId INTEGER PRIMARY KEY, VersionName TEXT, ManualMedia INTEGER, FtpVersionName TEXT)"
    
//    static let CREATE_TABLE_ASSESSMENT  =  "CREATE TABLE " + ASSESSMENT + "(aid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , AssessmentId INTEGER, OrganizationLocationId INTEGER," +
//        "QuestionBlueprintId INTEGER, ChapterId INTEGER, AssessmentText TEXT, AssessmentDate TEXT, NoOfQuestionPapers INTEGER, Remarks TEXT," +
//        "ExamMode INTEGER, AssessmentTotalMarks INTEGER, Term INTEGER, PaperGenerate INTEGER, Activeyn INTEGER, CreatedDate TEXT, UpdatedDate TEXT," +
//        "CreatedUserId INTEGER, UpdatedUserId INTEGER, Deletedyn INTEGER, DeletedUserId INTEGER, DeletedDate TEXT, ResultStatus TEXT, ChapterNames TEXT, Publish INTEGER, Chapterids INTEGER" +
//    ", Academicyearid INTEGER, Subjectid INTEGER, Examtype INTEGER, Assessmenttypeid INTEGER, Assessmentresultid INTEGER, ExamDuration INTEGER, moveid INTEGER)"
   
//    static let MODIFICAR_TABLE_ASSESSMENT_DURATION =
//        "ALTER TABLE " + ASSESSMENT + " ADD ExamDuration INTEGER"
//
//    static let MODIFICAR_TABLE_ASSESSMENT_MOVEID  =
//        "ALTER TABLE " + ASSESSMENT + " ADD moveid INTEGER"
    
    static let MODIFICAR_TABLE_VERSIONDATA_FTPVERSIONNAME  =
        "ALTER TABLE " + VERSION_DATA + " ADD FtpVersionName TEXT"
    
//    static let CREATE_TABLE_ASSESSMENT_TYPE  =  "CREATE TABLE " + ASSESSMENT_TYPE + "(AssessmenttypeId INTEGER PRIMARY KEY, AssessmenttypeName TEXT)"
    
    static let CREATE_TABLE_WHITELIST_URLS  =  "CREATE TABLE " + WHITELIST_URLS + "(WhitelistId INTEGER PRIMARY KEY, WhitelistUrlName TEXT, WhitelistUrl TEXT)"
    
    static let CREATE_TABLE_MESSAGE_CENTER  =  "CREATE TABLE " + MESSAGE_CENTER + "(MessagesenderId INTEGER PRIMARY KEY, StandardId INTEGER, SectionId INTEGER,  UserId INTEGER, MessageContent TEXT, CreatedDate TEXT, CreatedUserId INTEGER, FileName TEXT, AcademicyearId INTEGER, AssessmentId TEXT, AssessmentName TEXT)"

//    static let CREATE_TABLE_ASSESSMENT_TIME  =  "CREATE TABLE " + ASSESSMENT_TIME + "(AssessmentTimingId INTEGER PRIMARY KEY, QuestionPatternId INTEGER, StandardId INTEGER, Time TEXT)"
//
//    static let CREATE_TABLE_ASSESSMENT_SCORE  =  "CREATE TABLE " + ASSESSMENT_SCORE + "(AssessmentId INTEGER PRIMARY KEY, TotalMarks INTEGER, ObtainedMarks INTEGER)"
//
//    static let CREATE_TABLE_ASSESSMENT_RESULT_UPLOAD  =  "CREATE TABLE " + ASSESSMENT_RESULT_UPLOAD + "(AssessmentId INTEGER PRIMARY KEY, AssessmentResultUpload TEXT, AssesmentResultUploaded INTEGER)"
//
//    static let CREATE_TABLE_ASSESSMENT_RESULT  =  "CREATE TABLE " + ASSESSMENT_RESULT + "(AssessmentResultDetailsId INTEGER PRIMARY KEY, StudentId INTEGER," +
//        " AssessmentQuestionId INTEGER, PaperNo INTEGER,  StudentAnswer TEXT , QuestionText TEXT , AssessmentId INTEGER , AssessmentResultId INTEGER, AssessmentQuestionDetailsId INTEGER, " +
//        "FullMarks INTEGER, ObtainedMarks INTEGER, Remarks TEXT , Part TEXT , QuestionBluePrintPartId INTEGER, Main TEXT, Section TEXT, " +
//    "CorectAnswer TEXT, AutoEvaluationyn INTEGER , IsColumnTypeQuestion INTEGER, QuestionId INTEGER )"
//
//    static let CREATE_TABLE_ASSESSMENT_QUESTION  =  "CREATE TABLE " + ASSESSMENT_QUESTION + "(AssessmentQuestionId INTEGER PRIMARY KEY, AssessmentId INTEGER," +
//        "Activeyn INTEGER,  CreatedDate TEXT,UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER," +
//    "Deletedyn INTEGER, DeletedUserId INTEGER, DeletedDate TEXT, PaperNo INTEGER)"
//
//    static let CREATE_TABLE_ASSESSMENT_QUESTION_DETAILS  =  "CREATE TABLE " + ASSESSMENT_QUESTION_DETAILS + "(AssessmentQuestionDetailsId INTEGER PRIMARY KEY, AssessmentId INTEGER, AssessmentQuestionId INTEGER," +
//        "PartId INTEGER, QuestionId INTEGER, Marks INTEGER, Activeyn INTEGER, CreatedDate TEXT," +
//    "UpdatedDate TEXT, CreatedUserId INTEGER, UpdatedUserId INTEGER, Deletedyn INTEGER, DeletedUserId INTEGER, DeletedDate TEXT, Section TEXT, Main TEXT, Part TEXT, IsColumnTypeQuestion INTEGER)"
    
    static let NOTIFICATION_POPTOSTANDARD = "pushWithNotify"
    static let NOTIFICATION_APNSMEDIA = "apnsMedia"
    static let NOTIFICATION_PUSH = "apnsPush"
     static let COLLECTION_RELOAD = "collectionreload"
     static let NOTIFICATION_DB_DOWNLOAD_API = "callDBDownloadAPI"
    
     // static var SERVICE_PORT = "8196"
     //static var SERVICE_PORT = "8180"
     static var VERSION_NUMBER = "1.0.13"
     static let GalaxyPassword = "galaxy213$"
     static var MACID = "F9FXF9CWJMVR"
     static var isFromPending = false
    
     static var NotificationZipInsert : Int = 0
     static var isNotInsert : Int = 1
     static var BOARD_ID :Int32 = 0
     static var BOARD_IMAGE = ""
     static var BOARD_NAME = ""
     static var STANDARD_ID :Int32 = 0
     static var STANDARD_NAME = ""
     static var STANDARD_IMAGE = ""
     static var SUBJECT_ID :Int32 = 0

     static var SUBJECT_NAME = ""
     static var SUBJECT_IMAGE = ""
     static var SEARCH_TEXT = ""
     static var CHAPTER_ID :Int32 = 0
     static var CHAPTER_NAME = ""
     static var CHAPTER_IMAGE = ""

     static var USER_DEFAULT_SYNC_URL = "sync_URL"
     static var USER_DEFAULT_MEDIA_URL = "media_URL"
     static var USER_DEFAULT_MAC_ID = "mac_ID"
    
     static var SYNC_URL = ""
     static var MEDIA_URL = ""
     static var FTP_URL = ""
     static var FTP_PASSWORD = ""
     static var FTP_USERNAME = ""

     static var FTP_CONFIGURATION_ID = 0
     static var CURRENTPAGE = 0
     static var isFromClearAll = false
     static var DEVICE_ID = ""
     static var DEVICE_TOKEN = ""

     static let USER_REGISTER = "isregister"
     static let USER_AUTHTOKEN = "authtoken"
     static let USER_IS_SNK = "isliveserver"
     static let USER_IS_COLUMN = "iscolumn"
    //New Assessment
    static var downloadAssessmentID:Int32 = 0
    //-----------For Exam--------------
    static let Exam_Start = "ExamStart"
    static let Exam_AssessmentID = "AssessmentID"
    static let Exam_AssessmentQuestionID = "AssessmentQuestionID"
    static let Exam_Time = "ExamTime"
    static let Exam_Date = "ExamDate"
    static let Exam_SubjectID = "ExamSubjectID"
    static let Exam_SubjectName = "ExamSubjectName"
    
   // static let pending = "Pending"
  //  static let Exam_Status_Pending = 'Pending'//"\(Constant.pending)"
    
     static var API_GET_DATA = "\(Constant.SYNC_URL)SyncMain.ashx?MacId=\(Constant.MACID)"

    static let API_GET_SUCCESS =  "\(Constant.SYNC_URL)SyncMain.ashx?MacId=\(Constant.MACID)&Status=Success"
    
    static var API_GET_DATABASE = "\(Constant.SYNC_URL)SyncGeneral.ashx?MacId=\(Constant.MACID)&Method=GetStandardZip"
    
    //TeacherTime
    static var API_GET_TeacherTimeData =  "\(Constant.SYNC_URL)SyncAssessmentMain.ashx?MacId=\(Constant.MACID)&subjectid="
    
    //Assessment
    static var ASSESSMENT_ID = ""
    
    static var API_GET_AssessmentData =  "\(Constant.SYNC_URL)SyncAssessment.ashx?MacId=\(Constant.MACID)&AssessmentId="
    static var API_Get_AssessmentResulrData = "\(Constant.SYNC_URL)SyncGeneral.ashx?MacId="
    //Assessment Result Upload API
    static var API_SyncAssessmentUpload = "\(Constant.SYNC_URL)SyncAssessmentUpload.ashx?MacId=\(Constant.MACID)"
    //New Assessment:
    static var API_Main_Sync_Assessment = "\(Constant.SYNC_URL)SyncSubjectAssessment.ashx?MacId="
    static var API_Download_Assessment = "\(Constant.SYNC_URL)SyncAssessmentDetail.ashx?MacId="
    static var API_Assessment_Check = "\(Constant.SYNC_URL)SyncAssessmentCheck.ashx?MacId=\(Constant.MACID)&"
    
    static var API_Search_DictionaryData = "https://owlbot.info/api/v1/dictionary/"
    
    //Live
    static let CONST_SYNC_URL = "http://service.snk.schoogle.co.in/AndroidForStandardAndChapter/"
     static let MEDIA_URL_STATIC = "http://media.snk.schoogle.co.in/media/blmzip/"
    
//     static let CONST_SYNC_URL = "http://testservice.snk.schoogle.co.in/AndroidForStandardAndChapter/"
    
    //Local
   /* static let  MEDIA_URL_STATIC = "http://testmedia.snk.schoogle.co.in:7011/media/blmzip/"
     static let CONST_SYNC_URL = "http://testservice.snk.schoogle.co.in/AndroidForStandardAndChapter/"
      //  static let  MEDIA_URL_STATIC = "http://192.168.1.215:7011/media/blmzip/"*/
    
     static let API_GET_USER_DETAIL = "\(Constant.SYNC_URL)SyncUserIdentity.ashx?macid="
     static let API_REGISTER_TOKEN = "\(Constant.SYNC_URL)pushapi.ashx?macid=\(Constant.MACID)&devicetoken=\(Constant.DEVICE_TOKEN)"

     static let DATE_LAST_SYNC = "07/12/1999 06:15:25 AM"
     static let IS_USER_SETTING_SAVE = "isUserSave"

    static let NOTIFICATION_SIDEMENU_CLICK = Notification.Name("onbuttonsidemenuclick")
    static let NOTIFICATION_POPOVER_CLICK = Notification.Name("onbuttonpopoverclick")
    static let NOTIFICATION_RECENTPAGE_CLICK = "RecentPage"
    
    //------------- Update API -------
    
    static let Update_Main_API_Call = "\(Constant.SYNC_URL)SyncDeviceData.ashx?MacId="
    
    static let Content_Main_API_Call = "\(Constant.SYNC_URL)SyncSubjectContent.ashx?MacId="
    
    static let Content_Allowcation_API_Call = "\(Constant.SYNC_URL)SyncSubjectContentAllocation.ashx?MacId="
    static let GetChepter_Content_API_Call = "\(Constant.SYNC_URL)GetChapterContent.ashx?MacId="
    
    static let GetContentType_Content_API_Call = "\(Constant.SYNC_URL)GetContentTypeContent.ashx?MacId="
    
    static let Media_MainAPI_Call = "\(Constant.SYNC_URL)SyncSubjectMedia.ashx?MacId="
    
    static let  Update_MEDIA_URL_STATIC = "http://testmedia.snk.schoogle.co.in:7011/media/contentmedia/"
    
    //-------- Update keys -------
    static let Old_Version = "OldVersion"
    static let Data_Version = "DataVersion"
    //-------- Update Variable ----------
    static let Update_ContentID = ""
    
    static var arrUpdateStandard = [Standard]()
    
    static var isFromUpdate = false
    static var mediaDownloadFromUpdate = false
    
}

struct MyVariables {
static var appOldVersion :  String = "2.3.0"
}

  func stringToDateConvert(strDate:String)->Date {
      let dateFormatter = DateFormatter()
      dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    let locale = NSLocale.current
    let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!

    if formatter.contains("a") {
        print("ipad is set to 12 hours")
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        let date = dateFormatter.date(from: strDate)!
        return date
     } else {
        print("ipad is set to 24 hours")
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        let date = dateFormatter.date(from: strDate)!
        return date
    }
      
  }
/*func stringToDateConvert(strDate:String)->Date {
    let dateFormatter = DateFormatter()
    //dateFormatter.calendar = Calendar(identifier: .iso8601)
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    //dateFormatter.timeZone = TimeZone(abbreviation: "GMT+5:30")//TimeZone(secondsFromGMT: 0)
    let locale = NSLocale.current
    let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!

    if formatter.contains("a") {
        print("ipad is set to 12 hours")
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        let date = dateFormatter.date(from: strDate)!
        print("ExamDate......:\(date)")
        return date
    }
    else {
      
        print("ipad is set to 24 hours")
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        let date = dateFormatter.date(from: strDate)!
        print("ExamDate......:\(date)")
        return date
       /* do {
            let dateAsString = strDate
            let df = DateFormatter()
            df.dateFormat = "MM/dd/yyyy HH:mm:ss a"

            let date1 = df.date(from: dateAsString)
            df.dateFormat = "MM/dd/yyyy hh:mm:ss a"

            let time12 = df.string(from: date1!)
            print(time12)
        }
        catch  {
               print(error)
        }*/
        
        /*dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        let date = dateFormatter.date(from: strDate)!
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        let goodDate = dateFormatter.string(from: date)
        let date1 = dateFormatter.date(from: goodDate)!
        print("ExamDate......:\(date1)")
        return date1*/
    }
}*/

/*func stringToDateConvert(strDate:String, completion: @escaping((Date) -> (Void))) {
         let dateFormatter = DateFormatter()
             dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let locale = NSLocale.current
        let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!

        if formatter.contains("a") {
               print("ipad is set to 12 hours")
               dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
               let date = dateFormatter.date(from: strDate)!
               completion(date)
        }
}*/
func getDisplayDate(date: String, dateFormat : String, displayFormat : String) -> String {
    let datetimeFormatOriginal = DateFormatter()
    datetimeFormatOriginal.dateFormat = dateFormat
    
    let datetimeFormatDisplay = DateFormatter()
    datetimeFormatDisplay.dateFormat = displayFormat
    
    return datetimeFormatDisplay.string(from: datetimeFormatOriginal.date(from: date)!)
}
extension Date {

    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }

    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }

}

func convertDateFormater(_ date: String,dateformatter:DateFormatter) -> String
{
    let dateFormatter = dateformatter
    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
    let date = dateFormatter.date(from: date)
    dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
    return  dateFormatter.string(from: date!)

}

func stringFromTimeInterval(interval: TimeInterval) -> NSString {

  let ti = NSInteger(interval)

  let seconds = ti % 60
  let minutes = (ti / 60) % 60
  let hours = (ti / 3600)

  return NSString(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
}

