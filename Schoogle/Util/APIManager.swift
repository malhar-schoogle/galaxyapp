//  APIManager.swift
//  ChainShopper
//  Created by macbook on 17/09/18.
//  Copyright © 2018 Flexbox Digital. All rights reserved.

import Foundation
import Alamofire
import AlamofireImage
import SVProgressHUD

class APIManager: downloadResponser{
    
    static let shared = APIManager()
    //fetch data from server
    func fetchDataFromAPI(strUrl : String, parameter: [String: Any],header: [String: Any] , successResponse: @escaping(NSArray?) -> Void, error:@escaping(Error?) -> Void){
       
        print(strUrl)
        print(parameter)
        Alamofire.request(strUrl, method: .post, parameters: parameter, headers:header as? HTTPHeaders).responseJSON(completionHandler: { (response) in
           
            //ERROR:
            if response.error != nil{
                print("RESPONSE ERROR : ",response.error?.localizedDescription ?? "nil")
                error(response.error)
                return
            }
            //SUCCESS:
            if let json = response.result.value as? NSArray{
                let jsonData = json 
              //  print("JSON: \(json)")
                successResponse(jsonData)
            }else{
                //BAD RESPONSE:
                successResponse(nil)
                print("Something wrong with JSON response")
            }
        })
    }
    
    func fetchDataForDictionarySearch(strUrl : String, parameter: [String: Any],header: [String: Any] , successResponse: @escaping(NSArray?) -> Void, error:@escaping(Error?) -> Void){
       
        print(strUrl)
        print(parameter)
        Alamofire.request(strUrl, method: .get, parameters: parameter, headers:header as? HTTPHeaders).responseJSON(completionHandler: { (response) in
           
            //ERROR:
            if response.error != nil{
                print("RESPONSE ERROR : ",response.error?.localizedDescription ?? "nil")
                error(response.error)
                return
            }
            //SUCCESS:
            if let json = response.result.value as? NSArray{
                let jsonData = json
              //  print("JSON: \(json)")
                successResponse(jsonData)
            }else{
                //BAD RESPONSE:
                successResponse(nil)
                print("Something wrong with JSON response")
            }
        })
    }
    
    func downloadDatabase(url : String, bIsMediaDownload: Bool , parameters : [String:Any] ,filePath : String, successResponse: @escaping(Bool?) -> Void, error:@escaping(Error?) -> Void){
        
        var fileURL: URL = URL(fileURLWithPath: filePath)
        var tempDirectory = fileURL
        
        if(!bIsMediaDownload){
            fileURL = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent(fileURL.lastPathComponent)
        }else{
            tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent(fileURL.lastPathComponent)
        }
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (tempDirectory, [.createIntermediateDirectories, .removePreviousFile])
        }
        if(bIsMediaDownload){
            self.downloadMediaFiles(sourceURL: URL.init(string: url)!, destinationURL: fileURL)
        }else{
          
        var totalPer = 0.0

         SVProgressHUD.setDefaultMaskType(.gradient)
        Alamofire.download(url, method: .get , parameters: parameters, encoding: JSONEncoding.default, to: destination)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                if bIsMediaDownload{
                    let currentPer = Double(String.init(format: "%.2f", progress.fractionCompleted))
                    if(totalPer != currentPer){
                        totalPer = currentPer ?? 0.0
                        DispatchQueue.main.async {
                            Constant.APPDELEGATE?.updateProgress(progress: Float(totalPer))
                        }
                        print("PERCNTG: ", totalPer)
                    }
                }
            }
            .validate { request, response, temporaryURL, destinationURL in
                return .success
            }
            .responseJSON { response in

                if(bIsMediaDownload){
                    if let errorObj = response.error{
                        Constant.APPDELEGATE?.showAlert(title: "Error", message: errorObj.localizedDescription, actionTitles:["OK"], actions:[{action in
                            },nil])
                        successResponse(false)
                        return
                    }
                    self.copyItems(tempDirectory: tempDirectory, fileURL: fileURL, successResponse: { (bIsSuccess) in
                        successResponse(response.destinationURL != nil ? true : false)
                    })
                }else{
                    print("Final Response=====>",response.debugDescription)
                    successResponse(response.destinationURL != nil ? true : false)
                }
            }
        }
       
    }
    
    //FOR DOWNLOAD MEDIA FROM SERVER AND APNS
    private func downloadMediaFiles(sourceURL : URL, destinationURL: URL){
     
        URLSession.shared.invalidateAndCancel()
        DownloadManager.shared.activate().invalidateAndCancel()
        let _ = DownloadManager.shared.activate()
        DownloadManager.shared.onProgress = { (progress) in
            OperationQueue.main.addOperation {
                 Constant.APPDELEGATE?.updateProgress(progress: Float(progress))
            }
        }
        
        DownloadManager.shared.downloaderDelegate = self
        DownloadManager.shared.destinationURL = destinationURL
        DownloadManager.shared.bIsProcessDone = true
        let task = DownloadManager.shared.activate().downloadTask(with: sourceURL)
        task.resume()
    }

    func downloadCompleted(error: Error?, desinationURL: URL?) {
        
        if(DownloadManager.shared.bIsProcessDone == false){
            return
        }

        DownloadManager.shared.bIsProcessDone = false
        print(error?.localizedDescription as Any)
        print("OUR DELEGATE")
        print(desinationURL?.absoluteString as Any)

        DownloadManager.shared.onProgress = nil

        DispatchQueue.main.async {
            let navController = Constant.APPDELEGATE?.window?.rootViewController
            
                if let navController = navController as? UINavigationController{
                    for standardvc in  navController.viewControllers{
                        if let currentVC = standardvc as? StandardViewController{
                            Constant.APPDELEGATE?.showCustomLoader(strMessage: "Unzipping Media...")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                currentVC.saveMediaWithLargeSize(strURL: desinationURL?.absoluteString ?? "")
                            }
                            navController.popToViewController(currentVC, animated:true)
                        }else{
                            print("check other view controller.........")
                        }
                    }
                }
            }
    }
    
    private func copyItems(tempDirectory: URL ,fileURL: URL, successResponse: @escaping(Bool) -> Void){
        DispatchQueue.main.async {
            do{
                let filemgr = FileManager.default
                try filemgr.copyItem(atPath: tempDirectory.path, toPath:fileURL.path)
                successResponse(true)
            }catch{
                print("COPY  ERROR : == > ", error)
            }
        }
        
    }

    func retrieveImage(for arrMedia: [Media], filePath : String, successResponse: @escaping(Bool?) -> Void, errorResponse:@escaping(Error?) -> Void){
        
        DispatchQueue.global().async {

            for objMedia in arrMedia {
                guard let url1 = URL(string: objMedia.MediaFile) else {
                    continue
                }
                let group = DispatchGroup()
                print(objMedia.MediaFile)
                print("-------GROUP ENTER-------")
                print("\(url1)")
                group.enter()
                
                let url =  Constant.MEDIA_URL + "\(objMedia.MediaFile)"

                URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { data, response, error in
                    
                    if let imgData = data, let image = UIImage(data: imgData) {

                        let imageName = URL(string: url)?.lastPathComponent
                        
                        let fileurl = URL(fileURLWithPath: "\(filePath)").appendingPathComponent(imageName!)
                        if let image = UIImage(data: imgData) {
                            
                            if let downloadURL = URL(string: url){
                                if downloadURL.pathExtension == "jpg" ||  downloadURL.pathExtension == "jpg"{
                                    if let data = UIImageJPEGRepresentation(image, 1.0),
                                        !FileManager.default.fileExists(atPath: fileurl.path) {
                                        var dirName = fileurl
                                        dirName = dirName.deletingLastPathComponent()
                                        if !FileManager.default.fileExists(atPath: dirName.path){
                                            do {
                                                let fileManager = FileManager.default
                                                try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                                                do {
                                                    try data.write(to: fileurl)
                                                    print("file saved")
                                                    successResponse(true)
                                                } catch {
                                                    print("error saving file:", error)
                                                    errorResponse(error)
                                                    
                                                }
                                            } catch {
                                                NSLog("Couldn't create document directory")
                                                errorResponse(error)
                                                group.leave()
                                            }
                                        }else{
                                            do {
                                                try data.write(to: fileurl)
                                                print("file saved")
                                                successResponse(true)
                                            } catch {
                                                print("error saving file:", error)
                                                errorResponse(error)
                                                group.leave()
                                            }
                                        }
                                        
                                    }
                                }else if  downloadURL.pathExtension == "png"{
                                    if let data = UIImagePNGRepresentation(image),
                                        !FileManager.default.fileExists(atPath: fileurl.path) {
                                        var dirName = fileurl
                                        dirName = dirName.deletingLastPathComponent()
                                        if !FileManager.default.fileExists(atPath: dirName.path){
                                            do {
                                                let fileManager = FileManager.default
                                                try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                                                do {
                                                    try data.write(to: fileurl)
                                                    print("file saved")
                                                    successResponse(true)
                                                    
                                                } catch {
                                                    print("error saving file:", error)
                                                    errorResponse(error)
                                                    group.leave()

                                                }
                                            } catch {
                                                NSLog("Couldn't create document directory")
                                                errorResponse(error)
                                                group.leave()
                                            }
                                        }else{
                                            do {
                                                try data.write(to: fileurl)
                                                print("file saved")
                                                successResponse(true)
                                            } catch {
                                                print("error saving file:", error)
                                                errorResponse(error)
                                                group.leave()
                                            }
                                        }
                                    }else{
                                        print("error saving file:")
                                        errorResponse(error)
                                        group.leave()
                                    }
                                }
                            }else{
                                print("url not found:")
                                errorResponse(error)
                                group.leave()
                            }
                        }

                     } else if let error = error {
                        print(error)
                        errorResponse(error)
                        group.leave()
                    }
                    
                    group.leave()
                }).resume()
                
                group.wait()
            }
        }

    }

    func downloadImage(url : String , filePath : String, successResponse: @escaping(Bool?) -> Void, error:@escaping(Error?) -> Void){
        
        print("Image Download URL :- \(url)")
        
        Alamofire.request(url).responseImage { response in

            let imageName = URL(string: url)?.lastPathComponent
            //let fileurl = URL(fileURLWithPath:  filePath + "/" + imageName!)
            
            let fileurl = URL(fileURLWithPath: "\(filePath)").appendingPathComponent(imageName!)
            print(fileurl)
            if let image = response.result.value {
                
                if let downloadURL = URL(string: url){
                    if downloadURL.pathExtension == "jpg" ||  downloadURL.pathExtension == "jpg"{
                        if let data = UIImageJPEGRepresentation(image, 1.0),
                            !FileManager.default.fileExists(atPath: fileurl.path) {
                            var dirName = fileurl
                            dirName = dirName.deletingLastPathComponent()
                            if !FileManager.default.fileExists(atPath: dirName.path){
                                do {
                                    let fileManager = FileManager.default
                                    try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                                    do {
                                        try data.write(to: fileurl)
                                        print("file saved")
                                        successResponse(true)
                                    } catch {
                                        print("error saving file:", error)
                                    }
                                } catch {
                                    NSLog("Couldn't create document directory")
                                }
                            }else{
                                do {
                                    try data.write(to: fileurl)
                                    print("file saved")
                                    successResponse(true)

                                } catch {
                                    print("error saving file:", error)
                                }
                            }
                            
                           
                        }
                    }else if  downloadURL.pathExtension == "png"{
                        if let data = UIImagePNGRepresentation(image),
                            !FileManager.default.fileExists(atPath: fileurl.path) {
                            var dirName = fileurl
                            dirName = dirName.deletingLastPathComponent()
                            if !FileManager.default.fileExists(atPath: dirName.path){
                                do {
                                    let fileManager = FileManager.default
                                    try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                                    do {
                                        try data.write(to: fileurl)
                                        print("file saved")
                                        successResponse(true)

                                    } catch {
                                        print("error saving file:", error)
                                    }
                                } catch {
                                    NSLog("Couldn't create document directory")
                                }
                            }else{
                                do {
                                    try data.write(to: fileurl)
                                    print("file saved")
                                    successResponse(true)

                                } catch {
                                    print("error saving file:", error)
                                }
                            }
                            
                            
                        }
                    }
                }
                
                
            }
            
        }

    }

}
