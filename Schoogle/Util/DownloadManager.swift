//  DownloadManager.swift
//  Schoogle
//  Created by Malhar on 13/08/19.
//  Copyright © 2019 Malhar. All rights reserved.

import Foundation

protocol downloadResponser: AnyObject {
    func downloadCompleted(error : Error?, desinationURL: URL?)
}


class DownloadManager : NSObject, URLSessionDelegate, URLSessionDownloadDelegate {
    
    var downloaderDelegate : downloadResponser?

    static var shared = DownloadManager()
    
    var destinationURL : URL?
    var bIsProcessDone = false
    var nAPNSCount = 0
    
    typealias ProgressHandler = (Float) -> ()
        
    var onProgress : ProgressHandler? {
        didSet {
            if onProgress != nil {
                let _ = activate()
            }
        }
    }
    
    
    override private init() {
        super.init()
    }
    
    func activate() -> URLSession {
        
        let config = URLSessionConfiguration.default
        
        // Warning: If an URLSession still exists from a previous download, it doesn't create a new URLSession object but returns the existing one with the old delegate object attached!
        return URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue())
    }
    
    private func calculateProgress(session : URLSession, completionHandler : @escaping (Float) -> ()) {
        session.getTasksWithCompletionHandler { (tasks, uploads, downloads) in
            let progress = downloads.map({ (task) -> Float in
                if task.countOfBytesExpectedToReceive > 0 {
                    return Float(task.countOfBytesReceived) / Float(task.countOfBytesExpectedToReceive)
                } else {
                    return 0.0
                }
            })
            completionHandler(progress.reduce(0.0, +))
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        if totalBytesExpectedToWrite > 0 {
            if let onProgress = onProgress {
                calculateProgress(session: session, completionHandler: onProgress)
            }
            let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            debugPrint("Progress \(downloadTask) \(progress)")
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        debugPrint("Download finished: \(location)")
        do{
            if(FileManager.default.fileExists(atPath: destinationURL?.absoluteURL.path ?? "")){
                try? FileManager.default.removeItem(at: destinationURL!)
            }
            try FileManager.default.copyItem(at: location, to: destinationURL!)
            try? FileManager.default.removeItem(at: location)
            self.downloaderDelegate?.downloadCompleted(error: nil, desinationURL: destinationURL)

        }catch{
            print("DOWNLOAD MANAGER ERROR : ",error)
            self.downloaderDelegate?.downloadCompleted(error: error, desinationURL: nil)
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        debugPrint("Task completed: \(task), error: \(String(describing: error))")
        self.downloaderDelegate?.downloadCompleted(error: error, desinationURL: nil)
    }
    
}


