//  ButtonExtension.swift
//  Schoogle
//  Created by Malhar on 29/10/18.
//  Copyright © 2018 Malhar. All rights reserved.

import Foundation
import UIKit
extension UIButton{
    
    //Button Corner Radius
    func setButtonCornerRadius(radius : CGFloat){
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

}

