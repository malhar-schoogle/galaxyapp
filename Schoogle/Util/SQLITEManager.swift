//  SQLITEManager.swift
//  Schoogle
//  Created by Malhar on 03/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit
import SQLite3
import Zip

class SQLITEManager: NSObject {
    
    static let shareInstanceSQLITEManager = SQLITEManager()
    var pathToDatabase: String!
    var db : OpaquePointer? = nil
    
    override init() {
        super.init()
        self.copyFileToDocs()
    }
    
    func copyFileToDocs() {
        
        // Move database file from bundle to documents folder
        
        let fileManager = FileManager.default
        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        
        guard documentsUrl.count != 0 else {
            return // Could not find documents URL
        }
        
        let finalDatabaseURL = documentsUrl.first!.appendingPathComponent("schoogle.sqlite")
        
        if !( (try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
            print("DB does not exist in documents folder")
            
            let documentsURL = Bundle.main.resourceURL?.appendingPathComponent("schoogle.sqlite")
            
            do {
                try fileManager.copyItem(atPath: (documentsURL?.path)!, toPath: finalDatabaseURL.path)
                
            } catch let error as NSError {
                print("Couldn't copy file to final location! Error:\(error.description)")
            }
            
        } else {
            print("Database file found at path: \(finalDatabaseURL.path)")
        }

    }
    
    func copynotificationTable(){
        
        let fileManager = FileManager.default
        
        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        
        guard documentsUrl.count != 0 else {
            return // Could not find documents URL
        }
        
        let finalDatabaseURL = documentsUrl.first!.appendingPathComponent("notification.sqlite")
        
        if !( (try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
            print("DB does not exist in documents folder")
            
            let documentsURL = Bundle.main.resourceURL?.appendingPathComponent("notification.sqlite")
            
            do {
                try fileManager.copyItem(atPath: (documentsURL?.path)!, toPath: finalDatabaseURL.path)
                
            } catch let error as NSError {
                print("Couldn't copy file to final location! Error:\(error.description)")
            }
            
        } else {
            print("Database file found at path: \(finalDatabaseURL.path)")
        }
        
    }
    
    func removeDataBase(){
        
        let fileManager = FileManager.default
        let documentsUrl = fileManager.urls(for: .documentDirectory,in: .userDomainMask)
        
        guard documentsUrl.count != 0 else {
            return // Could not find documents URL
        }
        
        let finalDatabaseURL = documentsUrl.first!.appendingPathComponent("schoogle.sqlite")
        do {
            try fileManager.removeItem(atPath: finalDatabaseURL.path)
            
        } catch let error as NSError {
            print("Couldn't copy file to final location! Error:\(error.description)")
        }
    }
    
    
    
    func getDownloadedDatabasePath(dbName : String) -> String{
         return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Database").appendingPathComponent("UNZIP").appendingPathComponent(dbName).path
    }
    
    func getApplicationDatabasePath() -> String{
        
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("schoogle.sqlite").path
    }
    
    func getApplicationNotificationDatabasePath() -> String{
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("notification.sqlite").path
    }
    
    public func getDatabaseZipFolderPath() -> String{
       
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Database").appendingPathComponent("ZIP").appendingPathComponent(Constant.MACID).appendingPathComponent(Constant.MACID).appendingPathExtension("zip").path
    }

    public func getDatabaseFolderPath() -> URL{
        print("Check seven.......>")
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Database")
    }
    
    public func getDownloadMediaFolderPath() -> String{
            return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Media").appendingPathExtension("zip").path
    }
    
    
    public func getDatabaseUNZipFolderPath() -> String{
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Database").appendingPathComponent("UNZIP").appendingPathComponent(Constant.MACID).path
    }
    public func getDatabaseMediaFolderPath() -> String{
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Content").path
    }
    public func getJavaScriptFolderPath() -> URL{
        return FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0].appendingPathComponent("assets")

    }
    func extract(){
        
        do {
            let filePath = URL(fileURLWithPath: getDatabaseZipFolderPath())
            let documentsDirectory = URL(fileURLWithPath: getDatabaseUNZipFolderPath()).deletingLastPathComponent().path
            
            try Zip.unzipFile(filePath, destination: URL(fileURLWithPath: documentsDirectory), overwrite: true, password: "", progress: { (progress) -> () in
                print(progress)
            })
        }
        catch {
            print("Something went wrong")
        }
    }
    
    func getDatabases() -> [String]?{
        
        let fileManager = FileManager.default
        let documentsDirectory = URL(fileURLWithPath: getDatabaseUNZipFolderPath()).deletingLastPathComponent().path
        do {
            let dirContents = try? fileManager.contentsOfDirectory(atPath: documentsDirectory)
            if let count = dirContents?.count , count > 0{
                return dirContents
            }else{
                return []
            }
        } catch{
            print(error)
        }
        
    }
    
    func openDatabase() -> OpaquePointer? {
        
        print(pathToDatabase)
        self.closeDB()

        if sqlite3_open(pathToDatabase, &db) == SQLITE_OK{
            print("Successfully opened connection to database at \(String(describing: pathToDatabase))")
            return db
        } else {
            print("Unable to open database. Verify that you created the directory described " + "in the Getting Started section.")
            return db
        }
    }
    
    func createTable(query : String) -> Bool{
        
        var isSuccess = Bool()
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Contact table created.")
                isSuccess = true
            } else {
                print("Contact table could not be created.")
                isSuccess = false
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
            isSuccess = false
        }
        sqlite3_finalize(createTableStatement)
        return isSuccess
    }
    
    func insertTable(query : String) -> Bool {
        var isSuccess = Bool()
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &insertStatement, nil) == SQLITE_OK {
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                isSuccess = true
            } else {
                isSuccess = false
            }
        } else {
            isSuccess = false
        }
        sqlite3_finalize(insertStatement)
        return isSuccess
    }
    
    func selectTable(query : String , success: @escaping(OpaquePointer?) -> Void , error : @escaping(String?) -> Void){
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) != SQLITE_OK {
            error("No Any Value")
            print("SELECT statement could not be prepared")
            return
        }
        
        success(queryStatement)

        sqlite3_finalize(queryStatement)
    }
    
    func updateTable(query : String) -> Bool{
        var isSuccess = false
        var updateStatement: OpaquePointer? = nil
          /*do {
              let data = try? sqlite3_prepare_v2(db, query, -1, &updateStatement, nil)
              
              if data == SQLITE_OK {
                  if sqlite3_step(updateStatement) == SQLITE_DONE {
                      print("Successfully updated row.")
                      isSuccess = true
                  } else {
                      print("Could not update row.")
                      isSuccess = false
                  }
              }
          }
          catch let error {
              print(error)
          }*/
        
            if sqlite3_prepare_v2(db, query, -1, &updateStatement, nil) == SQLITE_OK {
                if sqlite3_step(updateStatement) == SQLITE_DONE {
                    print("Successfully updated row.")
                    isSuccess = true
                } else {
                    print("Could not update row.")
                    isSuccess = false
                }
            } else {
                print("UPDATE statement could not be prepared")
                isSuccess = false
            }
            sqlite3_finalize(updateStatement)
            return isSuccess
          
    }
    
    func delete(query : String) -> Bool {
      
        var isSucess = false
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &deleteStatement, nil) == SQLITE_OK {
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
                isSucess = true
            } else {
                print("Could not delete row.")
                isSucess = false
            }
        } else {
            isSucess = false
            print("DELETE statement could not be prepared")
        }
        
        sqlite3_finalize(deleteStatement)
        
        return isSucess
        
    }
    
    //MARK: ALTER TABLE FOR ADDED NEW COLUMN
    func alterTable(query : String) -> Bool {
        
        var isSucess = false
        var alterstatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, query, -1, &alterstatement, nil) == SQLITE_OK {
            if sqlite3_step(alterstatement) == SQLITE_DONE {
                print("Successfully added column.")
                isSucess = true
            } else {
                print("Could not added column.")
                isSucess = false
            }
        } else {
            isSucess = false
            print("add statement could not be prepared")
        }
        
        sqlite3_finalize(alterstatement)
        
        return isSucess
        
    }
    
    
    
    func prepareMalformedQuery(malformedQueryString : String) {
        var malformedStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, malformedQueryString, -1, &malformedStatement, nil) == SQLITE_OK {
            print("This should not have happened.")
        } else {
            let errorMessage = String.init(cString: sqlite3_errmsg(db))
            //print("Query could not be prepared! \(errorMessage)")
        }
        
        sqlite3_finalize(malformedStatement)
    }
    
    func handleStringValue(ptrString : UnsafePointer<UInt8>?) -> String{
        var strTemp = ""
        
        if ptrString != nil{
            strTemp = String(cString: UnsafePointer(ptrString!))
            strTemp = strTemp.replacingOccurrences(of: "'", with: "''")
        }else{
            strTemp = ""
        }
        return strTemp
    }
    
    
    //MARK: ADDED on 16 OCT 2019 BY PRAMIT
    func handleStringValuewithBLM(ptrString : UnsafePointer<UInt8>?) -> String{
           var strTemp = ""
           
           if ptrString != nil{
               strTemp = String(cString: UnsafePointer(ptrString!))
               strTemp = strTemp.replacingOccurrences(of: "'", with: "'")
           }else{
               strTemp = ""
           }
           return strTemp
       }
    
    
    func handleStringEscapeValue(ptrString : UnsafePointer<UInt8>?) -> String{
        var strTemp = ""
        
        if ptrString != nil{
            strTemp = String(cString: UnsafePointer(ptrString!))
        }else{
            strTemp = ""
        }
        return strTemp
    }
    
    func closeDB(){
        sqlite3_close(db)
    }
   
    
    //Added By Ravi 11/09/2020
    
    func tableHasContainColumn(tableName: String, columnName: String) -> Bool {

            var retVal = false
            var tableColumnsQueryStatement: OpaquePointer? = nil
        
            if sqlite3_prepare_v2(db, "PRAGMA table_info(\(tableName));",
                                -1,
                                &tableColumnsQueryStatement,
                                nil) == SQLITE_OK {

                while (sqlite3_step(tableColumnsQueryStatement) == SQLITE_ROW) {

                    let queryResultCol1 = sqlite3_column_text(tableColumnsQueryStatement, 1)
                    let currentColumnName = String(cString: queryResultCol1!)

                    if currentColumnName == columnName {
                        retVal = true
                        break
                    }
                }
            }
            sqlite3_finalize(tableColumnsQueryStatement)
            return retVal
    }
}
