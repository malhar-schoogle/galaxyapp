//  ViewControllerExtension.swift
//  Schoogle
//  Created by Malhar on 31/10/18.
//  Copyright © 2018 Malhar. All rights reserved.

import Foundation
import UIKit

extension UIViewController : UIPopoverPresentationControllerDelegate{
    
    func showPopover(arrPopOverMenu : [PopOverMenuModel]){
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "PopoverMenuController") as! PopoverMenuController
        popoverContent.arrPopOverMenu = arrPopOverMenu
        popoverContent.delegateNavigate = self
        popoverContent.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = popoverContent.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width: 200, height: arrPopOverMenu.count * 40)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .up
        popover?.sourceRect = CGRect(x: self.view.frame.size.width - 35, y: 64, width: 0, height: 0 )
        self.present(popoverContent, animated: true, completion: nil)

    }

    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func setAppOrintation(){
        
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.shouldRotate = true
        }
    }

    func setCustomAnimation(){

        self.view.window?.backgroundColor = .white
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController!.view.layer.add(transition, forKey: kCATransition)
    }
    
}
extension UIViewController : PopOverNavigateDelegate{
    func popController(viewController: UIViewController) {
        self.navigationController?.popToViewController(viewController, animated: true)
    }
    
    func pushController(viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func presentController(viewController: UIViewController) {
        print(self)
        print(viewController)
        self.present(viewController, animated: true, completion: nil)
    }
    
    func alertControllerExt (strMessage : String){
        let alertController = UIAlertController(title: "galaxy", message: strMessage, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertController.addAction(ok)
        //self.present(alertController, animated: true, completion: nil)
        if let topVC = UIApplication.getTopViewController() {
            topVC.present(alertController, animated: true, completion: nil)
        }
    }
}

extension UINavigationController{
    
    /// Given the kind of a (UIViewController subclass),
    /// removes any matching instances from self's
    /// viewControllers array.

    func removeAnyViewControllers(ofKind kind: AnyClass){
        self.viewControllers = self.viewControllers.filter { !$0.isKind(of: kind)}
    }
    
    /// Given the kind of a (UIViewController subclass),
    /// returns true if self's viewControllers array contains at
    /// least one matching instance.
    
    func containsViewController(ofKind kind: AnyClass) -> Bool{
        return self.viewControllers.contains(where: { $0.isKind(of: kind) })
    }

}


extension UISearchBar {
    
    private func getViewElement<T>(type: T.Type) -> T? {
        
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    func getSearchBarTextField() -> UITextField? {
        return getViewElement(type: UITextField.self)
    }

    func setTextColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            textField.textColor = color
        }
    }
    
    func setTextFieldColor(color: UIColor) {
        
        if let textField = getViewElement(type: UITextField.self) {
            switch searchBarStyle {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 6
                
            case .prominent, .default:
                textField.backgroundColor = color
            }
        }
    }
    
    func setPlaceholderTextColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            textField.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedStringKey.foregroundColor: color])
        }
    }
    
    func setTextFieldClearButtonColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            
            let button = textField.value(forKey: "clearButton") as! UIButton
            if let image = button.imageView?.image {
                button.setImage(image.transform(withNewColor: color), for: .normal)
            }
        }
    }
    
    func setSearchImageColor(color: UIColor) {
        if let imageView = getSearchBarTextField()?.leftView as? UIImageView {
            imageView.image = imageView.image?.transform(withNewColor: color)
        }
    }
    
    func setSearchbarBackGroundColorAndTextColor(){
        
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.init(red: 39.0/255.0, green: 173.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        textFieldInsideSearchBar?.textColor = UIColor.white
        
    }
}

extension UIImage {
    
    func transform(withNewColor color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}






extension String {
        var htmlToAttributedString: NSAttributedString? {
            guard let data = data(using: .utf8) else { return NSAttributedString() }
            do {
                return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            } catch {
                return NSAttributedString()
            }
        }
        var htmlToString: String {
            return htmlToAttributedString?.string ?? ""
        }
    
    func getImageFromDocumentDirectory() -> UIImage{
        let image = UIImage(named: "PlaceHolderImage")
        var boardImageUrl = URL(fileURLWithPath: SQLITEManager.shareInstanceSQLITEManager.getDatabaseMediaFolderPath())
        boardImageUrl = boardImageUrl.appendingPathComponent(self)
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: boardImageUrl.path) {
            if let imgObj = UIImage(contentsOfFile: boardImageUrl.path) {
                 return imgObj
            }
        }
        
        return image!
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    func setCurrentDate() -> String{
        let date = Date()
        return date.string(with: "EEEE dd.MMM.yyyy")
    }
    
    
    
}

extension UIApplication {

    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}

extension Date {
    func string(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    static func getCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        //dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: Date())
    }
    
    static func getCurrentDateAfter15min() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        return dateFormatter.string(from: Date().addingTimeInterval(+900))
    }
    //Get Time diffrence between two date
    static func  getTimeDifferenceBetweenTwoDate(lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}

extension UIApplication {
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

