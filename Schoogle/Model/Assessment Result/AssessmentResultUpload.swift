//
//  AssessmentResultUpload.swift
//  Schoogle
//
//  Created by Emxcel on 04/03/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit


class AssessmentResultUploadModel : NSObject {
    
    var AssessmentId:Int32 = 0
    var AssessmentResultUpload:String = ""
    var AssesmentResultUploaded:Int32 = 0
    
    
    override init() {
        
    }
    
    init(AssessmentId : Int32?,
         AssessmentResultUpload : String?,AssesmentResultUploaded : Int32?){
        
       if let AssessmentId = AssessmentId{
            self.AssessmentId = AssessmentId
        }else{
            self.AssessmentId = 0
        }
        
        if let AssessmentResultUpload = AssessmentResultUpload{
            self.AssessmentResultUpload = AssessmentResultUpload
        }else{
            self.AssessmentResultUpload = ""
        }
        
        if let AssesmentResultUploaded = AssesmentResultUploaded{
            self.AssesmentResultUploaded = AssesmentResultUploaded
        }else{
            self.AssesmentResultUploaded = 0
        }
    }
    
}
