//
//  AssessmentResult.swift
//  Schoogle
//
//  Created by Emxcel on 11/03/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation

class AssessmentResult : NSObject {
    
    var AssessmentId:Int32 = 0
    var PaperNo:Int32 = 0
    var StudentId:Int32 = 0
    var AssessmentQuestionId:Int32 = 0
    var AssessmentResultDetailsId:Int32 = 0
    var AssessmentResultId:Int32 = 0
    var StudentAnswer:String = ""
    var AssessmentQuestionDetailsId:Int32 = 0
    var FullMarks:Int32 = 0
    var ObtainedMarks:Int32 = 0
    var Remarks:String = ""
    var Part:String = ""
    var QuestionBluePrintPartId:Int32 = 0
    var Main:String = ""
    var Section:String = ""
    var CorectAnswer:String = ""
    var QuestionText:String = ""
    var QuestionId:Int32 = 0
    var AutoEvaluationyn:Int32 = 0
    var IsColumnTypeQuestion:Int32 = 0
    var TotalMarks:Int32 = 0
    //var questionpatternname:String = ""
    
    override init() {
        
    }
    
    init(AssessmentId:Int32?,PaperNo:Int32?,StudentId:Int32?,AssessmentQuestionId:Int32?,AssessmentResultDetailsId:Int32?,AssessmentResultId:Int32?,StudentAnswer:String?,AssessmentQuestionDetailsId:Int32?,FullMarks:Int32?,ObtainedMarks:Int32?,Remarks:String?,Part:String?,QuestionBluePrintPartId:Int32?,Main:String?,Section:String?,CorectAnswer:String?,QuestionText:String?,QuestionId:Int32? ,AutoEvaluationyn:Int32?,IsColumnTypeQuestion:Int32?,TotalMarks:Int32?) {
        
    
        if let AssessmentId = AssessmentId{
            self.AssessmentId = AssessmentId
        }else{
            self.AssessmentId = 0
        }
        
        if let PaperNo = PaperNo{
            self.PaperNo = PaperNo
        }else{
            self.PaperNo = 0
        }
        
        if let StudentId = StudentId{
            self.StudentId = StudentId
        }else{
            self.StudentId = 0
        }
        
        if let AssessmentQuestionId = AssessmentQuestionId{
            self.AssessmentQuestionId = AssessmentQuestionId
        }else{
            self.AssessmentQuestionId = 0
        }
        
        if let AssessmentResultDetailsId = AssessmentResultDetailsId{
            self.AssessmentResultDetailsId = AssessmentResultDetailsId
        }else{
            self.AssessmentResultDetailsId = 0
        }
        
        if let AssessmentResultId = AssessmentResultId{
            self.AssessmentResultId = AssessmentResultId
        }else{
            self.AssessmentResultId = 0
        }
        
        if let StudentAnswer = StudentAnswer{
            self.StudentAnswer = StudentAnswer
        }else{
            self.StudentAnswer = ""
        }
        
        if let AssessmentQuestionDetailsId = AssessmentQuestionDetailsId{
            self.AssessmentQuestionDetailsId = AssessmentQuestionDetailsId
        }else{
            self.AssessmentQuestionDetailsId = 0
        }
        
        if let FullMarks = FullMarks{
            self.FullMarks = FullMarks
        }else{
            self.FullMarks = 0
        }
        
        if let ObtainedMarks = ObtainedMarks{
            self.ObtainedMarks = ObtainedMarks
        }else{
            self.ObtainedMarks = 0
        }
        
        if let Remarks = Remarks{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        
        if let Part = Part{
            self.Part = Part
        }else{
            self.Part = ""
        }
        
        if let QuestionBluePrintPartId = QuestionBluePrintPartId{
            self.QuestionBluePrintPartId = QuestionBluePrintPartId
        }else{
            self.QuestionBluePrintPartId = 0
        }
        
        if let Main = Main{
            self.Main = Main
        }else{
            self.Main = ""
        }
        
        if let Section = Section{
            self.Section = Section
        }else{
            self.Section = ""
        }
        
        if let CorectAnswer = CorectAnswer{
            self.CorectAnswer = CorectAnswer
        }else{
            self.CorectAnswer = ""
        }
        
        if let QuestionText = QuestionText{
            self.QuestionText = QuestionText
        }else{
            self.QuestionText = ""
        }
        
        if let QuestionId = QuestionId{
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        
        if let AutoEvaluationyn = AutoEvaluationyn{
            self.AutoEvaluationyn = AutoEvaluationyn
        }else{
            self.AutoEvaluationyn = 0
        }
        
        if let IsColumnTypeQuestion = IsColumnTypeQuestion{
            self.IsColumnTypeQuestion = IsColumnTypeQuestion
        }else{
            self.IsColumnTypeQuestion = 0
        }
        
        if let TotalMarks = TotalMarks{
            self.TotalMarks = TotalMarks
        }else{
            self.TotalMarks = 0
        }
    
    }
    
    init(dictResponse : [String:Any]) {
        

        if let AssessmentId = dictResponse["assessmentid"] as? Int32 {
            self.AssessmentId = AssessmentId
        }else{
            self.AssessmentId = 0
        }
        
        if let PaperNo = dictResponse["paperno"] as? Int32 {
            self.PaperNo = PaperNo
        }else{
            self.PaperNo = 0
        }
        
        if let StudentId = dictResponse["studentid"] as? Int32 {
            self.StudentId = StudentId
        }else{
            self.StudentId = 0
        }
        
        if let AssessmentQuestionId = dictResponse["assessmentquestionid"] as? Int32 {
            self.AssessmentQuestionId = AssessmentQuestionId
        }else{
            self.AssessmentQuestionId = 0
        }
        
        if let AssessmentResultDetailsId = dictResponse["assessmentresultdetailsid"] as? Int32 {
            self.AssessmentResultDetailsId = AssessmentResultDetailsId
        }else{
            self.AssessmentResultDetailsId = 0
        }
        
        if let AssessmentResultId = dictResponse["assessmentresultid"] as? Int32 {
            self.AssessmentResultId = AssessmentResultId
        }else{
            self.AssessmentResultId = 0
        }
        
        if let StudentAnswer = dictResponse["studentanswer"] as? String {
            self.StudentAnswer = StudentAnswer
        }else{
            self.StudentAnswer = ""
        }
        
        if let AssessmentQuestionDetailsId = dictResponse["assessmentquestiondetailsid"] as? Int32 {
            self.AssessmentQuestionDetailsId = AssessmentQuestionDetailsId
        }else{
            self.AssessmentQuestionDetailsId = 0
        }
        
        if let FullMarks = dictResponse["fullmarks"] as? Int32{
            self.FullMarks = FullMarks
        }else{
            self.FullMarks = 0
        }
        
        if let ObtainedMarks = dictResponse["obtainedmarks"] as? Int32{
            self.ObtainedMarks = ObtainedMarks
        }else{
            self.ObtainedMarks = 0
        }
        
        if let Remarks = dictResponse["remarks"] as? String{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        
        if let Part = dictResponse["part"] as? String{
            self.Part = Part
        }else{
            self.Part = ""
        }
        
        if let QuestionBluePrintPartId = dictResponse["questionblueprintpartid"] as? Int32{
            self.QuestionBluePrintPartId = QuestionBluePrintPartId
        }else{
            self.QuestionBluePrintPartId = 0
        }
        
        if let Main = dictResponse["main"] as? String{
            self.Main = Main
        }else{
            self.Main = ""
        }
        
        if let Section = dictResponse["section"] as? String{
            self.Section = Section
        }else{
            self.Section = ""
        }
        
        if let CorectAnswer = dictResponse["corectanswer"] as? String{
            self.CorectAnswer = CorectAnswer
        }else{
            self.CorectAnswer = ""
        }
        
        if let QuestionText = dictResponse["questiontext"] as? String{
            self.QuestionText = QuestionText
        }else{
            self.QuestionText = ""
        }
        
        if let QuestionId = dictResponse["questionid"] as? Int32{
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        
        if let AutoEvaluationyn = dictResponse["autoevaluationyn"] as? Int32{
            self.AutoEvaluationyn = AutoEvaluationyn
        }else{
            self.AutoEvaluationyn = 0
        }
        
        if let IsColumnTypeQuestion = dictResponse["iscolumntypequestion"] as? Int32{
            self.IsColumnTypeQuestion = IsColumnTypeQuestion
        }else{
            self.IsColumnTypeQuestion = 0
        }
        
        if let TotalMarks = dictResponse["totalmarks"] as? Int32{
            self.TotalMarks = TotalMarks
        }else{
            self.TotalMarks = 0
        }

    }
    
    
    
}
