//
//  AssessmentScore.swift
//  Schoogle
//
//  Created by Emxcel on 06/03/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class AssessmentScore : NSObject {

    var AssessmentId:Int32 = 0
    var TotalMarks:Int32 = 0
    var ObtainedMarks:Int32 = 0
    
    override init() {
        
    }
    
    init(AssessmentId: Int32?,TotalMarks : Int32?,ObtainedMarks : Int32?){
        
        
        if let AssessmentId = AssessmentId{
            self.AssessmentId = AssessmentId
        }else{
            self.AssessmentId = 0
        }
        
        if let TotalMarks = TotalMarks{
            self.TotalMarks = TotalMarks
        }else{
            self.TotalMarks = 0
        }
        
        if let ObtainedMarks = ObtainedMarks{
            self.ObtainedMarks = ObtainedMarks
        }else{
            self.ObtainedMarks = 0
        }
    }
    
    
    init(dictResponse : [String:Any],assessmentID:Int32?) {
        
        if let AssessmentId = assessmentID {
            self.AssessmentId = AssessmentId
        }else{
            self.AssessmentId = 0
        }
        if let TotalMarks = dictResponse["totalmarks"] as? Int32{
            self.TotalMarks = TotalMarks
        }else{
            self.TotalMarks = 0
        }
        if let ObtainedMarks = dictResponse["obtainedmarks"] as? Int32{
            self.ObtainedMarks = ObtainedMarks
        }else{
            self.ObtainedMarks = 0
        }
    }
}
