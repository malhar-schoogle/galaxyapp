//  RecentPageView.swift
//  Schoogle
//  Created by sprybitmac on 27/05/19.
//  Copyright © 2019 Malhar. All rights reserved.


import Foundation

class RecentPageView:NSObject {
    
    var contentPageTitle :String = ""
    var contentPageID : Int32 = 0
    
    override init(){

    }

    init(pageTitle : String? ,
         pageID : Int32?) {
        
        if let title = pageTitle{
            self.contentPageTitle = title
        }else{
            self.contentPageTitle = ""
        }
        if let pageID = pageID{
            self.contentPageID = pageID
        }else{
            self.contentPageID = 0
        }
    }
}
