//
//  Assessment.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class Assessment : NSObject {
    var Assessmentid : Int32 = 0
    var OrganizationLocationId : Int32 = 0
    var QuestionBlueprintId : Int32 = 0
    var ChapterId : Int32 = 0
    var AssessmentText : String = ""
    var AssessmentDate : String = ""
    var NoOfQuestionPapers : Int32 = 0
    var Remarks : String = ""
    var ExamMode : Int32 = 0
    var AssessmentTotalMarks : Int32 = 0
    var Term : Int32 = 0
    var PaperGenerate : Int32 = 0
    var Activeyn : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var Deletedyn : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    var Publish : Int32 = 0
    var Academicyearid : Int32 = 0
    var Subjectid : Int32 = 0
    var Examtype : Int32 = 0
    var Chapterids : String = ""
    var Assessmenttypeid : Int32 = 0
    var moveid : Int32 = 0
    var Assessmentresultid : Int32 = 0
    var ResultStatus : String = ""
    var ChapterNames : String = ""
    var ExamDuration : Int32 = 0
    var IsDownload : Int32 = 0
    override init() {
        
    }
    
    init(Assessmentid : Int32?,OrganizationLocationId : Int32?,QuestionBlueprintId : Int32?,ChapterId : Int32?,AssessmentText : String?,AssessmentDate : String?,NoOfQuestionPapers : Int32?,Remarks : String?,ExamMode : Int32?,AssessmentTotalMarks : Int32?,Term : Int32?,PaperGenerate : Int32?,Activeyn : Int32?,CreatedDate : String?,UpdatedDate : String?,CreatedUserId : Int32?,UpdatedUserId : Int32?,Deletedyn : Int32?,DeletedUserId : Int32?,DeletedDate : String?,Publish : Int32?,Academicyearid : Int32?,Subjectid : Int32?,Examtype : Int32?,Chapterids : String?,Assessmenttypeid : Int32?,moveid : Int32?,Assessmentresultid : Int32?,ResultStatus : String?,ChapterNames : String?,ExamDuration : Int32?,IsDownload : Int32?) {
        
        if let Assessmentid = Assessmentid{
            self.Assessmentid = Assessmentid
        }else{
            self.Assessmentid = 0
        }
        
        if let OrganizationLocationId = OrganizationLocationId{
            self.OrganizationLocationId = OrganizationLocationId
        }else{
            self.OrganizationLocationId = 0
        }
        
        if let QuestionBlueprintId = QuestionBlueprintId{
            self.QuestionBlueprintId = QuestionBlueprintId
        }else{
            self.QuestionBlueprintId = 0
        }
        
        if let ChapterId = ChapterId{
            self.ChapterId = ChapterId
        }else{
            self.ChapterId = 0
        }
        
        if let AssessmentText = AssessmentText{
            self.AssessmentText = AssessmentText
        }else{
            self.AssessmentText = ""
        }
        
        if let AssessmentDate = AssessmentDate{
            self.AssessmentDate = AssessmentDate
        }else{
            self.AssessmentDate = ""
        }
        
        
        if let NoOfQuestionPapers = NoOfQuestionPapers{
            self.NoOfQuestionPapers = NoOfQuestionPapers
        }else{
            self.NoOfQuestionPapers = 0
        }
        
        if let Remarks = Remarks{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        
        if let ExamMode = ExamMode{
            self.ExamMode = ExamMode
        }else{
            self.ExamMode = 0
        }
        
        if let AssessmentTotalMarks = AssessmentTotalMarks{
            self.AssessmentTotalMarks = AssessmentTotalMarks
        }else{
            self.AssessmentTotalMarks = 0
        }
        
        if let Term = Term{
            self.Term = Term
        }else{
            self.Term = 0
        }
        if let PaperGenerate = PaperGenerate{
            self.PaperGenerate = PaperGenerate
        }else{
            self.PaperGenerate = 0
        }
        
        if let Activeyn = Activeyn{
            self.Activeyn = Activeyn
        }else{
            self.Activeyn = 0
        }
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        if let Deletedyn = Deletedyn{
            self.Deletedyn = Deletedyn
        }else{
            self.Deletedyn = 0
        }
        
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        if let DeletedDate = DeletedDate{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
        if let Publish = Publish{
            self.Publish = Publish
        }else{
            self.Publish = 0
        }
        if let Academicyearid = Academicyearid{
            self.Academicyearid = Academicyearid
        }else{
            self.Academicyearid = 0
        }
        if let Subjectid = Subjectid{
            self.Subjectid = Subjectid
        }else{
            self.Subjectid = 0
        }
        
        if let Examtype = Examtype{
            self.Examtype = Examtype
        }else{
            self.Examtype = 0
        }
        
        if let Chapterids = Chapterids{
            self.Chapterids = Chapterids
        }else{
            self.Chapterids = ""
        }
        
        if let Assessmenttypeid = Assessmenttypeid{
            self.Assessmenttypeid = Assessmenttypeid
        }else{
            self.Assessmenttypeid = 0
        }
        
        if let moveid = moveid{
            self.moveid = moveid
        }else{
            self.moveid = 0
        }
        
        if let Assessmentresultid = Assessmentresultid{
            self.Assessmentresultid = Assessmentresultid
        }else{
            self.Assessmentresultid = 0
        }
        
        if let ResultStatus = ResultStatus{
            self.ResultStatus = ResultStatus
        }else{
            self.ResultStatus = ""
        }
        
        if let ChapterNames = ChapterNames{
            self.ChapterNames = ChapterNames
        }else{
            self.ChapterNames = ""
        }
        
        if let ExamDuration = ExamDuration{
            self.ExamDuration = ExamDuration
        }else{
            self.ExamDuration = 0
        }
        
        if let IsDownload = IsDownload{
            self.IsDownload = IsDownload
        }else{
            self.IsDownload = 0
        }
        
    }
    
    init(dictResponse : [String:Any]) {
       if let assessmentid = dictResponse["assessmentid"] as? Int32 {
            self.Assessmentid = assessmentid
        }else{
            self.Assessmentid = 0
        }
        
        if let organizationlocationid = dictResponse["organizationlocationid"] as? Int32{
            self.OrganizationLocationId = organizationlocationid
        }else{
            self.OrganizationLocationId = 0
        }
        
        if let questionblueprintid = dictResponse["questionblueprintid"] as? Int32{
            self.QuestionBlueprintId = questionblueprintid
        }else{
            self.QuestionBlueprintId = 0
        }
        
        if let chapterid = dictResponse["chapterid"] as? Int32{
            self.ChapterId = chapterid
        }else{
            self.ChapterId = 0
        }
        
        if let assessmenttext = dictResponse["assessmenttext"] as? String{
            self.AssessmentText = assessmenttext
        }else{
            self.AssessmentText = ""
        }
        
        if let assessmentdate = dictResponse["assessmentdate"] as? String{
            self.AssessmentDate = assessmentdate
        }else{
            self.AssessmentDate = ""
        }
        
        
        if let noofquestionpapers = dictResponse["noofquestionpapers"] as? Int32{
            self.NoOfQuestionPapers = noofquestionpapers
        }else{
            self.NoOfQuestionPapers = 0
        }
        
        if let remarks = dictResponse["remarks"] as? String{
            self.Remarks = remarks
        }else{
            self.Remarks = ""
        }
        
        if let exammode = dictResponse["exammode"] as? Int32{
            self.ExamMode = exammode
        }else{
            self.ExamMode = 0
        }
        
        if let assessmenttotalmarks = dictResponse["assessmenttotalmarks"] as? Int32{
            self.AssessmentTotalMarks = assessmenttotalmarks
        }else{
            self.AssessmentTotalMarks = 0
        }
        
        if let term = dictResponse["term"] as? Int32{
            self.Term = term
        }else{
            self.Term = 0
        }
        if let papergenerate = dictResponse["papergenerate"] as? Int32{
            self.PaperGenerate = papergenerate
        }else{
            self.PaperGenerate = 0
        }
        
        if let activeyn = dictResponse["activeyn"] as? Int32{
            self.Activeyn = activeyn
        }else{
            self.Activeyn = 0
        }
        
        if let createddate = dictResponse["createddate"] as? String{
            self.CreatedDate = createddate
        }else{
            self.CreatedDate = ""
        }
        
        if let updateddate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = updateddate
        }else{
            self.UpdatedDate = ""
        }
        
        if let createduserid = dictResponse["createduserid"] as? Int32{
            self.CreatedUserId = createduserid
        }else{
            self.CreatedUserId = 0
        }
        if let updateduserid = dictResponse["updateduserid"] as? Int32{
            self.UpdatedUserId = updateduserid
        }else{
            self.UpdatedUserId = 0
        }
        if let deletedyn = dictResponse["deletedyn"] as? Int32{
            self.Deletedyn = deletedyn
        }else{
            self.Deletedyn = 0
        }
        
        if let deleteduserid = dictResponse["deleteduserid"] as? Int32{
            self.DeletedUserId = deleteduserid
        }else{
            self.DeletedUserId = 0
        }
        
        if let deleteddate = dictResponse["deleteddate"] as? String{
            self.DeletedDate = deleteddate
        }else{
            self.DeletedDate = ""
        }
        
        if let publish = dictResponse["publish"] as? Int32{
            self.Publish = publish
        }else{
            self.Publish = 0
        }
        if let academicyearid = dictResponse["academicyearid"] as? Int32{
            self.Academicyearid = academicyearid
        }else{
            self.Academicyearid = 0
        }
        if let subjectid = dictResponse["subjectid"] as? Int32{
            self.Subjectid = subjectid
        }else{
            self.Subjectid = 0
        }
        
        if let examtype = dictResponse["examtype"] as? Int32{
            self.Examtype = examtype
        }else{
            self.Examtype = 0
        }
        
        if let chapterids = dictResponse["chapterids"] as? String{
            self.Chapterids = chapterids
        }else{
            self.Chapterids = ""
        }
        
        if let assessmenttypeid = dictResponse["assessmenttypeid"] as? Int32{
            self.Assessmenttypeid = assessmenttypeid
        }else{
            self.Assessmenttypeid = 0
        }
        
        if let moveid = dictResponse["moveid"] as? Int32{
            self.moveid = moveid
        }else{
            self.moveid = 0
        }
        
        if let assessmentresultid = dictResponse["assessmentresultid"] as? Int32{
            self.Assessmentresultid = assessmentresultid
        }else{
            self.Assessmentresultid = 0
        }
        
        if let resultstatus = dictResponse["resultstatus"] as? String{
            self.ResultStatus = resultstatus
        }else{
            self.ResultStatus = ""
        }
        
        if let chapternames = dictResponse["chapternames"] as? String{
            self.ChapterNames = chapternames
        }else{
            self.ChapterNames = ""
        }
        
        if let examduration = dictResponse["examduration"] as? Int32{
            self.ExamDuration = examduration
        }else{
            self.ExamDuration = 0
        }
    }
}
