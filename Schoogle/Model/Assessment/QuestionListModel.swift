//
//  QuestionListModel.swift
//  Schoogle
//
//  Created by Emxcel on 29/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class QuestionListModel : NSObject {
    
    var QuestionName:String = ""
    var QuestionId:Int32 = 0
    var QuestionPatternId:Int32 = 0
    
    override init() {
        
    }
    
    init(QuestionName : String?,
         QuestionId : Int32?,QuestionPatternId : Int32?){
        
       if let QuestionName = QuestionName{
            self.QuestionName = QuestionName
        }else{
            self.QuestionName = ""
        }
        
        if let QuestionId = QuestionId{
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        if let QuestionPatternId = QuestionPatternId{
            self.QuestionPatternId = QuestionPatternId
        }else{
            self.QuestionPatternId = 0
        }
    }
    
}
