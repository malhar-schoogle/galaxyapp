//
//  AssessmentQuestion.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class AssessmentQuestion: NSObject {
    
    var AssessmentQuestionId:Int32 = 0
    var AssessmentId:Int32 = 0
    var Activeyn:Int32 = 0
    var CreatedDate:String = ""
    var UpdatedDate:String = ""
    var CreatedUserId:Int32 = 0
    var UpdatedUserId:Int32 = 0
    var Deletedyn:Int32 = 0
    var DeletedUserId:Int32 = 0
    var DeletedDate:String = ""
    var PaperNo:Int32 = 0
   // var isextra:Int32 = 0
   // var moveid:Int32 = 0
    
    override init() {
        
    }
    
    init(AssessmentQuestionId:Int32?,AssessmentId:Int32?,Activeyn:Int32?,CreatedDate:String?,UpdatedDate:String?,CreatedUserId:Int32?,UpdatedUserId:Int32?,Deletedyn:Int32?,DeletedUserId:Int32?,DeletedDate:String?,PaperNo:Int32?) {
        //,isextra:Int32?,moveid:Int32?
        if let AssessmentQuestionId = AssessmentQuestionId{
            self.AssessmentQuestionId = AssessmentQuestionId
        }else{
            self.AssessmentQuestionId = 0
        }
        
        if let AssessmentId = AssessmentId{
            self.AssessmentId = AssessmentId
        }else{
            self.AssessmentId = 0
        }
        
        if let Activeyn = Activeyn{
            self.Activeyn = Activeyn
        }else{
            self.Activeyn = 0
        }
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let Deletedyn = Deletedyn{
            self.Deletedyn = Deletedyn
        }else{
            self.Deletedyn = 0
        }
        
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        
        if let DeletedDate = DeletedDate{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
        if let PaperNo = PaperNo{
            self.PaperNo = PaperNo
        }else{
            self.PaperNo = 0
        }
        
       /* if let isextra = isextra{
            self.isextra = isextra
        }else{
            self.isextra = 0
        }
        if let moveid = moveid{
            self.moveid = moveid
        }else{
            self.moveid = 0
        }*/
    }
    
    init(dictResponse : [String:Any]) {
       if let AssessmentQuestionId = dictResponse["assessmentquestionid"] as? Int32{
            self.AssessmentQuestionId = AssessmentQuestionId
        }else{
            self.AssessmentQuestionId = 0
        }
        
        if let AssessmentId = dictResponse["assessmentid"] as? Int32{
            self.AssessmentId = AssessmentId
        }else{
            self.AssessmentId = 0
        }
        
        if let Activeyn = dictResponse["activeyn"] as? Int32{
            self.Activeyn = Activeyn
        }else{
            self.Activeyn = 0
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = dictResponse["createduserid"] as? Int32{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = dictResponse["updateduserid"] as? Int32{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let Deletedyn = dictResponse["deletedyn"] as? Int32{
            self.Deletedyn = Deletedyn
        }else{
            self.Deletedyn = 0
        }
        
        if let DeletedUserId = dictResponse["deleteduserid"] as? Int32{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        
        if let DeletedDate = dictResponse["deleteddate"] as? String{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
        if let PaperNo = dictResponse["paperno"] as? Int32{
            self.PaperNo = PaperNo
        }else{
            self.PaperNo = 0
        }
        
       /* if let isextra = dictResponse["isextra"] as? Int32{
            self.isextra = isextra
        }else{
            self.isextra = 0
        }
        if let moveid = dictResponse["moveid"] as? Int32{
            self.moveid = moveid
        }else{
            self.moveid = 0
        }*/
    }
}
