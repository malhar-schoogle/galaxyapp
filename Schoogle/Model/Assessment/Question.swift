//
//  Question.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class Question : NSObject {
 
    var QuestionId:Int32 = 0
    var QuestionPatternId:Int32 = 0
    var AssessmentTypeId:Int32 = 0
    var TopicId:Int32 = 0
    var QuestionName:String = ""
    var AnswerText:String = ""
    var SolutionText:String = ""
    var DifficultyLevel:Int32 = 0
    var SuggestedMarks:Int32 = 0
    var Hint:String = ""
    var Remarks:String = ""
    var ListOrder:Int32 = 0
    var IsActive:Int32 = 0
    var CreatedDate:String = ""
    var UpdatedDate:String = ""
    var CreatedUserId:Int32 = 0
    var UpdatedUserId:Int32 = 0
    var IsDeleted:Int32 = 0
    var DeletedUserId:Int32 = 0
    var DeletedDate:String = ""
    
    override init() {
        
    }
    
    init(QuestionId:Int32?,QuestionPatternId:Int32?,AssessmentTypeId:Int32?,TopicId:Int32?,QuestionName:String?,AnswerText:String?,SolutionText:String?,DifficultyLevel:Int32?,SuggestedMarks:Int32?,Hint:String?,Remarks:String?,ListOrder:Int32?,IsActive:Int32?,CreatedDate:String?,UpdatedDate:String?,CreatedUserId:Int32?,UpdatedUserId:Int32?,IsDeleted:Int32?,DeletedUserId:Int32?,DeletedDate:String?) {
        
        if let QuestionId = QuestionId{
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        
        if let QuestionPatternId = QuestionPatternId{
            self.QuestionPatternId = QuestionPatternId
        }else{
            self.QuestionPatternId = 0
        }
        
        if let AssessmentTypeId = AssessmentTypeId{
            self.AssessmentTypeId = AssessmentTypeId
        }else{
            self.AssessmentTypeId = 0
        }
        
        if let TopicId = TopicId{
            self.TopicId = TopicId
        }else{
            self.TopicId = 0
        }
        if let QuestionName = QuestionName{
            self.QuestionName = QuestionName
        }else{
            self.QuestionName = ""
        }
        if let AnswerText = AnswerText{
            self.AnswerText = AnswerText
        }else{
            self.AnswerText = ""
        }
        if let SolutionText = SolutionText{
            self.SolutionText = SolutionText
        }else{
            self.SolutionText = ""
        }
        if let DifficultyLevel = DifficultyLevel{
            self.DifficultyLevel = DifficultyLevel
        }else{
            self.DifficultyLevel = 0
        }
        if let SuggestedMarks = SuggestedMarks{
            self.SuggestedMarks = SuggestedMarks
        }else{
            self.SuggestedMarks = 0
        }
        if let Hint = Hint{
            self.Hint = Hint
        }else{
            self.Hint = ""
        }
        if let Remarks = Remarks {
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        if let ListOrder = ListOrder {
            self.ListOrder = ListOrder
        }else{
            self.ListOrder = 0
        }
        
        if let IsActive = IsActive {
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        if let CreatedDate = CreatedDate {
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        if let UpdatedDate = UpdatedDate {
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId {
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId {
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let IsDeleted = IsDeleted {
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        if let DeletedUserId = DeletedUserId {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        if let DeletedDate = DeletedDate {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
    }
    
    init(dictResponse : [String:Any]) {
        
        if let QuestionId = dictResponse["questionid"] as? Int32{
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        
        if let QuestionPatternId = dictResponse["questionpatternid"] as? Int32{
            self.QuestionPatternId = QuestionPatternId
        }else{
            self.QuestionPatternId = 0
        }
        
        if let AssessmentTypeId = dictResponse["assessmenttypeid"] as? Int32{
            self.AssessmentTypeId = AssessmentTypeId
        }else{
            self.AssessmentTypeId = 0
        }
        
        if let TopicId = dictResponse["topicid"] as? Int32{
            self.TopicId = TopicId
        }else{
            self.TopicId = 0
        }
        if let QuestionName = dictResponse["questionname"] as? String {
            self.QuestionName = QuestionName
        }else{
            self.QuestionName = ""
        }
        if let AnswerText = dictResponse["answertext"] as? String {
            self.AnswerText = AnswerText
        }else{
            self.AnswerText = ""
        }
        if let SolutionText = dictResponse["solutiontext"] as? String {
            self.SolutionText = SolutionText
        }else{
            self.SolutionText = ""
        }
        if let DifficultyLevel = dictResponse["difficultylevel"] as? Int32{
            self.DifficultyLevel = DifficultyLevel
        }else{
            self.DifficultyLevel = 0
        }
        if let SuggestedMarks = dictResponse["suggestedmarks"] as? Int32{
            self.SuggestedMarks = SuggestedMarks
        }else{
            self.SuggestedMarks = 0
        }
        if let Hint = dictResponse["hint"] as? String {
            self.Hint = Hint
        }else{
            self.Hint = ""
        }
        if let Remarks = dictResponse["remarks"] as? String {
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        if let ListOrder = dictResponse["listorder"] as? Int32 {
            self.ListOrder = ListOrder
        }else{
            self.ListOrder = 0
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32 {
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String {
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        if let UpdatedDate = dictResponse["updateddate"] as? String {
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = dictResponse["createduserid"] as? Int32 {
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = dictResponse["updateduserid"] as? Int32 {
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32 {
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        if let DeletedUserId = dictResponse["deleteduserid"] as? Int32 {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        if let DeletedDate = dictResponse["deleteddate"] as? String {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
    }
}
