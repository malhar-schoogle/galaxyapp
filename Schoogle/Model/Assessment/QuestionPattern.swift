//
//  QuestionPattern.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class QuestionPattern : NSObject {
    
    var QuestionPatternId : Int32 = 0
    var QuestionPatternName : String = ""
    var QuestionPatternAlias : String = ""
    var ShortDescription : String = ""
    var LongDescription : String = ""
    var ListOrder : Int32 = 0
    var IsAutoEvaluation : Int32 = 0
    var Remarks : String = ""
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    
    override init() {
        
    }
    
    init(QuestionPatternId : Int32?,QuestionPatternName : String?,QuestionPatternAlias : String?,ShortDescription : String?,LongDescription : String?,ListOrder : Int32?,IsAutoEvaluation : Int32?,Remarks : String?,IsActive : Int32?,CreatedDate : String?,UpdatedDate : String?,CreatedUserId : Int32?,UpdatedUserId : Int32?,IsDeleted : Int32?,DeletedUserId : Int32?,DeletedDate : String?) {
        
        
        if let QuestionPatternId = QuestionPatternId{
            self.QuestionPatternId = QuestionPatternId
        }else{
            self.QuestionPatternId = 0
        }
        
        if let QuestionPatternName = QuestionPatternName{
            self.QuestionPatternName = QuestionPatternName
        }else{
            self.QuestionPatternName = ""
        }
        
        if let QuestionPatternAlias = QuestionPatternAlias{
            self.QuestionPatternAlias = QuestionPatternAlias
        }else{
            self.QuestionPatternAlias = ""
        }
        
        if let ShortDescription = ShortDescription{
            self.ShortDescription = ShortDescription
        }else{
            self.ShortDescription = ""
        }
        
        if let LongDescription = LongDescription{
            self.LongDescription = LongDescription
        }else{
            self.LongDescription = ""
        }
        if let ListOrder = ListOrder{
            self.ListOrder = ListOrder
        }else{
            self.ListOrder = 0
        }
        
        if let IsAutoEvaluation = IsAutoEvaluation{
            self.IsAutoEvaluation = IsAutoEvaluation
        }else{
            self.IsAutoEvaluation = 0
        }
        
        if let Remarks = Remarks{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        
        if let IsActive = IsActive{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let IsDeleted = IsDeleted{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        if let DeletedUserId = DeletedUserId {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        if let DeletedDate = DeletedDate {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
    }
    
    init(dictResponse : [String:Any]) {
    
     if let QuestionPatternId = dictResponse["questionpatternid"] as? Int32{
            self.QuestionPatternId = QuestionPatternId
        }else{
            self.QuestionPatternId = 0
        }
        
        if let QuestionPatternName = dictResponse["questionpatternname"] as? String{
            self.QuestionPatternName = QuestionPatternName
        }else{
            self.QuestionPatternName = ""
        }
        
        if let QuestionPatternAlias = dictResponse["questionpatternalias"] as? String{
            self.QuestionPatternAlias = QuestionPatternAlias
        }else{
            self.QuestionPatternAlias = ""
        }
        
        if let ShortDescription = dictResponse["shortdescription"] as? String{
            self.ShortDescription = ShortDescription
        }else{
            self.ShortDescription = ""
        }
        
        if let LongDescription = dictResponse["longdescription"] as? String{
            self.LongDescription = LongDescription
        }else{
            self.LongDescription = ""
        }
        if let ListOrder = dictResponse["listorder"] as? Int32{
            self.ListOrder = ListOrder
        }else{
            self.ListOrder = 0
        }
        
        if let IsAutoEvaluation = dictResponse["autoevaluationyn"] as? Int32{
            self.IsAutoEvaluation = IsAutoEvaluation
        }else{
            self.IsAutoEvaluation = 0
        }
        
        if let Remarks = dictResponse["remarks"] as? String{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = dictResponse["createduserid"] as? Int32{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        if let UpdatedUserId = dictResponse["updateduserid"] as? Int32{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        if let DeletedUserId = dictResponse["deleteduserid"] as? Int32 {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        if let DeletedDate = dictResponse["deleteddate"] as? String {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
    }
    
}
