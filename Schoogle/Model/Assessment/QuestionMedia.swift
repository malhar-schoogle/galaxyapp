//
//  QuestionMedia.swift
//  Schoogle
//
//  Created by Emxcel on 21/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class QuestionMedia: NSObject {
    
    var QuestionMediaId:Int32 = 0
    var QuestionId:Int32 = 0
    var MediaId:Int32 = 0
    var IsActive:Int32 = 0
    var CreatedDate:String = ""
    var UpdatedDate:String = ""
    var CreatedUserId:Int32 = 0
    var UpdatedUserId:Int32 = 0
    var IsDeleted:Int32 = 0
    var DeletedUserId:Int32 = 0
    var DeletedDate:String = ""
    
    override init() {
        
    }
    
     init(QuestionMediaId:Int32?,
     QuestionId:Int32?,
     MediaId:Int32?,
     IsActive:Int32?,
     CreatedDate:String?,
     UpdatedDate:String?,
     CreatedUserId:Int32?,
     UpdatedUserId:Int32?,
     IsDeleted:Int32?,
     DeletedUserId:Int32?,
     DeletedDate:String?) {
        
        if let QuestionMediaId = QuestionMediaId{
            self.QuestionMediaId = QuestionMediaId
        }else{
            self.QuestionMediaId = 0
        }
        if let QuestionId = QuestionId{
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        if let MediaId = MediaId{
            self.MediaId = MediaId
        }else{
            self.MediaId = 0
        }
        if let IsActive = IsActive{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        if let IsDeleted = IsDeleted{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        if let DeletedDate = DeletedDate{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
    }
    
    init(dictResponse : [String:Any]) {
    
        
        if let QuestionMediaId = dictResponse["questionmediaid"] as? Int32{
            self.QuestionMediaId = QuestionMediaId
        }else{
            self.QuestionMediaId = 0
        }
        if let QuestionId = dictResponse["questionid"] as? Int32{
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        if let MediaId = dictResponse["mediaid"] as? Int32{
            self.MediaId = MediaId
        }else{
            self.MediaId = 0
        }
        if let IsActive = dictResponse["activeyn"] as? Int32{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        if let CreatedUserId = dictResponse["createduserid"] as? Int32{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        if let UpdatedUserId = dictResponse["updateduserid"] as? Int32{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        if let IsDeleted = dictResponse["deletedyn"] as? Int32{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        if let DeletedUserId = dictResponse["deleteduserid"] as? Int32{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        if let DeletedDate = dictResponse["deleteddate"] as? String{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
    }
    
    
    
}
