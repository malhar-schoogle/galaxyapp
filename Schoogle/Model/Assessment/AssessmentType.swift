//
//  AssessmentType.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class AssessmentType : NSObject {
    
    var AssessmenttypeId:Int32 = 0
    var AssessmenttypeName:String = ""
    
    override init() {
        
    }
    
    init(AssessmenttypeId : Int32?,
         AssessmenttypeName : String?){
        
       if let AssessmenttypeId = AssessmenttypeId{
            self.AssessmenttypeId = AssessmenttypeId
        }else{
            self.AssessmenttypeId = 0
        }
        
        if let AssessmenttypeName = AssessmenttypeName{
            self.AssessmenttypeName = AssessmenttypeName
        }else{
            self.AssessmenttypeName = ""
        }
    }
    
    init(dictResponse : [String:Any]) {
    
        
        if let AssessmenttypeId = dictResponse["assessmenttypeid"] as? Int32{
            self.AssessmenttypeId = AssessmenttypeId
        }else{
            self.AssessmenttypeId = 0
        }
        
        if let AssessmenttypeName = dictResponse["assessmenttypename"] as? String{
            self.AssessmenttypeName = AssessmenttypeName
        }else{
            self.AssessmenttypeName = ""
        }
    }
}
