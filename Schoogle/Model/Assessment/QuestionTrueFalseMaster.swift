//
//  QuestionTrueFalseMaster.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class QuestionTrueFalseMaster : NSObject {

    var questiontruefalseid : Int32 = 0
    var questiontruefalsename : String = ""
    var questiontruefalsealias : String = ""
    var shortdescription : String = ""
    var longdescription : String = ""
    var remarks : String = ""
    var listorder : Int32 = 0
    var activeyn : Int32 = 0
    var createddate : String = ""
    var createduserid : Int32 = 0
    var updateddate : String = ""
    var updateduserid : Int32 = 0
    var deletedyn : Int32 = 0
    var deleteduserid : Int32 = 0
    var deleteddate : String = ""
    
    
    override init() {
        
    }
    
    init(questiontruefalseid : Int32?,questiontruefalsename : String?,questiontruefalsealias : String?,shortdescription : String?,longdescription : String?,remarks : String?,listorder : Int32?,activeyn : Int32?,createddate : String?,createduserid : Int32?,updateddate : String?,updateduserid : Int32?,deletedyn : Int32?,deleteduserid : Int32?,deleteddate : String?) {
        
    
        if let questiontruefalseid = questiontruefalseid{
            self.questiontruefalseid = questiontruefalseid
        }else{
            self.questiontruefalseid = 0
        }
        
        if let questiontruefalsename = questiontruefalsename{
            self.questiontruefalsename = questiontruefalsename
        }else{
            self.questiontruefalsename = ""
        }
        
        if let questiontruefalsealias = questiontruefalsealias{
            self.questiontruefalsealias = questiontruefalsealias
        }else{
            self.questiontruefalsealias = ""
        }
        
        if let shortdescription = shortdescription{
            self.shortdescription = shortdescription
        }else{
            self.shortdescription = ""
        }
        
        if let longdescription = longdescription{
            self.longdescription = longdescription
        }else{
            self.longdescription = ""
        }
        if let listorder = listorder{
            self.listorder = listorder
        }else{
            self.listorder = 0
        }
        

        if let remarks = remarks{
            self.remarks = remarks
        }else{
            self.remarks = ""
        }
        
        if let activeyn = activeyn{
            self.activeyn = activeyn
        }else{
            self.activeyn = 0
        }
        
        if let createddate = createddate{
            self.createddate = createddate
        }else{
            self.createddate = ""
        }
        
        if let updateddate = updateddate{
            self.updateddate = updateddate
        }else{
            self.updateddate = ""
        }
        
        if let createduserid = createduserid{
            self.createduserid = createduserid
        }else{
            self.createduserid = 0
        }
        if let updateduserid = updateduserid{
            self.updateduserid = updateduserid
        }else{
            self.updateduserid = 0
        }
        
        if let deletedyn = deletedyn{
            self.deletedyn = deletedyn
        }else{
            self.deletedyn = 0
        }
        
        if let deleteduserid = deleteduserid {
            self.deleteduserid = deleteduserid
        }else{
            self.deleteduserid = 0
        }
        
        if let deleteddate = deleteddate {
            self.deleteddate = deleteddate
        }else{
            self.deleteddate = ""
        }
        
    }
    
    init(dictResponse : [String:Any]) {
    
        
     if let questiontruefalseid = dictResponse["questiontruefalseid"] as? Int32{
            self.questiontruefalseid = questiontruefalseid
        }else{
            self.questiontruefalseid = 0
        }
        
        if let questiontruefalsename = dictResponse["questiontruefalsename"] as? String{
            self.questiontruefalsename = questiontruefalsename
        }else{
            self.questiontruefalsename = ""
        }
        
        if let questiontruefalsealias = dictResponse["questiontruefalsealias"] as? String{
            self.questiontruefalsealias = questiontruefalsealias
        }else{
            self.questiontruefalsealias = ""
        }
        
        if let shortdescription = dictResponse["shortdescription"] as? String{
            self.shortdescription = shortdescription
        }else{
            self.shortdescription = ""
        }
        
        if let longdescription = dictResponse["longdescription"] as? String{
            self.longdescription = longdescription
        }else{
            self.longdescription = ""
        }
        if let listorder = dictResponse["listorder"] as? Int32{
            self.listorder = listorder
        }else{
            self.listorder = 0
        }
        

        if let remarks = dictResponse["remarks"] as? String{
            self.remarks = remarks
        }else{
            self.remarks = ""
        }
        
        if let activeyn = dictResponse["activeyn"] as? Int32{
            self.activeyn = activeyn
        }else{
            self.activeyn = 0
        }
        
        if let createddate = dictResponse["createddate"] as? String{
            self.createddate = createddate
        }else{
            self.createddate = ""
        }
        
        if let updateddate = dictResponse["updateddate"] as? String{
            self.updateddate = updateddate
        }else{
            self.updateddate = ""
        }
        
        if let createduserid = dictResponse["createduserid"] as? Int32{
            self.createduserid = createduserid
        }else{
            self.createduserid = 0
        }
        if let updateduserid = dictResponse["updateduserid"] as? Int32{
            self.updateduserid = updateduserid
        }else{
            self.updateduserid = 0
        }
        
        if let deletedyn = dictResponse["deletedyn"] as? Int32{
            self.deletedyn = deletedyn
        }else{
            self.deletedyn = 0
        }
        
        if let deleteduserid = dictResponse["deleteduserid"] as? Int32 {
            self.deleteduserid = deleteduserid
        }else{
            self.deleteduserid = 0
        }
        
        if let deleteddate = dictResponse["deleteddate"] as? String {
            self.deleteddate = deleteddate
        }else{
            self.deleteddate = ""
        }
        
        
    }
    
    
}

