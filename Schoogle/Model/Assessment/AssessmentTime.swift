//
//  AssessmentTime.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class AssessmentTime : NSObject {
    
    var AssessmentTimingId:Int32 = 0
    var QuestionPatternId:Int32 = 0
    var StandardId:Int32 = 0
    var Time:Int32 = 0
    
    override init() {
        
    }
    
    init(AssessmentTimingId : Int32?,
         QuestionPatternId : Int32?,
         StandardId : Int32?,
         Time : Int32?){
        
       if let AssessmentTimingId = AssessmentTimingId{
            self.AssessmentTimingId = AssessmentTimingId
        }else{
            self.AssessmentTimingId = 0
        }
        
        if let QuestionPatternId = QuestionPatternId{
            self.QuestionPatternId = QuestionPatternId
        }else{
            self.QuestionPatternId = 0
        }
        
        if let StandardId = StandardId{
            self.StandardId = StandardId
        }else{
            self.StandardId = 0
        }
        
        if let Time = Time{
            self.Time = Time
        }else{
            self.Time = 0
        }

    }
    
    init(dictResponse : [String:Any]) {
    

      if let AssessmentTimingId = dictResponse["assessmenttimingid"] as? Int32{
            self.AssessmentTimingId = AssessmentTimingId
        }else{
            self.AssessmentTimingId = 0
        }
        
        if let QuestionPatternId = dictResponse["questionpatternid"] as? Int32{
            self.QuestionPatternId = QuestionPatternId
        }else{
            self.QuestionPatternId = 0
        }
        
        if let StandardId = dictResponse["standardid"] as? Int32{
            self.StandardId = StandardId
        }else{
            self.StandardId = 0
        }
        
        if let Time = dictResponse["time"] as? Int32{
            self.Time = Time
        }else{
            self.Time = 0
        }
    }
}
