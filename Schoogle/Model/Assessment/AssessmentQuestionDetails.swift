//
//  AssessmentQuestionDetails.swift
//  Schoogle
//
//  Created by Emxcel on 19/02/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit

class AssessmentQuestionDetails: NSObject {
   
    //"AssessmentId"    INTEGER,
    
    var Section:String = ""
    var Main:String = ""
    var Part:String = ""
    var AssessmentQuestionDetailsId:Int32 = 0
    var AssessmentQuestionId:Int32 = 0
    var PartId:Int32 = 0
    var QuestionId:Int32 = 0
    var Marks:Int32 = 0
    var Activeyn:Int32 = 0
    var CreatedDate:String = ""
    var UpdatedDate:String = ""
    var CreatedUserId:Int32 = 0
    var UpdatedUserId:Int32 = 0
    var Deletedyn:Int32 = 0
    var DeletedUserId:Int32 = 0
    var DeletedDate:String = ""
    var IsColumnTypeQuestion:Int32 = 0
   // var moveid:Int32 = 0
    
    override init() {
        
    }
    
    init(Section:String?,Main:String?,Part:String?,AssessmentQuestionDetailsId:Int32?,AssessmentQuestionId:Int32?,PartId:Int32?,QuestionId:Int32?,Marks:Int32?,Activeyn:Int32?,CreatedDate:String?,UpdatedDate:String?,CreatedUserId:Int32?,UpdatedUserId:Int32?,Deletedyn:Int32?,DeletedUserId:Int32?,DeletedDate:String?,IsColumnTypeQuestion:Int32?) {
        //,moveid:Int32?
        if let Section = Section{
            self.Section = Section
        }else{
            self.Section = ""
        }
        
        if let Main = Main {
            self.Main = Main
        }else{
            self.Main = ""
        }
        if let Part = Part {
            self.Part = Part
        }else{
            self.Part = ""
        }
        if let AssessmentQuestionDetailsId = AssessmentQuestionDetailsId {
            self.AssessmentQuestionDetailsId = AssessmentQuestionDetailsId
        }else{
            self.AssessmentQuestionDetailsId = 0
        }
        
        if let AssessmentQuestionId = AssessmentQuestionId {
            self.AssessmentQuestionId = AssessmentQuestionId
        }else{
            self.AssessmentQuestionId = 0
        }
        
        if let PartId = PartId {
            self.PartId = PartId
        }else{
            self.PartId = 0
        }
        
        if let QuestionId = QuestionId {
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        
        if let Marks = Marks {
            self.Marks = Marks
        }else{
            self.Marks = 0
        }
        
        if let Activeyn = Activeyn {
            self.Activeyn = Activeyn
        }else{
            self.Activeyn = 0
        }
        
        if let CreatedDate = CreatedDate {
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        if let UpdatedDate = UpdatedDate {
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        if let CreatedUserId = CreatedUserId {
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId {
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let Deletedyn = Deletedyn {
            self.Deletedyn = Deletedyn
        }else{
            self.Deletedyn = 0
        }
        if let DeletedUserId = DeletedUserId {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        if let DeletedDate = DeletedDate {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        if let IsColumnTypeQuestion = IsColumnTypeQuestion {
            self.IsColumnTypeQuestion = IsColumnTypeQuestion
        }else{
            self.IsColumnTypeQuestion = 0
        }
        /*if let moveid = moveid {
            self.moveid = moveid
        }else{
            self.moveid = 0
        }*/
    }
    
    init(dictResponse : [String:Any]) {
        
       if let Section = dictResponse["section"] as? String {
            self.Section = Section
        }else{
            self.Section = ""
        }
        
        if let Main = dictResponse["main"] as? String {
            self.Main = Main
        }else{
            self.Main = ""
        }
        if let Part = dictResponse["part"] as? String {
            self.Part = Part
        }else{
            self.Part = ""
        }
        if let AssessmentQuestionDetailsId = dictResponse["assessmentquestiondetailsid"] as? Int32 {
            self.AssessmentQuestionDetailsId = AssessmentQuestionDetailsId
        }else{
            self.AssessmentQuestionDetailsId = 0
        }
        
        if let AssessmentQuestionId = dictResponse["assessmentquestionid"] as? Int32 {
            self.AssessmentQuestionId = AssessmentQuestionId
        }else{
            self.AssessmentQuestionId = 0
        }
        
        if let PartId = dictResponse["partid"] as? Int32 {
            self.PartId = PartId
        }else{
            self.PartId = 0
        }
        
        if let QuestionId = dictResponse["questionid"] as? Int32 {
            self.QuestionId = QuestionId
        }else{
            self.QuestionId = 0
        }
        
        if let Marks = dictResponse["marks"] as? Int32 {
            self.Marks = Marks
        }else{
            self.Marks = 0
        }
        
        if let Activeyn = dictResponse["activeyn"] as? Int32 {
            self.Activeyn = Activeyn
        }else{
            self.Activeyn = 0
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String {
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        if let UpdatedDate = dictResponse["updateddate"] as? String {
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        if let CreatedUserId = dictResponse["createduserid"] as? Int32 {
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = dictResponse["updateduserid"] as? Int32 {
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let Deletedyn = dictResponse["deletedyn"] as? Int32 {
            self.Deletedyn = Deletedyn
        }else{
            self.Deletedyn = 0
        }
        if let DeletedUserId = dictResponse["deleteduserid"] as? Int32 {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        if let DeletedDate = dictResponse["deleteddate"] as? String {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        if let IsColumnTypeQuestion = dictResponse["iscolumntypequestion"] as? Int32 {
            self.IsColumnTypeQuestion = IsColumnTypeQuestion
        }else{
            self.IsColumnTypeQuestion = 0
        }
       /* if let moveid = dictResponse["moveid"] as? Int32 {
            self.moveid = moveid
        }else{
            self.moveid = 0
        }*/
    }
}
