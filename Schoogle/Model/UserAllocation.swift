//
//  UserAllocation.swift
//  Schoogle
//
//  Created by Malhar on 11/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class UserAllocation: NSObject {
    var UserAllocationId : Int32 = 0
    var OrganizationLocationId : Int32 = 0
    var UserId : Int32 = 0
    var UserTypeId : Int32 = 0
    var StandardId : Int32 = 0
    var SubjectId : Int32 = 0
    var ContentId : Int32 = 0
    var ChapterId : String = ""
    var BoardId : Int32 = 0
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    var crudFlag : String = ""

    var Sectionid : Int32 = 0
    var Academicyearid : Int32 = 0
    
    override init() {
        
    }
    
    init(UserAllocationId : Int32? ,
         UserId : Int32? ,
         StandardId : Int32? ,
         SubjectId : Int32? ,
         ContentId : Int32? ,
         ChapterId : String? ,
         BoardId : Int32? ,
         IsActive : Int32? ,
         CreatedDate : String? ,
         UpdatedDate : String? ,
         CreatedUserId : Int32? ,
         UpdatedUserId : Int32? ,
         IsDeleted : Int32? ,
         DeletedUserId : Int32? ,
         DeletedDate : String?,
         Sectionid : Int32?,
         Academicyearid : Int32?) {
        
        if let UserAllocationId = UserAllocationId{
            self.UserAllocationId = UserAllocationId
        }else{
            self.UserAllocationId = 0
        }
        
        if let UserId = UserId{
            self.UserId = UserId
        }else{
            self.UserId = 0
        }
        
        if let StandardId = StandardId{
            self.StandardId = StandardId
        }else{
            self.StandardId = 0
        }
        
        
        if let SubjectId = SubjectId{
            self.SubjectId = SubjectId
        }else{
            self.SubjectId = 0
        }
        
        
        if let ContentId = ContentId{
            self.ContentId = ContentId
        }else{
            self.ContentId = 0
        }
        
        
        if let ChapterId = ChapterId{
            self.ChapterId = ChapterId
        }else{
            self.ChapterId = ""
        }
        
        
        if let BoardId = BoardId{
            self.BoardId = BoardId
        }else{
            self.BoardId = 0
        }
        
        
        if let IsActive = IsActive{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        
        if let IsDeleted = IsDeleted{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        
        if let DeletedDate = DeletedDate{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
       //add 02/06/2020
        if let Sectionid = Sectionid  {
            self.Sectionid = Sectionid
        }else{
            self.Sectionid = 0
        }
        if let Academicyearid = Academicyearid  {
            self.Academicyearid = Academicyearid
        }else{
            self.Academicyearid = 0
        }
        //end
    }
    
    init(dictResponse : [String:Any]) {
        
        if let UserAllocationId = dictResponse["userallocationid"] as? Int32  {
            self.UserAllocationId = UserAllocationId
        }else{
            self.UserAllocationId = 0
        }
        if let UserId = dictResponse["userid"] as? Int32  {
            self.UserId = UserId
        }else{
            self.UserId = 0
        }
        if let UserTypeId = dictResponse["usertypeid"] as? Int32  {
            self.UserTypeId = UserTypeId
        }else{
            self.UserTypeId = 0
        }
        if let BoardId = dictResponse["boardid"] as? Int32  {
            self.BoardId = BoardId
        }else{
            self.BoardId = 0
        }
        if let StandardId = dictResponse["standardid"] as? Int32  {
            self.StandardId = StandardId
        }else{
            self.StandardId = 0
        }
        
        
        if let SubjectId = dictResponse["subjectid"] as? Int32  {
            self.SubjectId = SubjectId
        }else{
            self.SubjectId = 0
        }
        if let OrganizationLocationId = dictResponse["organizationlocationid"] as? Int32{
            self.OrganizationLocationId = OrganizationLocationId
        }else{
            self.OrganizationLocationId = 0
        }
        if let ContentId = dictResponse["contentid"] as? Int32  {
            self.ContentId = ContentId
        }else{
            self.ContentId = 0
        }
        if let IsActive = dictResponse["activeyn"] as? Int32  {
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        if let IsDeleted = dictResponse["deletedyn"] as? Int32  {
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        if let CreatedDate = dictResponse["createddate"] as? String  {
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        
        if let UpdatedDate = dictResponse["updateddate"] as? String  {
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        if let DeletedDate = dictResponse["deleteddate"] as? String  {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
        if let Sectionid = dictResponse["sectionid"] as? Int32  {
            self.Sectionid = Sectionid
        }else{
            self.Sectionid = 0
        }
        if let Academicyearid = dictResponse["academicyearid"] as? Int32  {
            self.Academicyearid = Academicyearid
        }else{
            self.Academicyearid = 0
        }
        
        
        
        //====================
        if let CreatedUserId = dictResponse["createduserid"] as? Int32  {
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = dictResponse["updateduserid"] as? Int32  {
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let DeletedUserId = dictResponse["deleteduserid"] as? Int32  {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        //=========================
        
    }
    

}
//Note: add Two key 02/06/2020
/*
 "sectionid": 1,
 "academicyearid": 6
 */
