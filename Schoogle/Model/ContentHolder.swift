//
//  ContentHolder.swift
//  Schoogle
//
//  Created by Malhar on 27/11/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class ContentHolder: NSObject {

    var contentId : UInt64 = 0
    var topicId : Int = 0
    var contentTypeId : Int = 0
    var contentName : String = ""
    var contentDescription : String = ""
    var contentSummary : String = ""
    var contentGroupId : Int = 0
    var isPasswordProtected : Int = 0
    var contentPassword : String = ""
    var prePageContentId : Int = 0
    var nextPageContentId : Int = 0
    var lisOrder : Int = 0
    var isForTeacher : Int = 0
    var isForStudent : Int = 0
    var remarks : String = ""
    var isActive : Int = 0
    var createdDate : String = ""
    var upadatedDate : String = ""
    var createdUserId : String = ""
    var updatedUserId : String = ""
    var isDeleted : Int = 0
    var deletedUserId : Int = 0
    var deletedDate : String = ""
    var crudFlag : String = ""
    var time : String = ""
    
    override init() {
        
    }
    
    init( contentId : UInt64 = 0 ,
          topicId : Int = 0 ,
          contentTypeId : Int = 0 ,
          contentName : String = "" ,
          contentDescription : String = "" ,
          contentSummary : String = "" ,
          contentGroupId : Int = 0 ,
          isPasswordProtected : Int = 0 ,
          contentPassword : String = "" ,
          prePageContentId : Int = 0 ,
          nextPageContentId : Int = 0 ,
          lisOrder : Int = 0 ,
          isForTeacher : Int = 0 ,
          isForStudent : Int = 0 ,
          remarks : String = "" ,
          isActive : Int = 0 ,
          createdDate : String = "" ,
          upadatedDate : String = "" ,
          createdUserId : String = "" ,
          updatedUserId : String = "" ,
          isDeleted : Int = 0 ,
          deletedUserId : Int = 0 ,
          deletedDate : String = "" ,
          crudFlag : String = "" ,
          time : String = "" ) {
        
        self.contentId = contentId
        self.topicId = topicId
        self.contentTypeId = contentTypeId
        self.contentName = contentName
        self.contentDescription = contentDescription
        self.contentSummary = contentSummary
        self.contentGroupId = contentGroupId
        self.isPasswordProtected = isPasswordProtected
        self.contentPassword = contentPassword
        self.prePageContentId = prePageContentId
        self.nextPageContentId = nextPageContentId
        self.lisOrder = lisOrder
        self.isForTeacher = isForTeacher
        self.isForStudent = isForStudent
        self.remarks =  remarks
        self.isActive = isActive
        self.createdDate = createdDate
        self.upadatedDate = upadatedDate
        self.createdUserId = createdUserId
        self.updatedUserId = updatedUserId
        self.isDeleted = isDeleted
        self.deletedUserId = deletedUserId
        self.deletedDate = deletedDate
        self.crudFlag = crudFlag
        self.time = time
        
    }
    
    
}
