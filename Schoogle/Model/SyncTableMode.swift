//  SyncTableMode.swift
//  Schoogle
//  Created by Malhar on 14/12/18.
//  Copyright © 2018 Malhar. All rights reserved.


import UIKit

class SyncTableMode: NSObject {
    var SyncTableModeId : Int32 = 0
    var Mode : String = ""
    var crudFlag : String = ""

    override init() {
        
    }
    
    init(SyncTableModeId : Int32? ,
         Mode : String? ){
        
        if let SyncTableModeId = SyncTableModeId {
            self.SyncTableModeId = SyncTableModeId
        }else{
            self.SyncTableModeId = 0
        }
        if let Mode = Mode {
            self.Mode = Mode
        }else{
            self.Mode = ""
        }
        
    }
    
    init(dictResponse : [String:Any]) {
        
        if let SyncTableModeId = dictResponse["userallocationid"] as? Int32 {
            self.SyncTableModeId = SyncTableModeId
        }else{
            self.SyncTableModeId = 0
        }
        
        if let Mode = dictResponse["createddate"] as? String  {
            self.Mode = Mode
        }else{
            self.Mode = ""
        }
        
        if let crudFlag = dictResponse["createddate"] as? String  {
            self.crudFlag = crudFlag
        }else{
            self.crudFlag = ""
        }
        
    }
}
