//  SearchAllTable.swift
//  Schoogle
//  Created by SpryBit on 08/05/19.
//  Copyright © 2019 Malhar. All rights reserved.

import Foundation

class SearchAllTable: NSObject {
    
    var contentId : Int32 = 0
    var topicId : Int32 = 0
    var contentName = ""
    var contentDescription = ""
    var topicname  = ""
    var chaptername  = ""
    var subjectname = ""
    var standardname  = ""
    var boardname  = ""
    var chapterId : Int32 = 0

    override init() {
    
    }

    init( contentId : Int32? ,
          topicId : Int32? ,
          contentName : String? ,
          contentDescription : String? ,
          topicname : String? ,
          chaptername : String? ,
          subjectname : String? ,
          standardname : String? ,
          boardname : String? ,
          chapterId : Int32? ) {
    
        if let contentId = contentId{
        self.contentId = contentId
        }else{
        self.contentId = 0
        }
    
        if let topicId = topicId{
        self.topicId = topicId
        }else{
        self.topicId = 0
        }
    
        if let contentDescription = contentDescription{
        self.contentDescription = contentDescription
        }else{
        self.contentDescription = ""
        }
        
        if let contentName = contentName{
            self.contentName = contentName
        }else{
            self.contentName = ""
        }
    
        if let topicname = topicname{
        self.topicname = topicname
        }else{
        self.topicname = ""
        }

        if let chaptername = chaptername{
        self.chaptername = chaptername
        }else{
        self.chaptername = ""
        }
    
        if let subjectname = subjectname{
        self.subjectname = subjectname
        }else{
        self.subjectname = ""
        }
    
       
        if let standardname = standardname{
        self.standardname = standardname
        }else{
        self.standardname = ""
        }
    
        if let boardname = boardname{
            self.boardname = boardname
        }else{
            self.boardname = ""
        }
    
        if let chapterId = chapterId{
            self.chapterId = chapterId
        }else{
            self.chapterId = 0
        }

    }

}

