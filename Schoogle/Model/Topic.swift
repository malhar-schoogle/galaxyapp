//
//  Topic.swift
//  SQliteDemo
//
//  Created by Malhar on 04/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class Topic: NSObject {
    
    var TopicId : Int32 = 0
    var ChapterId : Int32 = 0
    var TopicName : String = ""
    var TopicAlias : String = ""
    var ShortDescription : String = ""
    var LongDescription : String = ""
    var ListOrder : Int32 = 0
    var Remarks : String = ""
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    
    override init() {
        
    }
    
    init(TopicId : Int32? ,
         ChapterId : Int32? ,
         TopicName : String? ,
         TopicAlias : String? ,
         ShortDescription : String? ,
         LongDescription : String? ,
         ListOrder : Int32? ,
         Remarks : String? ,
         IsActive : Int32? ,
         CreatedDate : String? ,
         UpdatedDate : String? ,
         CreatedUserId : Int32? ,
         UpdatedUserId : Int32? ,
         IsDeleted : Int32? ,
         DeletedUserId : Int32? ,
         DeletedDate : String?) {
        
        if let TopicId = TopicId {
            self.TopicId = TopicId
        }else{
            self.TopicId = 0
            
        }
        
        if let ChapterId = ChapterId{
            self.ChapterId = ChapterId
        }else{
            self.ChapterId = 0
            
        }
        
        if let TopicName = TopicName{
            self.TopicName = TopicName
        }else{
            self.TopicName = ""
            
        }
        
        if let TopicAlias = TopicAlias {
            self.TopicAlias = TopicAlias
        }else{
            self.TopicAlias = ""
        }
        
        if let ShortDescription = ShortDescription{
            self.ShortDescription = ShortDescription
        }else{
            self.ShortDescription = ""
            
        }
        
        if let LongDescription = LongDescription{
            self.LongDescription = LongDescription
        }else{
            self.LongDescription = ""
            
        }
        
        if let ListOrder = ListOrder{
            self.ListOrder = ListOrder
        }else{
            self.ListOrder = 0
            
        }
        
        if let Remarks = Remarks{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
            
        }
        
        if let IsActive = IsActive{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
            
        }
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
            
        }
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
            
        }
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.TopicId = 0
            
        }
        
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
            
        }
        
        if let IsDeleted = IsDeleted{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
            
        }
        
        if let DeletedUserId = DeletedUserId {
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
            
        }
        
        if let DeletedDate = DeletedDate {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
            
        }
        
        //        self.TopicId = TopicId
        //        self.ChapterId = ChapterId
        //        self.TopicName = TopicName
        //        self.TopicAlias = TopicAlias
        //        self.ShortDescription = ShortDescription
        //        self.LongDescription = LongDescription
        //        self.ListOrder = ListOrder
        //        self.Remarks = Remarks
        //        self.IsActive = IsActive
        //        self.CreatedDate = CreatedDate
        //        self.UpdatedDate = UpdatedDate
        //        self.CreatedUserId = CreatedUserId
        //        self.UpdatedUserId = UpdatedUserId
        //        self.IsDeleted = IsDeleted
        //        self.DeletedUserId = DeletedUserId
        //        self.DeletedDate = DeletedDate
        
    }
    
    init(dictResponse : [String:Any]) {
        
        if let TopicId = dictResponse["topicid"] as? Int32 {
            self.TopicId = TopicId
        }
        else{
            self.TopicId = 0
        }
        
        if let ChapterId = dictResponse["chapterid"] as? Int32 {
            self.ChapterId = ChapterId
        }
        else{
            self.ChapterId = 0
        }
        
        if let TopicName = dictResponse["topicname"] as? String {
            self.TopicName = TopicName
        }
        else{
            self.TopicName = ""
        }
        
        if let ListOrder = dictResponse["listorder"] as? Int32 {
            self.ListOrder = ListOrder
        }
        else{
            self.ListOrder = 0
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32 {
            self.IsActive = IsActive
        }
        else{
            self.IsActive = 0
        }
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32 {
            self.IsDeleted = IsDeleted
        }
        else{
            self.IsDeleted = 0
        }
        
        //Add 02/06/2020
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
            
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
            
        }
        
        if let DeletedDate = dictResponse["deleteddate"] as? String {
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
            
        }
    }
}
