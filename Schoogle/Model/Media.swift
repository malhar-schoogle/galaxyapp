//
//  Media.swif lett
//  SQliteDemo
//
//  Created by Malhar on 04/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class Media: NSObject {
    var MediaId : Int32 = 0
    var MediaTypeId : Int32 = 0
    var MediaFile : String = ""
    var ManualMedia : Int32 = 0
    var Version : Int32 = 0
    var IsImageInFolderYN : Int32 = 0
    var FileSize : Int32 = 0
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    var MediaFlag : String = ""
    var crudFlag : String = ""
    var mediaPath : String = ""
    var IsDownload : Int32 = 0
    
    override init() {
        
    }
    
    init(MediaId : Int32? ,
         MediaTypeId : Int32? ,
         MediaFile : String? ,
         ManualMedia : Int32? ,
         Version : Int32? ,
         IsImageInFolderYN : Int32? ,
         FileSize : Int32? ,
         IsActive : Int32? ,
         CreatedDate : String? ,
         UpdatedDate : String? ,
         CreatedUserId : Int32? ,
         UpdatedUserId : Int32? ,
         IsDeleted : Int32? ,
         DeletedUserId : Int32? ,
         DeletedDate : String? ,
         MediaFlag : String?,IsDownload:Int32?) {
        
        if let MediaId = MediaId{
            self.MediaId = MediaId
        }else{
            self.MediaId = 0
        }
        
        
        if let MediaTypeId = MediaTypeId{
            self.MediaTypeId = MediaTypeId
        }else{
            self.MediaTypeId = 0
        }
        
        if let MediaFile = MediaFile{
            self.MediaFile = MediaFile
        }else{
            self.MediaFile = ""
        }
        
        
        if let ManualMedia = ManualMedia{
            self.ManualMedia = ManualMedia
        }else{
            self.ManualMedia = 0
        }
        
        
        if let Version = Version{
            self.Version = Version
        }else{
            self.Version = 0
        }
        
        
        if let IsImageInFolderYN = IsImageInFolderYN{
            self.IsImageInFolderYN = IsImageInFolderYN
        }else{
            self.IsImageInFolderYN = 0
        }
        
        
        if let FileSize = FileSize{
            self.FileSize = FileSize
        }else{
            self.FileSize = 0
        }
        
        
        if let IsActive = IsActive{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        
        if let IsDeleted = IsDeleted{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        
        if let DeletedDate = DeletedDate{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }

        
        if let MediaFlag = MediaFlag{
            self.MediaFlag = MediaFlag
        }else{
            self.MediaFlag = ""
        }
        
        if let IsDownload = IsDownload{
            self.IsDownload = IsDownload
        }else{
            self.IsDownload = 0
        }
    }
    
    init(dictResponse : [String:Any]) {
        
        if let MediaId = dictResponse["mediaid"] as? Int32{
            self.MediaId = MediaId
        }else{
            self.MediaId = 0
        }
        
        if let MediaTypeId = dictResponse["mediatypeid"] as? Int32{
            self.MediaTypeId = MediaTypeId
        }else{
            self.MediaTypeId = 0
        }
        
        if let Version = dictResponse["version"] as? Int32{
            self.Version = Version
        }else{
            self.Version = 0
        }
        
        if let IsImageInFolderYN = dictResponse["isimageinfolderyn"] as? Int32{
            self.IsImageInFolderYN = IsImageInFolderYN
        }else{
            self.IsImageInFolderYN = 0
        }
        
        if let MediaFile = dictResponse["mediafile"] as? String{
            self.MediaFile = MediaFile
        }else{
            self.MediaFile = ""
        }
        if let MediaFlag = dictResponse["mediaflag"] as? String{
            self.MediaFlag = MediaFlag
        }else{
            self.MediaFlag = ""
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        if let DeletedDate = dictResponse["deleteddate"] as? String{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
        
        //===========================
        if let ManualMedia = dictResponse["userallocationid"] as? Int32{
            self.ManualMedia = ManualMedia
        }else{
            self.ManualMedia = 0
        }
        
        if let IsImageInFolderYN = dictResponse["0"] as? Int32{
            self.IsImageInFolderYN = IsImageInFolderYN
        }else{
            self.IsImageInFolderYN = 0
        }
        
        
        if let FileSize = dictResponse["filesize"] as? Int32{
            self.FileSize = FileSize
        }else{
            self.FileSize = 0
        }
        
    
        if let CreatedUserId = dictResponse["createduserid"] as? Int32{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = dictResponse["updateduserid"] as? Int32{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
    
        if let DeletedUserId = dictResponse["deleteduserid"] as? Int32{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
    
        if let crudFlag = dictResponse["createddate"] as? String{
            self.crudFlag = crudFlag
        }else{
            self.crudFlag = ""
        }
        
        
        if let mediaPath = dictResponse["mediapath"] as? String{
            self.mediaPath = mediaPath
        }else{
            self.mediaPath = ""
        }
        //=======================
    }
}

