//
//  Chapter.swift
//  SQliteDemo
//
//  Created by Malhar on 04/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class Chapter: NSObject {
    
    var ChapterId : Int32 = 0
    var SubjectId : Int32 = 0
    var ChapterName : String = ""
    var ChapterAlias : String = ""
    var ShortDescription : String = ""
    var LongDescription : String = ""
    var Importance : String = ""
    var HasPreREading : Int32 = 0
    var ListOrder : Int32 = 0
    var ChapterImage : String = ""
    var Remarks : String = ""
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    var imageName = ""
    
    override init() {
        
    }
    
    init(ChapterId : Int32? ,
         SubjectId : Int32? ,
         ChapterName : String? ,
         ChapterAlias : String? ,
         ShortDescription : String? ,
         LongDescription : String? ,
         Importance : String? ,
         HasPreREading : Int32? ,
         ListOrder : Int32? ,
         ChapterImage : String? ,
         Remarks : String? ,
         IsActive : Int32? ,
         CreatedDate : String? ,
         UpdatedDate : String? ,
         CreatedUserId : Int32? ,
         UpdatedUserId : Int32? ,
         IsDeleted : Int32? ,
         DeletedUserId : Int32? ,
         DeletedDate : String?) {
        
        if let ChapterId = ChapterId{
            self.ChapterId = ChapterId
        }else{
            self.ChapterId = 0
        }
        
        if let SubjectId = SubjectId{
            self.SubjectId = SubjectId
        }else{
            self.SubjectId = 0
        }
        
        if let ChapterName = ChapterName{
            self.ChapterName = ChapterName
        }else{
            self.ChapterName = ""
        }
        
        if let ChapterAlias = ChapterAlias{
            self.ChapterAlias = ChapterAlias
        }else{
            self.ChapterAlias = ""
        }
        
        if let ShortDescription = ShortDescription{
            self.ShortDescription = ShortDescription
        }else{
            self.ShortDescription = ""
        }
        
        if let LongDescription = LongDescription{
            self.LongDescription = LongDescription
        }else{
            self.LongDescription = ""
        }
        
        if let Importance = Importance{
            self.Importance = Importance
        }else{
            self.Importance = ""
        }
        
        if let HasPreREading = HasPreREading{
            self.HasPreREading = HasPreREading
        }else{
            self.HasPreREading = 0
        }
        
        if let ListOrder = ListOrder{
            self.ListOrder = ListOrder
        }else{
            self.ListOrder = 0
        }
        
        if let ChapterImage = ChapterImage{
            self.ChapterImage = ChapterImage
        }else{
            self.ChapterImage = ""
        }
        
        if let Remarks = Remarks{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        
        if let IsActive = IsActive{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        if let IsDeleted = IsDeleted{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        if let DeletedDate = DeletedDate{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
    }
    
    init(dictResponse : [String:Any]) {
        
        if let ChapterId = dictResponse["chapterid"] as? Int32 {
            self.ChapterId = ChapterId
        }
        else{
            self.ChapterId = 0
        }
        
        if let SubjectId = dictResponse["subjectid"] as? Int32 {
            self.SubjectId = SubjectId
        }
        else{
            self.SubjectId = 0
        }
        
        if let ChapterName = dictResponse["chaptername"] as? String {
            self.ChapterName = ChapterName
        }
        else{
            self.ChapterName = ""
        }
        
        if let ListOrder = dictResponse["listorder"] as? Int32 {
            self.ListOrder = ListOrder
        }
        else{
            self.ListOrder = 0
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32 {
            self.IsActive = IsActive
        }
        else{
            self.IsActive = 0
        }
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32 {
            self.IsDeleted = IsDeleted
        }
        else{
            self.IsDeleted = 0
        }
        
        //Add 02/06/2020
        if let ChapterImage = dictResponse["chapterimage"] as? String{
            self.ChapterImage = ChapterImage
        }else{
            self.ChapterImage = ""
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let DeletedDate = dictResponse["deleteddate"] as? String{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
    }
}

