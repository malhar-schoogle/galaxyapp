//  Board.swift
//  Schoogle
//  Created by Malhar on 11/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class Board: NSObject {
    
    var BoardId : Int32 = 0
    var BoardName : String = ""
    var BoardAlias : String = ""
    var ShortDescription : String = ""
    var LongDescription : String = ""
    var BoardImage : String = ""
    var Remarks : String = ""
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    var crudFlag : String = ""

    override init() {
        
    }
    
    init(   BoardId : Int32? ,
            BoardName : String? ,
            BoardAlias : String? ,
            ShortDescription : String? ,
            LongDescription : String? ,
            BoardImage : String? ,
            Remarks : String? ,
            IsActive : Int32? ,
            CreatedDate : String? ,
            UpdatedDate : String? ,
            CreatedUserId : Int32? ,
            UpdatedUserId : Int32? ,
            IsDeleted : Int32? ,
            DeletedUserId : Int32? ,
            DeletedDate : String? ) {
        
        if let BoardId = BoardId{
            self.BoardId = BoardId
        }else{
            self.BoardId = 0
        }
        
        
        if let BoardName = BoardName{
            self.BoardName = BoardName
        }else{
            self.BoardName = ""
        }
        
        if let BoardAlias = BoardAlias{
            self.BoardAlias = BoardAlias
        }else{
            self.BoardAlias = ""
        }
        
        
        if let ShortDescription = ShortDescription{
            self.ShortDescription = ShortDescription
        }else{
            self.ShortDescription = ""
        }
        
        
        if let LongDescription = LongDescription{
            self.LongDescription = LongDescription
        }else{
            self.LongDescription = ""
        }
        
        
        if let BoardImage = BoardImage{
            self.BoardImage = BoardImage
        }else{
            self.BoardImage = ""
        }
        
        
        if let Remarks = Remarks{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
        
        
        if let IsActive = IsActive{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        
        
        if let IsDeleted = IsDeleted{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        
        
        if let DeletedDate = DeletedDate{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
    }
    
    init(dictResponse : [String:Any]) {
        
        if let BoardId = dictResponse["boardid"] as? Int32{
            self.BoardId = BoardId
        }else{
            self.BoardId = 0
        }
        
        if let BoardName = dictResponse["boardname"] as? String{
            self.BoardName = BoardName
        }else{
            self.BoardName = ""
        }
        
        if let BoardAlias = dictResponse["boardalias"] as? String{
            self.BoardAlias = BoardAlias
        }else{
            self.BoardAlias = ""
        }
        
        if let BoardImage = dictResponse["boardimage"] as? String{
            self.BoardImage = BoardImage
        }else{
            self.BoardImage = ""
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32{
            self.IsActive = IsActive
        }else{
            self.IsActive = 0
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32{
            self.IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        
        if let DeletedDate = dictResponse["deleteddate"] as? String{
            self.DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }

        //============================
          if let ShortDescription = dictResponse["shortdescription"] as? String{
             self.ShortDescription = ShortDescription
         }else{
             self.ShortDescription = ""
         }
         
         if let LongDescription = dictResponse["longdescription"] as? String{
             self.LongDescription = LongDescription
         }else{
             self.LongDescription = ""
        }
         
         if let Remarks = dictResponse["remarks"] as? String{
            self.Remarks = Remarks
        }else{
            self.Remarks = ""
        }
         
         if let CreatedUserId = dictResponse["createduserid"] as? Int32{
             self.CreatedUserId = CreatedUserId
         }else{
             self.CreatedUserId = 0
         }
         
         if let UpdatedUserId = dictResponse["updateduserid"] as? Int32{
             self.UpdatedUserId = UpdatedUserId
         }else{
             self.UpdatedUserId = 0
         }
         
         if let DeletedUserId = dictResponse["deleteduserid"] as? Int32{
             self.DeletedUserId = DeletedUserId
         }else{
             self.DeletedUserId = 0
         }
         //===================================
    }
}
