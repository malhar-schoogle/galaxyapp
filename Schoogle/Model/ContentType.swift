//
//  ContentType.swift
//  SQliteDemo
//
//  Created by Malhar on 04/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class ContentType: NSObject {

    var ContentTypeId : Int32 = 0
    var ContentTypeName : String = ""
    var ContentTypeAlias : String = ""
    var StandardId : Int32 = 0
    var ContentFiles : String = ""
    var ShortDescription : String = ""
    var LongDescription : String = ""
    var IsForTeacher : Int32 = 0
    var IsForStudent : Int32 = 0
    var ListOrder : Int32 = 0
    var Remarks : String = ""
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    var SubjectId : Int32 = 0
    
    override init() {
        
    }
    
    init( ContentTypeId : Int32?,
          ContentTypeName : String?,
          ContentTypeAlias : String?,
          StandardId : Int32?,
          ContentFiles : String?,
          ShortDescription : String?,
          LongDescription : String?,
          IsForTeacher : Int32?,
          IsForStudent : Int32?,
          ListOrder : Int32?,
          Remarks  : String?,
          IsActive : Int32?,
          CreatedDate : String?,
          UpdatedDate : String?,
          CreatedUserId : Int32?,
          UpdatedUserId : Int32?,
          IsDeleted : Int32?,
          DeletedUserId : Int32?,
          DeletedDate : String?,
          SubjectId : Int32?) {
        
        
        if let ContentTypeId = ContentTypeId{
            self.ContentTypeId = ContentTypeId
        }else{
            self.ContentTypeId = 0
        }
        
        if let ContentTypeName = ContentTypeName{
            self.ContentTypeName = ContentTypeName
        }else{
            self.ContentTypeName = ""
        }
        
        if let ContentTypeAlias = ContentTypeAlias {
            self.ContentTypeAlias = ContentTypeAlias
        }else{
            self.ContentTypeAlias = ""
        }
        
        if let StandardId = StandardId {
            self.StandardId = StandardId
        }else{
            self.StandardId = 0
        }
        
        if let ContentFiles = ContentFiles {
            self.ContentFiles = ContentFiles
        }else{
            self.ContentFiles = ""
        }
        
        if let ShortDescription = ShortDescription {
            self.ShortDescription = ShortDescription
        }else{
            self.ShortDescription = ""
        }
        
        if let LongDescription = LongDescription{
            self .LongDescription = LongDescription
        }else{
            self.LongDescription = ""
        }
        
        if let IsForTeacher = IsForTeacher{
            self .IsForTeacher = IsForTeacher
        }else{
            self.IsForTeacher = 0
            
        }
        if let IsForStudent = IsForStudent{
            self .IsForStudent = IsForStudent
        }else{
            self.IsForStudent = 0
            
        }
        if let ListOrder = ListOrder{
            self .ListOrder = ListOrder
        }else{
            self.ListOrder = 0
            
        }
        if let Remarks  = Remarks{
            self .Remarks  = Remarks
        }else{
            self.Remarks  = ""
        }
        
        if let IsActive = IsActive{
            self .IsActive = IsActive
        }else{
            self.IsActive = 0
            
        }
        if let CreatedDate = CreatedDate{
            self .CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = UpdatedDate{
            self .UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId{
            self .CreatedUserId = CreatedUserId
        }else{
            self.CreatedUserId = 0
        }
        if let UpdatedUserId = UpdatedUserId{
            self .UpdatedUserId = UpdatedUserId
        }else{
            self.UpdatedUserId = 0
        }
        if let IsDeleted = IsDeleted{
            self .IsDeleted = IsDeleted
        }else{
            self.IsDeleted = 0
        }
        if let DeletedUserId = DeletedUserId{
            self .DeletedUserId = DeletedUserId
        }else{
            self.DeletedUserId = 0
        }
        if let DeletedDate = DeletedDate{
            self .DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
        if let SubjectId = SubjectId{
            self .SubjectId = SubjectId
        }else{
            self.SubjectId = 0
        }
    }
    
    
    init(dictResponse : [String:Any]) {
            
            if let ContentTypeId = dictResponse["contenttypeid"] as? Int32 {
                self.ContentTypeId = ContentTypeId
            }
            else{
                self.ContentTypeId = 0
            }
        
        if let StandardId = dictResponse["standardid"] as? Int32 {
            self.StandardId = StandardId
        }
        else{
            self.StandardId = 0
        }
        if let SubjectId = dictResponse["subjectid"] as? Int32 {
            self.SubjectId = SubjectId
        }
        else{
            self.SubjectId = 0
        }
        if let ContentTypeName = dictResponse["contenttypename"] as? String {
            self.ContentTypeName = ContentTypeName
        }
        else{
            self.ContentTypeName = ""
        }
        if let ListOrder = dictResponse["listorder"] as? Int32 {
            self.ListOrder = ListOrder
        }
        else{
            self.ListOrder = 0
        }
        if let IsActive = dictResponse["activeyn"] as? Int32 {
            self.IsActive = IsActive
        }
        else{
            self.IsActive = 0
        }
        if let IsDeleted = dictResponse["deletedyn"] as? Int32 {
            self.IsDeleted = IsDeleted
        }
        else{
            self.IsDeleted = 0
        }
        
        //Add 02/06/2020
        if let CreatedDate = dictResponse["createddate"] as? String{
            self .CreatedDate = CreatedDate
        }else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self .UpdatedDate = UpdatedDate
        }else{
            self.UpdatedDate = ""
        }
        
        if let DeletedDate = dictResponse["deleteddate"] as? String{
            self .DeletedDate = DeletedDate
        }else{
            self.DeletedDate = ""
        }
        
        if let IsForTeacher = dictResponse["isforteacher"] as? Int32{
            self .IsForTeacher = IsForTeacher
        }else{
            self.IsForTeacher = 0
            
        }
        if let IsForStudent = dictResponse["isforstudent"] as? Int32{
            self .IsForStudent = IsForStudent
        }else{
            self.IsForStudent = 0
            
        }
    }
}
