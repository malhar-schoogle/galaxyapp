//
//  SyncTableDetail.swift
//  Schoogle
//
//  Created by Malhar on 14/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class SyncTableDetail: NSObject {
    
    var TableId : Int32 = 0
    var TableName : String = ""
    var SyncTableModeId : Int32 = 0
    var crudFlag : String = ""

    override init() {
        
    }
    
    init(TableId : Int32? ,
         TableName : String? ,
         SyncTableModeId : Int32? ){
        
        if let TableId = TableId {
            self.TableId = TableId
        }else{
            self.TableId = 0
        }
        
        if let TableName = TableName {
            self.TableName = TableName
        }else{
            self.TableName = ""
        }
        
        if let SyncTableModeId = SyncTableModeId {
            self.SyncTableModeId = SyncTableModeId
        }else{
            self.SyncTableModeId = 0
        }
        
    }
    
    init(dictResponse : [String:Any]) {
        
        
        if let TableId = dictResponse["userallocationid"] as? Int32 {
            self.TableId = TableId
        }else{
            self.TableId = 0
        }
        
        if let TableName = dictResponse["createddate"] as? String {
            self.TableName = TableName
        }else{
            self.TableName = ""
        }
        
        if let SyncTableModeId = dictResponse["userallocationid"] as? Int32 {
            self.SyncTableModeId = SyncTableModeId
        }else{
            self.SyncTableModeId = 0
        }
        
        if let crudFlag = dictResponse["createddate"] as? String  {
            self.crudFlag = crudFlag
        }else{
            self.crudFlag = ""
        }
        
    }
}
