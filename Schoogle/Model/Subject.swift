//
//  Subject.swif lett
//  SQliteDemo
//
//  Created by Malhar on 04/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class Subject: NSObject {
    var SubjectId : Int32 = 0
    var StandardId : Int32 = 0
    var SubjectName : String = ""
    var SubjectAlias : String = ""
    var ShortDescription : String = ""
    var LongDescription : String = ""
    var ListOrder : Int32 = 0
    var SubjectImage : String = ""
    var Remarks : String = ""
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    
    override init() {
                
    }
    
    init(      SubjectId : Int32? ,
               StandardId : Int32? ,
               SubjectName : String? ,
               SubjectAlias : String? ,
               ShortDescription : String? ,
               LongDescription : String? ,
               ListOrder : Int32? ,
               SubjectImage : String? ,
               Remarks : String? ,
               IsActive : Int32? ,
               CreatedDate : String? ,
               UpdatedDate : String? ,
               CreatedUserId : Int32? ,
               UpdatedUserId : Int32? ,
               IsDeleted : Int32? ,
               DeletedUserId : Int32? ,
               DeletedDate : String?
        ) {
        
        if let SubjectId = SubjectId{
            self.SubjectId =  SubjectId
        }else{
            self.SubjectId =  0
        }
        
        if let StandardId = StandardId{
            self.StandardId =  StandardId
        }else{
            self.StandardId =  0
        }
        
        if let SubjectName = SubjectName{
            self.SubjectName =  SubjectName
        }else{
            self.SubjectName =  ""
        }
        
        if let SubjectAlias = SubjectAlias{
            self.SubjectAlias =  SubjectAlias
        }else{
            self.SubjectAlias =  ""
        }
        
        if let ShortDescription = ShortDescription{
            self.ShortDescription =  ShortDescription
        }else{
            self.ShortDescription =  ""
        }
        
        if let LongDescription = LongDescription{
            self.LongDescription =  LongDescription
        }else{
            self.LongDescription =  ""
        }
        
        if let ListOrder = ListOrder{
            self.ListOrder =  ListOrder
        }else{
            self.ListOrder =  0
        }
        
        if let SubjectImage = SubjectImage{
            self.SubjectImage =  SubjectImage
        }else{
            self.SubjectImage =  ""
        }
        
        if let Remarks = Remarks{
            self.Remarks =  Remarks
        }else{
            self.Remarks =  ""
        }
        
        if let IsActive = IsActive{
            self.IsActive =  IsActive
        }else{
            self.IsActive =  0
        }
        
        if let CreatedDate = CreatedDate{
            self.CreatedDate =  CreatedDate
            
        }else{
            self.CreatedDate =  ""
        }
        
        if let UpdatedDate = UpdatedDate{
            self.UpdatedDate =  UpdatedDate
        }else{
            self.UpdatedDate =  ""
        }
        
        if let CreatedUserId = CreatedUserId{
            self.CreatedUserId =  CreatedUserId
        }else{
            self.CreatedUserId =  0
        }
        
        if let UpdatedUserId = UpdatedUserId{
            self.UpdatedUserId =  UpdatedUserId
        }else{
            self.UpdatedUserId =  0
        }
        
        if let IsDeleted = IsDeleted{
            self.IsDeleted =  IsDeleted
        }else{
            self.IsDeleted =  0
        }
        
        if let DeletedUserId = DeletedUserId{
            self.DeletedUserId =  DeletedUserId
        }else{
            self.DeletedUserId =  0
        }
        
        if let DeletedDate = DeletedDate{
            self.DeletedDate =  DeletedDate
        }else{
            self.DeletedDate =  ""
        }
        
    }
    
    init(dictResponse : [String:Any]) {
        
       if let SubjectId = dictResponse["subjectid"] as? Int32 {
            self.SubjectId =  SubjectId
        }else{
            self.SubjectId =  0
        }
        
        if let StandardId = dictResponse["standardid"] as? Int32 {
            self.StandardId =  StandardId
        }else{
            self.StandardId =  0
        }
        
        if let SubjectName = dictResponse["subjectname"] as? String {
            self.SubjectName =  SubjectName
        }else{
            self.SubjectName =  ""
        }
        
        if let ListOrder = dictResponse["listorder"] as? Int32 {
            self.ListOrder =  ListOrder
        }else{
            self.ListOrder =  0
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32 {
            self.IsActive =  IsActive
        }else{
            self.IsActive =  0
        }
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32 {
            self.IsDeleted =  IsDeleted
        }else{
            self.IsDeleted =  0
        }
        
        //Add New 02/06/2020
        if let SubjectName = dictResponse["subjectimage"] as? String{
            self.SubjectImage =  SubjectName
        }else{
            self.SubjectImage =  ""
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String{
            self.CreatedDate =  CreatedDate
            
        }else{
            self.CreatedDate =  ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String{
            self.UpdatedDate =  UpdatedDate
        }else{
            self.UpdatedDate =  ""
        }
        
        if let DeletedDate = dictResponse["deleteddate"] as? String{
            self.DeletedDate =  DeletedDate
        }else{
            self.DeletedDate =  ""
        }
    }
}
