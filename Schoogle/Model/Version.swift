//  Version.swift
//  SQliteDemo
//  Created by Malhar on 04/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class Version: NSObject {
    
    var StandardId : Int32 = 0
    var VersionName : String = ""
    var ManualMedia : Int32 = 0
    var FtpVersionName : String = ""
    
    var Versionid : Int32 = 0
    var Boardid : Int32 = 0
    var Releasedate : String = ""
    
    
    override init() {
        
    }
    
    init( StandardId : Int32?,
          VersionName : String?,
          ManualMedia : Int32?,
          FtpVersionName : String?,Versionid : Int32?,Boardid : Int32?,Releasedate : String?) {
        
        if let StandardId = StandardId{
            self.StandardId = StandardId
        }else{
            self.StandardId = 0
        }
        
        if let VersionName = VersionName{
            self.VersionName = VersionName
        }else{
            self.VersionName = ""
        }
        
        if let ManualMedia = ManualMedia{
            self.ManualMedia = ManualMedia
        }else{
            self.ManualMedia = 0
        }
        
        if let FtpVersionName = FtpVersionName{
            self.FtpVersionName = FtpVersionName
        }else{
            self.FtpVersionName = ""
        }
        
        if let Versionid = Versionid{
            self.Versionid = Versionid
        }else{
            self.Versionid = 0
        }
        
        if let Boardid = Boardid{
            self.Boardid = Boardid
        }else{
            self.Boardid = 0
        }
        
        if let Releasedate = Releasedate{
            self.Releasedate = Releasedate
        }else{
            self.Releasedate = ""
        }
    }
    
    
    init(dictResponse : [String:Any]) {
        
        if let Versionid = dictResponse["versionid"] as? Int32{
            self.Versionid = Versionid
        }else{
            self.Versionid = 0
        }
        
        if let Boardid = dictResponse["boardid"] as? Int32{
            self.Boardid = Boardid
        }else{
            self.Boardid = 0
        }
        
        if let StandardId = dictResponse["standardid"] as? Int32 {
            self.StandardId = StandardId
        }else{
            self.StandardId = 0
        }
        
        if let ManualMedia = dictResponse["manualmedia"] as? Int32{
            self.ManualMedia = ManualMedia
        }else{
            self.ManualMedia = 0
        }
        
        if let VersionName = dictResponse["versionname"] as? String{
            self.VersionName = VersionName
        }else{
            self.VersionName = ""
        }
        
        
        if let Releasedate = dictResponse["releasedate"] as? String{
            self.Releasedate = Releasedate
        }else{
            self.Releasedate = ""
        }
    }
}






