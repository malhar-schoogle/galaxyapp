//  MenuModel.swift
//  Schoogle
//  Created by Malhar on 19/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class MenuModel: NSObject {

    var sectionID : Int32 = 0
    var sectionName : String = ""
    var arrSideMenuModel : [SideMenuModel] = []
    var arrContentModel : [ContentType] = []
    
    override init() {
        
    }
    
    init(sectionID : Int32?, sectionName : String?, arrSideMenuModel : [SideMenuModel]? ) {
        
        if let sectionID = sectionID{
            self.sectionID = sectionID
        }else{
            self.sectionID = 0
        }
        
        
        if let sectionName = sectionName{
            self.sectionName = sectionName
        }else{
            self.sectionName = ""
        }
        
        if let arrSideMenuModel = arrSideMenuModel{
            self.arrSideMenuModel = arrSideMenuModel
        }else{
            self.arrSideMenuModel = []
        }
        
    }
    
    init(sectionID : Int32?, sectionName : String?, arrContentModel : [ContentType]? ) {
        
        if let sectionID = sectionID{
            self.sectionID = sectionID
        }else{
            self.sectionID = 0
        }
        
        
        if let sectionName = sectionName{
            self.sectionName = sectionName
        }else{
            self.sectionName = ""
        }
        
        if let arrContentModel = arrContentModel{
            self.arrContentModel = arrContentModel
        }else{
            self.arrContentModel = []
        }
    }
    
    
}
