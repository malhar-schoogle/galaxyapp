//  PopOverMenuModel.swift
//  Schoogle
//  Created by Malhar on 31/10/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class PopOverMenuModel: NSObject {
    var popOverMenuName : String?
    
    init(popOverMenuName : String) {
        self.popOverMenuName = popOverMenuName
    }
}
