//
//  TopicModel.swift
//  Schoogle
//
//  Created by Malhar on 30/10/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class TopicModel: NSObject {
    var topicName: String?
    var backgound_Image: String?
    
    init(topicName : String , backgound_Image : String ) {
        self.topicName = topicName
        self.backgound_Image = backgound_Image
    }
}
