//
//  StandardModel.swift
//  Schoogle
//
//  Created by Malhar on 23/10/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import Foundation

class StandardModel: NSObject {
    var standard: String?
    var standard_Image: String?
    
    init(standard : String , standard_Image : String) {
        self.standard = standard
        self.standard_Image = standard_Image
    }
}
