//
//  SubjectModel.swift
//  Schoogle
//
//  Created by Malhar on 26/10/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class SubjectModel: NSObject {
    var subjectName: String?
    var subject_Image: String?
    
    init(subjectName : String , subject_Image : String ) {
        self.subjectName = subjectName
        self.subject_Image = subject_Image
    }
}
