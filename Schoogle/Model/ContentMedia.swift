//
//  ContentMedia.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class ContentMedia: NSObject {
    
    var ContentMediaId : Int32 = 0
    var ContentId : Int32 = 0
    var MediaId : Int32 = 0
    var isActive : Int32 = 0
    var createdDate : String = ""
    var upadatedDate : String = ""
    var createdUserId : Int32 = 0
    var updatedUserId : Int32 = 0
    var isDeleted : Int32 = 0
    var deletedUserId : Int32 = 0
    var deletedDate : String = ""
    
    override init() {
        
    }
    
    init(    ContentMediaId : Int32?,
             ContentId : Int32?,
             MediaId : Int32?,
             isActive : Int32?,
             createdDate : String?,
             upadatedDate : String?,
             createdUserId : Int32?,
             updatedUserId : Int32?,
             isDeleted : Int32?,
             deletedUserId : Int32?,
             deletedDate : String?
    ) {
        
        if let ContentMediaId = ContentMediaId{
            self.ContentMediaId =  ContentMediaId
        }else{
            self.ContentMediaId =  0
        }
        
        if let ContentId = ContentId{
            self.ContentId =  ContentId
        }else{
            self.ContentId =  0
        }
        
        if let MediaId = MediaId{
            self.MediaId =  MediaId
        }else{
            self.MediaId =  0
        }
        
        if let isActive = isActive{
            self.isActive =  isActive
        }else{
            self.isActive =  0
        }
        
        if let createdDate = createdDate{
            self.createdDate  = createdDate
        }else{
            self.createdDate  = ""
        }
        
        if let upadatedDate = upadatedDate{
            self.upadatedDate  = upadatedDate
        }else{
            self.upadatedDate  = ""
        }
        
        if let createdUserId = createdUserId{
            self.createdUserId = createdUserId
        }else{
            self.createdUserId = 0
        }
        
        if let updatedUserId = updatedUserId{
            self.updatedUserId = updatedUserId
        }else{
            self.updatedUserId = 0
        }
        
        if let isDeleted = isDeleted{
            self.isDeleted = isDeleted
        }else{
            self.isDeleted = 0
        }
        
        if let deletedUserId = deletedUserId{
            self.deletedUserId = deletedUserId
        }else{
            self.deletedUserId = 0
        }
        
        if let deletedDate = deletedDate {
            self.deletedDate  = deletedDate
        }else{
            self.deletedDate  = ""
        }
        
    }
    
    init(dictResponse : [String:Any]) {
        
        if let ContentMediaId = dictResponse["contentmediaid"] as? Int32 {
            self.ContentMediaId = ContentMediaId
        }else{
            self.ContentMediaId = 0
        }
        
        if let MediaId = dictResponse["mediaid"] as? Int32{
            self.MediaId = MediaId
        }else{
            self.MediaId = 0
        }
        
        if let ContentId = dictResponse["contentid"] as? Int32{
            self.ContentId = ContentId
        }else{
            self.ContentId = 0
        }
        
        if let isActive = dictResponse["activeyn"] as? Int32{
            self.isActive = isActive
        }else{
            self.isActive = 0
        }
        if let isDeleted = dictResponse["deletedyn"] as? Int32{
            self.isDeleted = isDeleted
        }else{
            self.isDeleted = 0
        }
        //Add 02/06/2020
        if let createdDate = dictResponse["createddate"] as? String{
            self.createdDate  = createdDate
        }else{
            self.createdDate  = ""
        }
        
        if let upadatedDate = dictResponse["updateddate"] as? String{
            self.upadatedDate  = upadatedDate
        }else{
            self.upadatedDate  = ""
        }
        if let deletedDate = dictResponse["deleteddate"] as? String {
            self.deletedDate  = deletedDate
        }else{
            self.deletedDate  = ""
        }
        
    }
}

