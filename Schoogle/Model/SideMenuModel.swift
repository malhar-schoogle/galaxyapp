//  SideMenuModel.swift
//  Schoogle
//  Created by Malhar on 01/11/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class SideMenuModel: NSObject {
    var SideMenuID  : Int32 = 0
    var SideMenuName : String = ""
    var SideMenuImage : String = ""

    init(SideMenuID : Int32? , SideMenuName : String? , SideMenuImage : String?) {
        
        if let SideMenuID = SideMenuID {
            self.SideMenuID = SideMenuID

        }else{
            self.SideMenuID = 0

        }
        if let SideMenuName = SideMenuName {
            self.SideMenuName = SideMenuName

        }else{
            self.SideMenuName = ""

        }
        if let SideMenuImage = SideMenuImage {
            self.SideMenuImage = SideMenuImage

        }else{
            self.SideMenuImage = ""

        }
    }
}
