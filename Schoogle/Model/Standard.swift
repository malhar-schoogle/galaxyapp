//
//  Standardswift
//  Schoogle
//
//  Created by Malhar on 11/12/18
//  Copyright © 2018 Malhar All rights reserved
//

import UIKit

class Standard: NSObject {
    var StandardId : Int32 = 0
    var BoardId : Int32 = 0
    var StandardName : String = ""
    var StandardAlias : String = ""
    var ShortDescription : String = ""
    var LongDescription : String = ""
    var ListOrder : Int32 = 0
    var StandardImage : String = ""
    var Remarks : String = ""
    var IsActive : Int32 = 0
    var CreatedDate : String = ""
    var UpdatedDate : String = ""
    var CreatedUserId : Int32 = 0
    var UpdatedUserId : Int32 = 0
    var IsDeleted : Int32 = 0
    var DeletedUserId : Int32 = 0
    var DeletedDate : String = ""
    var VersionName : String = ""
    var IsManual : Int32 = 0
    var crudFlag : String = ""
    
    override init() {
        
    }
    
    init(StandardId : Int32?,
         BoardId : Int32?,
         StandardName : String?,
         StandardAlias : String?,
         ShortDescription : String?,
         LongDescription : String?,
         ListOrder : Int32?,
         StandardImage : String?,
         Remarks : String?,
         IsActive : Int32?,
         CreatedDate : String?,
         UpdatedDate : String?,
         CreatedUserId : Int32?,
         UpdatedUserId : Int32?,
         IsDeleted : Int32?,
         DeletedUserId : Int32?,
         DeletedDate : String?,
         VersionName : String?,
         IsManual : Int32?) {
        
        if let StandardId = StandardId {
            self.StandardId = StandardId
        }
        else{
            self.StandardId = 0
        }
        
        if let BoardId = BoardId {
            self.BoardId = BoardId
        }
        else{
            self.BoardId = 0
        }
        
        if let StandardName = StandardName {
            self.StandardName = StandardName
        }
        else{
            self.StandardName = ""
        }
        
        if let StandardAlias = StandardAlias {
            self.StandardAlias = StandardAlias
        }
        else{
            self.StandardAlias = ""
        }
        
        if let ShortDescription = ShortDescription {
            self.ShortDescription = ShortDescription
        }
        else{
            self.ShortDescription = ""
        }
        
        if let LongDescription = LongDescription {
            self.LongDescription = LongDescription
        }
        else{
            self.LongDescription = ""
        }
        
        if let ListOrder = ListOrder {
            self.ListOrder = ListOrder
        }
        else{
            self.ListOrder = 0
        }
        
        if let StandardImage = StandardImage {
            self.StandardImage = StandardImage
        }
        else{
            self.StandardImage = ""
        }
        
        if let Remarks = Remarks {
            self.Remarks = Remarks
        }
        else{
            self.Remarks = ""
        }
        
        if let IsActive = IsActive {
            self.IsActive = IsActive
        }
        else{
            self.IsActive = 0
        }
        
        if let CreatedDate = CreatedDate {
            self.CreatedDate = CreatedDate
        }
        else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = UpdatedDate {
            self.UpdatedDate = UpdatedDate
        }
        else{
            self.UpdatedDate = ""
        }
        
        if let CreatedUserId = CreatedUserId {
            self.CreatedUserId = CreatedUserId
        }
        else{
            self.CreatedUserId = 0
        }
        
        if let UpdatedUserId = UpdatedUserId {
            self.UpdatedUserId = UpdatedUserId
        }
        else{
            self.UpdatedUserId = 0
        }
        
        if let IsDeleted = IsDeleted {
            self.IsDeleted = IsDeleted
        }
        else{
            self.IsDeleted = 0
        }
        
        if let DeletedUserId = DeletedUserId {
            self.DeletedUserId = DeletedUserId
        }
        else{
            self.DeletedUserId = 0
        }
        
        if let DeletedDate = DeletedDate {
            self.DeletedDate = DeletedDate
        }
        else{
            self.DeletedDate = ""
        }
        
        if let VersionName = VersionName {
            self.VersionName = VersionName
        }
        else{
            self.VersionName = ""
        }
        
        if let IsManual = IsManual {
            self.IsManual = IsManual
        }
        else{
            self.IsManual = 0
        }
    }
    
    init(dictResponse : [String:Any]) {
        
        if let StandardId = dictResponse["standardid"] as? Int32 {
            self.StandardId = StandardId
        }
        else{
            self.StandardId = 0
        }
        
        if let BoardId = dictResponse["boardid"] as? Int32 {
            self.BoardId = BoardId
        }
        else{
            self.BoardId = 0
        }
        
        if let StandardName = dictResponse["standardname"] as? String {
            self.StandardName = StandardName
        }
        else{
            self.StandardName = ""
        }
        
        if let StandardAlias = dictResponse["standardalias"] as? String {
            self.StandardAlias = StandardAlias
        }
        else{
            self.StandardAlias = ""
        }
        
        if let ListOrder = dictResponse["listorder"] as? Int32 {
            self.ListOrder = ListOrder
        }
        else{
            self.ListOrder = 0
        }
        
        if let StandardImage = dictResponse["standardimage"] as? String {
            self.StandardImage = StandardImage
        }
        else{
            self.StandardImage = ""
        }
        
        if let IsActive = dictResponse["activeyn"] as? Int32 {
            self.IsActive = IsActive
        }
        else{
            self.IsActive = 0
        }
        
        if let CreatedDate = dictResponse["createddate"] as? String {
            self.CreatedDate = CreatedDate
        }
        else{
            self.CreatedDate = ""
        }
        
        if let UpdatedDate = dictResponse["updateddate"] as? String {
            self.UpdatedDate = UpdatedDate
        }
        else{
            self.UpdatedDate = ""
        }
        
       
        
        if let IsDeleted = dictResponse["deletedyn"] as? Int32 {
            self.IsDeleted = IsDeleted
        }
        else{
            self.IsDeleted = 0
        }
        
       
        
        if let DeletedDate = dictResponse["deleteddate"] as? String {
            self.DeletedDate = DeletedDate
        }
        else{
            self.DeletedDate = ""
        }
        //====================
        
          if let ShortDescription = dictResponse["shortdescription"] as? String {
             self.ShortDescription = ShortDescription
         }
         else{
             self.ShortDescription = ""
         }
         
         if let LongDescription = dictResponse["longdescription"] as? String {
             self.LongDescription = LongDescription
         }
         else{
             self.LongDescription = ""
         }
         
         if let Remarks = dictResponse["remarks"] as? String {
            self.Remarks = Remarks
        }
        else{
            self.Remarks = ""
        }
          if let CreatedUserId = dictResponse["createduserid"] as? Int32 {
             self.CreatedUserId = CreatedUserId
         }
         else{
             self.CreatedUserId = 0
         }
         
         if let UpdatedUserId = dictResponse["updateduserid"] as? Int32 {
             self.UpdatedUserId = UpdatedUserId
         }
         else{
             self.UpdatedUserId = 0
         }
          if let DeletedUserId = dictResponse["deleteduserid"] as? Int32 {
             self.DeletedUserId = DeletedUserId
         }
         else{
             self.DeletedUserId = 0
         }
          if let VersionName = dictResponse["versionname"] as? String {
             self.VersionName = VersionName
         }
         else{
             self.VersionName = ""
         }
         
         if let IsManual = dictResponse["userallocationid"] as? Int32 {
             self.IsManual = IsManual
         }
         else{
             self.IsManual = 0
         }

         if let crudFlag = dictResponse["versionname"] as? String {
             self.crudFlag = crudFlag
         }
         else{
             self.crudFlag = ""
         }
         
        //===================
       
    }
}

//"standardid": 5,
//"boardid": 6,
//"standardname": "Standard-V",
//"standardalias": "",
//"listorder": 5,
//"standardimage": "42220.png",
//"activeyn": 1,
//"deletedyn": 0,
//"createddate": "9/5/2014 10:43:01 PM",
//"updateddate": "4/19/2017 10:25:34 AM",
//"deleteddate": ""
