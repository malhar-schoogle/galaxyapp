//  HistoryModel.swift
//  Schoogle
//  Created by Malhar on 21/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class HistoryModel: NSObject {
    
    var historyID : Int32 = 0
    var contentID : Int32 = 0
    var contentName : String = ""
    
    override init() {
        
    }
    
    init( historyID : Int32?,
          contentID : Int32?,
          contentName : String?) {
        
        if let historyID = historyID{
            self.historyID = historyID
        }else{
            self.historyID = 0
        }
        
        if let contentID = contentID{
            self.contentID = contentID
        }else{
            self.contentID = 0
        }
        
        if let contentName = contentName{
            self.contentName = contentName
        }else{
            self.contentName = ""
        }
        
    }
    
}
