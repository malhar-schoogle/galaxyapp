//
//  Content.swift
//  SQliteDemo
//
//  Created by Malhar on 06/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class Content: NSObject {
    
    var contentId : Int32 = 0
    var topicId : Int32 = 0
    var contentTypeId : Int32 = 0
    var contentName : String = ""
    var contentDescription : String = ""
    var contentSummary : String = ""
    var contentGroupId : Int32 = 0
    var isPasswordProtected : Int32 = 0
    var contentPassword : String = ""
    var prePageContentId : Int32 = 0
    var nextPageContentId : Int32 = 0
    var lisOrder : Int32 = 0
    var isForTeacher : Int32 = 0
    var isForStudent : Int32 = 0
    var remarks : String = ""
    var isActive : Int32 = 0
    var createdDate : String = ""
    var upadatedDate : String = ""
    var createdUserId : Int32 = 0
    var updatedUserId : Int32 = 0
    var isDeleted : Int32 = 0
    var deletedUserId : Int32 = 0
    var deletedDate : String = ""
    var crudFlag : String = ""
    var time : String = ""
    var IsDownload : Int32 = 0
    
    override init() {
        
    }
    
    init( contentId : Int32? ,
          topicId : Int32? ,
          contentTypeId : Int32? ,
          contentName : String? ,
          contentDescription : String? ,
          contentSummary : String? ,
          contentGroupId : Int32? ,
          isPasswordProtected : Int32? ,
          contentPassword : String? ,
          prePageContentId : Int32? ,
          nextPageContentId : Int32? ,
          lisOrder : Int32? ,
          isForTeacher : Int32? ,
          isForStudent : Int32? ,
          remarks : String? ,
          isActive : Int32? ,
          createdDate : String? ,
          upadatedDate : String? ,
          createdUserId : Int32? ,
          updatedUserId : Int32? ,
          isDeleted : Int32? ,
          deletedUserId : Int32? ,
          deletedDate : String? ,IsDownload : Int32?) {
        
        if let contentId = contentId{
            self.contentId = contentId
        }else{
            self.contentId = 0
        }
        
        if let topicId = topicId{
            self.topicId = topicId
        }else{
            self.topicId = 0
        }
        
        
        if let contentTypeId = contentTypeId{
            self.contentTypeId = contentTypeId
        }else{
            self.contentTypeId = 0
        }
        
        
        if let contentName = contentName{
            self.contentName = contentName
        }else{
            self.contentName = ""
        }
        
        
        if let contentDescription = contentDescription{
            self.contentDescription = contentDescription
        }else{
            self.contentDescription = ""
        }
        
        if let contentSummary = contentSummary{
            self.contentSummary = contentSummary
        }else{
            self.contentSummary = ""
        }
        
        if let contentGroupId = contentGroupId{
            self.contentGroupId = contentGroupId
        }else{
            self.contentGroupId = 0
        }
        
        if let isPasswordProtected = isPasswordProtected{
            self.isPasswordProtected = isPasswordProtected
        }else{
            self.isPasswordProtected = 0
        }
        
        if let contentPassword = contentPassword{
            self.contentPassword = contentPassword
        }else{
            self.contentPassword = ""
        }
        
        if let prePageContentId = prePageContentId{
            self.prePageContentId = prePageContentId
        }else{
            self.prePageContentId = 0
        }
        
        if let nextPageContentId = nextPageContentId{
            self.nextPageContentId = nextPageContentId
        }else{
            self.nextPageContentId = 0
        }
        
        if let lisOrder = lisOrder{
            self.lisOrder = lisOrder
        }else{
            self.lisOrder = 0
        }
        
        if let isForTeacher = isForTeacher{
            self.isForTeacher = isForTeacher
        }else{
            self.isForTeacher = 0
        }
        
        if let isForStudent = isForStudent{
            self.isForStudent = isForStudent
        }else{
            self.isForStudent = 0
        }
        
        if let remarks = remarks{
            self.remarks =  remarks
        }else{
            self.remarks =  ""
        }
        
        if let isActive = isActive{
            self.isActive = isActive
        }else{
            self.isActive = 0
        }
        
        if let createdDate = createdDate{
            self.createdDate = createdDate
        }else{
            self.createdDate = ""
        }
        
        
        if let upadatedDate = upadatedDate{
            self.upadatedDate = upadatedDate
        }else{
            self.upadatedDate = ""
        }
        
        if let createdUserId = createdUserId{
            self.createdUserId = createdUserId
        }else{
            self.createdUserId = 0
        }
        
        if let updatedUserId = updatedUserId{
            self.updatedUserId = updatedUserId
        }else{
            self.updatedUserId = 0
        }
        
        if let isDeleted = isDeleted{
            self.isDeleted = isDeleted
        }else{
            self.isDeleted = 0
        }
        
        if let deletedUserId = deletedUserId{
            self.deletedUserId = deletedUserId
        }else{
            self.deletedUserId = 0
        }
        
        if let deletedDate = deletedDate{
            self.deletedDate = deletedDate
        }else{
            self.deletedDate = ""
        }
        
        if let IsDownload = IsDownload{
            self.IsDownload = IsDownload
        }else{
            self.IsDownload = 0
        }
        
    }
    
    init(dictResponse : [String:Any]) {
    
      if let contentId = dictResponse["contentid"] as? Int32 {
          self.contentId = contentId
      }else{
          self.contentId = 0
      }
        
      if let contentDescription = dictResponse["contentdescription"] as? String{
            self.contentDescription = contentDescription
        }else{
            self.contentDescription = ""
        }
        
        
        if let topicId = dictResponse["topicid"] as? Int32{
            self.topicId = topicId
        }else{
            self.topicId = 0
        }
         
          if let contentTypeId = dictResponse["contenttypeid"] as? Int32{
              self.contentTypeId = contentTypeId
          }else{
              self.contentTypeId = 0
          }
        if let contentName = dictResponse["contentname"] as? String{
            self.contentName = contentName
        }else{
            self.contentName = ""
        }
        
        if let lisOrder = dictResponse["listorder"] as? Int32{
            self.lisOrder = lisOrder
        }else{
            self.lisOrder = 0
        }
        if let isActive = dictResponse["activeyn"] as? Int32{
            self.isActive = isActive
        }else{
            self.isActive = 0
        }
        
        if let isDeleted = dictResponse["deletedyn"] as? Int32{
            self.isDeleted = isDeleted
        }else{
            self.isDeleted = 0
        }
        if let createdDate = dictResponse["createddate"] as? String{
            self.createdDate = createdDate
        }else{
            self.createdDate = ""
        }
        
        if let upadatedDate = dictResponse["updateddate"] as? String{
            self.upadatedDate = upadatedDate
        }else{
            self.upadatedDate = ""
        }
        
        if let deletedDate = dictResponse["deleteddate"] as? String{
            self.deletedDate = deletedDate
        }else{
            self.deletedDate = ""
        }
        //Add 02/06/2020
        if let isForTeacher = dictResponse["isforteacher"] as? Int32{
            self.isForTeacher = isForTeacher
        }else{
            self.isForTeacher = 0
        }
        
        if let isForStudent = dictResponse["isforstudent"] as? Int32{
            self.isForStudent = isForStudent
        }else{
            self.isForStudent = 0
        }
    }
}

