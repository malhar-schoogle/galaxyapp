//
//  HeaderModel.swift
//  Schoogle
//
//  Created by Malhar on 30/10/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class HeaderModel: NSObject {
    var boardLogo: String?
    var boardName: String?
    var standardLogo : String?
    var standardName : String?
    var schoolCode : String?
    var employeeCode : String?
    var title : String?
    
    init(boardLogo : String , boardName : String , standardLogo : String , standardName : String , schoolCode : String , employeeCode : String , title : String ) {
        self.boardLogo = boardLogo
        self.boardName = boardName
        self.standardLogo = standardLogo
        self.standardName = standardName
        self.schoolCode = schoolCode
        self.employeeCode = employeeCode
        self.title = title
    }

}
