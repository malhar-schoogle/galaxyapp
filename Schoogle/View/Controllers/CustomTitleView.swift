//  CustomTitleView.swift
//  Schoogle
//  Created by Malhar on 12/11/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class CustomTitleView: UIView {

    var view: UIView!
    @IBOutlet weak var navLogo: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomTitleView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

}
