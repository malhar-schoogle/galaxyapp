//  DisplaySettingViewController.swift
//  Schoogle
//  Created by Malhar on 02/11/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit

class DisplaySettingViewController: UIViewController {

    @IBOutlet weak var sliderBrightness: UISlider!

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setBackButton()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionBrightnessChange(_ sender: UISlider) {
        UIScreen.main.brightness = CGFloat(sender.value)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
