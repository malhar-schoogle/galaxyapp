//
//  ShadowView.swift
//  Schoogle
//  Created by Malhar on 23/10/18.
//  Copyright © 2018 Malhar. All rights reserved.


import UIKit

class ShadowView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowColor = UIColor(red: Constant.SHADOW_GRY, green: Constant.SHADOW_GRY, blue: Constant.SHADOW_GRY, alpha: Constant.SHADOW_GRY).cgColor
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.cornerRadius = 2.0
    }
    
}
