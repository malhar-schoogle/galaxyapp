//  ACBCollectionViewCell.swift
//  RadialCollectionView
//  Created by Akhil on 11/26/16.
//  Copyright © 2016 Akhil. All rights reserved.

import UIKit

class ACBCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imgStandard: UIImageView!
    
    override var bounds : CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
       // self.makeItCircle()
    }
    
    //MARK: CHAPTER IMAGE CELL
    func confiqureUserCell(standard: Standard){
        
        self.imgStandard.contentMode = .scaleAspectFit
        
        if !standard.StandardImage.isEmpty{

            if UIImage(contentsOfFile: URL(fileURLWithPath: SQLITEManager.shareInstanceSQLITEManager.getDatabaseMediaFolderPath()).appendingPathComponent(standard.StandardImage).path) != nil{
                self.imgStandard.image = UIImage(contentsOfFile: URL(fileURLWithPath: SQLITEManager.shareInstanceSQLITEManager.getDatabaseMediaFolderPath()).appendingPathComponent(standard.StandardImage).path)
            }else{
            }
        }else{
            
        }
    }
    
    //MARK: SUBJECT CELL
    
    func confiqureSubjectCell(subject: Subject){
        self.imgStandard.contentMode = .scaleAspectFit
        self.label.text = subject.SubjectName
        
        if !subject.SubjectImage.isEmpty{
            if UIImage(contentsOfFile: URL(fileURLWithPath: SQLITEManager.shareInstanceSQLITEManager.getDatabaseMediaFolderPath()).appendingPathComponent(subject.SubjectImage).path) != nil{
                self.imgStandard.image = UIImage(contentsOfFile: URL(fileURLWithPath: SQLITEManager.shareInstanceSQLITEManager.getDatabaseMediaFolderPath()).appendingPathComponent(subject.SubjectImage).path)
            }else{
                //self.imgStandard.image = UIImage(named: "PlaceHolderImage")
            }
        }else{
            //self.imgStandard.image = UIImage(named: "PlaceHolderImage")
        }
        
    }
    
    //MARK: CHAPTER VIEW IMAGE CELL
    func confiqureChapterCell(chapter: Chapter){
        self.imgStandard.contentMode = .scaleAspectFit
        self.label.text = chapter.ChapterName
        
        if !chapter.ChapterImage.isEmpty{
            if UIImage(contentsOfFile: URL(fileURLWithPath: SQLITEManager.shareInstanceSQLITEManager.getDatabaseMediaFolderPath()).appendingPathComponent(chapter.ChapterImage).path) != nil{
                self.imgStandard.image = UIImage(contentsOfFile: URL(fileURLWithPath: SQLITEManager.shareInstanceSQLITEManager.getDatabaseMediaFolderPath()).appendingPathComponent(chapter.ChapterImage).path)
            }else{
                //self.imgStandard.image = UIImage(named: "PlaceHolderImage")
            }
        }else{
            //self.imgStandard.image = UIImage(named: "PlaceHolderImage")
        }
        
    }
    
    func makeItCircle() {
        self.imgStandard.layer.masksToBounds = true
        self.imgStandard.layer.cornerRadius  = CGFloat(round(self.imgStandard.frame.size.width / 2.0))
    }

}
