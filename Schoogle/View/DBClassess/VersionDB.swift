//
//  VersionDB.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class VersionDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrVersionDownloaded = [Version]()
    var arrVersion = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    //Init with database method
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrVersionDownloaded = self.setDataIntoVersionModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrVersion = self.getOnlyIDsFromDatabase()
        
    }
    
    //Init with database method when Call Api and copyData
    init(arrVersionDoanloaded : [Version]) {
        super.init()
        arrVersionDownloaded = arrVersionDoanloaded
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrVersion = self.getOnlyIDsFromDatabase()
    }
    
    //Insert data
    func insertVersionData() -> Bool{
        var isSuccess = false
        
        if arrVersion.count > 0{
            let arrOfNewVersion = self.compareVersion(arrVersionDownload: arrVersionDownloaded, arrVersionApplication: arrVersion)
            
            if arrOfNewVersion.count > 0 {
                isSuccess = self.insertVersion(arrNewVersion: arrOfNewVersion)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }else{
            if arrVersionDownloaded.count > 0 {
                isSuccess = self.insertVersion(arrNewVersion: arrVersionDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }
        arrVersion.removeAll()
        arrVersionDownloaded.removeAll()
        print("Version Done")
        sqliteManager.closeDB()
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoVersionModel() -> [Version] {
        
        var arrVersionTemp = [Version]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Version", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let StandardId = sqlite3_column_int(queryStatement, 0)
                    let VersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let ManualMedia = sqlite3_column_int(queryStatement, 2)
                    let FtpVersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let Versionid = sqlite3_column_int(queryStatement, 4)
                    let Boardid = sqlite3_column_int(queryStatement, 5)
                    let Releasedate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    
                    let objVersionModel = Version(StandardId: StandardId, VersionName: VersionName, ManualMedia: ManualMedia, FtpVersionName:FtpVersionName,Versionid:Versionid,Boardid:Boardid,Releasedate: Releasedate)
                    
                    arrVersionTemp.append(objVersionModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrVersionTemp
        
    }
    
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrMediaTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Version", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrMediaTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
    }
    
    // Check Version is exist or not
    func compareVersion(arrVersionDownload : [Version] , arrVersionApplication : [IDModel]) -> [Version]{
        
        var arrNewVersion = [Version]()
        if arrVersionApplication.count > 0{
            for VersionObj in arrVersionDownload{
                let filterObj = arrVersionApplication.filter({ $0.id == VersionObj.StandardId })
                if filterObj.count == 0{
                    arrNewVersion.append(VersionObj)
                }
            }
        }else{
            arrNewVersion.append(contentsOf: arrVersionDownload)
        }
        return arrNewVersion
    }
    
    //Insert Version into database
    func insertVersion(arrNewVersion : [Version]) -> Bool{
        var isSuccess = false
        for newVersion in arrNewVersion {
                let isInsert = self.sqliteManager.insertTable(query: self.makeInsertQuery(Version: newVersion))
                if isInsert {
                    //print("Inserted Successfully")
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
            }
        return isSuccess
    }
    
    
    //Insert Query
    func makeInsertQuery(Version : Version) -> String {
        
        return "INSERT INTO Version (StandardId,VersionName,ManualMedia,FtpVersionName,Versionid,Boardid,Releasedate) VALUES (\(Version.StandardId),'\(Version.VersionName.replacingOccurrences(of: "\'", with: "\'\'"))',\(Version.ManualMedia),'\(Version.FtpVersionName.replacingOccurrences(of: "\'", with: "\'\'"))','\(Version.Versionid)', '\(Version.Boardid)','\(Version.Releasedate)');"
        
    }
    
    //UPDATE -----------------------------------
    func searchVersionData(StandardId : Int32) -> Version{
        
        var objVersion = Version()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Version where StandardId = \(StandardId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let StandardId = sqlite3_column_int(queryStatement, 0)
                    let VersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let ManualMedia = sqlite3_column_int(queryStatement, 2)
                    let FtpVersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let Versionid = sqlite3_column_int(queryStatement, 4)
                    let Boardid = sqlite3_column_int(queryStatement, 5)
                    let Releasedate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    
                    objVersion =  Version(StandardId: StandardId, VersionName: VersionName, ManualMedia: ManualMedia, FtpVersionName:FtpVersionName,Versionid:Versionid,Boardid:Boardid,Releasedate: Releasedate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return objVersion
    }
    
    //Perform Update Operation
    func updateVersionData(Version : Version) -> Bool{
        
        var isSuccess = false
        //Seach Version
        let objVersion = searchVersionData(StandardId: Version.StandardId)
        
        if Version.StandardId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Version: Version))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Version: Version))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    
    //Make update query
    func makeUpdateQuery(Version : Version) -> String {
       
        return "UPDATE Version SET VersionName = '\(Version.VersionName)' , ManualMedia = '\(Version.ManualMedia)' , FtpVersionName = '\(Version.FtpVersionName)' , Versionid = '\(Version.Versionid)' , Boardid = '\(Version.Boardid)' , Releasedate = '\(Version.Releasedate)'  where StandardId = '\(Version.StandardId)'"
        
    }
    /*
     var Releasedate : String = ""
     */
    
    
    func getVersionData(StandardId : Int32) -> Version{
        
        var objVersion = Version()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Version where StandardId = \(StandardId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let StandardId = sqlite3_column_int(queryStatement, 0)
                    let VersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let ManualMedia = sqlite3_column_int(queryStatement, 2)
                    let FtpVersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let Versionid = sqlite3_column_int(queryStatement, 4)
                    let Boardid = sqlite3_column_int(queryStatement, 5)
                    let Releasedate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    
                    objVersion =  Version(StandardId: StandardId, VersionName: VersionName, ManualMedia: ManualMedia, FtpVersionName:FtpVersionName,Versionid:Versionid,Boardid:Boardid,Releasedate: Releasedate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        
        return objVersion
        
    }


    
    
}
