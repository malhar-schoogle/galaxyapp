//  SearchAllTableDB.swift
//  Schoogle
//  Created by macbook on 06/05/19.
//  Copyright © 2019 Malhar. All rights reserved.

import UIKit
import SQLite

class SearchAllTableDB {

    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    static let shared = SearchAllTableDB()
    var arrSearchMedia = [SearchAllTable]()

    //FETCH DATA FROM DATABASE
    func SearchDataFromDB(StrString:String,contentId:Int, successResponse: @escaping([SearchAllTable]?)->Void, error:@escaping(Error?) -> Void) {
         
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        
        self.arrSearchMedia.removeAll()
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: getDataTableQuery(strkeyword:StrString, tableID:contentId), success: { (queryStatement) in
                
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let contentId = sqlite3_column_int(queryStatement, 0)
                    let topicId = sqlite3_column_int(queryStatement, 1)
                    let contentName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let contentDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let topicname = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let chaptername = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    
                    let subjectname = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    
                    let standardname = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    
                    let boardname = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))

                    let chapterid = sqlite3_column_int(queryStatement, 9)
                    let objContentModel = SearchAllTable(contentId: contentId, topicId: topicId, contentName: contentName,contentDescription:contentDescription, topicname: topicname, chaptername: chaptername, subjectname: subjectname, standardname: standardname, boardname: boardname, chapterId: chapterid)
                    self.arrSearchMedia.append(objContentModel)
                
                }
                successResponse(self.arrSearchMedia)

            }){ (error) in
                print(error as Any)
                 successResponse(nil)
            }
        }else{
            print("Something went wrong")
        }
    }

    func getDataTableQuery(strkeyword:String,tableID:Int) -> String{
      
        var updateQuery = ""
        if Constant.CURRENTPAGE == 0{
       
        //MARK: STANDARD SEARCH QUERY
        updateQuery = """
            SELECT ct.ContentId, ct.TopicId, ct.ContentName, ct.ContentDescription, tp.TopicName, cp.ChapterName, sub.SubjectName, std.StandardName,bd.BoardName, cp.ChapterId, ct.IsDeleted, ct.IsActive FROM Content ct LEFT JOIN Topic tp ON tp.TopicId = ct.TopicId AND tp.IsDeleted = 0 AND tp.IsActive = 1 LEFT JOIN Chapter cp ON cp.ChapterId = tp.ChapterId AND cp.IsDeleted = 0 AND cp.IsActive = 1 LEFT JOIN Subject sub ON sub.SubjectId = cp.SubjectId AND sub.IsDeleted = 0 AND sub.IsActive = 1 LEFT JOIN Standard std ON std.StandardId = sub.StandardId AND std.IsDeleted = 0 AND std.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = ct.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 LEFT JOIN Board bd ON bd.BoardId = std.BoardId WHERE ct.IsDeleted = 0 AND ct.IsActive = 1 AND ct.ContentGroupId = 0 AND (ct.ContentName LIKE '%\(strkeyword)%' OR ct.ContentDescription LIKE '%\(strkeyword)%' OR ct.ContentSummary LIKE '%\(strkeyword)%'
              OR ct.ContentId LIKE '%\(strkeyword)%')
            """
        
        
        }else if Constant.CURRENTPAGE == 1{
            
             updateQuery = """
            SELECT ct.ContentId, ct.TopicId, ct.ContentName, ct.ContentDescription, tp.TopicName, cp.ChapterName, sub.SubjectName, std.StandardName,bd.BoardName, cp.ChapterId, ct.IsDeleted, ct.IsActive FROM Content ct LEFT JOIN Topic tp ON tp.TopicId = ct.TopicId AND tp.IsDeleted = 0 AND tp.IsActive = 1 LEFT JOIN Chapter cp ON cp.ChapterId = tp.ChapterId AND cp.IsDeleted = 0 AND cp.IsActive = 1 LEFT JOIN Subject sub ON sub.SubjectId = cp.SubjectId AND sub.IsDeleted = 0 AND sub.IsActive = 1 LEFT JOIN Standard std ON std.StandardId = sub.StandardId AND std.IsDeleted = 0 AND std.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = ct.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 LEFT JOIN Board bd ON bd.BoardId = std.BoardId WHERE std.StandardId = \(tableID) AND ct.IsDeleted = 0 AND ct.IsActive = 1 AND ct.ContentGroupId = 0 AND (ct.ContentName LIKE '%\(strkeyword)%' OR ct.ContentDescription LIKE '%\(strkeyword)%' OR ct.ContentSummary LIKE '%\(strkeyword)%'
            OR ct.ContentId LIKE '%\(strkeyword)%')
            """
            
        }else if Constant.CURRENTPAGE == 2{
            
             updateQuery = """
            SELECT ct.ContentId, ct.TopicId, ct.ContentName, ct.ContentDescription, tp.TopicName, cp.ChapterName, sub.SubjectName, std.StandardName,bd.BoardName, cp.ChapterId, ct.IsDeleted, ct.IsActive FROM Content ct LEFT JOIN Topic tp ON tp.TopicId = ct.TopicId AND tp.IsDeleted = 0 AND tp.IsActive = 1 LEFT JOIN Chapter cp ON cp.ChapterId = tp.ChapterId AND cp.IsDeleted = 0 AND cp.IsActive = 1 LEFT JOIN Subject sub ON sub.SubjectId = cp.SubjectId AND sub.IsDeleted = 0 AND sub.IsActive = 1 LEFT JOIN Standard std ON std.StandardId = sub.StandardId AND std.IsDeleted = 0 AND std.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = ct.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 LEFT JOIN Board bd ON bd.BoardId = std.BoardId WHERE sub.SubjectId = \(tableID) AND ct.IsDeleted = 0 AND ct.IsActive = 1 AND ct.ContentGroupId = 0 AND (ct.ContentName LIKE '%\(strkeyword)%' OR ct.ContentDescription LIKE '%\(strkeyword)%' OR ct.ContentSummary LIKE '%\(strkeyword)%'
            OR ct.ContentId LIKE '%\(strkeyword)%')
            """
        
        }else if Constant.CURRENTPAGE == 3{
            
            updateQuery = """
            SELECT ct.ContentId, ct.TopicId, ct.ContentName, ct.ContentDescription, tp.TopicName, cp.ChapterName, sub.SubjectName, std.StandardName,bd.BoardName, cp.ChapterId, ct.IsDeleted, ct.IsActive FROM Content ct LEFT JOIN Topic tp ON tp.TopicId = ct.TopicId AND tp.IsDeleted = 0 AND tp.IsActive = 1 LEFT JOIN Chapter cp ON cp.ChapterId = tp.ChapterId AND cp.IsDeleted = 0 AND cp.IsActive = 1 LEFT JOIN Subject sub ON sub.SubjectId = cp.SubjectId AND sub.IsDeleted = 0 AND sub.IsActive = 1 LEFT JOIN Standard std ON std.StandardId = sub.StandardId AND std.IsDeleted = 0 AND std.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = ct.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 LEFT JOIN Board bd ON bd.BoardId = std.BoardId WHERE cp.ChapterId = \(tableID) AND ct.IsDeleted = 0 AND ct.IsActive = 1 AND ct.ContentGroupId = 0 AND (ct.ContentName LIKE '%\(strkeyword)%' OR ct.ContentDescription LIKE '%\(strkeyword)%' OR ct.ContentSummary LIKE '%\(strkeyword)%'
            OR ct.ContentId LIKE '%\(strkeyword)%')
            """
            
        }
        
        
        return updateQuery
    
    }

    //MARK: CONTENT SEARCH QUERY
    func getContentTableQuery(strkeyword:String,tableID:Int) -> String{
        
        let updateQuery = """
        SELECT ct.ContentId, ct.TopicId, ct.ContentName, ct.ContentDescription, tp.TopicName, cp.ChapterName, sub.SubjectName, std.StandardName,bd.BoardName, cp.ChapterId, ct.IsDeleted, ct.IsActive FROM Content ct LEFT JOIN Topic tp ON tp.TopicId = ct.TopicId AND tp.IsDeleted = 0 AND tp.IsActive = 1 LEFT JOIN Chapter cp ON cp.ChapterId = tp.ChapterId AND cp.IsDeleted = 0 AND cp.IsActive = 1 LEFT JOIN Subject sub ON sub.SubjectId = cp.SubjectId AND sub.IsDeleted = 0 AND sub.IsActive = 1 LEFT JOIN Standard std ON std.StandardId = sub.StandardId AND std.IsDeleted = 0 AND std.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = ct.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 LEFT JOIN Board bd ON bd.BoardId = std.BoardId WHERE  cp.ChapterId = \(tableID) AND ct.IsDeleted = 0 AND ct.IsActive = 1 AND ct.ContentGroupId = 0 AND (ct.ContentName LIKE '%\(strkeyword)%' OR ct.ContentDescription LIKE '%\(strkeyword)%' OR ct.ContentSummary LIKE '%\(strkeyword)%'
        OR ct.ContentId LIKE '%\(strkeyword)%')
        """
        return updateQuery
    }
    
    func fetchSideMenuTextAndImages(strChapterId : Int32){        
        let strQuery = """
        SELECT cp.ChapterName, cp.ChapterImage, sb.SubjectName, sb.SubjectImage, std.StandardName, std.StandardImage, b.BoardName, b.BoardImage, cp.ChapterId,  sb.SubjectId, std.StandardId, b.BoardId FROM Chapter cp LEFT JOIN Subject sb ON sb.SubjectId = cp.SubjectId AND sb.IsDeleted = 0 AND sb.IsActive =1 LEFT JOIN Standard std ON std.StandardId = sb.StandardId  AND std.IsDeleted = 0 AND std.IsActive =1 LEFT JOIN Board b ON b.BoardId = std.BoardId WHERE cp.ChapterId = '\(strChapterId)' ORDER BY cp.ListOrder, cp.ChapterId
        """
        
        let topicDB = TopicDB.init()
        topicDB.getChapterInfoFromChapterId(strQuery: strQuery)
    }
    
//    //MARK: SUBJECT SEARCH QUERY
//
//    func getSubjectTableQuery(strkeyword:String,tableID:Int) -> String{
//
//        let updateQuery = """
//        SELECT ct.ContentId, ct.TopicId, ct.ContentName, ct.ContentDescription, tp.TopicName, cp.ChapterName, sub.SubjectName, std.StandardName,bd.BoardName, cp.ChapterId, ct.IsDeleted, ct.IsActive FROM Content ct LEFT JOIN Topic tp ON tp.TopicId = ct.TopicId AND tp.IsDeleted = 0 AND tp.IsActive = 1 LEFT JOIN Chapter cp ON cp.ChapterId = tp.ChapterId AND cp.IsDeleted = 0 AND cp.IsActive = 1 LEFT JOIN Subject sub ON sub.SubjectId = cp.SubjectId AND sub.IsDeleted = 0 AND sub.IsActive = 1 LEFT JOIN Standard std ON std.StandardId = sub.StandardId AND std.IsDeleted = 0 AND std.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = ct.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 LEFT JOIN Board bd ON bd.BoardId = std.BoardId WHERE  sub.SubjectId = \(tableID) AND ct.IsDeleted = 0 AND ct.IsActive = 1 AND ct.ContentGroupId = 0 AND (ct.ContentName LIKE '%\(strkeyword)%' OR ct.ContentDescription LIKE '%\(strkeyword)%' OR ct.ContentSummary LIKE '%\(strkeyword)%'
//        OR ct.ContentId LIKE '%\(strkeyword)%')
//        """
//        return updateQuery
//    }
    
    
    
    
    
    
    
    

}
