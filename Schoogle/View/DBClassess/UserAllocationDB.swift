//  UserAllocation.swift
//  Schoogle
//  Created by Malhar on 11/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit
import SQLite

let Query_Get_Board_ID = "SELECT DISTINCT BoardId FROM UserAllocation WHERE IsDeleted = 0 AND IsActive = 1"

class UserAllocationDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrUserAllocationDownloaded = [UserAllocation]()
    var arrUserAllocation = [IDModel]()
    
    var isFromContentUpdate = false
    
    // Init Method
    override init() {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        print(sqliteManager.pathToDatabase)
        sqliteManager.db = sqliteManager.openDatabase()
        //arrUserAllocation = self.getOnlyIDsFromDatabase()
    }
    
    //Init with database method when Call Api and copyData
    init(arrUserAllocationDoanloaded : [UserAllocation]) {
        super.init()
        
        arrUserAllocationDownloaded = arrUserAllocationDoanloaded
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrUserAllocation = self.getOnlyIDsFromDatabase()

    }
    
    //Insert data
    func insertUserAllocationData() -> Bool{
        var isSuccess = false
        
        if arrUserAllocation.count > 0 {
            
            let arrOfNewUserAllocation = self.compareUserAllocation(arrUserAllocationDownload: arrUserAllocationDownloaded, arrUserAllocationApplication: arrUserAllocation)
            
            if arrOfNewUserAllocation.count > 0 {
                isSuccess = self.insertUserAllocation(arrNewUserAllocation: arrOfNewUserAllocation)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }else{
            
            if arrUserAllocationDownloaded.count > 0 {
                isSuccess = self.insertUserAllocation(arrNewUserAllocation: arrUserAllocationDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }
        
        if self.isFromContentUpdate != true {
           self.compareUserAllocation2(arrUserAllocationDownload:arrUserAllocationDownloaded , arrUserAllocationApplication: arrUserAllocation)
        }
      
        
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoUserAllocationModel() -> [UserAllocation] {
        
        var arrUserAllocationTemp = [UserAllocation]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from UserAllocation", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let UserAllocationId = sqlite3_column_int(queryStatement, 0)
                    let UserId = sqlite3_column_int(queryStatement, 1)
                    let StandardId = sqlite3_column_int(queryStatement, 2)
                    let SubjectId = sqlite3_column_int(queryStatement, 3)
                    let ContentId = sqlite3_column_int(queryStatement, 4)
                    let ChapterId  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let BoardId = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let Sectionid = sqlite3_column_int(queryStatement, 15)
                    let Academicyearid = sqlite3_column_int(queryStatement, 16)
                    
                    let objUserAllocationModel = UserAllocation(UserAllocationId: UserAllocationId, UserId: UserId, StandardId: StandardId, SubjectId: SubjectId, ContentId: ContentId, ChapterId: ChapterId, BoardId: BoardId, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, Sectionid : Sectionid,
                    Academicyearid : Academicyearid)
                    
                    arrUserAllocationTemp.append(objUserAllocationModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrUserAllocationTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrUserAllocationTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from UserAllocation", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrUserAllocationTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrUserAllocationTemp
    }
    
    // Check UserAllocation is exist or not
    func compareUserAllocation(arrUserAllocationDownload : [UserAllocation] , arrUserAllocationApplication : [IDModel]) -> [UserAllocation]{
        
        var arrNewUserAllocation = [UserAllocation]()
        if arrUserAllocationApplication.count > 0{
            for UserAllocationObj in arrUserAllocationDownload{
                let filterObj = arrUserAllocationApplication.filter({ $0.id == UserAllocationObj.UserAllocationId })
                if filterObj.count == 0{
                    arrNewUserAllocation.append(UserAllocationObj)
                }else{
                        if(filterObj.count > 0){
                            let bIsUpdate = self.updateUserAllocationData(UserAllocation: UserAllocationObj)
                            if bIsUpdate{
                                print("User allocation Updated")
                            }
                        }
                    
                    
                }
            }
        }else{
            arrNewUserAllocation.append(contentsOf: arrUserAllocationDownload)
        }
        return arrNewUserAllocation
    }
    
    //delete allocation if removed
    func compareUserAllocation2(arrUserAllocationDownload : [UserAllocation] , arrUserAllocationApplication : [IDModel]){
        
        if arrUserAllocationApplication.count > 0{
            
            for UserAllocationObj in arrUserAllocationApplication{
                let filterObj = arrUserAllocationDownload.filter({ $0.UserAllocationId == UserAllocationObj.id })
                if filterObj.count == 0{
                    let isUpdate = sqliteManager.updateTable(query: self.makeDeleteQuery(allocationId: UserAllocationObj.id))
                    
                    if isUpdate {
                        print("Deleted Successfully")
                    }else{
                        print("Not Updated")
                    }
                }
            }
        }
        
    }
    
    //Insert UserAllocation into database
    func insertUserAllocation(arrNewUserAllocation : [UserAllocation]) -> Bool{
        var isSuccess = false
        for newUserAllocation in arrNewUserAllocation {
            let isInsert = self.sqliteManager.insertTable(query: self.makeInsertQuery(UserAllocation: newUserAllocation))
            if isInsert {
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
        }
        arrUserAllocation.removeAll()
        arrUserAllocationDownloaded.removeAll()
        print("UserAllocation Done")
        return isSuccess
    }
    
    func insertTable(query : String) -> Bool {
        var isSuccess = Bool()
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(sqliteManager.db, query, -1, &insertStatement, nil) == SQLITE_OK {
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                isSuccess = true
            } else {
                isSuccess = false
            }
        } else {
            isSuccess = false
        }
        sqlite3_finalize(insertStatement)
        return isSuccess
    }
    
    //Delete Subject From User Allocation
    func makeDeleteUserAllocationSubjectQuery(subjectid:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"delete from UserAllocation where SubjectId = \(subjectid)")
        }
        return issuccess
    }
    
    //MARK: Delete Standard From User Allocation
    func makeDeleteUserAllocationStandardQuery(standardid:Int) -> Bool{
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"delete from UserAllocation where StandardId = \(standardid)")
        }
        return issuccess
    }
    
    //MARK: Delete Standard From User Allocation
    func makeDeleteUserAllocationSubjectQuery(subjectId:Int) -> Bool{
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"delete from UserAllocation where SubjectId = \(subjectId)")
        }
        return issuccess
    }
    
    //MARK: Delete Chapter From User Allocation
    func makeDeleteUserAllocationChapterQuery(chapterID:Int) -> Bool{
        
        sqliteManager.closeDB()
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"DELETE FROM UserAllocation WHERE ContentId in(SELECT ContentId FROM Content WHERE TopicId in(SELECT TopicId FROM Topic WHERE ChapterId = \(chapterID)))")
        }
        return issuccess
    }
    
    //DELETE FROM UserAllocation WHERE ContentId in(SELECT ContentId FROM Content WHERE TopicId in(SELECT TopicId FROM Topic WHERE ChapterId = \(chapterID)))
    
  //  DELETE FROM UserAllocation WHERE ContentId in (SELECT ContentId  FROM Content WHERE TopicId  in (SELECT TopicId FROM Topic WHERE ChapterId = 551))

    
    /*
     Delete From UserAllocation Where ContentId In (
     Select ContentId From Content
     Where TopicId In (
     Select TopicId From Topic
     Where ChapterId = @ChapterId
     )
     )
     
     //delete from UserAllocation where StandardId = \(chapterID)"
     */

    
    //Insert Query
    func makeInsertQuery(UserAllocation : UserAllocation) -> String {
        
        return "INSERT INTO UserAllocation (UserAllocationId ,UserId ,StandardId ,SubjectId ,ContentId ,ChapterId ,BoardId ,IsActive ,CreatedDate ,UpdatedDate ,CreatedUserId ,UpdatedUserId ,IsDeleted ,DeletedUserId ,DeletedDate,Sectionid,Academicyearid) VALUES (\(UserAllocation.UserAllocationId ),\(UserAllocation.UserId ),\(UserAllocation.StandardId ),\(UserAllocation.SubjectId ),\(UserAllocation.ContentId ),'\(UserAllocation.ChapterId )',\(UserAllocation.BoardId ),\(UserAllocation.IsActive ),'\(UserAllocation.CreatedDate )','\(UserAllocation.UpdatedDate )',\(UserAllocation.CreatedUserId ),\(UserAllocation.UpdatedUserId ),\(UserAllocation.IsDeleted ),\(UserAllocation.DeletedUserId ),'\(UserAllocation.DeletedDate )','\(UserAllocation.Sectionid )','\(UserAllocation.Academicyearid)');"
    }
    
    //UPDATE -----------------------------------
    func searchUserAllocationData(UserAllocationId : Int32) -> UserAllocation{
        
        var objUserAllocation = UserAllocation()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from UserAllocation where UserAllocationId = \(UserAllocationId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let UserAllocationId = sqlite3_column_int(queryStatement, 0)
                    let UserId = sqlite3_column_int(queryStatement, 1)
                    let StandardId = sqlite3_column_int(queryStatement, 2)
                    let SubjectId = sqlite3_column_int(queryStatement, 3)
                    let ContentId = sqlite3_column_int(queryStatement, 4)
                    let ChapterId  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let BoardId = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let Sectionid = sqlite3_column_int(queryStatement, 15)
                    let Academicyearid = sqlite3_column_int(queryStatement, 16)
                    
                    objUserAllocation = UserAllocation(UserAllocationId: UserAllocationId, UserId: UserId, StandardId: StandardId, SubjectId: SubjectId, ContentId: ContentId, ChapterId: ChapterId, BoardId: BoardId, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, Sectionid : Sectionid,
                    Academicyearid : Academicyearid)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objUserAllocation
        
    }
    
    //Perform Update Operation
    func updateUserAllocationData(UserAllocation : UserAllocation) -> Bool{
        
        var isSuccess = false
        //Seach UserAllocation
        let objUserAllocation = searchUserAllocationData(UserAllocationId: UserAllocation.UserAllocationId)
        
        if UserAllocation.UserAllocationId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(UserAllocation: UserAllocation))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(UserAllocation: UserAllocation))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery(UserAllocation : UserAllocation) -> String {
        print("UPDATE DELETE: ", UserAllocation.IsDeleted)
        
        return "UPDATE UserAllocation SET UserId  = '\(UserAllocation.UserId)' , StandardId  = '\(UserAllocation.StandardId)' ,  SubjectId  = '\(UserAllocation.SubjectId)' ,  ContentId  = '\(UserAllocation.ContentId)' ,  ChapterId  = '\(UserAllocation.ChapterId.replacingOccurrences(of: "\'", with: "\'\'"))' ,  BoardId  = '\(UserAllocation.BoardId)' ,  IsActive  = '\(UserAllocation.IsActive)' ,  CreatedDate  = '\(UserAllocation.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  UpdatedDate  = '\(UserAllocation.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  CreatedUserId  = '\(UserAllocation.CreatedUserId)' ,  UpdatedUserId  = '\(UserAllocation.UpdatedUserId)' ,  IsDeleted  = '\(UserAllocation.IsDeleted)' ,  DeletedUserId  = '\(UserAllocation.DeletedUserId)' ,  DeletedDate  = '\(UserAllocation.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))', Sectionid = '\(UserAllocation.Sectionid)', Academicyearid = '\(UserAllocation.Academicyearid)' , Sectionid = '\(UserAllocation.Sectionid)'  where UserAllocationId = '\(UserAllocation.UserAllocationId)'"
    }
    
    
    func makeDeleteQuery(allocationId : Int32) -> String {
        print("UPDATE DELETE: ", allocationId)
        return "UPDATE UserAllocation SET  IsDeleted  = 1 where UserAllocationId = \(allocationId)"
    }
    
    // -----------------------> STANDARD <------------------------

    func getAllocatedBoardId() -> [UserAllocation]{
        
        var arrUserAllocation = [UserAllocation]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "SELECT DISTINCT BoardId FROM UserAllocation WHERE IsDeleted = 0 AND IsActive = 1", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let BoardId = sqlite3_column_int(queryStatement, 0)
                    
                    let objUserAllocation = UserAllocation()
                    objUserAllocation.BoardId = BoardId
                    arrUserAllocation.append(objUserAllocation)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }

        return arrUserAllocation
        
    }
    
    func getAllocatedStandardId(boardId : Int32) -> [UserAllocation]{
        
        var arrUserAllocation = [UserAllocation]()

        let Query_Get_Standard_ID = "SELECT distinct a.StandardId FROM UserAllocation a INNER JOIN Standard b On a.standardid = b.standardid  WHERE a.BoardId = \(boardId) AND a.IsDeleted = 0 AND a.IsActive = 1 order by ListOrder asc"

        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Standard_ID, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let StandardID = sqlite3_column_int(queryStatement, 0)
                    
                    let objUserAllocation = UserAllocation()
                    objUserAllocation.StandardId = StandardID
                    arrUserAllocation.append(objUserAllocation)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return arrUserAllocation

    }
    
    // -----------------------> SUBJECT <------------------------
    
    func getAllocatedSubjectID(StandardID : Int32) -> [UserAllocation]{
        
        var arrUserAllocation = [UserAllocation]()
        
        let Query_Get_Subject_ID = "SELECT distinct SubjectId FROM UserAllocation WHERE StandardId = \(StandardID) AND IsDeleted = 0 AND IsActive = 1"
    
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Subject_ID, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let SubjectID = sqlite3_column_int(queryStatement, 0)
                    
                    let objUserAllocation = UserAllocation()
                    objUserAllocation.SubjectId = SubjectID
                    arrUserAllocation.append(objUserAllocation)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return arrUserAllocation
        
    }
    
    // -----------------------> CHAPTER <------------------------
    
    func getAllocatedChapterIds(subjectId : Int32) -> [UserAllocation]{
        
        var arrUserAllocation = [UserAllocation]()
        
        let Query_Get_Chapter_ID = "SELECT distinct tpc.ChapterId FROM UserAllocation ua, CONTENT ctn, TOPIC tpc WHERE ua.ContentId = ctn.ContentId AND ctn.TopicId = tpc.TopicId AND ua.IsDeleted = 0 AND ua.IsActive = 1 AND ua.ContentId in (SELECT distinct ContentId FROM UserAllocation WHERE SubjectId = \(subjectId) AND IsDeleted = 0 AND IsActive = 1)"
        
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Chapter_ID, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ChapterID = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 0))
                    
                    let objUserAllocation = UserAllocation()
                    objUserAllocation.ChapterId = ChapterID
                    arrUserAllocation.append(objUserAllocation)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return arrUserAllocation
        
    }
    // -----------------------> CONTENTS <------------------------
    func getAllocatedContentId(subjectId : Int32) -> [UserAllocation]{
        
        var arrUserAllocation = [UserAllocation]()
        
        let Query_Get_Content_ID = "SELECT distinct ContentId FROM UserAllocation WHERE SubjectId = \(subjectId) AND IsDeleted = 0 AND IsActive = 1"
        
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Content_ID, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ContentID = sqlite3_column_int(queryStatement, 0)
                    
                    let objUserAllocation = UserAllocation()
                    objUserAllocation.ContentId = ContentID
                    arrUserAllocation.append(objUserAllocation)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return arrUserAllocation
        
    }


}
