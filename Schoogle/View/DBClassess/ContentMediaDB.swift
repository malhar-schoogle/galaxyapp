//
//  ContentMediaDB.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class ContentMediaDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrContentMediaDownloaded = [ContentMedia]()
    var arrContentMedia = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    
    //Init with database method
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrContentMediaDownloaded = self.setDataIntoContentMediaModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrContentMedia = self.getOnlyIDsFromDatabase()
        
    }
    
    //Init with database method when Call Api and copyData
    init(arrContentMediaDoanloaded : [ContentMedia]) {
        super.init()
        
        self.arrContentMediaDownloaded = arrContentMediaDoanloaded
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        self.arrContentMedia = self.getOnlyIDsFromDatabase()
        
    }
    
    //Insert data
    func insertContentMediaData() -> Bool{
        var isSuccess = false
        let arrOfNewContentMedia = self.compareContentMedia(arrContentMediaDownload: arrContentMediaDownloaded, arrContentMediaApplication: arrContentMedia)
        
        if arrOfNewContentMedia.count > 0 {
            isSuccess = self.insertContentMedia(arrNewContentMedia: arrOfNewContentMedia)
        }else{
            print("No Any Update")
            isSuccess = true
        }
        arrContentMedia.removeAll()
        arrContentMediaDownloaded.removeAll()
        print("ContentMedia Done")
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.sqliteManager.closeDB()
        })
        
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoContentMediaModel() -> [ContentMedia] {
        
        var arrContentMediaTemp = [ContentMedia]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from ContentMedia", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ContentMediaId = sqlite3_column_int(queryStatement, 0)
                    let ContentId = sqlite3_column_int(queryStatement, 1)
                    let MediaId = sqlite3_column_int(queryStatement, 2)
                    let IsActive = sqlite3_column_int(queryStatement, 3)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 6)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 7)
                    let IsDeleted = sqlite3_column_int(queryStatement, 8)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 9)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    
                    let objContentMediaModel = ContentMedia(ContentMediaId: ContentMediaId, ContentId: ContentId, MediaId: MediaId, isActive: IsActive, createdDate: CreatedDate, upadatedDate: UpdatedDate, createdUserId: CreatedUserId, updatedUserId: UpdatedUserId, isDeleted: IsDeleted, deletedUserId: DeletedUserId, deletedDate: DeletedDate)
                    
                    arrContentMediaTemp.append(objContentMediaModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrContentMediaTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrMediaTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from ContentMedia", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrMediaTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
    }
    
    // Check ContentMedia is exist or not
    func compareContentMedia(arrContentMediaDownload : [ContentMedia] , arrContentMediaApplication : [IDModel]) -> [ContentMedia]{
        
        var arrNewContentMedia = [ContentMedia]()
        if arrContentMediaApplication.count > 0{
            for ContentMediaObj in arrContentMediaDownload{
                let filterObj = arrContentMediaApplication.filter({ $0.id == ContentMediaObj.ContentMediaId })
                if filterObj.count == 0{
                    arrNewContentMedia.append(ContentMediaObj)
                }
            }
        }else{
            arrNewContentMedia.append(contentsOf: arrContentMediaDownload)
        }
        return arrNewContentMedia
    }
    
    //Insert ContentMedia into database
    func insertContentMedia(arrNewContentMedia : [ContentMedia]) -> Bool{
        var isSuccess = false
        for newContentMedia in arrNewContentMedia {
                let isInsert = self.sqliteManager.insertTable(query: self.makeInsertQuery(ContentMedia: newContentMedia))
                if isInsert {
                    //print("Inserted Successfully")
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
            }
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(ContentMedia : ContentMedia) -> String {
        
        return "INSERT INTO ContentMedia (ContentMediaId, ContentId, MediaId, IsActive, CreatedDate, UpdatedDate, CreatedUserId, UpdatedUserId, IsDeleted, DeletedUserId, DeletedDate) VALUES (\(ContentMedia.ContentMediaId),\(ContentMedia.ContentId),\(ContentMedia.MediaId),\(ContentMedia.isActive),'\(ContentMedia.createdDate)','\(ContentMedia.upadatedDate)',\(ContentMedia.createdUserId),\(ContentMedia.updatedUserId),\(ContentMedia.isDeleted),\(ContentMedia.deletedUserId),'\(ContentMedia.deletedDate)');"
        
    }
    
    //UPDATE -----------------------------------
    func searchContentMediaData(ContentMediaId : Int32) -> ContentMedia{
        
        var objContentMedia = ContentMedia()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from ContentMedia where ContentMediaId = \(ContentMediaId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ContentMediaId = sqlite3_column_int(queryStatement, 0)
                    let ContentId = sqlite3_column_int(queryStatement, 1)
                    let MediaId = sqlite3_column_int(queryStatement, 2)
                    let IsActive = sqlite3_column_int(queryStatement, 3)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 6)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 7)
                    let IsDeleted = sqlite3_column_int(queryStatement, 8)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 9)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    
                    objContentMedia = ContentMedia(ContentMediaId: ContentMediaId, ContentId: ContentId, MediaId: MediaId, isActive: IsActive, createdDate: CreatedDate, upadatedDate: UpdatedDate, createdUserId: CreatedUserId, updatedUserId: UpdatedUserId, isDeleted: IsDeleted, deletedUserId: DeletedUserId, deletedDate: DeletedDate)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        return objContentMedia

    }
    //Delete Query of Content Media Table FROM BLM
    func makeDeleteContentMediaQuery(chapterId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In (Select TopicId From Topic Where ChapterId = \(chapterId)))")
        }
        return issuccess
    }

    
    //Delete Query of Content Media Table FROM Standard
    func makeDeleteStandardContentMediaQuery(standardId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In (Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId In(Select SubjectId From Subject Where StandardId = \(standardId)))))")
        }
        return issuccess
    }
    
    
    //Delete Query of Content Media Table FROM Subject
    func makeDeleteSubjectContentMediaQuery(subjectId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In (Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId = \(subjectId))))")
        }
        return issuccess
    }
    
    //Perform Update Operation
    func updateContentMediaData(ContentMedia : ContentMedia) -> Bool{
        
        var isSuccess = false
        //Seach ContentMedia
        let objContentMedia = searchContentMediaData(ContentMediaId: ContentMedia.ContentMediaId)
        
        if ContentMedia.ContentMediaId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(ContentMedia: ContentMedia))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(ContentMedia: ContentMedia))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    
    //Make update query
    func makeUpdateQuery(ContentMedia : ContentMedia) -> String {
        
        return "UPDATE ContentMedia SET ContentId = '\(ContentMedia.ContentId)' , MediaId = '\(ContentMedia.MediaId)' , IsActive = '\(ContentMedia.isActive)' , CreatedDate = '\(ContentMedia.createdDate.replacingOccurrences(of: "\'", with: "\'\'"))' , UpdatedDate = '\(ContentMedia.upadatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , CreatedUserId = '\(ContentMedia.createdUserId)' , UpdatedUserId = '\(ContentMedia.updatedUserId)' , IsDeleted = '\(ContentMedia.isDeleted)' , DeletedUserId = '\(ContentMedia.deletedUserId)' , DeletedDate = '\(ContentMedia.deletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' where ContentMediaId = '\(ContentMedia.ContentMediaId)'"
        
    }
}

