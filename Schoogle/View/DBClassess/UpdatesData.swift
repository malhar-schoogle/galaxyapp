//
//  UpdatesData.swift
//  Schoogle
//
//  Created by Emxcel on 20/05/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import SVProgressHUD
import MBProgressHUD

class UpdateData : NSObject {
    

    //Variables
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var mbHUD = MBProgressHUD()
    
    var arrContentID = [Int32]()
    var arrContent = [Content]()
    var arrStandard = [Standard]()
    
    //Variavle for Update table
    var arrUpdateVersion = [Version]()
    var arrUpdateBoard = [Board]()
    var arrUpdateStandard = [Standard]()
    var arrUpdateMediaType = [MediaType]()
    var arrUpdateMedia = [Media]()
    var arrUpdateSubject = [Subject]()
    var arrUpdateContentType = [ContentType]()
    var arrUpdateChapter = [Chapter]()
    var arrUpdateTopic = [Topic]()
    var arrUpdateContent = [Content]()
    var arrUpdateUserAllocation = [UserAllocation]()
    var arrUpdateContentMedia = [ContentMedia]()
    
    
    //Initialize class
    override init() {
        sqliteManager.copyFileToDocs()
        //sqliteManager.extract()
    }
    
    func hideLoader(){
        Constant.APPDELEGATE?.hideCustomLoader()
    }
    func getDataVersion() {
        let versionDB = VersionDB.init()
        let standardDB = StandardDB.init()
        let arrstandard = standardDB.getAllStandardData()
        var arrDataVersion = [String]()
        var dataVersion = ""
        if !arrstandard.isEmpty {
            for standard in arrstandard {
                let versionData = versionDB.getVersionData(StandardId:standard.StandardId)
                arrDataVersion.append("\(standard.StandardId):\(versionData.VersionName)")
            }
        }
        
        dataVersion = arrDataVersion.joined(separator: ",")
        print("dataVersion :\(dataVersion)")
        
        UserDefaults.standard.set(dataVersion, forKey: Constant.Data_Version)
        UserDefaults.standard.synchronize()
        
    }
    
    //MARK:- GET OLD VERSION
    func getOldversion(standardID:Int32) {
        let versionDB = VersionDB.init()
        let versionData = versionDB.getVersionData(StandardId:standardID )
        UserDefaults.standard.set(versionData.VersionName, forKey: Constant.Old_Version)
        UserDefaults.standard.synchronize()
        
    }
    
    //MARK:- Service Call 1
    func callMainUpdateAPI() {
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "SyncUpdate...")
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        let DataVersion = UserDefaults.standard.string(forKey: Constant.Data_Version) == "" ? "0" :UserDefaults.standard.string(forKey: Constant.Data_Version)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
            print("API GET DATA URL IS ====>",Constant.Update_Main_API_Call + "\(Constant.MACID)&DataVersion=\(DataVersion!)")
            let URL = Constant.Update_Main_API_Call + "\(Constant.MACID)&DataVersion=\(DataVersion!)"
            
            APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
                
                if let arrJson = dictResponse{
                    
                    if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                        
                        // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                        if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                            
                            print(arrStatus[0])
                            
                            if let responseStatus = arrStatus[0]["status"] as? String{
                                if responseStatus == "Success"{
                                    
                                    for dictTable in arrJson{
                                        let dicTemp = dictTable as! Dictionary<String,Any>
                                        
                                        let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                        
                                        if arrOfKeys.count > 0{
                                            
                                            if arrOfKeys.contains("Version"){
                                                
                                                let arrVersion = dicTemp["Version"] as! [Dictionary<String,Any>]
                                                
                                                if arrVersion.count > 0{
                                                    
                                                    var arrModelVersion = [Version]()
                                                    
                                                    for dictVersion in arrVersion{
                                                        let objVersionModel = Version(dictResponse: dictVersion)
                                                        arrModelVersion.append(objVersionModel)
                                                    }
                                                    self.arrUpdateVersion = arrModelVersion
                                                    
                                                    let objVersion = VersionDB(arrVersionDoanloaded:arrModelVersion)
                                                    let isSuccess = objVersion.insertVersionData()
                                                    print("VersionDB :-",isSuccess)
                                                    
                                                    if !self.arrUpdateVersion.isEmpty {
                                                        self.updateVersionData()
                                                    }
                                                    
                                                }
                                            }else if arrOfKeys.contains("Board"){
                                                
                                                let arrBoard = dicTemp["Board"] as! [Dictionary<String,Any>]
                                                
                                                if arrBoard.count > 0{
                                                    
                                                    var arrModelBoard = [Board]()
                                                    
                                                    for dictBoard in arrBoard{
                                                        let objBoardModel = Board(dictResponse: dictBoard)
                                                        arrModelBoard.append(objBoardModel)
                                                    }
                                                    
                                                    self.arrUpdateBoard = arrModelBoard
                                                    
                                                    let objBoard = BoardDB(arrBoardDoanloaded: arrModelBoard)
                                                    let isSuccess = objBoard.insertBoardData()
                                                    print("BoardDB :-",isSuccess)
                                                    
                                                    
                                                    if !self.arrUpdateBoard.isEmpty {
                                                        self.updateBoardData()
                                                    }
                                                    
                                                    
                                                }
                                            }else if arrOfKeys.contains("Standard"){
                                                let arrStandard = dicTemp["Standard"] as! [Dictionary<String,Any>]
                                                if arrStandard.count > 0{
                                                    
                                                    var arrModelStandard = [Standard]()
                                                    
                                                    for dictStandard in arrStandard {
                                                        let objStandardModel = Standard(dictResponse: dictStandard)
                                                        arrModelStandard.append(objStandardModel)
                                                    }
                                                    
                                                    self.arrUpdateStandard = arrModelStandard
                                                    
                                                    let objStandard = StandardDB(arrStandardDoanloaded: arrModelStandard)
                                                    let isSuccess = objStandard.insertStandardData()
                                                    print("StandardDB :-",isSuccess)
                                                    
                                                    
                                                    if !self.arrUpdateStandard.isEmpty {
                                                        self.updateStandardData()
                                                    }
                                                }
                                            }else if arrOfKeys.contains("MediaType"){
                                                let arrMediaType = dicTemp["MediaType"] as! [Dictionary<String,Any>]
                                                if arrMediaType.count > 0{
                                                    
                                                    var arrModelMediaType = [MediaType]()
                                                    
                                                    for dictStandard in arrMediaType {
                                                        let objMediaTypeModel = MediaType(dictResponse: dictStandard)
                                                        arrModelMediaType.append(objMediaTypeModel)
                                                    }
                                                    
                                                    self.arrUpdateMediaType = arrModelMediaType
                                                    
                                                    let objMediaType = MediaTypeDB(arrMediaTypeDoanloaded: arrModelMediaType)
                                                    let isSuccess = objMediaType.insertMediaTypeData()
                                                    print("MediaTypeDB :-",isSuccess)
                                                    
                                                    if !self.arrUpdateMediaType.isEmpty {
                                                        self.updateMediaTypeData()
                                                    }
                                                    
                                                }
                                            }else if arrOfKeys.contains("Media"){
                                                
                                                let arrMedia = dicTemp["Media"] as! [Dictionary<String,Any>]
                                                
                                                if arrMedia.count > 0{
                                                    
                                                    var arrModelMedia = [Media]()
                                                    let manualMedia = Int32()
                                                    
                                                    for dictMedia in arrMedia{
                                                        let objMediaModel = Media(dictResponse: dictMedia)
                                                        
                                                        objMediaModel.MediaFlag = "Public"
                                                        
                                                        if let FileSize = dictMedia["filesize"] as? Int32{
                                                            objMediaModel.FileSize = FileSize
                                                        }else{
                                                            objMediaModel.FileSize = 0
                                                        }
                                                        
                                                        if let mediaPath = dictMedia["mediapath"] as? String{
                                                            objMediaModel.mediaPath = mediaPath
                                                        }else{
                                                            objMediaModel.mediaPath = ""
                                                        }
                                                        
                                                        if let mediaPath = dictMedia["mediapath"] as? String{
                                                            objMediaModel.mediaPath = mediaPath
                                                        }else{
                                                            objMediaModel.mediaPath = ""
                                                        }
                                                        objMediaModel.ManualMedia = manualMedia
                                                        
                                                        arrModelMedia.append(objMediaModel)
                                                        
                                                    }
                                                    
                                                    self.arrUpdateMedia = arrModelMedia
                                                    
                                                    let objMedia = MediaDB(arrMediaDoanloaded: arrModelMedia)
                                                    let isSuccess = objMedia.insertMediaData()
                                                    print("MediaDB :-",isSuccess)
                                                    
                                                    if !self.arrUpdateMedia.isEmpty {
                                                        self.updateMediaData()
                                                    }
                                                }
                                            }
                                            else  if arrOfKeys.contains("Subject"){
                                                
                                                let arrSubject = dicTemp["Subject"] as! [Dictionary<String,Any>]
                                                
                                                if arrSubject.count > 0{
                                                    
                                                    var arrModelSubject = [Subject]()
                                                    
                                                    for dictSubject in arrSubject{
                                                        let objSubjectModel = Subject(dictResponse:dictSubject)
                                                        arrModelSubject.append(objSubjectModel)
                                                    }
                                                    
                                                    self.arrUpdateSubject = arrModelSubject
                                                    
                                                    let objSubject = SubjectDB(arrSubjectDoanloaded:arrModelSubject)
                                                    let isSuccess = objSubject.insertSubjectData()
                                                    print("SubjectDB :-",isSuccess)
                                                    
                                                    if !self.arrUpdateSubject.isEmpty {
                                                        self.updateSubjectData()
                                                    }
                                                    
                                                }
                                            }
                                            else if arrOfKeys.contains("ContentType"){
                                                
                                                let arrContentType = dicTemp["ContentType"] as! [Dictionary<String,Any>]
                                                
                                                if arrContentType.count > 0{
                                                    
                                                    var arrModelContentType = [ContentType]()
                                                    
                                                    for dictContentType in arrContentType{
                                                        let objContentTypeModel = ContentType(dictResponse:dictContentType)
                                                        arrModelContentType.append(objContentTypeModel)
                                                    }
                                                    
                                                    self.arrUpdateContentType = arrModelContentType
                                                    
                                                    let objContentType = ContentTypeDB(arrContentTypeDoanloaded:arrModelContentType)
                                                    let isSuccess = objContentType.insertContentTypeData()
                                                    print("ContentTypeDB :-",isSuccess)
                                                    
                                                    if !self.arrUpdateContentType.isEmpty {
                                                        self.updateContentTypeData()
                                                    }
                                                }
                                            }
                                            else if arrOfKeys.contains("Chapter"){
                                                
                                                let arrChapterType = dicTemp["Chapter"] as! [Dictionary<String,Any>]
                                                
                                                if arrChapterType.count > 0{
                                                    
                                                    var arrModelChapterType = [Chapter]()
                                                    
                                                    for dictChapterType in arrChapterType{
                                                        let objChapterModel = Chapter(dictResponse: dictChapterType)
                                                        arrModelChapterType.append(objChapterModel)
                                                    }
                                                    
                                                    self.arrUpdateChapter = arrModelChapterType
                                                    
                                                    let objChapter = ChapterDB(arrChapterDoanloaded:arrModelChapterType)
                                                    let isSuccess = objChapter.insertChapterData()
                                                    print("ChapterDB :-",isSuccess)
                                                    
                                                    
                                                    if !self.arrUpdateChapter.isEmpty {
                                                        self.updateChapterData()
                                                    }
                                                    
                                                }
                                            }
                                            else if arrOfKeys.contains("Topic"){
                                                
                                                let arrTopic = dicTemp["Topic"] as! [Dictionary<String,Any>]
                                                
                                                if arrTopic.count > 0{
                                                    
                                                    var arrModelTopic = [Topic]()
                                                    
                                                    for dictTopic in arrTopic {
                                                        let objTopicModel = Topic(dictResponse: dictTopic)
                                                        arrModelTopic.append(objTopicModel)
                                                    }
                                                    self.arrUpdateTopic = arrModelTopic
                                                    
                                                    let objTopic = TopicDB(arrTopicDoanloaded:arrModelTopic)
                                                    let isSuccess = objTopic.insertTopicData()
                                                    print("TopicDB :-",isSuccess)
                                                    
                                                    if !self.arrUpdateTopic.isEmpty {
                                                        self.updateTopicData()
                                                    }
                                                }
                                            }
                                            print(arrOfKeys)
                                        }
                                    }
                                    self.hideLoader()
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncUpdateComplete"), object: nil, userInfo: nil)
                                    
                                }else{
                                    UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                    self.hideLoader()
                                }
                            }else{
                                self.hideLoader()
                            }
                            
                        }else{
                            print("No Any Status Array ")
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status ")
                        self.hideLoader()
                    }
                    
                }else{
                    self.hideLoader()
                }
            }) {(error) in
                print(error?.localizedDescription as Any)
                UIViewController().alertControllerExt(strMessage: "\(error?.localizedDescription as Any)")
                self.hideLoader()
            }
        })
        
    }
    
    //MARK:- Service Call 2
    func callMainContentAPI(subjectID:String) {
        
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "SyncContent...")
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        
        let oldVersion = UserDefaults.standard.string(forKey: Constant.Old_Version) == "" ? "0" :UserDefaults.standard.string(forKey: Constant.Old_Version)
        
        let URL = Constant.Content_Main_API_Call + "\(Constant.MACID)&SubjectId=\(subjectID)&DataVersion=\(oldVersion!)"
        
        print("API GET DATA URL IS ====>",URL)
        
        APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if let arrJson = dictResponse{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        
                        print(arrStatus[0])
                        
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success"{
                                
                                for dictTable in arrJson{
                                    let dicTemp = dictTable as! Dictionary<String,Any>
                                    
                                    let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                    
                                    if arrOfKeys.count > 0{
                                        
                                        if arrOfKeys.contains("Content"){
                                            
                                            let arrContent = dicTemp["Content"] as! [Dictionary<String,Any>]
                                            
                                            if arrContent.count > 0{
                                                
                                                var arrModelContent = [Content]()
                                                
                                                for dictContent in arrContent{
                                                    let objContentModel = Content(dictResponse: dictContent)
                                                    arrModelContent.append(objContentModel)
                                                }
                                                
                                                self.arrUpdateContent = arrModelContent
                                                
                                                let objContent = ContentDB(arrContentDoanloaded:arrModelContent)
                                                let isSuccess = objContent.insertContentData()
                                                print("ContentDB :-",isSuccess)
                                                
                                                if !self.arrUpdateContent.isEmpty {
                                                    self.updateContentData()
                                                }
                                            }
                                        }
                                        
                                        print(arrOfKeys)
                                    }
                                }
                                
                                //MARK:- Call Content Allocation API
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                    self.callContentAllocationAPI(subjectID: subjectID)
                                })
                                
                                
                            }else{
                                UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                self.hideLoader()
                            }
                        }else{
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status Array ")
                        self.hideLoader()
                    }
                    
                }else{
                    print("No Any Status ")
                    self.hideLoader()
                }
                
            }else{
                self.hideLoader()
            }
        }) {(error) in
            print(error?.localizedDescription as Any)
            UIViewController().alertControllerExt(strMessage: "\(error?.localizedDescription as Any)")
            self.hideLoader()
        }
        
    }
    //MARK:- Service Call 3
    func callContentAllocationAPI(subjectID:String) {
        
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        let URL = Constant.Content_Allowcation_API_Call + "\(Constant.MACID)&SubjectId=\(subjectID)"
        
        print("API GET DATA URL IS ====>",URL)
        
        APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if let arrJson = dictResponse{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        
                        print(arrStatus[0])
                        
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success"{
                                
                                for dictTable in arrJson{
                                    let dicTemp = dictTable as! Dictionary<String,Any>
                                    
                                    let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                    
                                    if arrOfKeys.count > 0{
                                        
                                        if arrOfKeys.contains("UserAllocation"){
                                            
                                            let arrUserAllowcation = dicTemp["UserAllocation"] as! [Dictionary<String,Any>]
                                            
                                            if arrUserAllowcation.count > 0{
                                                
                                                var arrModelUserAllocation = [UserAllocation]()
                                                
                                                for dictUserAllowcation in arrUserAllowcation {
                                                    let objUserAllocationModel = UserAllocation(dictResponse: dictUserAllowcation)
                                                    arrModelUserAllocation.append(objUserAllocationModel)
                                                }
                                                
                                                self.arrUpdateUserAllocation = arrModelUserAllocation
                                                
                                                let objContent = UserAllocationDB(arrUserAllocationDoanloaded:arrModelUserAllocation)
                                                objContent.isFromContentUpdate = true
                                                let isSuccess = objContent.insertUserAllocationData()
                                                print("UserAllocationDB :-",isSuccess)
                                                
                                                
                                               
                                                
                                            }
                                        }
                                        
                                        print(arrOfKeys)
                                    }
                                }
                                
                                if !self.arrUpdateUserAllocation.isEmpty {
                                    self.UpdateUserAllocationData()
                                }
                                else {
                                    self.hideLoader()
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncContentComplete"), object: nil, userInfo: nil)
                                }
                                
                              
                                
                            }else{
                                UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                self.hideLoader()
                            }
                        }else{
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status Array ")
                        self.hideLoader()
                    }
                    
                }else{
                    print("No Any Status ")
                    self.hideLoader()
                }
                
            }else{
                self.hideLoader()
            }
        }) {(error) in
            print(error?.localizedDescription as Any)
            UIViewController().alertControllerExt(strMessage: "\(error?.localizedDescription as Any)")
            self.hideLoader()
        }
        
    }
    //MARK:- CALL CHEPTER CONTENT API / sync call 4
    func callAPIChepterContent(ChepterID:String) {
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "Sync Chapter Content...")
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        
        let DataVersion = UserDefaults.standard.string(forKey: Constant.Old_Version) == "" ? "0" :UserDefaults.standard.string(forKey: Constant.Old_Version)
        
        
        let URL = Constant.GetChepter_Content_API_Call + "\(Constant.MACID)&Id=" + ChepterID + "&DataVersion=\(DataVersion!)"
        
        print("API GET DATA URL IS ====>",URL)
        
        APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if let arrJson = dictResponse{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        
                        print(arrStatus[0])
                        
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success"{
                                
                                for dictTable in arrJson{
                                    let dicTemp = dictTable as! Dictionary<String,Any>
                                    
                                    let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                    
                                    if arrOfKeys.count > 0{
                                        
                                        if arrOfKeys.contains("Content"){
                                            
                                            let arrContent = dicTemp["Content"] as! [Dictionary<String,Any>]
                                            
                                            if arrContent.count > 0{
                                                
                                                var arrModelContent = [Content]()
                                                self.arrContentID.removeAll()
                                                for dictContent in arrContent{
                                                    let objContentModel = Content(dictResponse: dictContent)
                                                    arrModelContent.append(objContentModel)
                                                    
                                                    self.arrContentID.append(objContentModel.contentId)
                                                }
                                                
                                                self.arrContent = arrModelContent
                                                
                                                let objContent = ContentDB(arrContentDoanloaded:arrModelContent)
                                                let isSuccess = objContent.insertContentData()
                                                print("ContentDB :-",isSuccess)
                                            }
                                        }
                                        
                                        print(arrOfKeys)
                                    }
                                }
                                if !self.arrContent.isEmpty {
                                    self.updateDownloadContent()
                                }
                                else {
                                    self.hideLoader()
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncHeaderContent"), object: nil, userInfo: nil)
                                }
                                
                                
                            }else{
                                UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                self.hideLoader()
                            }
                        }else{
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status Array ")
                        self.hideLoader()
                    }
                    
                }else{
                    print("No Any Status ")
                    self.hideLoader()
                }
                
            }else{
                self.hideLoader()
            }
        }) {(error) in
            print(error?.localizedDescription as Any)
            UIViewController().alertControllerExt(strMessage: "\(error?.localizedDescription as Any)")
            self.hideLoader()
        }
    }
    //MARK:- Sync call 5
    func callAPIContentTypeContent(contentTypeID:String,chapterID:String) {
        
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "Sync Chapter Content...")
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        
        let DataVersion = UserDefaults.standard.string(forKey: Constant.Old_Version) == "" ? "0" :UserDefaults.standard.string(forKey: Constant.Old_Version)
        
        let URL = Constant.GetContentType_Content_API_Call + "\(Constant.MACID)&ChapterId=\(chapterID)&ContentTypeId=\(contentTypeID)" + "&DataVersion=\(DataVersion!)"
        
        print("API GET DATA URL IS ====>",URL)
        
        APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if let arrJson = dictResponse{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        
                        print(arrStatus[0])
                        
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success"{
                                
                                for dictTable in arrJson{
                                    let dicTemp = dictTable as! Dictionary<String,Any>
                                    
                                    let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                    
                                    if arrOfKeys.count > 0{
                                        
                                        if arrOfKeys.contains("Content"){
                                            
                                            let arrContent = dicTemp["Content"] as! [Dictionary<String,Any>]
                                            
                                            if arrContent.count > 0{
                                                
                                                var arrModelContent = [Content]()
                                                self.arrContentID.removeAll()
                                                
                                                for dictContent in arrContent{
                                                   print(dictContent["contentdescription"])
                                                    let objContentModel = Content(dictResponse: dictContent)
                                                    arrModelContent.append(objContentModel)
                                                    
                                                    self.arrContentID.append(objContentModel.contentId)
                                                }
                                                
                                                self.arrContent = arrModelContent
                                                
                                                let objContent = ContentDB(arrContentDoanloaded:arrModelContent)
                                                let isSuccess = objContent.insertContentData()
                                                print("ContentDB :-",isSuccess)
                                            }
                                        }
                                        
                                        print(arrOfKeys)
                                    }
                                }
                                if !self.arrContent.isEmpty {
                                    self.updateDownloadContent()
                                }
                                else {
                                    self.hideLoader()
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncHeaderContent"), object: nil, userInfo: nil)
                                }
                                
                            }else{
                                UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                self.hideLoader()
                            }
                        }else{
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status Array ")
                        self.hideLoader()
                    }
                    
                }else{
                    print("No Any Status ")
                    self.hideLoader()
                }
                
            }else{
                self.hideLoader()
            }
        }) {(error) in
            print(error?.localizedDescription as Any)
            UIViewController().alertControllerExt(strMessage: "\(error?.localizedDescription as Any)")
            self.hideLoader()
        }
    }
    
    func updateDownloadContent() {
        print(self.arrContent)
        if !self.arrContent.isEmpty {
            let objContent = self.arrContent[0]
            let contentDB = ContentDB.init()
            let isSuccess = contentDB.updateDownloadContent(contentID:objContent.contentId, contentDescription: objContent.contentDescription)
            
            if isSuccess == true {
                self.arrContent.remove(at: 0)
                if !self.arrContent.isEmpty {
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                        self.updateDownloadContent()
                    })
                }
                else {
                    self.hideLoader()
                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncHeaderContent"), object: nil, userInfo: nil)
                }
            }
            else {
                self.hideLoader()
                print("Not Update Content....")
            }
        }
    }
    
    
    //MARK:- Sync Call 6
    func callMainMediaAPI(subjectID:String) {
        
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "SyncMedia...")
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        let oldVersion = UserDefaults.standard.string(forKey: Constant.Old_Version) == "" ? "0" :UserDefaults.standard.string(forKey: Constant.Old_Version)
        let URL = Constant.Media_MainAPI_Call + "\(Constant.MACID)&SubjectId=\(subjectID)&DataVersion=\(oldVersion!)"
        
        print("API GET DATA URL IS ====>",URL)
        
        APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if let arrJson = dictResponse{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        
                        print(arrStatus[0])
                        
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success"{
                            
                                for dictTable in arrJson{
                                    let dicTemp = dictTable as! Dictionary<String,Any>
                                    
                                    let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                    
                                    if arrOfKeys.count > 0{
                                        
                                        if arrOfKeys.contains("ContentMedia"){
                                            
                                            let arrContentMedia = dicTemp["ContentMedia"] as! [Dictionary<String,Any>]
                                            
                                            if arrContentMedia.count > 0{
                                                var arrModelContentMedia = [ContentMedia]()
                                                
                                                for dictContent in arrContentMedia{
                                                    let objContentMediaModel = ContentMedia(dictResponse: dictContent)
                                                    arrModelContentMedia.append(objContentMediaModel)
                                                }
                                                
                                                self.arrUpdateContentMedia = arrModelContentMedia
                                                
                                                let objContentMedia = ContentMediaDB(arrContentMediaDoanloaded:arrModelContentMedia)
                                                let isSuccess = objContentMedia.insertContentMediaData()
                                                print("ContentMediaDB :-",isSuccess)
                                                
                                                if !self.arrUpdateContentMedia.isEmpty {
                                                    self.updateContentMediaData()
                                                }
                                                
                                            }
                                        }
                                        else if arrOfKeys.contains("Media"){
                                            
                                            let arrMedia = dicTemp["Media"] as! [Dictionary<String,Any>]
                                            
                                            if arrMedia.count > 0{
                                                
                                                var arrModelMedia = [Media]()
                                                let manualMedia = Int32()
                                                
                                                for dictMedia in arrMedia{
                                                    let objMediaModel = Media(dictResponse: dictMedia)
                                                    
                                                    objMediaModel.MediaFlag = "Public"
                                                    
                                                    if let FileSize = dictMedia["filesize"] as? Int32{
                                                        objMediaModel.FileSize = FileSize
                                                    }else{
                                                        objMediaModel.FileSize = 0
                                                    }
                                                    
                                                    if let mediaPath = dictMedia["mediapath"] as? String{
                                                        objMediaModel.mediaPath = mediaPath
                                                    }else{
                                                        objMediaModel.mediaPath = ""
                                                    }
                                                    
                                                    if let mediaPath = dictMedia["mediapath"] as? String{
                                                        objMediaModel.mediaPath = mediaPath
                                                    }else{
                                                        objMediaModel.mediaPath = ""
                                                    }
                                                    objMediaModel.ManualMedia = manualMedia
                                                    
                                                    arrModelMedia.append(objMediaModel)
                                                    
                                                }
                                                
                                                self.arrUpdateMedia = arrModelMedia
                                                
                                                let objMedia = MediaDB(arrMediaDoanloaded: arrModelMedia)
                                                
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                                    let isSuccess = objMedia.insertMediaData()
                                                    print("MediaDB :-",isSuccess)
                                                })
                                                
                                                if !self.arrUpdateMedia.isEmpty {
                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                        self.updateMediaDataServiceCall6()
                                                    })
                                                    
                                                }
                                               /* DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                                    if !self.arrUpdateMedia.isEmpty {
                                                        self.updateMediaDataServiceCall6()
                                                    }
                                                })*/
                                                
                                            }
                                        }
                                        
                                        print(arrOfKeys)
                                    }
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                                    self.hideLoader()
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncMediaComplete"), object: nil, userInfo: nil)
                                })
                                /*if !self.arrUpdateContentMedia.isEmpty {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                                    self.updateContentMediaData()
                                    })
                                }
                                else {
                                    //if Data dictionaryfound nil.....
                                    self.hideLoader()
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncMediaComplete"), object: nil, userInfo: nil)
                                }*/
                               
                                
                            }else{
                                UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                self.hideLoader()
                            }
                        }else{
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status Array ")
                        self.hideLoader()
                    }
                    
                }else{
                    print("No Any Status ")
                    self.hideLoader()
                }
                
            }else{
                self.hideLoader()
            }
        }) {(error) in
            print(error?.localizedDescription as Any)
            UIViewController().alertControllerExt(strMessage: "\(error?.localizedDescription as Any)")
            self.hideLoader()
        }
    }
    
}
//MARK:- Extension for Update table
extension UpdateData {
    //Update Version Table
    func updateVersionData() {
       print(self.arrUpdateVersion.count)
        let db = VersionDB.init()
        let isUpdate = db.updateVersionData(Version: self.arrUpdateVersion[0])
        print("isUpdateVersion :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateVersion.remove(at: 0)
            if !self.self.arrUpdateVersion.isEmpty {
                self.updateVersionData()
            }
        }
    }
   
//Update Board Table
   func updateBoardData() {
       print(self.arrUpdateBoard.count)
        let db = BoardDB.init()
        let isUpdate = db.updateBoardData(Board: self.arrUpdateBoard[0])
        print("isUpdateBoard :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateBoard.remove(at: 0)
            if !self.self.arrUpdateBoard.isEmpty {
                self.updateBoardData()
            }
        }
    }
    
  //Update Standard Table
   func updateStandardData() {
       print(self.arrUpdateStandard.count)
        let db = StandardDB.init()
        let isUpdate = db.updateStandardData(Standard: self.arrUpdateStandard[0])
        print("isUpdateStandard :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateStandard.remove(at: 0)
            if !self.self.arrUpdateStandard.isEmpty {
                self.updateStandardData()
            }
        }
    }
    
   //Update Mediatype Table
   func updateMediaTypeData() {
       print(self.arrUpdateMediaType.count)
        let db = MediaTypeDB.init()
        let isUpdate = db.updateMediaTypeData(MediaType: self.arrUpdateMediaType[0])
        print("isUpdateMediaType :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateMediaType.remove(at: 0)
            if !self.self.arrUpdateMediaType.isEmpty {
                self.updateMediaTypeData()
            }
        }
    }
    
    //Update Media Table
    func updateMediaData() {
       print(self.arrUpdateMedia.count)
        let db = MediaDB.init()
        let isUpdate = db.updateMediaData(Media: self.arrUpdateMedia[0])
        print("isUpdateMedia :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateMedia.remove(at: 0)
            if !self.self.arrUpdateMedia.isEmpty {
                self.updateMediaData()
            }
        }
    }
    
    //Update Subject Data
    func updateSubjectData() {
       print(self.arrUpdateSubject.count)
        let db = SubjectDB.init()
        let isUpdate = db.updateSubjectData(Subject: self.arrUpdateSubject[0])
        print("isUpdateSubject :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateSubject.remove(at: 0)
            if !self.self.arrUpdateSubject.isEmpty {
                self.updateSubjectData()
            }
        }
    }
    
    //Update ContentType Data
    
    func updateContentTypeData() {
        print(self.arrUpdateContentType.count)
        let db = ContentTypeDB.init()
        let isUpdate = db.updateContentTypeData(ContentType: self.arrUpdateContentType[0])
        print("isUpdateContentType :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateContentType.remove(at: 0)
            if !self.self.arrUpdateContentType.isEmpty {
                self.updateContentTypeData()
            }
        }
    }
   //update Chapter Data
   func updateChapterData() {
    print(self.arrUpdateChapter.count)
        let db = ChapterDB.init()
        let isUpdate = db.updateChapterData(Chapter: self.arrUpdateChapter[0])
        print("isUpdateChapter :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateChapter.remove(at: 0)
            if !self.self.arrUpdateChapter.isEmpty {
                self.updateChapterData()
            }
        }
    }
   //Update Topic Data
    func updateTopicData() {
        print(self.arrUpdateTopic.count)
        let db = TopicDB.init()
        let isUpdate = db.updateTopicData(Topic: self.arrUpdateTopic[0])
        print("isUpdateTopic :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateTopic.remove(at: 0)
            if !self.self.arrUpdateTopic.isEmpty {
                self.updateTopicData()
            }
        }
    }
    
    //Update Content Data
    func updateContentData() {
        print(self.arrUpdateContent.count)
        let db = ContentDB.init()
        let isUpdate = db.updateContentData1(content: self.arrUpdateContent[0])
        print("isUpdateContent :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateContent.remove(at: 0)
            if !self.self.arrUpdateContent.isEmpty {
                self.updateContentData()
            }
        }
    }
    
    //Update User Allocation
    func UpdateUserAllocationData() {
        print(self.arrUpdateUserAllocation.count)
        let db = UserAllocationDB.init()
        let isUpdate = db.updateUserAllocationData(UserAllocation: self.arrUpdateUserAllocation[0])
        print("isUpdateUserAllocation :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateUserAllocation.remove(at: 0)
            if !self.self.arrUpdateUserAllocation.isEmpty {
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self.UpdateUserAllocationData()
                })
            }
            else {
                self.hideLoader()
                NotificationCenter.default.post(name: NSNotification.Name.init("SyncContentComplete"), object: nil, userInfo: nil)
            }
        }
        else {
            self.hideLoader()
            NotificationCenter.default.post(name: NSNotification.Name.init("SyncContentComplete"), object: nil, userInfo: nil)
        }
    }
    
    //-------------------- Service Call 6 -------------------------
    
   //Update ContentMedia Data
    func updateContentMediaData() {
        print(self.arrUpdateContentMedia.count)
        let db = ContentMediaDB.init()
        let isUpdate = db.updateContentMediaData(ContentMedia: self.arrUpdateContentMedia[0])
        print("isUpdateContentMedia :-",isUpdate)
        if isUpdate == true {
              self.arrUpdateContentMedia.remove(at: 0)
            if !self.self.arrUpdateContentMedia.isEmpty {
                DispatchQueue.main.asyncAfter(deadline: .now() , execute: {
                    self.updateContentMediaData()
                })
                
            }
            /*else {
                
                if !self.arrUpdateMedia.isEmpty {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        self.updateMediaDataServiceCall6()
                    })
                    
                }
                else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        self.hideLoader()
                        NotificationCenter.default.post(name: NSNotification.Name.init("SyncMediaComplete"), object: nil, userInfo: nil)
                    })
                }
            }*/
        }
        /*else {
            if !self.arrUpdateMedia.isEmpty {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.updateMediaDataServiceCall6()
                })
                
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.hideLoader()
                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncMediaComplete"), object: nil, userInfo: nil)
                })
            }
        }*/
    }
    
    //Update Media Table
    func updateMediaDataServiceCall6() {
       print(self.arrUpdateMedia.count)
        let db = MediaDB.init()
        let isUpdate = db.updateMediaData(Media: self.arrUpdateMedia[0])
        print("isUpdateMedia :-",isUpdate)
        if isUpdate == true {
            self.arrUpdateMedia.remove(at: 0)
            if !self.self.arrUpdateMedia.isEmpty {
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self.updateMediaDataServiceCall6()
                })
            }
            /*else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.hideLoader()
                    NotificationCenter.default.post(name: NSNotification.Name.init("SyncMediaComplete"), object: nil, userInfo: nil)
                })
            }*/
        }
        /*else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.hideLoader()
                NotificationCenter.default.post(name: NSNotification.Name.init("SyncMediaComplete"), object: nil, userInfo: nil)
            })
        }*/
    }
    
}
