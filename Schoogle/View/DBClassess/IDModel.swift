//
//  IDModel.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit

class IDModel: NSObject {
    var id : Int32 = 0
    
    override init() {
        
    }
    
    init( id : Int32?){
        if let id = id{
            self.id = id
        }else{
            self.id = 0
        }
        
    }
}
