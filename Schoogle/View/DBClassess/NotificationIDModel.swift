//  NotificationIDModel.swift
//  Schoogle
//  Created by sprybitmac on 30/07/19.
//  Copyright © 2019 Malhar. All rights reserved.

import UIKit

class NotificationIDModel: NSObject {

    var id : Int32 = 0
    var status:Int32 = 0
    var worktype:Int32 = 0
    var apnID:Int32 = 0
    var createdDate:String = ""
    
    override init() {
    }
    
     init(id : Int32?,status:Int32?,worktype:Int32?,apndID:Int32?,createdDate:String?){
        
        if let id = id{
            self.id = id
        }else{
            self.id = 0
        }
        
        if let Status = status{
            self.status = Status
        }else{
            self.status = 0
        }
        if let worktype = worktype{
            self.worktype = worktype
        }else{
            self.worktype = 0
        }
        if let apnId = apndID {
            self.apnID = apnId
        }else{
            self.apnID = 0
        }
        if let createdate = createdDate{
            self.createdDate = createdate
        }else{
            self.createdDate = ""
        }
        
    }
    
    
}
