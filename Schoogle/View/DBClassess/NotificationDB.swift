//  NotificationDB.swift
//  Schoogle
//  Created by sprybitmac on 30/07/19.
//  Copyright © 2019 Malhar. All rights reserved.

import UIKit
import SQLite

class NotificationDB: NSObject {

    var sqlitemanager = SQLITEManager.shareInstanceSQLITEManager
    var arrnotification = NSArray()
   
    //INIT METHOD
    override init() {
        super.init()
    }

    //INIT WITH DATABASE METHOD
    init(dbName:String){
        super.init()
        sqlitemanager.pathToDatabase = sqlitemanager.getApplicationNotificationDatabasePath()
        sqlitemanager.db = sqlitemanager.openDatabase()
    }
    
    //MARK: UPDATE PENDING NOTIFICATION
    func updatePendingNotificationStatus(mediaID:Int) -> Bool{
       
        var isSuccess = false
        let isUpdate = self.sqlitemanager.updateTable(query:makeUpdateQuery(mediaZipID:mediaID, status:Constant.NotificationZipInsert))
                
                if isUpdate {
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
        sqlitemanager.closeDB()
        return isSuccess

    }
    

    //UPDATE NOTIFICATION STATUS IN TABLE
    func updateNotificationStatus(arrnotification:[NotificationIDModel]) -> Bool{
        var isSuccess = false

        //  let currentdate = self.setCurrentDate()
        if arrnotification.count > 0 {
            for zipID in arrnotification{

                let zipID = Int(zipID.id)
                
                let isUpdate = self.sqlitemanager.updateTable(query:makeUpdateQuery(mediaZipID:zipID, status:Constant.NotificationZipInsert))
                
                if isUpdate {
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
            }

        }else{
            print("No Any Update")
            isSuccess = true
        }
        sqlitemanager.closeDB()
        return isSuccess

    }
    
    //MARK : FOR CHECK NOTUPDATED NOTIFICATION ON 10 Aug
    func updateNotificationStatusFromStandard(arrnotification:[NotificationIDModel]) -> Bool{
        var isSuccess = false
        
        //  let currentdate = self.setCurrentDate()
        if arrnotification.count > 0 {
            for zipID in arrnotification{
                
                let zipID = Int(zipID.id)
                let isUpdate = self.sqlitemanager.updateTable(query:makeUpdateQueryFromStandard(mediaZipID:zipID, status:Constant.isNotInsert))
                
                if isUpdate {
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
            }
            
        }else{
            print("No Any Update")
            isSuccess = true
        }
        print("Version Done")
        sqlitemanager.closeDB()
        return isSuccess
    }
    
    //MARK: INSERT DATA FROM NOTIFICATIONS
    func insertNotificationDataFromAppDelegate() -> Bool{
        var isSuccess = false
        
          let currentdate = self.setCurrentDate()
        if Constant.APPDELEGATE!.arrNotification.count > 0 {
            for zipID in Constant.APPDELEGATE!.arrNotification{
                
                let isInsert = self.sqlitemanager.insertTable(query: self.makeInsertQuery(mediaZipID: zipID as! Int,status:Constant.NotificationZipInsert,worktype:Constant.APPDELEGATE?.WorkType ?? 0,apnid: Constant.APPDELEGATE?.apnID ?? 0,createddata: currentdate))
                
                if isInsert {
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
            }
            
        }else{
            print("No Any Update")
            isSuccess = true
        }
        sqlitemanager.closeDB()
        return isSuccess
    }
    
    //Insert Query
//    func makeInsertQuery(mediaZipID : Int,status:Int,currentdate:String) -> String {
//        return "INSERT INTO NotificationTable (mediaZipID,status,createdDate) VALUES (\(mediaZipID),\(status),'\(currentdate)');"
//    }
    
    func makeInsertQuery(mediaZipID : Int,status:Int,worktype:Int,apnid:Int,createddata:String) -> String {
        return "INSERT INTO NotificationTable (mediaZipID,status,workType,apnID,createdDate) VALUES (\(mediaZipID),\(status),\(worktype),\(apnid),'\(createddata)');"
    }

    func makeUpdateQuery(mediaZipID : Int,status:Int) -> String{
        return "UPDATE NotificationTable SET status = \(Constant.NotificationZipInsert) where mediaZipID = \(mediaZipID)"
    }

    func makeUpdateQueryFromStandard(mediaZipID : Int,status:Int) -> String{
        return "UPDATE NotificationTable SET status = \(Constant.isNotInsert) where mediaZipID = \(mediaZipID)"
    }
    
    
    //Make update query
//    func makeUpdateQuery(Media : Media) -> String {
//
//        return "UPDATE MEDIA SET MediaTypeId = \(Media.MediaTypeId) , MediaFile = '\(Media.MediaFile)' , ManualMedia = \(Media.ManualMedia) ,Version = \(Media.Version) ,IsImageInFolderYN = \(Media.IsImageInFolderYN) ,FileSize = \(Media.FileSize) ,IsActive = \(Media.IsActive) ,CreatedDate = '\(Media.CreatedDate)' ,UpdatedDate = '\(Media.UpdatedDate)' ,CreatedUserId = \(Media.CreatedUserId) ,UpdatedUserId = \(Media.UpdatedUserId) ,IsDeleted = \(Media.IsDeleted) ,DeletedUserId = \(Media.DeletedUserId) ,DeletedDate = '\(Media.DeletedDate)' ,MediaFlag = '\(Media.MediaFile)' where MediaId = \(Media.MediaId)"
//
//    }
    

    func setCurrentDate() -> String{
        let date = Date()
        return date.string(with: "EEEE dd.MMM.yyyy")
    }
    
//    func getNotificationIDsFromDatabase() -> [NotificationIDModel]{
//        
//        var arrNotification = [NotificationIDModel]()
//        
//        if sqlitemanager.openDatabase() != nil{
//            sqlitemanager.selectTable(query: "select * from Notification", success: { (queryStatement) in
//                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
//                    
//                    let id = sqlite3_column_int(queryStatement, 0)
//                    
//                    let objIDModel = NotificationIDModel(id: id)
//                    
//                    arrNotification.append(objIDModel)
//                }
//                
//            }){ (error) in
//                print(error as Any)
//            }
//        }else{
//            print("Somethinf went wrong")
//        }
//        return arrNotification
//    }

    
}
