//
//  StandardDB.swift
//  Schoogle
//
//  Created by Malhar on 11/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class StandardDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrStandardDownloaded = [Standard]()
    var arrStandard = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    //Init with database method when Download database and copyData
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrStandardDownloaded = self.setDataIntoStandardModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrStandard = self.getOnlyIDsFromDatabase()
        
    }
    
    
    //Init with database method when Call Api and copyData
    init(arrStandardDoanloaded : [Standard]) {
        super.init()
        
        arrStandardDownloaded = arrStandardDoanloaded
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrStandard = self.getOnlyIDsFromDatabase()
        
    }
    
    //Insert data
    func insertStandardData() -> Bool{
        var isSuccess = false
        
        if arrStandard.count > 0 {
            let arrOfNewStandard = self.compareStandard(arrStandardDownload: arrStandardDownloaded, arrStandardApplication: arrStandard)
            
            if arrOfNewStandard.count > 0 {
                isSuccess = self.insertStandard(arrNewStandard: arrOfNewStandard)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }else{
            
            if arrStandardDownloaded.count > 0 {
                isSuccess = self.insertStandard(arrNewStandard: arrStandardDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }
        
        //sqliteManager.closeDB()
        return isSuccess
    }
    
    //Delete Query
    func makeDeleteStandardQuery(standardId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"delete from Standard where StandardId = \(standardId)")
        }
        return issuccess
    }

    //Download data and insert into model
    func setDataIntoStandardModel() -> [Standard] {
        
        var arrStandardTemp = [Standard]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Standard", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let StandardId = sqlite3_column_int(queryStatement, 0)
                    let BoardId = sqlite3_column_int(queryStatement, 1)
                    let StandardName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let StandardAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let StandardImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let IsActive = sqlite3_column_int(queryStatement, 9)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                    let IsDeleted = sqlite3_column_int(queryStatement, 14)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    let VersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 17))
                    let IsManual = sqlite3_column_int(queryStatement, 18)

                    let objStandardModel = Standard(StandardId: StandardId, BoardId: BoardId, StandardName: StandardName, StandardAlias: StandardAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, StandardImage: StandardImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, VersionName: VersionName, IsManual: IsManual)
                    
                    arrStandardTemp.append(objStandardModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrStandardTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrStandardTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Standard", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrStandardTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrStandardTemp
    }
    
    // Check Standard is exist or not
    func compareStandard(arrStandardDownload : [Standard] , arrStandardApplication : [IDModel]) -> [Standard]{
        
        var arrNewStandard = [Standard]()
        if arrStandardApplication.count > 0{
            for StandardObj in arrStandardDownload{
                let filterObj = arrStandardApplication.filter({ $0.id == StandardObj.StandardId })
                if filterObj.count == 0{
                    arrNewStandard.append(StandardObj)
                }
            }
        }else{
            arrNewStandard.append(contentsOf: arrStandardDownload)
        }
        return arrNewStandard
    }
    
    //Insert Standard into database
    func insertStandard(arrNewStandard : [Standard]) -> Bool{
        var isSuccess = false
        for newStandard in arrNewStandard {
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Standard: newStandard))
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
                break
            }
        }
        arrStandard.removeAll()
        arrStandardDownloaded.removeAll()
        print("Standard Done")
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(Standard : Standard) -> String {
        
        return "INSERT INTO STANDARD (StandardId,BoardId,StandardName,StandardAlias,ShortDescription,LongDescription,ListOrder,StandardImage,Remarks,IsActive,CreatedDate,UpdatedDate,CreatedUserId,UpdatedUserId,IsDeleted,DeletedUserId,DeletedDate,VersionName,IsManual) VALUES (\(Standard.StandardId),\(Standard.BoardId),'\(Standard.StandardName.replacingOccurrences(of: "\'", with: "\'\'"))' ,'\(Standard.StandardAlias.replacingOccurrences(of: "\'", with: "\'\'"))' ,'\(Standard.ShortDescription)' ,'\(Standard.LongDescription)' ,\(Standard.ListOrder),'\(Standard.StandardImage.replacingOccurrences(of: "\'", with: "\'\'"))' ,'\(Standard.Remarks)' ,\(Standard.IsActive),'\(Standard.CreatedDate)' ,'\(Standard.UpdatedDate)' ,\(Standard.CreatedUserId),\(Standard.UpdatedUserId),\(Standard.IsDeleted),\(Standard.DeletedUserId),'\(Standard.DeletedDate)' ,'\(Standard.VersionName)' ,\(Standard.IsManual));"
        
    }
    
    
    //UPDATE -----------------------------------
    func searchStandardData(StandardId : Int32) -> Standard{
        
        var objStandard = Standard()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Standard where StandardId = \(StandardId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW)    {
                    
                let StandardId = sqlite3_column_int(queryStatement, 0)
                let BoardId = sqlite3_column_int(queryStatement, 1)
                let StandardName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                let StandardAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                let ListOrder = sqlite3_column_int(queryStatement, 6)
                let StandardImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                let IsActive = sqlite3_column_int(queryStatement, 9)
                let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                let IsDeleted = sqlite3_column_int(queryStatement, 14)
                let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                let VersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 17))
                let IsManual = sqlite3_column_int(queryStatement, 18)
                
                objStandard = Standard(StandardId: StandardId, BoardId: BoardId, StandardName: StandardName, StandardAlias: StandardAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, StandardImage: StandardImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, VersionName: VersionName, IsManual: IsManual)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objStandard
        
    }
    
    //Perform Update Operation
    func updateStandardData(Standard : Standard) -> Bool{
        
        var isSuccess = false
        //Seach Standard
        let objStandard = searchStandardData(StandardId: Standard.StandardId)
        
        if Standard.StandardId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Standard: Standard))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Standard: Standard))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery(Standard : Standard) -> String {

        return "UPDATE Standard SET BoardId = '\(Standard.BoardId)' , StandardName = '\(Standard.StandardName.replacingOccurrences(of: "\'", with: "\'\'"))' , StandardAlias = '\(Standard.StandardAlias.replacingOccurrences(of: "\'", with: "\'\'"))' , ShortDescription = '\(Standard.ShortDescription.replacingOccurrences(of: "\'", with: "\'\'"))' , LongDescription = '\(Standard.LongDescription.replacingOccurrences(of: "\'", with: "\'\'"))' , ListOrder = '\(Standard.ListOrder)' , StandardImage = '\(Standard.StandardImage.replacingOccurrences(of: "\'", with: "\'\'"))' , Remarks = '\(Standard.Remarks.replacingOccurrences(of: "\'", with: "\'\'"))' , IsActive = '\(Standard.IsActive)' , CreatedDate = '\(Standard.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , UpdatedDate = '\(Standard.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , CreatedUserId = '\(Standard.CreatedUserId)' , UpdatedUserId = '\(Standard.UpdatedUserId)' , IsDeleted = '\(Standard.IsDeleted)' , DeletedUserId = '\(Standard.DeletedUserId)' , DeletedDate = '\(Standard.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , VersionName = '\(Standard.VersionName.replacingOccurrences(of: "\'", with: "\'\'"))' , IsManual = '\(Standard.IsManual)' where StandardId = '\(Standard.StandardId)'"
        
    }
    
    func getStandardDataFromId(standardId : Int32) -> Standard{
        
        let Query_Get_Standard_ID = "SELECT * FROM STANDARD WHERE IsDeleted = 0 AND IsActive = 1 AND StandardId = \(standardId) ORDER BY ListOrder, StandardId"
        
        var objStandard = Standard()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Standard_ID, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let StandardId = sqlite3_column_int(queryStatement, 0)
                    let BoardId = sqlite3_column_int(queryStatement, 1)
                    let StandardName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let StandardAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let StandardImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let IsActive = sqlite3_column_int(queryStatement, 9)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                    let IsDeleted = sqlite3_column_int(queryStatement, 14)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    let VersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 17))
                    let IsManual = sqlite3_column_int(queryStatement, 18)
                    
                    objStandard = Standard(StandardId: StandardId, BoardId: BoardId, StandardName: StandardName, StandardAlias: StandardAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, StandardImage: StandardImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, VersionName: VersionName, IsManual: IsManual)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objStandard
    }
    
    
    func getAllStandardData() -> [Standard]{
        //----Query1: to get standard
        //"Select * From Standard Where StandardId In (Select Distinct StandardId From UserAllocation Where IsDeleted = 0)"
        let Query_Get_Standard_ID = "Select * From Standard"
        
        var arrStandard = [Standard]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Standard_ID, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let StandardId = sqlite3_column_int(queryStatement, 0)
                    let BoardId = sqlite3_column_int(queryStatement, 1)
                    let StandardName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let StandardAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let StandardImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let IsActive = sqlite3_column_int(queryStatement, 9)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                    let IsDeleted = sqlite3_column_int(queryStatement, 14)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    let VersionName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 17))
                    let IsManual = sqlite3_column_int(queryStatement, 18)
                    
                  let  objStandard = Standard(StandardId: StandardId, BoardId: BoardId, StandardName: StandardName, StandardAlias: StandardAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, StandardImage: StandardImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, VersionName: VersionName, IsManual: IsManual)
                    
                    arrStandard.append(objStandard)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return arrStandard
    }
    
}
