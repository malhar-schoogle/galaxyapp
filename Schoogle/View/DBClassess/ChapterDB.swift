//  ChapterDB.swift
//  Schoogle
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit
import SQLite
class ChapterDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrChapterDownloaded = [Chapter]()
    var arrChapter = [IDModel]()
    var arrChapterCount = [Int]()
    var arrTemp = [Chapter]()
    var subjectCount = 0
    var tempDictionary = [String: [Chapter]]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    
    //Init with database method
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrChapterDownloaded = self.setDataIntoChapterModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrChapter = self.getOnlyIDsFromDatabase()
        
    }
    //Init with database method when Call Api and copyData
       init(arrChapterDoanloaded : [Chapter]) {
           super.init()
           
           self.arrChapterDownloaded = arrChapterDoanloaded
           sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
           sqliteManager.db = sqliteManager.openDatabase()
           self.arrChapter = self.getOnlyIDsFromDatabase()
           
       }
    
    //Insert data
    func insertChapterData() -> Bool{
        var isSuccess = false
        
        if arrChapter.count > 0 {
            let arrOfNewChapter = self.compareChapter(arrChapterDownload: arrChapterDownloaded, arrChapterApplication: arrChapter)
            
            if arrOfNewChapter.count > 0 {
                isSuccess = self.insertChapter(arrNewChapter: arrOfNewChapter)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }else{
            if arrChapterDownloaded.count > 0 {
                isSuccess = self.insertChapter(arrNewChapter: arrChapterDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }
        
        arrChapter.removeAll()
        arrChapterDownloaded.removeAll()
        print("Chapter Done")
        sqliteManager.closeDB()
        return isSuccess
    }

    //Delete Query
    func makeDeleteChapterQuery(chapterId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"delete from Chapter where ChapterId = \(chapterId)")
        }
        return issuccess
    }

    //Download data and insert into model
    func setDataIntoChapterModel() -> [Chapter] {
        
        var arrChapterTemp = [Chapter]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Chapter", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ChapterId = sqlite3_column_int(queryStatement, 0)
                    let SubjectId = sqlite3_column_int(queryStatement, 1)
                    let ChapterName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ChapterAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Importance = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let HasPreREading = sqlite3_column_int(queryStatement, 7)
                    let ListOrder = sqlite3_column_int(queryStatement, 8)
                    let ChapterImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    
                    let objChapterModel = Chapter(ChapterId: ChapterId, SubjectId: SubjectId, ChapterName: ChapterName, ChapterAlias: ChapterAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, Importance: Importance, HasPreREading: HasPreREading, ListOrder: ListOrder, ChapterImage: ChapterImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                    arrChapterTemp.append(objChapterModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrChapterTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrMediaTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Chapter", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrMediaTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        return arrMediaTemp
    }
    
    // Check Chapter is exist or not
    func compareChapter(arrChapterDownload : [Chapter] , arrChapterApplication : [IDModel]) -> [Chapter]{
        
        var arrNewChapter = [Chapter]()
        if arrChapterApplication.count > 0{
            for ChapterObj in arrChapterDownload{
                let filterObj = arrChapterApplication.filter({ $0.id == ChapterObj.ChapterId })
                if filterObj.count == 0{
                    arrNewChapter.append(ChapterObj)
                }
            }
        }else{
            arrNewChapter.append(contentsOf: arrChapterDownload)
        }
        return arrNewChapter
    }
    
    //Insert Chapter into database
    func insertChapter(arrNewChapter : [Chapter]) -> Bool{
        var isSuccess = false
        for newChapter in arrNewChapter {

            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Chapter: newChapter))
            if isInsert {
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
                break
            }
        }
        return isSuccess
    }
    
    //MARK: GET SUBJECT ID FROM CHAPERTABLE FOR PENDING NOTIFICATIONS
    func searchSubjectFromChapterData(subjectID : Int32,subjectName:String) -> Chapter{
        
        var objChapter = Chapter()
        var totalCount = 0

        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        
        if sqliteManager.openDatabase() != nil{

         sqliteManager.selectTable(query:"Select Ch.* FROM Chapter Ch Inner Join Topic T On Ch.ChapterId = T.ChapterId Inner Join Content C On C.TopicId = T.TopicId Inner Join UserAllocation U On U.ContentId = C.ContentId Where U.SubjectId = \(subjectID) And U.IsDeleted = 0 Group By Ch.ChapterId", success: { (queryStatement) in
            
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    totalCount += 1
                    let ChapterId = sqlite3_column_int(queryStatement, 0)
                    let SubjectId = sqlite3_column_int(queryStatement, 1)
                    let ChapterName = self.sqliteManager.handleStringValuewithBLM(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ChapterAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Importance = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let HasPreREading = sqlite3_column_int(queryStatement, 7)
                    let ListOrder = sqlite3_column_int(queryStatement, 8)
                    let ChapterImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    
                    objChapter = Chapter(ChapterId: ChapterId, SubjectId: SubjectId, ChapterName: ChapterName, ChapterAlias: ChapterAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, Importance: Importance, HasPreREading: HasPreREading, ListOrder: ListOrder, ChapterImage: ChapterImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                   
                    self.arrTemp.append(objChapter)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong...")
        }
        
        tempDictionary["subject\(subjectCount)"] = self.arrTemp
        subjectCount += 1
         
        self.arrTemp.removeAll()
        self.arrChapterCount.append(totalCount)
        return objChapter

    }
    
    
    
    
    
    //Delete Chapter Query From Standard
    func makeDeleteChapterFromStandard(StandardId:Int) -> Bool{

        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Chapter Where SubjectId In(Select SubjectId From Subject Where StandardId = \(StandardId))")
        }
        return issuccess
    }
    
    //Delete Chapter Query From Standard
    func makeDeleteChapterFromSubject(SubjectId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Chapter Where SubjectId = \(SubjectId)")
        }
        return issuccess
    }
    

    //Insert Query
    func makeInsertQuery(Chapter : Chapter) -> String {
        
        return "INSERT INTO Chapter (ChapterId, SubjectId, ChapterName, ChapterAlias, ShortDescription, LongDescription, Importance, HasPreREading, ListOrder, ChapterImage, Remarks, IsActive, CreatedDate, UpdatedDate, CreatedUserId, UpdatedUserId, IsDeleted, DeletedUserId, DeletedDate) VALUES (\(Chapter.ChapterId),\(Chapter.SubjectId),'\(Chapter.ChapterName.replacingOccurrences(of: "\'", with: "\'\'"))','\(Chapter.ChapterAlias)','\(Chapter.ShortDescription)','\(Chapter.LongDescription)','\(Chapter.Importance)',\(Chapter.HasPreREading),\(Chapter.ListOrder),'\(Chapter.ChapterImage)','\(Chapter.Remarks)',\(Chapter.IsActive),'\(Chapter.CreatedDate)','\(Chapter.UpdatedDate)',\(Chapter.CreatedUserId),\(Chapter.UpdatedUserId),\(Chapter.IsDeleted),\(Chapter.DeletedUserId),'\(Chapter.DeletedDate)');"
        
    }
    
    //UPDATE -----------------------------------
    func searchChapterData(ChapterId : Int32) -> Chapter{
        
        var objChapter = Chapter()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Chapter where ChapterId = \(ChapterId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ChapterId = sqlite3_column_int(queryStatement, 0)
                    let SubjectId = sqlite3_column_int(queryStatement, 1)
                    let ChapterName = self.sqliteManager.handleStringValuewithBLM(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ChapterAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Importance = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let HasPreREading = sqlite3_column_int(queryStatement, 7)
                    let ListOrder = sqlite3_column_int(queryStatement, 8)
                    let ChapterImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    
                    objChapter = Chapter(ChapterId: ChapterId, SubjectId: SubjectId, ChapterName: ChapterName, ChapterAlias: ChapterAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, Importance: Importance, HasPreREading: HasPreREading, ListOrder: ListOrder, ChapterImage: ChapterImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objChapter
        
    }
    
    //Perform Update Operation
    func updateChapterData(Chapter : Chapter) -> Bool{
        
        var isSuccess = false
        //Seach Chapter
        let objChapter = searchChapterData(ChapterId: Chapter.ChapterId)
        
        if Chapter.ChapterId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Chapter: Chapter))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Chapter: Chapter))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    
    //Make update query
    func makeUpdateQuery(Chapter : Chapter) -> String {
        
        return "UPDATE CHAPTER SET SubjectId = '\(Chapter.SubjectId)' ,  ChapterName = '\(Chapter.ChapterName.replacingOccurrences(of: "\'", with: "\'\'"))' ,  ChapterAlias = '\(Chapter.ChapterAlias.replacingOccurrences(of: "\'", with: "\'\'"))' ,  ShortDescription = '\(Chapter.ShortDescription.replacingOccurrences(of: "\'", with: "\'\'"))' ,  LongDescription = '\(Chapter.LongDescription.replacingOccurrences(of: "\'", with: "\'\'"))' ,  Importance = '\(Chapter.Importance.replacingOccurrences(of: "\'", with: "\'\'"))' ,  HasPreREading = '\(Chapter.HasPreREading)' ,  ListOrder = '\(Chapter.ListOrder)' ,  ChapterImage = '\(Chapter.ChapterImage.replacingOccurrences(of: "\'", with: "\'\'"))' ,  Remarks = '\(Chapter.Remarks.replacingOccurrences(of: "\'", with: "\'\'"))' ,  IsActive = '\(Chapter.IsActive)' ,  CreatedDate = '\(Chapter.CreatedDate)' ,  UpdatedDate = '\(Chapter.UpdatedDate)' ,  CreatedUserId = '\(Chapter.CreatedUserId)' ,  UpdatedUserId = '\(Chapter.UpdatedUserId)' ,  IsDeleted = '\(Chapter.IsDeleted)' ,  DeletedUserId = '\(Chapter.DeletedUserId)' ,  DeletedDate = '\(Chapter.DeletedDate)' where ChapterId = '\(Chapter.ChapterId)'"
        
    }

    func getChapterDataFromId(chapterId : Int32) -> Chapter{
        var objChapter = Chapter()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "SELECT * FROM CHAPTER WHERE IsDeleted = 0 AND IsActive = 1 AND ChapterId = \(chapterId) ORDER BY ListOrder, ChapterId", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ChapterId = sqlite3_column_int(queryStatement, 0)
                    let SubjectId = sqlite3_column_int(queryStatement, 1)
                    let ChapterName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ChapterAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Importance = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let HasPreREading = sqlite3_column_int(queryStatement, 7)
                    let ListOrder = sqlite3_column_int(queryStatement, 8)
                    let ChapterImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    
                    objChapter = Chapter(ChapterId: ChapterId, SubjectId: SubjectId, ChapterName: ChapterName, ChapterAlias: ChapterAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, Importance: Importance, HasPreREading: HasPreREading, ListOrder: ListOrder, ChapterImage: ChapterImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objChapter
        
    }
    
    //Download data and insert into model
    func getChapterDataFromSubjectID(query:String) -> [Chapter] {
        
        var arrChapterTemp = [Chapter]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query:query ,success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ChapterId = sqlite3_column_int(queryStatement, 0)
                    let SubjectId = sqlite3_column_int(queryStatement, 1)
                    let ChapterName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ChapterAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Importance = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let HasPreREading = sqlite3_column_int(queryStatement, 7)
                    let ListOrder = sqlite3_column_int(queryStatement, 8)
                    let ChapterImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    
                    let objChapterModel = Chapter(ChapterId: ChapterId, SubjectId: SubjectId, ChapterName: ChapterName, ChapterAlias: ChapterAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, Importance: Importance, HasPreREading: HasPreREading, ListOrder: ListOrder, ChapterImage: ChapterImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                    arrChapterTemp.append(objChapterModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrChapterTemp
        
    }
    
}
