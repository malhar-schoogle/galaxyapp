//
//  SyncAssessmentData.swift
//  Schoogle
//
//  Created by Emxcel on 27/07/20.
//  Copyright © 2020 Malhar. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import MBProgressHUD

class SyncAssessmentData: NSObject {
    
    //Variables
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var mbHUD = MBProgressHUD()
    var arrAssessmentID = [Int32]()
    var arrAssessmentResult = [Int32]()
    
    var arrAssessment = [Assessment]()
    
    //-------------- Update Assessment Array -------------
    var arrAssessment1 = [Assessment]()
    var arrQuestionPattern = [QuestionPattern]()
    var arrMediaType = [MediaType]()
    var arrAssessmentType = [AssessmentType]()
    var arrAssessmentTime = [AssessmentTime]()
    var arrAssessmentQuestion = [AssessmentQuestion]()
    var arrAssessmentQuestionDetails = [AssessmentQuestionDetails]()
    var arrQuestion = [Question]()
    var arrQuestionOptions = [QuestionOptions]()
    var arrQuestionMedia = [QuestionMedia]()
    var arrQuestionOptionsMedia = [QuestionOptionsMedia]()
    var arrMedia = [Media]()
    var arrAssessmentScore = [AssessmentScore]()
    var arrAssessmentResult1 = [AssessmentResult]()
    
    //Initialize class
    override init() {
        
        if let macID = UserDefaults.standard.value(forKey: Constant.USER_DEFAULT_MAC_ID) as? String{
            Constant.MACID = macID
        }
        
        sqliteManager.copyFileToDocs()
        //sqliteManager.extract()
    }
    
    func hideLoader(){
        Constant.APPDELEGATE?.hideCustomLoader()
    }
    
    //MARK:- Call Assesment API
    func syncMainAssessment_API_call(SUBJECTID:Int32) {
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "Sync Assessment...")
        print(Constant.API_Main_Sync_Assessment + "\(Constant.MACID)&subjectid=\(SUBJECTID)")
        let URL = Constant.API_Main_Sync_Assessment + "\(Constant.MACID)&subjectid=\(SUBJECTID)"
        APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if let arrJson = dictResponse{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        
                        print(arrStatus[0])
                        
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success" {
                                
                                for dictTable in arrJson{
                                    let dicTemp = dictTable as! Dictionary<String,Any>
                                    
                                    let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                    
                                    if arrOfKeys.count > 0{
                                        
                                        if arrOfKeys.contains("Assessment"){
                                            
                                            let arrAssessmentData = dicTemp["Assessment"] as! [Dictionary<String,Any>]
                                            
                                            if arrAssessmentData.count > 0{
                                                
                                                var arrAssessmentDataModal = [Assessment]()
                                                
                                                for assessmentData in arrAssessmentData{
                                                    
                                                    let objAssessmentDataModel = Assessment(dictResponse: assessmentData)
                                                    arrAssessmentDataModal.append(objAssessmentDataModel)
                                                    
                                                    let id = objAssessmentDataModel.Assessmentid
                                                    self.arrAssessmentID.append(id)
                                                    self.arrAssessmentResult.append(id)
                                                }
                                                // print(arrAssessmentDataModal)
                                                
                                                print("arrAssessmentID:-\(self.arrAssessmentID)")
                                                let objAssessment = AssessmentDB(arrAssessmentDoanloaded:arrAssessmentDataModal)
                                                let isSuccess = objAssessment.insertAssessmentData()
                                                print("Assessment :-",isSuccess)
                                                //Update Assessment
                                                self.arrAssessment1.removeAll()
                                                self.arrAssessment1 = arrAssessmentDataModal
                                                if !self.arrAssessment1.isEmpty {
                                                   self.updateAssessmentData()
                                                }
                                                
                                            }
                                        }else if arrOfKeys.contains("QuestionPattern") {
                                            
                                            let arrQuestionPatternData = dicTemp["QuestionPattern"] as! [Dictionary<String,Any>]
                                            
                                            if arrQuestionPatternData.count > 0{
                                                
                                                var arrQuestionPatternDataModal = [QuestionPattern]()
                                                
                                                for questionPatternData in arrQuestionPatternData{
                                                    let objquestionPatternDataModel = QuestionPattern(dictResponse: questionPatternData)
                                                    arrQuestionPatternDataModal.append(objquestionPatternDataModel)
                                                }
                                                // print(arrQuestionPatternDataModal)
                                                let objQuestionPattern = QuestionPatternDB(arrQuestionPatternDoanloaded: arrQuestionPatternDataModal)
                                                let isSuccess = objQuestionPattern.insertQuestionPatternData()
                                                print("QuestionPattern :-",isSuccess)
                                                //Update QuestionPattern
                                                self.arrQuestionPattern = arrQuestionPatternDataModal
                                                if !self.arrQuestionPattern.isEmpty {
                                                   self.updateQuestionPatternData()
                                                }
                                                
                                            }
                                        }else if arrOfKeys.contains("MediaType"){
                                            let arrMediaType = dicTemp["MediaType"] as! [Dictionary<String,Any>]
                                            if arrMediaType.count > 0{
                                                
                                                var arrModelMediaType = [MediaType]()
                                                
                                                for dictStandard in arrMediaType {
                                                    let objMediaTypeModel = MediaType(dictResponse: dictStandard)
                                                    arrModelMediaType.append(objMediaTypeModel)
                                                }
                                                //print(arrModelMediaType)
                                                let objMediaType = MediaTypeDB(arrMediaTypeDoanloaded: arrModelMediaType)
                                                let isSuccess = objMediaType.insertMediaTypeData()
                                                print("MediaTypeDB :-",isSuccess)
                                                //Update Media
                                                self.arrMediaType = arrModelMediaType
                                                if !self.arrMediaType.isEmpty {
                                                   self.updateMediaType()
                                                }
                                                
                                            }
                                        }else if arrOfKeys.contains("AssessmentType") {
                                            
                                            let arrAssessmentTypeData = dicTemp["AssessmentType"] as! [Dictionary<String,Any>]
                                            
                                            if arrAssessmentTypeData.count > 0{
                                                
                                                var arrAssessmentTypeDataModal = [AssessmentType]()
                                                
                                                for assessmentTypeData in arrAssessmentTypeData{
                                                    let objassessmentTypeDataModel = AssessmentType(dictResponse: assessmentTypeData)
                                                    arrAssessmentTypeDataModal.append(objassessmentTypeDataModel)
                                                }
                                                // print(arrAssessmentTypeDataModal)
                                                let objAssessmentType = AssessmentTypeDB(arrAssessmentTypeDoanloaded: arrAssessmentTypeDataModal)
                                                let isSuccess = objAssessmentType.insertAssessmentTypeData()
                                                print("AssessmentType :-",isSuccess)
                                                //Update AssessmentType
                                                self.arrAssessmentType = arrAssessmentTypeDataModal
                                                if !self.arrAssessmentType.isEmpty {
                                                   self.updateAssessmentType()
                                                }
                                                
                                            }
                                        }else if arrOfKeys.contains("AssessmentTime") {
                                            
                                            let arrAssessmentTimeData = dicTemp["AssessmentTime"] as! [Dictionary<String,Any>]
                                            
                                            if arrAssessmentTimeData.count > 0{
                                                
                                                var arrAssessmentTimeDataModal = [AssessmentTime]()
                                                
                                                for assessmentTimeData in arrAssessmentTimeData{
                                                    let objassessmentTimeDataModel = AssessmentTime(dictResponse: assessmentTimeData)
                                                    arrAssessmentTimeDataModal.append(objassessmentTimeDataModel)
                                                }
                                                // print(arrAssessmentTimeDataModal)
                                                let objAssessmentTime = AssessmentTimeDB(arrAssessmentTimeDoanloaded: arrAssessmentTimeDataModal)
                                                let isSuccess = objAssessmentTime.insertAssessmentTimeData()
                                                print("AssessmentTime :-",isSuccess)
                                                //Update AssessmentTime
                                                self.arrAssessmentTime = arrAssessmentTimeDataModal
                                                if !self.arrAssessmentTime.isEmpty {
                                                   self.updateAssessmentTime()
                                                }
                                                
                                            }
                                        }
                                        print(arrOfKeys)
                                    }
                                }
                               
                                DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                                   // self.hideLoader()
                                    if !self.arrAssessmentResult.isEmpty {
                                        self.callAssessmentResultAPI(assessmentID: "\(self.arrAssessmentResult[0])")
                                    }
                                })
                                
                                
                            }else{
                                UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                self.hideLoader()
                            }
                        }else{
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status Array ")
                        self.hideLoader()
                    }
                    
                }else{
                    print("No Any Status ")
                    self.hideLoader()
                }
                
            }else{
                self.hideLoader()
            }
        }) { (Error) in
            print(Error?.localizedDescription ?? "")
            self.hideLoader()
            UIViewController().alertControllerExt(strMessage:"\(Error?.localizedDescription ?? "")")
        }
    }
        
    //MARK:- Call AssesmentResult API
    func callAssessmentResultAPI(assessmentID:String) {
         Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
         
         print("mac id is ====>",Constant.MACID)
        //Constant.APPDELEGATE?.showCustomLoader(strMessage: "Sync Assessment Result ...")
        print(self.arrAssessmentResult)
        if !self.arrAssessmentResult.isEmpty {
            
            // for id in self.arrAssessmentID {
            
            print(Constant.API_GET_AssessmentData + "\(Constant.MACID)&AssessmentId=" + assessmentID + "&method=GetAssessmentResult")
            let URL = Constant.API_Get_AssessmentResulrData + "\(Constant.MACID)&AssessmentId=" + assessmentID + "&method=GetAssessmentResult"
            APIManager.shared.fetchDataFromAPI(strUrl:URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
                
                if let arrJson = dictResponse{
                    
                    if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                        
                        // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                        if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                            
                            print(arrStatus[0])
                            
                            if let responseStatus = arrStatus[0]["status"] as? String{
                                if responseStatus == "Success" {
                                    
                                    for dictTable in arrJson{
                                        let dicTemp = dictTable as! Dictionary<String,Any>
                                        
                                        let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                        
                                        if arrOfKeys.count > 0{
                                            
                                            if arrOfKeys.contains("AssessmentScore"){
                                                
                                                let arrAssessmentScoreData = dicTemp["AssessmentScore"] as! [Dictionary<String,Any>]
                                                
                                                if arrAssessmentScoreData.count > 0{
                                                    
                                                    var arrAssessmentScoreDataModal = [AssessmentScore]()
                                                    
                                                    for assessmentData in arrAssessmentScoreData{
                                                        let objAssessmentDataModel = AssessmentScore(dictResponse: assessmentData, assessmentID: Int32(assessmentID))
                                                        arrAssessmentScoreDataModal.append(objAssessmentDataModel)
                                                    }
                                                    //  print(arrAssessmentDataModal)
                                                    let objAssessmentScore = AssessmentScoreDB(arrAssessmentScoreDoanloaded:arrAssessmentScoreDataModal)
                                                    let isSuccess = objAssessmentScore.insertAssessmentScoreData()
                                                    print("AssessmentStore :-",isSuccess)
                                                    //Update Assessment Score
                                                    self.arrAssessmentScore = arrAssessmentScoreDataModal
                                                    if !self.arrAssessmentScore.isEmpty {
                                                        self.updateAssessmentScore()
                                                    }
                                                    
                                                }
                                            }
                                            else if arrOfKeys.contains("AssessmentResult"){
                                                
                                                let arrAssessmentResultData = dicTemp["AssessmentResult"] as! [Dictionary<String,Any>]
                                                
                                                if arrAssessmentResultData.count > 0{
                                                    
                                                    var arrAssessmentResultDataModal = [AssessmentResult]()
                                                    
                                                    for assessmentData in arrAssessmentResultData{
                                                        let objAssessmentDataModel = AssessmentResult(dictResponse: assessmentData)
                                                        arrAssessmentResultDataModal.append(objAssessmentDataModel)
                                                    }
                                                    print(arrAssessmentResultDataModal)
                                                    let objAssessmentScore =  AssessmentResultDB(arrAssessmentResultDoanloaded:arrAssessmentResultDataModal)
                                                    let isSuccess = objAssessmentScore.insertAssessmentResultData()
                                                    print("AssessmentResult :-",isSuccess)
                                                    //Update Assessment Result
                                                    self.arrAssessmentResult1 = arrAssessmentResultDataModal
                                                    if !self.arrAssessmentResult1.isEmpty {
                                                        self.updateAssessmentResult()
                                                    }
                                                    
                                                }
                                            }
                                            print(arrOfKeys)
                                        }
                                        
                                    }
                                    print(self.arrAssessmentResult)
                                    if !self.arrAssessmentResult.isEmpty {
                                        self.arrAssessmentResult.remove(at: 0)
                                        print(self.arrAssessmentResult)
                                    }
                                    if !self.arrAssessmentResult.isEmpty {
                                        self.callAssessmentResultAPI(assessmentID: "\(self.arrAssessmentResult[0])")
                                    }
                                    
                                    
                                    if self.arrAssessmentResult.count == 0 {
                                        self.hideLoader()
                                        NotificationCenter.default.post(name: NSNotification.Name.init("SyncNewAssessmentComplete"), object: nil, userInfo: nil)
                                    }
                                }else{
                                    UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                    self.hideLoader()
                                }
                            }else{
                                self.hideLoader()
                            }
                            
                        }else{
                            print("No Any Status Array ")
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status ")
                        self.hideLoader()
                    }
                    
                }else{
                    self.hideLoader()
                }
            }) { (Error) in
                print(Error?.localizedDescription ?? "")
                self.hideLoader()
                UIViewController().alertControllerExt(strMessage:"\(Error?.localizedDescription ?? "")")
            }
            // }
        }
    }
    
    //MARK:- Call Assesment API
    func call_API_FOR_Download_Assessment(assessmentID:String) {
        Constant.MACID = UserDefaults.standard.string(forKey:Constant.USER_DEFAULT_MAC_ID) ?? ""
        
        print("mac id is ====>",Constant.MACID)
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "Download Assessment...")
      
        print(Constant.API_Download_Assessment + "\(Constant.MACID)&AssessmentId=" + "\(assessmentID)")
        let URL = Constant.API_Download_Assessment + "\(Constant.MACID)&AssessmentId=" + assessmentID
            //API_GET_AssessmentData
            APIManager.shared.fetchDataFromAPI(strUrl:URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
                
                if let arrJson = dictResponse{
                    
                    if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                        
                        // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                        if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                            
                            print(arrStatus[0])
                            
                            if let responseStatus = arrStatus[0]["status"] as? String{
                                if responseStatus == "Success" {
                                    
                                    for dictTable in arrJson{
                                        let dicTemp = dictTable as! Dictionary<String,Any>
                                        
                                        let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                        
                                        if arrOfKeys.count > 0{
                                            
                                            if arrOfKeys.contains("Assessment"){
                                                
                                                let arrAssessmentData = dicTemp["Assessment"] as! [Dictionary<String,Any>]
                                                
                                                if arrAssessmentData.count > 0{
                                                    
                                                    var arrAssessmentDataModal = [Assessment]()
                                                    
                                                    for assessmentData in arrAssessmentData{
                                                        let objAssessmentDataModel = Assessment(dictResponse: assessmentData)
                                                        arrAssessmentDataModal.append(objAssessmentDataModel)
                                                    }
                                                    //  print(arrAssessmentDataModal)
                                                    let objAssessment = AssessmentDB(arrAssessmentDoanloaded:arrAssessmentDataModal)
                                                    //DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objAssessment.insertAssessmentData()
                                                        print("Assessment :-",isSuccess)
                                                        //Update Assessment
                                                        self.arrAssessment1.removeAll()
                                                        self.arrAssessment1 = arrAssessmentDataModal
                                                        if !self.arrAssessment1.isEmpty {
                                                           self.updateDownloadAssessmentData()
                                                        }
                                                    //})
                                                    
                                                }
                                            }else if arrOfKeys.contains("AssessmentQuestion") {
                                                
                                                let arrAssessmentQuestionData = dicTemp["AssessmentQuestion"] as! [Dictionary<String,Any>]
                                                
                                                if arrAssessmentQuestionData.count > 0{
                                                    
                                                    var arrAssessmentQuestionDataModal = [AssessmentQuestion]()
                                                    
                                                    for assessmentQuestionData in arrAssessmentQuestionData{
                                                        let objAssessmentQuestionDataModel = AssessmentQuestion(dictResponse: assessmentQuestionData)
                                                        arrAssessmentQuestionDataModal.append(objAssessmentQuestionDataModel)
                                                    }
                                                    // print(arrAssessmentQuestionDataModal)
                                                    let objAssessmentQuestion = AssessmentQuestionDB(arrAssessmentQuestionDoanloaded: arrAssessmentQuestionDataModal)
                                                  //  DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objAssessmentQuestion.insertAssessmentQuestionData()
                                                        print("AssessmentQuestion :-",isSuccess)
                                                        //Update AssessmentQuestion
                                                        self.arrAssessmentQuestion = arrAssessmentQuestionDataModal
                                                        if !self.arrAssessmentQuestion.isEmpty {
                                                           self.updateAssessmentQuestion()
                                                        }
                                                   // })
                                                    
                                                    
                                                }
                                            }else if arrOfKeys.contains("AssessmentQuestionDetails") {
                                                
                                                let arrAssessmentQuestionDetailsData = dicTemp["AssessmentQuestionDetails"] as! [Dictionary<String,Any>]
                                                
                                                if arrAssessmentQuestionDetailsData.count > 0{
                                                    
                                                    var arrAssessmentQuestionDetailsDataModal = [AssessmentQuestionDetails]()
                                                    
                                                    for assessmentQuestionData in arrAssessmentQuestionDetailsData{
                                                        let objAssessmentQuestionDetailsDataModel = AssessmentQuestionDetails(dictResponse: assessmentQuestionData)
                                                        arrAssessmentQuestionDetailsDataModal.append(objAssessmentQuestionDetailsDataModel)
                                                    }
                                                    // print(arrAssessmentQuestionDetailsDataModal)
                                                    let objAssessmentQuestionDetails = AssessmentQuestionDetailsDB(arrAssessmentQuestionDetailsDoanloaded: arrAssessmentQuestionDetailsDataModal)
                                                  //  DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objAssessmentQuestionDetails.insertAssessmentQuestionData()
                                                        print("AssessmentQuestionDetails :-",isSuccess)
                                                        //Update AssessmentQuestionDetails
                                                        self.arrAssessmentQuestionDetails = arrAssessmentQuestionDetailsDataModal
                                                        if !self.arrAssessmentQuestionDetails.isEmpty {
                                                           self.updateAssessmentQuestionDetails()
                                                        }
                                                  //  })
                                                    
                                                    
                                                }
                                            }else if arrOfKeys.contains("Question") {
                                                
                                                let arrQuestionData = dicTemp["Question"] as! [Dictionary<String,Any>]
                                                
                                                if arrQuestionData.count > 0{
                                                    
                                                    var arrQuestionDataModal = [Question]()
                                                    
                                                    for questionData in arrQuestionData{
                                                        let objQuestionDataModel = Question(dictResponse: questionData)
                                                        arrQuestionDataModal.append(objQuestionDataModel)
                                                    }
                                                    // print(arrQuestionDataModal)
                                                    let objQuestion = QuestionDB(arrQuestionDoanloaded: arrQuestionDataModal)
                                                  //  DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objQuestion.insertQuestionData()
                                                        print("Question :-",isSuccess)
                                                        //Update Question
                                                        self.arrQuestion = arrQuestionDataModal
                                                        if !self.arrQuestion.isEmpty {
                                                           self.updateQuestion()
                                                        }
                                                   // })
                                                    
                                                    
                                                }
                                            }else if arrOfKeys.contains("QuestionOptions") {
                                                
                                                let arrQuestionOptionsData = dicTemp["QuestionOptions"] as! [Dictionary<String,Any>]
                                                
                                                if arrQuestionOptionsData.count > 0{
                                                    
                                                    var arrQuestionOptionsDataModal = [QuestionOptions]()
                                                    
                                                    for questionOptionsData in arrQuestionOptionsData{
                                                        let objQuestionOptionsDataModel = QuestionOptions(dictResponse: questionOptionsData)
                                                        arrQuestionOptionsDataModal.append(objQuestionOptionsDataModel)
                                                    }
                                                    //  print(arrQuestionOptionsDataModal)
                                                    let objQuestionOptions = QuestionOptionsDB(arrQuestionOptionsDoanloaded: arrQuestionOptionsDataModal)
                                                //    DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objQuestionOptions.insertOnQuestionOptionsData()
                                                        print("QuestionOptions :-",isSuccess)
                                                        //Update Assessment QuestionOptions
                                                        self.arrQuestionOptions = arrQuestionOptionsDataModal
                                                        if !self.arrQuestionOptions.isEmpty {
                                                           self.updateQuestionOptions()
                                                        }
                                                  //  })
                                                    
                                                    
                                                }
                                            }else if arrOfKeys.contains("QuestionMedia") {
                                                
                                                let arrQuestionMediaData = dicTemp["QuestionMedia"] as! [Dictionary<String,Any>]
                                                
                                                if arrQuestionMediaData.count > 0{
                                                    
                                                    var arrQuestionMediaDataModal = [QuestionMedia]()
                                                    
                                                    for questionMediaData in arrQuestionMediaData{
                                                        let objQuestionMediaDataModel = QuestionMedia(dictResponse: questionMediaData)
                                                        arrQuestionMediaDataModal.append(objQuestionMediaDataModel)
                                                    }
                                                    // print(arrQuestionMediaDataModal)
                                                    let objQuestionMedia = QuestionMediaDB(arrQuestionMediaDoanloaded: arrQuestionMediaDataModal)
                                                 //   DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objQuestionMedia.insertQuestionMediaData()
                                                        print("QuestionMedia :-",isSuccess)
                                                        //Update QuestionMedia
                                                        self.arrQuestionMedia = arrQuestionMediaDataModal
                                                        if !self.arrQuestionMedia.isEmpty {
                                                            self.updateQuestionMedia()
                                                        }
                                                  //  })
                                                    
                                                    
                                                }
                                            }else if arrOfKeys.contains("QuestionOptionsMedia") {
                                                
                                                let arrQuestionOptionsMediaData = dicTemp["QuestionOptionsMedia"] as! [Dictionary<String,Any>]
                                                
                                                if arrQuestionOptionsMediaData.count > 0{
                                                    
                                                    var arrQuestionOptionsMediaDataModal = [QuestionOptionsMedia]()
                                                    
                                                    for questionMediaData in arrQuestionOptionsMediaData{
                                                        let objQuestionMediaDataModel = QuestionOptionsMedia(dictResponse: questionMediaData)
                                                        arrQuestionOptionsMediaDataModal.append(objQuestionMediaDataModel)
                                                    }
                                                    //  print(arrQuestionOptionsMediaDataModal)
                                                    let objQuestionOptionsMedia = QuestionOptionsMediaDB(arrQuestionOptionsMediaDoanloaded: arrQuestionOptionsMediaDataModal)
                                                //    DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objQuestionOptionsMedia.insertQuestionOptionsMediaData()
                                                        print("QuestionOptionsMedia :-",isSuccess)
                                                        //Update Question Option media
                                                        self.arrQuestionOptionsMedia = arrQuestionOptionsMediaDataModal
                                                        if !self.arrQuestionOptionsMedia.isEmpty {
                                                            self.updateQuestionOptionsMedia()
                                                        }
                                                 //   })
                                                }
                                            }else if arrOfKeys.contains("Media") {
                                                
                                                let arrMediaData = dicTemp["Media"] as! [Dictionary<String,Any>]
                                                
                                                if arrMediaData.count > 0{
                                                    
                                                    var arrMediaDataModal = [Media]()
                                                    
                                                    for questionMediaData in arrMediaData{
                                                        let objMediaDataModel = Media(dictResponse: questionMediaData)
                                                        arrMediaDataModal.append(objMediaDataModel)
                                                    }
                                                    // print(arrMediaDataModal)
                                                    let objMedia = MediaDB(arrMediaDoanloaded: arrMediaDataModal)
                                                    // objMedia.isForStandard = isStandard
                                                //    DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                                                        let isSuccess = objMedia.insertMediaData()
                                                        print("MediaDB :-",isSuccess)
                                                        //Update Media
                                                        self.arrMedia = arrMediaDataModal
                                                        if !self.arrMedia.isEmpty {
                                                            self.updateMedia()
                                                        }
                                                 //   })
                                                    
                                                    
                                                }
                                            }
                                            print(arrOfKeys)
                                        }
                                        
                                    }
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now()+4, execute: {
                                        self.hideLoader()
                                        NotificationCenter.default.post(name: NSNotification.Name.init("DownloadAssessmentComplete"), object: nil, userInfo: nil)
                                    })
                                    
                                }else{
                                    UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                    self.hideLoader()
                                }
                            }else{
                                self.hideLoader()
                            }
                            
                        }else{
                            print("No Any Status Array ")
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status ")
                        self.hideLoader()
                    }
                    
                }else{
                    self.hideLoader()
                }
            }) { (Error) in
                print(Error?.localizedDescription ?? "")
                self.hideLoader()
                UIViewController().alertControllerExt(strMessage:"\(Error?.localizedDescription ?? "")")
            }
    }

    
    //MARK:- Call Assesment API
    func updateAssessment() {
        if let macID = UserDefaults.standard.value(forKey: Constant.USER_DEFAULT_MAC_ID) as? String{
            Constant.MACID = macID
        }
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "Sync Assessment...")
        print(Constant.API_Main_Sync_Assessment + "\(Constant.MACID)&subjectid=\(Constant.SUBJECT_ID)")
        let URL = Constant.API_Main_Sync_Assessment + "\(Constant.MACID)&subjectid=\(Constant.SUBJECT_ID)"
        APIManager.shared.fetchDataFromAPI(strUrl: URL, parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if let arrJson = dictResponse{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    // print(dictStatus["Status"] as? [[String : Any]] ?? [])
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        
                        print(arrStatus[0])
                        
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success" {
                                
                                var arrAssessmentDataModal = [Assessment]()
                                for dictTable in arrJson{
                                    let dicTemp = dictTable as! Dictionary<String,Any>
                                    
                                    let arrOfKeys : [String] = Array((dictTable as! Dictionary<String,Any>).keys)
                                    
                                    if arrOfKeys.count > 0{
                                        
                                        if arrOfKeys.contains("Assessment"){
                                            
                                            let arrAssessmentData = dicTemp["Assessment"] as! [Dictionary<String,Any>]
                                            
                                            if arrAssessmentData.count > 0{
                                                
                                                
                                                
                                                for assessmentData in arrAssessmentData{
                                                    
                                                    let objAssessmentDataModel = Assessment(dictResponse: assessmentData)
                                                    arrAssessmentDataModal.append(objAssessmentDataModel)
                                                    
                                                }
                                            }
                                        }
                                        print(arrOfKeys)
                                        
                                    }
                                }
                                self.arrAssessment.removeAll()
                                self.arrAssessment = arrAssessmentDataModal
                                if !self.arrAssessment.isEmpty {
                                    self.setAssessmentData()
                                }
                                self.hideLoader()
                                
                            }else{
                                UIViewController().alertControllerExt(strMessage: arrStatus[0]["message"] as? String ?? "Something went wrong!")
                                self.hideLoader()
                            }
                        }else{
                            self.hideLoader()
                        }
                        
                    }else{
                        print("No Any Status Array ")
                        self.hideLoader()
                    }
                    
                }else{
                    print("No Any Status ")
                    self.hideLoader()
                }
                
            }else{
                self.hideLoader()
            }
        }) { (Error) in
            print(Error?.localizedDescription ?? "")
            UIViewController().alertControllerExt(strMessage:"\(Error?.localizedDescription ?? "")")
            self.hideLoader()
            
        }
    }
    
    
    func  setAssessmentData() {
        print(self.arrAssessment)
        let db = AssessmentDB.init()
        let isUpdate = db.updateResultAssessmentData(Assessment: self.arrAssessment[0])
        print("Assessment :-",isUpdate)
        if isUpdate == true {
            self.arrAssessment.remove(at: 0)
            if !self.arrAssessment.isEmpty {
                self.setAssessmentData()
            }
            else {
                NotificationCenter.default.post(name: NSNotification.Name.init("SyncNewResultDispatchAssessment"), object: nil, userInfo: nil)
            }
        }
    }
   
    
   //-------------------- Update Assessment -------------------
    
    func updateAssessmentData() {
       print(self.arrAssessment1)
        let db = AssessmentDB.init()
        let isUpdate = db.updateAssessmentData(Assessment: self.arrAssessment1[0])
        print("isUpdateAssessment :-",isUpdate)
        if isUpdate == true {
            self.arrAssessment1.remove(at: 0)
            if !self.arrAssessment1.isEmpty {
                self.updateAssessmentData()
            }
        }
    }
    
    func updateDownloadAssessmentData() {
       print(self.arrAssessment1)
        let db = AssessmentDB.init()
        let isUpdate = db.updateDownloadAssessmentData(Assessment: self.arrAssessment1[0])
        print("isUpdateAssessment :-",isUpdate)
        if isUpdate == true {
            self.arrAssessment1.remove(at: 0)
            if !self.arrAssessment1.isEmpty {
                self.updateDownloadAssessmentData()
            }
        }
    }
    
    
    func updateQuestionPatternData() {
        print(self.arrQuestionPattern)
        let db = QuestionPatternDB.init()
        let isUpdate = db.updateQuestionPatternData(QuestionPattern: self.arrQuestionPattern[0])
        print("isUpdateQuestionPattern :-",isUpdate)
        if isUpdate == true {
            self.arrQuestionPattern.remove(at: 0)
            if !self.arrQuestionPattern.isEmpty {
                self.updateQuestionPatternData()
            }
        }
    }
  
    func updateMediaType() {
        print(self.arrMediaType)
        
        let db = MediaTypeDB.init()
        let isUpdate = db.updateMediaTypeData(MediaType: self.arrMediaType[0])
        print("isUpdateMediaType :-",isUpdate)
        if isUpdate == true {
            self.arrMediaType.remove(at: 0)
            if !self.arrMediaType.isEmpty {
                self.updateMediaType()
            }
        }
    }
    
    func updateAssessmentType() {
      print(self.arrAssessmentType)
       let db = AssessmentTypeDB.init()
        let isUpdate = db.updateAssessmentTypeData(AssessmentType: self.arrAssessmentType[0])
        print("isUpdateAssessmentType :-",isUpdate)
        if isUpdate == true {
            self.arrAssessmentType.remove(at: 0)
            if !self.arrAssessmentType.isEmpty {
                self.updateAssessmentType()
            }
        }
    }
    
    func updateAssessmentTime() {
        print(self.arrAssessmentTime)
        let db = AssessmentTimeDB.init()
         let isUpdate = db.updateAssessmentTimeData(AssessmentTime: self.arrAssessmentTime[0])
         print("isUpdateAssessmentTime :-",isUpdate)
         if isUpdate == true {
             self.arrAssessmentTime.remove(at: 0)
             if !self.arrAssessmentTime.isEmpty {
                 self.updateAssessmentTime()
             }
         }
    }
    
    func updateAssessmentQuestion() {
       print(self.arrAssessmentQuestion)
        let db = AssessmentQuestionDB.init()
         let isUpdate = db.updateAssessmentQuestionData(AssessmentQuestion: self.arrAssessmentQuestion[0])
         print("isUpdateAssessmentQuestion :-",isUpdate)
         if isUpdate == true {
             self.arrAssessmentQuestion.remove(at: 0)
             if !self.arrAssessmentQuestion.isEmpty {
                 self.updateAssessmentQuestion()
             }
         }
    }
    
    func updateAssessmentQuestionDetails() {
        print(self.arrAssessmentQuestionDetails)
        let db = AssessmentQuestionDetailsDB.init()
        let isUpdate = db.updateAssessmentQuestionDetailsData(AssessmentQuestionDetails: self.arrAssessmentQuestionDetails[0])
        print("isUpdateAssessmentQuestionDetails :-",isUpdate)
        if isUpdate == true {
            self.arrAssessmentQuestionDetails.remove(at: 0)
            if !self.arrAssessmentQuestionDetails.isEmpty {
                self.updateAssessmentQuestionDetails()
            }
        }
    }
    
    func updateQuestion() {
        print(self.arrQuestion)
        let db = QuestionDB.init()
        let isUpdate = db.updateQuestionData(Question: self.arrQuestion[0])
        print("isUpdateQuestion :-",isUpdate)
        if isUpdate == true {
            self.arrQuestion.remove(at: 0)
            if !self.arrQuestion.isEmpty {
                self.updateQuestion()
            }
        }
    }
    
    func updateQuestionOptions() {
        print(self.arrQuestionOptions)
        let db = QuestionOptionsDB.init()
        let isUpdate = db.updateQuestionData(QuestionOptions: self.arrQuestionOptions[0])
        print("isUpdateQuestionOptions :-",isUpdate)
        if isUpdate == true {
            self.arrQuestionOptions.remove(at: 0)
            if !self.arrQuestionOptions.isEmpty {
                self.updateQuestionOptions()
            }
        }
    }
    
    func updateQuestionMedia() {
        print(self.arrQuestionMedia)
        let db = QuestionMediaDB.init()
        let isUpdate = db.updateQuestionData(QuestionMedia: self.arrQuestionMedia[0])
        print("isUpdateQuestionMedia :-",isUpdate)
        if isUpdate == true {
            self.arrQuestionMedia.remove(at: 0)
            if !self.arrQuestionMedia.isEmpty {
                self.updateQuestionMedia()
            }
        }
    }
    
    func updateQuestionOptionsMedia() {
      print(self.arrQuestionOptionsMedia)
        let db = QuestionOptionsMediaDB.init()
        let isUpdate = db.updateQuestionData(QuestionOptionsMedia: self.arrQuestionOptionsMedia[0])
        print("isUpdateQuestionOptionsMedia :-",isUpdate)
        if isUpdate == true {
            self.arrQuestionOptionsMedia.remove(at: 0)
            if !self.arrQuestionOptionsMedia.isEmpty {
                self.updateQuestionOptionsMedia()
            }
        }
    }
    
    func updateMedia() {
        print(self.arrMedia)
        let db = MediaDB.init()
        let isUpdate = db.updateMediaData(Media: self.arrMedia[0])
        print("isupdateMedia :-",isUpdate)
        if isUpdate == true {
            self.arrMedia.remove(at: 0)
            if !self.arrMedia.isEmpty {
                self.updateMedia()
            }
        }
    }
    
    func updateAssessmentScore() {
        print(self.arrAssessmentScore)
        let db = AssessmentScoreDB.init()
        let isUpdate = db.updateAssessmentScoreData(AssessmentScore: self.arrAssessmentScore[0])
        print("isupdateAssessmentScore :-",isUpdate)
        if isUpdate == true {
            self.arrAssessmentScore.remove(at: 0)
            if !self.arrAssessmentScore.isEmpty {
                self.updateAssessmentScore()
            }
        }
    }
    
    func updateAssessmentResult() {
       print(self.arrAssessmentResult1)
        let db = AssessmentResultDB.init()
        let isUpdate = db.updateAssessmentResultData(AssessmentResult: self.arrAssessmentResult1[0])
        print("isupdateAssessmentResult :-",isUpdate)
        if isUpdate == true {
            self.arrAssessmentResult1.remove(at: 0)
            if !self.arrAssessmentResult1.isEmpty {
                self.updateAssessmentResult()
            }
        }
    }
}
