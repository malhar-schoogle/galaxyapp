//
//  MediaDB.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class MediaDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrMediaDownloaded = [Media]()
    var arrMedia = [IDModel]()
    var isForStandard = Bool()
    
    // Init Method
    override init() {
        super.init()
    }

    //Init with database method when Download database and copyData
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrMediaDownloaded = self.setDataIntoMediaModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrMedia = self.getOnlyIDsFromDatabase()

    }

    //Init with database method when Call Api and copyData
    init(arrMediaDoanloaded : [Media]) {
        super.init()
        sqliteManager.db = nil
        arrMediaDownloaded = arrMediaDoanloaded
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrMedia = self.getOnlyIDsFromDatabase()
        
    }
    
    init(isMediaDownload : Bool) {
        if isMediaDownload{
            sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
            sqliteManager.db = sqliteManager.openDatabase()
        }
    }
    
    //Insert data
    func insertMediaData() -> Bool{
        var isSuccess = false
        
        if arrMedia.count > 0 {
            let arrOfNewMedia = self.compareMedia(arrMediaDownload: arrMediaDownloaded, arrMediaApplication: arrMedia)
            
                if arrOfNewMedia.count > 0 {
                    isSuccess = self.insertMedia(arrNewMedia: arrOfNewMedia)
                }else{
                    print("No Any Update")
                    isSuccess = true
                }
            
            
            
        }else{
            
            if arrMediaDownloaded.count > 0 {
                isSuccess = self.insertMedia(arrNewMedia: arrMediaDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.sqliteManager.closeDB()
        })
        
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoMediaModel() -> [Media] {
        
        var arrMediaTemp = [Media]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Media", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let MediaId = sqlite3_column_int(queryStatement, 0)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                     let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    let objMediaModel = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)
                    
                    arrMediaTemp.append(objMediaModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        return arrMediaTemp
    }
    
    //Delete Query From Media Table FROM BLM
    func makeDeleteMediaQuery(chapterId:Int) -> Bool{
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Media Where MediaId In(Select MediaId From ContentMedia Where ContentId In (Select ContentId From Content Where TopicId In (Select TopicId From Topic Where ChapterId = \(chapterId))))And MediaId Not In (0)")
        }
        return issuccess
    }

    //Delete Query From Media Table FROM Standard
    func makeDeleteMediaQueryStandard(standardId:Int) -> Bool{
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Media Where MediaId In(Select MediaId From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In(Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId In(Select SubjectId From Subject Where StandardId = \(standardId))))))And MediaId Not In (0)")
        }
        return issuccess
    }
    
    
    //Delete Query From Media Table FROM Standard
    func makeDeleteMediaQuerySubject(subjectId:Int) -> Bool{
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Media Where MediaId In(Select MediaId From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In(Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId = \(subjectId)))))And MediaId Not In (0)")
        }
        return issuccess
    }
    
    //MARK: SELECT MEDIA FROM MEDIA TABLE WHICH NEED TO DELETE IN BLM QUERY
    func selectMediaFromMediaTableToDelete(chapterid:Int) -> [Media]{
        
        var arrMediaTemp = [Media]()
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query:"Select MediaFile From Media Where MediaId In(Select MediaId From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In(Select TopicId From Topic Where ChapterId = \(chapterid))))", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 0))
                    let MediaId = sqlite3_column_int(queryStatement, 0)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    let objMediaModel = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)

                    arrMediaTemp.append(objMediaModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        return arrMediaTemp
    }
    
    //MARK: SELECT MEDIA FROM MEDIA TABLE WHICH NEED TO DELETE IN STANDARD QUERY
    func selectStandardMediaFromMediaTableToDelete(standardId:Int) -> [Media]{
        
        var arrMediaTemp = [Media]()
        if sqliteManager.openDatabase() != nil{
            
    sqliteManager.selectTable(query:"Select MediaFile From Media Where MediaId In(Select MediaId From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In(Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId In( Select SubjectId From Subject Where StandardId = \(standardId))))))And MediaId Not In (0)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 0))
                    let MediaId = sqlite3_column_int(queryStatement, 2)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    let objMediaModel = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)
                    
                    arrMediaTemp.append(objMediaModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        return arrMediaTemp
    }
    
    
    //MARK: SELECT MEDIA FROM MEDIA TABLE WHICH NEED TO DELETE IN Subject query
    func selectSubjectMediaFromMediaTableToDelete(subjectId:Int) -> [Media]{

        var arrMediaTemp = [Media]()
        if sqliteManager.openDatabase() != nil{
            
            sqliteManager.selectTable(query:"Select MediaFile From Media Where MediaId In(Select MediaId From ContentMedia Where ContentId In(Select ContentId From Content Where TopicId In(Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId = \(subjectId)))))And MediaId Not In (0)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 0))
                    let MediaId = sqlite3_column_int(queryStatement, 2)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    let objMediaModel = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)
                    
                    arrMediaTemp.append(objMediaModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        return arrMediaTemp
    }
    
    
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrMediaTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Media", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    let objIDModel = IDModel(id: id)
                    arrMediaTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
    }
    
    // Check Media is exist or not
    func compareMedia(arrMediaDownload : [Media] , arrMediaApplication : [IDModel]) -> [Media]{
        
        var arrNewMedia = [Media]()
        if arrMediaApplication.count > 0{
            for MediaObj in arrMediaDownload{
                let filterObj = arrMediaApplication.filter({ $0.id == MediaObj.MediaId })
                if filterObj.count == 0{
                    arrNewMedia.append(MediaObj)
                }
            }
        }else{
            arrNewMedia.append(contentsOf: arrMediaDownload)
        }
        return arrNewMedia
    }
    
    //Insert Media into database
    func insertMedia(arrNewMedia : [Media]) -> Bool{
        var isSuccess = false
        for newMedia in arrNewMedia {
            
            let isInsert = self.sqliteManager.insertTable(query:self.makeInsertQuery(Media: newMedia) )

            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
               // break
            }
        }
        arrMedia.removeAll()
        arrMediaDownloaded.removeAll()
        print("Media Done")
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(Media : Media) -> String {
        
        return "INSERT INTO Media (MediaId,MediaTypeId,MediaFile,ManualMedia,Version,IsImageInFolderYN,FileSize,IsActive,CreatedDate,UpdatedDate,CreatedUserId,UpdatedUserId,IsDeleted,DeletedUserId,DeletedDate,MediaFlag,IsDownload) VALUES (\(Media.MediaId),\(Media.MediaTypeId),'\(Media.MediaFile)',\(Media.ManualMedia),\(Media.Version),\(Media.IsImageInFolderYN),\(Media.FileSize),\(Media.IsActive),'\(Media.CreatedDate)','\(Media.UpdatedDate)',\(Media.CreatedUserId),\(Media.UpdatedUserId),\(Media.IsDeleted),\(Media.DeletedUserId),'\(Media.DeletedDate)','\(Media.MediaFlag)','\(Media.IsDownload)');"
        
    }
    
    
   
    
    //UPDATE -----------------------------------
    func searchMediaData(MediaId : Int32) -> Media{
        
        var objMedia = Media()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Media where MediaId = \(MediaId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {

                    let MediaId = sqlite3_column_int(queryStatement, 0)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    objMedia = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return objMedia
        
    }
    
    //Perform Update Operation
    func updateMediaData(Media : Media) -> Bool{
        
        var isSuccess = false
        //Seach Media
        let objMedia = searchMediaData(MediaId: Media.MediaId)
        
        if Media.MediaId != 0{ // if exist
            //Perform Update
            let oldMedia = searchMediaData(MediaId: Media.MediaId)
            var isUpdate = Bool()
            if oldMedia.CreatedDate == Media.CreatedDate && oldMedia.UpdatedDate == Media.UpdatedDate && oldMedia.DeletedDate == Media.DeletedDate {
                //Not Change
                isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Media: Media))
            }
            else {
                //Change
                 isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery2(Media: Media))
            }
            
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{
            // not exist
            //Perform insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Media: Media))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
        }
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery(Media : Media) -> String {
        
        return "UPDATE MEDIA SET MediaTypeId = '\(Media.MediaTypeId)' , MediaFile = '\(Media.MediaFile.replacingOccurrences(of: "\'", with: "\'\'"))' , ManualMedia = '\(Media.ManualMedia)' , Version = '\(Media.Version)' , IsImageInFolderYN = '\(Media.IsImageInFolderYN)' , FileSize = '\(Media.FileSize)' , IsActive = '\(Media.IsActive)' , CreatedDate = '\(Media.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , UpdatedDate = '\(Media.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , CreatedUserId = '\(Media.CreatedUserId)' , UpdatedUserId = '\(Media.UpdatedUserId)' , IsDeleted = '\(Media.IsDeleted)' , DeletedUserId = '\(Media.DeletedUserId)' , DeletedDate = '\(Media.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , MediaFlag = '\(Media.MediaFile.replacingOccurrences(of: "\'", with: "\'\'"))'  where MediaId = '\(Media.MediaId)'"
          //, IsDownload = '\(Media.IsDownload)'
    }
    
    //Make update query
    func makeUpdateQuery2(Media : Media) -> String {
        let isDownload:Int32 = 0
        return "UPDATE MEDIA SET MediaTypeId = '\(Media.MediaTypeId)' , MediaFile = '\(Media.MediaFile.replacingOccurrences(of: "\'", with: "\'\'"))' , ManualMedia = '\(Media.ManualMedia)' , Version = '\(Media.Version)' , IsImageInFolderYN = '\(Media.IsImageInFolderYN)' , FileSize = '\(Media.FileSize)' , IsActive = '\(Media.IsActive)' , CreatedDate = '\(Media.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , UpdatedDate = '\(Media.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , CreatedUserId = '\(Media.CreatedUserId)' , UpdatedUserId = '\(Media.UpdatedUserId)' , IsDeleted = '\(Media.IsDeleted)' , DeletedUserId = '\(Media.DeletedUserId)' , DeletedDate = '\(Media.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , MediaFlag = '\(Media.MediaFile.replacingOccurrences(of: "\'", with: "\'\'"))' , IsDownload = '\(isDownload)' where MediaId = '\(Media.MediaId)'"
          
    }
    
    //Download Media
    func getMediaFilesToDownload(lastSyncDate : String) -> [Media]{
        
        var arrMediaTemp = [Media]()
        
        let Query_Media_To_Download = "SELECT * FROM Media WHERE IsDeleted = 0 AND IsActive = 1 AND IsImageInFolderYN = 0"
        
        print(Query_Media_To_Download)
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Media_To_Download, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let MediaId = sqlite3_column_int(queryStatement, 0)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    let objMediaModel = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)
                    
                    if self.getDateFormated(strDate: objMediaModel.UpdatedDate){
                        arrMediaTemp.append(objMediaModel)
                    }else{
                        print("NNNNNNNNNNNN")
                    }
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
        
    }
    
    func getDateFormated(strDate : String) -> Bool{
        
        //var syncDate = Date()
        let lastSyncDate = Constant.DATE_LAST_SYNC
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MM/dd/yyyy HH:mm:ss a"

        if let syncDate = dateFormatterGet.date(from: strDate) , let staticDate = dateFormatterGet.date(from: lastSyncDate){
            
            print(syncDate)
            print(staticDate)
            
            if syncDate.compare(staticDate) == .orderedSame{
                return false
            }else if syncDate.compare(staticDate) == .orderedAscending{
                return false
            }else{
                return true
            }

        }else{
            return false
        }
        
        
    }
    
    func getMediaData(Query : String) -> [Media]{
        
        var arrMediaTemp = [Media]()
        
        let Query_Media_To_Download = Query
        
        print(Query_Media_To_Download)
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Media_To_Download, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let MediaId = sqlite3_column_int(queryStatement, 0)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    let objMediaModel = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)
                    
                    if self.getDateFormated(strDate: objMediaModel.UpdatedDate){
                        arrMediaTemp.append(objMediaModel)
                    }else{
                        print("NNNNNNNNNNNN")
                    }
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
        
    }
    
    func getMediaDataForUpdate(Query : String) -> [Media]{
        
        var arrMediaTemp = [Media]()
        
        let Query_Media_To_Download = Query
        
        print(Query_Media_To_Download)
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Media_To_Download, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let MediaId = sqlite3_column_int(queryStatement, 0)
                    let MediaTypeId = sqlite3_column_int(queryStatement, 1)
                    let MediaFile = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ManualMedia = sqlite3_column_int(queryStatement, 3)
                    let Version = sqlite3_column_int(queryStatement, 4)
                    let IsImageInFolderYN = sqlite3_column_int(queryStatement, 5)
                    let FileSize = sqlite3_column_int(queryStatement, 6)
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let MediaFlag = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let IsDownload = sqlite3_column_int(queryStatement, 16)
                    
                    let objMediaModel = Media(MediaId: MediaId, MediaTypeId: MediaTypeId, MediaFile: MediaFile, ManualMedia: ManualMedia, Version: Version, IsImageInFolderYN: IsImageInFolderYN, FileSize: FileSize, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, MediaFlag: MediaFlag, IsDownload: IsDownload)
                    
                    
                        arrMediaTemp.append(objMediaModel)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
        
    }
    
    func UpdateMediaDownload(MediaID : Int32) -> Bool {
    
            var isSuccess = false
            //Seach Media
            let objMedia = searchMediaData(MediaId: MediaID)
            
            if objMedia.MediaId != 0{ // if exist
                //Perform Update
                let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery1(Media: objMedia))
                
                if isUpdate {
                    print("Updated Successfully")
                    isSuccess = true
                }else{
                    print("Not Updated")
                    isSuccess = false
                }
            }
            return isSuccess
        
    }
    
    func makeUpdateQuery1(Media : Media) -> String {
        let isDownload:Int32 = 1
        return "UPDATE MEDIA SET IsDownload = '\(isDownload)' where MediaId = '\(Media.MediaId)'"

       }
    
   
    
    
    /*func makeUpdateQuery2(MediaFile : String) -> String {
     let isDownload:Int32 = 1
     return "UPDATE MEDIA SET IsDownload = '\(isDownload)' where MediaFile = '\(MediaFile)'"

    }*/
}
