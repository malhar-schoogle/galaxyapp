//
//  BoardDB.swift
//  Schoogle
//
//  Created by Malhar on 11/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
let Query_GET_DATA_FROM_BOARD_ID = "SELECT * FROM BOARD WHERE IsDeleted = 0 AND IsActive = 1 AND BoardId = "
class BoardDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrBoardDownloaded = [Board]()
    var arrBoard = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        print(sqliteManager.pathToDatabase)
        sqliteManager.db = sqliteManager.openDatabase()
    }
    
    //Init with database method when Download database and copyData
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrBoardDownloaded = self.setDataIntoBoardModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrBoard = self.getOnlyIDsFromDatabase()
        
    }
    
    
    //Init with database method when Call Api and copyData
    init(arrBoardDoanloaded : [Board]) {
        super.init()
        
        arrBoardDownloaded = arrBoardDoanloaded
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrBoard = self.getOnlyIDsFromDatabase()
        
    }
    
    //Insert data
    func insertBoardData() -> Bool{
        var isSuccess = false
        
        if arrBoard.count > 0{
            let arrOfNewBoard = self.compareBoard(arrBoardDownload: arrBoardDownloaded, arrBoardApplication: arrBoard)
            
            if arrOfNewBoard.count > 0 {
                isSuccess = self.insertBoard(arrNewBoard: arrOfNewBoard)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }else{
            
            if arrBoardDownloaded.count > 0 {
                isSuccess = self.insertBoard(arrNewBoard: arrBoardDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }
        
        //        sqliteManager.closeDB()
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoBoardModel() -> [Board] {
        
        var arrBoardTemp = [Board]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Board", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let BoardId = sqlite3_column_int(queryStatement, 0)
                    let BoardName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let BoardAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let BoardImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    
                    
                    let objBoardModel = Board(BoardId: BoardId, BoardName: BoardName, BoardAlias: BoardAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, BoardImage: BoardImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                    arrBoardTemp.append(objBoardModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrBoardTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrBoardTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Board", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrBoardTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrBoardTemp
    }
    
    // Check Board is exist or not
    func compareBoard(arrBoardDownload : [Board] , arrBoardApplication : [IDModel]) -> [Board]{
        
        var arrNewBoard = [Board]()
        if arrBoardApplication.count > 0{
            for BoardObj in arrBoardDownload{
                let filterObj = arrBoardApplication.filter({ $0.id == BoardObj.BoardId })
                if filterObj.count == 0{
                    arrNewBoard.append(BoardObj)
                }
            }
        }else{
            arrNewBoard.append(contentsOf: arrBoardDownload)
        }
        return arrNewBoard
    }
    
    //Insert Board into database
    func insertBoard(arrNewBoard : [Board]) -> Bool{
        var isSuccess = false
        for newBoard in arrNewBoard {
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Board: newBoard))
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
                break
            }
        }
        arrBoard.removeAll()
        arrBoardDownloaded.removeAll()
        print("Board Done")
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(Board : Board) -> String {
        
        return "INSERT INTO Board (BoardId,BoardName,BoardAlias,ShortDescription,LongDescription,BoardImage,Remarks,IsActive,CreatedDate,UpdatedDate,CreatedUserId,UpdatedUserId,IsDeleted,DeletedUserId,DeletedDate) VALUES (\(Board.BoardId),'\(Board.BoardName.replacingOccurrences(of: "\'", with: "\'\'"))','\(Board.BoardAlias)','\(Board.ShortDescription)','\(Board.LongDescription)','\(Board.BoardImage.replacingOccurrences(of: "\'", with: "\'\'"))','\(Board.Remarks)',\(Board.IsActive),'\(Board.CreatedDate)','\(Board.UpdatedDate)',\(Board.CreatedUserId),\(Board.UpdatedUserId),\(Board.IsDeleted),\(Board.DeletedUserId),'\(Board.DeletedDate)');"
        
    }
    
    
    //UPDATE -----------------------------------
    func searchBoardData(BoardId : Int32) -> Board{
        
        var objBoard = Board()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Board where BoardId = \(BoardId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let BoardId = sqlite3_column_int(queryStatement, 0)
                    let BoardName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let BoardAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let BoardImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    
                    objBoard = Board(BoardId: BoardId, BoardName: BoardName, BoardAlias: BoardAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, BoardImage: BoardImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objBoard
        
    }
    
    //Perform Update Operation
    func updateBoardData(Board : Board) -> Bool{
        
        var isSuccess = false
        //Seach Board
        let objBoard = searchBoardData(BoardId: Board.BoardId)
        
        if Board.BoardId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Board: Board))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Board: Board))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery(Board : Board) -> String {
        
        return "UPDATE Board SET BoardName = '\(Board.BoardName.replacingOccurrences(of: "\'", with: "\'\'"))', BoardAlias = '\(Board.BoardAlias.replacingOccurrences(of: "\'", with: "\'\'"))' , ShortDescription = '\(Board.ShortDescription.replacingOccurrences(of: "\'", with: "\'\'"))', LongDescription = '\(Board.LongDescription.replacingOccurrences(of: "\'", with: "\'\'"))', BoardImage = '\(Board.BoardImage.replacingOccurrences(of: "\'", with: "\'\'"))', Remarks = '\(Board.Remarks.replacingOccurrences(of: "\'", with: "\'\'"))', IsActive = '\(Board.IsActive)', CreatedDate = '\(Board.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))', UpdatedDate = '\(Board.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))', CreatedUserId = '\(Board.CreatedUserId)', UpdatedUserId = '\(Board.UpdatedUserId)', IsDeleted = '\(Board.IsDeleted)', DeletedUserId = '\(Board.DeletedUserId)', DeletedDate =  '\(Board.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' where BoardId = '\(Board.BoardId)'"
        
    }
    //----------------------------------
    
    func getBoardDataFromID(boardID : Int32) -> Board{
        
        var objBoardTemp = Board()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_GET_DATA_FROM_BOARD_ID + "\(boardID)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let BoardId = sqlite3_column_int(queryStatement, 0)
                    let BoardName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let BoardAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let BoardImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let IsActive = sqlite3_column_int(queryStatement, 7)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 10)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 11)
                    let IsDeleted = sqlite3_column_int(queryStatement, 12)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 13)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    
                    
                    objBoardTemp = Board(BoardId: BoardId, BoardName: BoardName, BoardAlias: BoardAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, BoardImage: BoardImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return objBoardTemp
        
    }
    
}
