//  contentDB.swift
//  Schoogle
//  Created by Malhar on 07/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit
import SQLite

class ContentDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrContentDownloaded = [Content]()
    var arrContent = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }

    //Init with database method
    init(dbName : String) {

        super.init()
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrContentDownloaded = self.setDataIntoContentModel()

        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrContent = self.getOnlyIDsFromDatabase()
    }

    //Init with database method when Call Api and copyData
    init(arrContentDoanloaded : [Content]) {
        super.init()
        
        self.arrContentDownloaded = arrContentDoanloaded
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        self.arrContent = self.getOnlyIDsFromDatabase()
        
    }
    
    //MARK: ADDED By Pramit on 2nd December 2019
    //Insert data
    func insertContentData() -> Bool{
 
    var isSuccess = false
   
    if arrContent.count > 0{

        let arrOfNewContent = self.compareContent(arrContentDownload: self.arrContentDownloaded, arrContentApplication: arrContent)
        
        if arrOfNewContent.count > 0 {
          isSuccess = self.insertContent(arrNewContent: arrOfNewContent)
        
        }else{
            isSuccess = true
            print("No Any Updates")
        }
            
    }else{
            
        if arrContentDownloaded.count > 0 {
            isSuccess = self.insertContent(arrNewContent: arrContentDownloaded)
        }else{
            print("No Any Update")
            isSuccess = true
        }
    }
        arrContentDownloaded.removeAll()
        sqliteManager.closeDB()
        print("Content Done")
        return isSuccess
    }

    
    //Insert data
//    func insertContentData() -> Bool{
//
//        var isSuccess = false
//
//        if arrContent.count > 0{
//            let arrOfNewContent = self.compareContent(arrContentDownload: arrContentDownloaded, arrContentApplication: arrContent)
//
//            if arrOfNewContent.count > 0 {
//                isSuccess = self.insertContent(arrNewContent: arrOfNewContent)
//            }else{
//                print("No Any Update")
//                isSuccess = true
//            }
//
//        }else{
//            if arrContentDownloaded.count > 0 {
//                isSuccess = self.insertContent(arrNewContent: arrContentDownloaded)
//            }else{
//                print("No Any Update")
//                isSuccess = true
//            }
//        }
//        arrContentDownloaded.removeAll()
//        sqliteManager.closeDB()
//        print("Content Done")
//        return isSuccess
//    }
    
    //Download data and insert into model
    func setDataIntoContentModel() -> [Content] {
        
        var arrContentTemp = [Content]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Content", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let contentId = sqlite3_column_int(queryStatement, 0)
                    let topicId = sqlite3_column_int(queryStatement, 1)
                    let contentTypeId = sqlite3_column_int(queryStatement, 2)
                    let contentName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let contentDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let contentSummary = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let contentGroupId = sqlite3_column_int(queryStatement, 6)
                    let isPasswordProtected = sqlite3_column_int(queryStatement, 7)
                    let contentPassword = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let prePageContentId = sqlite3_column_int(queryStatement, 9)
                    let nextPageContentId = sqlite3_column_int(queryStatement, 10)
                    let lisOrder = sqlite3_column_int(queryStatement, 11)
                    let isForTeacher = sqlite3_column_int(queryStatement, 12)
                    let isForStudent = sqlite3_column_int(queryStatement, 13)
                    let remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let isActive = sqlite3_column_int(queryStatement, 15)
                    let createdDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    let upadatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 17))
                    let createdUserId = sqlite3_column_int(queryStatement, 18)
                    let updatedUserId = sqlite3_column_int(queryStatement, 29)
                    let isDeleted = sqlite3_column_int(queryStatement, 20)
                    let deletedUserId = sqlite3_column_int(queryStatement, 21)
                    let deletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 22))
                    let isDownload = sqlite3_column_int(queryStatement, 23)
                    
                    let objContentModel = Content(contentId: contentId , topicId: topicId, contentTypeId: contentTypeId, contentName: contentName, contentDescription: contentDescription, contentSummary: contentSummary, contentGroupId: contentGroupId, isPasswordProtected: isPasswordProtected, contentPassword: contentPassword, prePageContentId: prePageContentId, nextPageContentId: nextPageContentId, lisOrder: lisOrder, isForTeacher: isForTeacher, isForStudent: isForStudent, remarks: remarks, isActive: isActive, createdDate: createdDate, upadatedDate: upadatedDate, createdUserId: createdUserId, updatedUserId: updatedUserId, isDeleted: isDeleted, deletedUserId: deletedUserId, deletedDate: deletedDate, IsDownload: isDownload)
                    
                    arrContentTemp.append(objContentModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrContentTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrContentTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Content", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    let objIDModel = IDModel(id: id)
                    arrContentTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrContentTemp
    }
    
    // Check content is exist or not
    func compareContent(arrContentDownload : [Content] , arrContentApplication : [IDModel]) -> [Content]{
        
        var arrNewContent = [Content]()
        if arrContentApplication.count > 0{
            for ContentObj in arrContentDownload{
                let filterObj = arrContentApplication.filter({ $0.id == ContentObj.contentId })
                if filterObj.count == 0{
                    arrNewContent.append(ContentObj)
                }
            }
        }else{
            arrNewContent.append(contentsOf: arrContentDownload)
        }
        return arrNewContent
    }

    //Insert content into database
    func insertContent(arrNewContent : [Content]) -> Bool{
        var isSuccess = false
        for newContent in arrNewContent {
            let isInsert = self.sqliteManager.insertTable(query: self.makeInsertQuery(Content: newContent))
            if isInsert {
                isSuccess = true
                //print("Inserted Successfully")
            }else{
                isSuccess = false
                print("Not Inserted")
                //break
            }
        }
        return isSuccess
    }
    
    
    //Insert Query
    func makeInsertQuery(Content : Content) -> String {
        
        return "INSERT INTO Content (ContentId, TopicId, ContentTypeId, ContentName, ContentDescription, ContentSummary, ContentGroupId, IsPasswordProtected, ContentPassword, PrePageContentId, NextPageContentId, ListOrder, IsForTeacher, IsForStudent, Remarks, IsActive, CreatedDate, UpdatedDate, CreatedUserId, UpdatedUserId, IsDeleted, DeletedUserId, DeletedDate, IsDownload) VALUES ('\(Content.contentId)','\(Content.topicId)','\(Content.contentTypeId)','\(Content.contentName.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.contentDescription.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.contentSummary.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.contentGroupId)','\(Content.isPasswordProtected)','\(Content.contentPassword.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.prePageContentId)','\(Content.nextPageContentId)','\(Content.lisOrder)','\(Content.isForTeacher)','\(Content.isForStudent)','\(Content.remarks.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.isActive)','\(Content.createdDate.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.upadatedDate.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.createdUserId)','\(Content.updatedUserId)','\(Content.isDeleted)','\(Content.deletedUserId)','\(Content.deletedDate.replacingOccurrences(of: "\'", with: "\'\'"))','\(Content.IsDownload)');"

    }
    
    //Delete BLM Query From Content  Table
    func makeDeleteContentQuery(chapterId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Content Where TopicId In(Select TopicId From Topic Where ChapterId = \(chapterId))")
        }
        return issuccess
    }
    
    
    //Delte Standard Query From Content Table
    func makeDeleteContentQueryFromStandard(standardId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Content Where TopicId In(Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId In(Select SubjectId From Subject Where StandardId = \(standardId))))")
        }
        return issuccess
    }
   
    
    //Delte Subject Query From Content Table
    func makeDeleteContentQueryFromSubject(subjectId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
        issuccess = sqliteManager.delete(query:"Delete From Content Where TopicId In(Select TopicId From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId = \(subjectId)))")
        }
        return issuccess
    }
  
    
    //UPDATE -----------------------------------
    func searchContentData(ContentId : Int32) -> Content{
        
        var objContent = Content()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Content where ContentId = \(ContentId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let contentId = sqlite3_column_int(queryStatement, 0)
                    let topicId = sqlite3_column_int(queryStatement, 1)
                    let contentTypeId = sqlite3_column_int(queryStatement, 2)
                    let contentName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let contentDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let contentSummary = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let contentGroupId = sqlite3_column_int(queryStatement, 6)
                    let isPasswordProtected = sqlite3_column_int(queryStatement, 7)
                    let contentPassword = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let prePageContentId = sqlite3_column_int(queryStatement, 9)
                    let nextPageContentId = sqlite3_column_int(queryStatement, 10)
                    let lisOrder = sqlite3_column_int(queryStatement, 11)
                    let isForTeacher = sqlite3_column_int(queryStatement, 12)
                    let isForStudent = sqlite3_column_int(queryStatement, 13)
                    let remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let isActive = sqlite3_column_int(queryStatement, 15)
                    let createdDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    let upadatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 17))
                    let createdUserId = sqlite3_column_int(queryStatement, 18)
                    let updatedUserId = sqlite3_column_int(queryStatement, 29)
                    let isDeleted = sqlite3_column_int(queryStatement, 20)
                    let deletedUserId = sqlite3_column_int(queryStatement, 21)
                    let deletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 22))
                    let isDownload = sqlite3_column_int(queryStatement, 23)
                    
                    objContent = Content(contentId: contentId , topicId: topicId, contentTypeId: contentTypeId, contentName: contentName, contentDescription: contentDescription, contentSummary: contentSummary, contentGroupId: contentGroupId, isPasswordProtected: isPasswordProtected, contentPassword: contentPassword, prePageContentId: prePageContentId, nextPageContentId: nextPageContentId, lisOrder: lisOrder, isForTeacher: isForTeacher, isForStudent: isForStudent, remarks: remarks, isActive: isActive, createdDate: createdDate, upadatedDate: upadatedDate, createdUserId: createdUserId, updatedUserId: updatedUserId, isDeleted: isDeleted, deletedUserId: deletedUserId, deletedDate: deletedDate, IsDownload: isDownload)
                    
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objContent
        
    }
    
    //Perform Update Operation
    func updateContentData(Content : Content) -> Bool{
        
        var isSuccess = false
        //Seach Content
       // let objContent = searchContentData(ContentId: Content.contentId)
        
        if Content.contentId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Content: Content))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Content: Content))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    
    //Make update query
    func makeUpdateQuery(Content : Content) -> String {
        
        return "UPDATE CONTENT SET TopicId = '\(Content.topicId)' ,  ContentTypeId = '\(Content.contentTypeId)' ,  ContentName = '\(Content.contentName.replacingOccurrences(of: "\'", with: "\'\'"))' , ContentDescription = '\(Content.contentDescription.replacingOccurrences(of: "\'", with: "\'\'"))' ,  ContentSummary = '\(Content.contentSummary.replacingOccurrences(of: "\'", with: "\'\'"))' ,  ContentGroupId = '\(Content.contentGroupId)' ,  IsPasswordProtected = '\(Content.isPasswordProtected)' ,  ContentPassword = '\(Content.contentPassword.replacingOccurrences(of: "\'", with: "\'\'"))' ,  PrePageContentId = '\(Content.prePageContentId)' ,  NextPageContentId = '\(Content.nextPageContentId)' ,  ListOrder = '\(Content.lisOrder)' ,  IsForTeacher = '\(Content.isForTeacher)' ,  IsForStudent = '\(Content.isForStudent)' ,  Remarks = '\(Content.remarks.replacingOccurrences(of: "\'", with: "\'\'"))' ,  IsActive = '\(Content.isActive)' ,  CreatedDate = '\(Content.createdDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  UpdatedDate = '\(Content.upadatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  CreatedUserId = '\(Content.createdUserId) ,  UpdatedUserId = \(Content.updatedUserId)' ,  IsDeleted = '\(Content.isDeleted) ,  DeletedUserId = \(Content.deletedUserId)' ,  DeletedDate = '\(Content.deletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , IsDownload = '\(Content.IsDownload)' where ContentId = '\(Content.contentId)'"
        
    }
    
    func getChapterContentsFromChapterId(chapterId : Int32) -> [Content]{
        
        let Query_Get_Content = "SELECT * FROM Content JOIN Topic ON Topic.TopicId = Content.TopicId AND Topic.IsDeleted = 0 AND Topic.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = Content.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 WHERE Content.IsDeleted = 0 AND Content.IsActive = 1 AND Content.ContentGroupId = 0 AND Topic.ChapterId = \(chapterId) GROUP BY Content.ContentId ORDER BY ListOrder, ContentId"

        var arrContentTemp = [Content]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Content, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let contentId = sqlite3_column_int(queryStatement, 0)
                    let topicId = sqlite3_column_int(queryStatement, 1)
                    let contentTypeId = sqlite3_column_int(queryStatement, 2)
                    let contentName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let contentDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let contentSummary = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let contentGroupId = sqlite3_column_int(queryStatement, 6)
                    let isPasswordProtected = sqlite3_column_int(queryStatement, 7)
                    let contentPassword = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let prePageContentId = sqlite3_column_int(queryStatement, 9)
                    let nextPageContentId = sqlite3_column_int(queryStatement, 10)
                    let lisOrder = sqlite3_column_int(queryStatement, 11)
                    let isForTeacher = sqlite3_column_int(queryStatement, 12)
                    let isForStudent = sqlite3_column_int(queryStatement, 13)
                    let remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 14))
                    let isActive = sqlite3_column_int(queryStatement, 15)
                    let createdDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    let upadatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 17))
                    let createdUserId = sqlite3_column_int(queryStatement, 18)
                    let updatedUserId = sqlite3_column_int(queryStatement, 29)
                    let isDeleted = sqlite3_column_int(queryStatement, 20)
                    let deletedUserId = sqlite3_column_int(queryStatement, 21)
                    let deletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 22))
                    let isDownload = sqlite3_column_int(queryStatement, 23)
                    
                    let objContentModel = Content(contentId: contentId , topicId: topicId, contentTypeId: contentTypeId, contentName: contentName, contentDescription: contentDescription, contentSummary: contentSummary, contentGroupId: contentGroupId, isPasswordProtected: isPasswordProtected, contentPassword: contentPassword, prePageContentId: prePageContentId, nextPageContentId: nextPageContentId, lisOrder: lisOrder, isForTeacher: isForTeacher, isForStudent: isForStudent, remarks: remarks, isActive: isActive, createdDate: createdDate, upadatedDate: upadatedDate, createdUserId: createdUserId, updatedUserId: updatedUserId, isDeleted: isDeleted, deletedUserId: deletedUserId, deletedDate: deletedDate, IsDownload: isDownload)
                    
                    arrContentTemp.append(objContentModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }
        return arrContentTemp
        
    }
    
   /* func updateDownloadContent(contentID:Int32)->Bool{
        
        let isUpdate = sqliteManager.updateTable(query: "UPDATE CONTENT SET IsDownload = 1 where ContentId = \(contentID)")
        if isUpdate == true {
            print("Update Content Data....")
        }
        else {
           print("Not Update Content Data...")
        }
       return isUpdate
    }*/
    
    //Perform Update Operation
    func updateDownloadContent(contentID:Int32,contentDescription:String)->Bool{
        
        var isSuccess = false
        //Seach Content
        let objContent = searchContentData(ContentId: contentID)
        
        if objContent.contentId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery1(Content: objContent,contentDescription:contentDescription))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery1(Content : Content,contentDescription:String) -> String {
        let isDownload:Int32 = 1
        return "UPDATE CONTENT SET IsDownload = '\(isDownload)' , ContentDescription = '\(contentDescription.replacingOccurrences(of: "\'", with: "\'\'"))' where ContentId = '\(Content.contentId)'"
    }
    
    func updateContentData1(content:Content)->Bool{
        
        var isSuccess = false
        //Seach Content
        let objContent = searchContentData(ContentId: content.contentId)
        
        if content.contentId != 0{ // if exist
            //Perform Update
            
            
            let oldContent = self.searchContentData(ContentId: content.contentId)
            
            var isUpdate  = Bool()
            if content.createdDate == oldContent.createdDate && content.upadatedDate == oldContent.upadatedDate  && content.deletedDate == oldContent.deletedDate {
                //No Change
                isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery2(Content: content))
            }
            else {
                //Change
                isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery3(Content: content))
                
            }
            
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    
    //Make update query
    func makeUpdateQuery2(Content : Content) -> String {
       
        return "UPDATE CONTENT SET TopicId = '\(Content.topicId)' , ContentTypeId = '\(Content.contentTypeId)' , ContentName = '\(Content.contentName.replacingOccurrences(of: "\'", with: "\'\'"))' ,  IsForTeacher = '\(Content.isForTeacher)' , IsForStudent = '\(Content.isForStudent)' , ListOrder = '\(Content.lisOrder)' , IsActive = '\(Content.isActive)' , IsDeleted = '\(Content.isDeleted) ' , CreatedDate = '\(Content.createdDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  UpdatedDate = '\(Content.upadatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , DeletedDate = '\(Content.deletedDate.replacingOccurrences(of: "\'", with: "\'\'"))'  where ContentId = '\(Content.contentId)'"
        
        //, IsDownload = '\(Content.IsDownload)'
    }
    
    //Make update query
    func makeUpdateQuery3(Content : Content) -> String {
       let isDownload:Int32 = 0
        return "UPDATE CONTENT SET TopicId = '\(Content.topicId)' , ContentTypeId = '\(Content.contentTypeId)' , ContentName = '\(Content.contentName.replacingOccurrences(of: "\'", with: "\'\'"))' ,  IsForTeacher = '\(Content.isForTeacher)' , IsForStudent = '\(Content.isForStudent)' , ListOrder = '\(Content.lisOrder)' , IsActive = '\(Content.isActive)' , IsDeleted = '\(Content.isDeleted) ' , CreatedDate = '\(Content.createdDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  UpdatedDate = '\(Content.upadatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , DeletedDate = '\(Content.deletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , IsDownload = '\(isDownload)'  where ContentId = '\(Content.contentId)'"
        
    }
    
}
