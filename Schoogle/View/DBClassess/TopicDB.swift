//
//  TopicDB.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class TopicDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrTopicDownloaded = [Topic]()
    var arrTopic = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    
    //Init with database method
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrTopicDownloaded = self.setDataIntoTopicModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrTopic = self.getOnlyIDsFromDatabase()
        
    }
    
    //Init with database method when Call Api and copyData
    init(arrTopicDoanloaded : [Topic]) {
        super.init()
        
        self.arrTopicDownloaded = arrTopicDoanloaded
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        self.arrTopic = self.getOnlyIDsFromDatabase()
        
    }
    //Insert data
    func insertTopicData() -> Bool{
        var isSuccess = false
        
        if arrTopic.count > 0 {
            
            let arrOfNewTopic = self.compareTopic(arrTopicDownload: arrTopicDownloaded, arrTopicApplication: arrTopic)
            
            if arrOfNewTopic.count > 0 {
                isSuccess = self.insertTopic(arrNewTopic: arrOfNewTopic)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }else{
            if arrTopicDownloaded.count > 0 {
                isSuccess = self.insertTopic(arrNewTopic: arrTopicDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }
        arrTopic.removeAll()
        arrTopicDownloaded.removeAll()
        print("Topic Done")
        sqliteManager.closeDB()
        return isSuccess

    }
    
    //Download data and insert into model
    func setDataIntoTopicModel() -> [Topic] {
        
        var arrTopicTemp = [Topic]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Topic", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let TopicId = sqlite3_column_int(queryStatement, 0)
                    let ChapterId = sqlite3_column_int(queryStatement, 1)
                    let TopicName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let TopicAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let IsActive = sqlite3_column_int(queryStatement, 8)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 11)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 12)
                    let IsDeleted = sqlite3_column_int(queryStatement, 13)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 14)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let objTopicModel = Topic(TopicId: TopicId, ChapterId: ChapterId, TopicName: TopicName, TopicAlias: TopicAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                    arrTopicTemp.append(objTopicModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrTopicTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrTopicTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Topic", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrTopicTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrTopicTemp
    }
    
    // Check Topic is exist or not
    func compareTopic(arrTopicDownload : [Topic] , arrTopicApplication : [IDModel]) -> [Topic]{
        
        var arrNewTopic = [Topic]()
        if arrTopicApplication.count > 0{
            for TopicObj in arrTopicDownload{
                let filterObj = arrTopicApplication.filter({ $0.id == TopicObj.TopicId })
                if filterObj.count == 0{
                    arrNewTopic.append(TopicObj)
                }
            }
        }else{
            arrNewTopic.append(contentsOf: arrTopicDownload)
        }
        return arrNewTopic
    }
    
    //Insert Topic into database
    func insertTopic(arrNewTopic : [Topic]) -> Bool{
        var isSuccess = false
        for newTopic in arrNewTopic {
                let isInsert = self.sqliteManager.insertTable(query: self.makeInsertQuery(topic: newTopic))
                if isInsert {
                    isSuccess = true
                    //print("Inserted Successfully")
                }else{
                    isSuccess = false
                    print("Not Inserted")
                }
            }
        return isSuccess
    }
    
    
    //Insert Query
    func makeInsertQuery(topic : Topic) -> String {
        
        return "INSERT INTO Topic (TopicId, ChapterId, TopicName, TopicAlias, ShortDescription, LongDescription, ListOrder, Remarks, IsActive, CreatedDate, UpdatedDate, CreatedUserId, UpdatedUserId, IsDeleted, DeletedUserId, DeletedDate) VALUES (\(topic.TopicId),\(topic.ChapterId),'\(topic.TopicName.replacingOccurrences(of: "\'", with: "\'\'"))','\(topic.TopicAlias)','\(topic.ShortDescription)','\(topic.LongDescription)',\(topic.ListOrder),'\(topic.Remarks)',\(topic.IsActive),'\(topic.CreatedDate)','\(topic.UpdatedDate)',\(topic.CreatedUserId),\(topic.UpdatedUserId),\(topic.IsDeleted),\(topic.DeletedUserId),'\(topic.DeletedDate)');"
        
    }

    //Delete Query From Topic Table From Chapter
    func makeDeleteTopic(chapterId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Topic Where ChapterId In(Select ChapterId = \(chapterId))")
        }
        return issuccess
    }

    
    //Delete Query From Topic Table From Standard
    func makeDeleteTopicFromStandard(standardId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId In(Select SubjectId From Subject Where StandardId = \(standardId)))")
        }
        return issuccess
    }
    
    
    //Delete Query From Topic Table From Subject
    func makeDeleteTopicFromSubject(subjectId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From Topic Where ChapterId In(Select ChapterId From Chapter Where SubjectId = \(subjectId))")
        }
        return issuccess
    }
    
    
    //UPDATE -----------------------------------
    func searchTopicData(TopicId : Int32) -> Topic{
        
        var objTopic = Topic()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Topic where TopicId = \(TopicId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let TopicId = sqlite3_column_int(queryStatement, 0)
                    let ChapterId = sqlite3_column_int(queryStatement, 1)
                    let TopicName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let TopicAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let IsActive = sqlite3_column_int(queryStatement, 8)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 11)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 12)
                    let IsDeleted = sqlite3_column_int(queryStatement, 13)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 14)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    
                    objTopic = Topic(TopicId: TopicId, ChapterId: ChapterId, TopicName: TopicName, TopicAlias: TopicAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objTopic
        
    }
    
    //Perform Update Operation
    func updateTopicData(Topic : Topic) -> Bool{
        
        var isSuccess = false
        //Seach Topic
        let objTopic = searchTopicData(TopicId: Topic.TopicId)
        
        if Topic.TopicId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Topic: Topic))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(topic: Topic))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    
    //Make update query
    func makeUpdateQuery(Topic : Topic) -> String {
        
        return "UPDATE TOPIC SET ChapterId = '\(Topic.ChapterId)' , TopicName = '\(Topic.TopicName.replacingOccurrences(of: "\'", with: "\'\'"))' , TopicAlias = '\(Topic.TopicAlias.replacingOccurrences(of: "\'", with: "\'\'"))' , ShortDescription = '\(Topic.ShortDescription.replacingOccurrences(of: "\'", with: "\'\'"))' , LongDescription = '\(Topic.LongDescription.replacingOccurrences(of: "\'", with: "\'\'"))' , ListOrder = '\(Topic.ListOrder)' , Remarks = '\(Topic.Remarks.replacingOccurrences(of: "\'", with: "\'\'"))' , IsActive = '\(Topic.IsActive)' , CreatedDate = '\(Topic.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , UpdatedDate = '\(Topic.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , CreatedUserId = '\(Topic.CreatedUserId)' , UpdatedUserId = '\(Topic.UpdatedUserId)' , IsDeleted = '\(Topic.IsDeleted)' , DeletedUserId = '\(Topic.DeletedUserId)' , DeletedDate = '\(Topic.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))'  where TopicId = '\(Topic.TopicId)'"
        
    }
    
    //MARK: GET CHAPTER INFO
    
    func getChapterInfoFromChapterId(strQuery: String){
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: strQuery, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    Constant.CHAPTER_NAME  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 0))
                    Constant.CHAPTER_IMAGE = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    Constant.SUBJECT_NAME  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    Constant.SUBJECT_IMAGE = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    Constant.STANDARD_NAME     = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    Constant.STANDARD_IMAGE    = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    Constant.BOARD_NAME    = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    Constant.BOARD_IMAGE  = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    Constant.CHAPTER_ID   = sqlite3_column_int(queryStatement, 8)
                    Constant.SUBJECT_ID   = sqlite3_column_int(queryStatement, 9)
                    Constant.STANDARD_ID   = sqlite3_column_int(queryStatement, 10)
                    Constant.BOARD_ID  = sqlite3_column_int(queryStatement, 11)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
    }
    
    func getTopicDataFromId(chapterID : Int32) -> [Topic]{
        
        let Query_Get_Topic_Data = "SELECT * FROM TOPIC WHERE IsDeleted = 0 AND IsActive = 1 AND ChapterId = \(chapterID) ORDER BY ListOrder, TopicId"
        
        var arrTopicTemp = [Topic]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Topic_Data, success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let TopicId = sqlite3_column_int(queryStatement, 0)
                    let ChapterId = sqlite3_column_int(queryStatement, 1)
                    let TopicName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let TopicAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let IsActive = sqlite3_column_int(queryStatement, 8)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 9))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 11)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 12)
                    let IsDeleted = sqlite3_column_int(queryStatement, 13)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 14)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 15))
                    let objTopicModel = Topic(TopicId: TopicId, ChapterId: ChapterId, TopicName: TopicName, TopicAlias: TopicAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                    arrTopicTemp.append(objTopicModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrTopicTemp

    }

    
}
