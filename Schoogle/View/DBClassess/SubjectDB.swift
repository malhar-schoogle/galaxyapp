//
//  SubjectDB.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class SubjectDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrSubjectDownloaded = [Subject]()
    var arrSubject = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    
    //Init with database method
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrSubjectDownloaded = self.setDataIntoSubjectModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrSubject = self.getOnlyIDsFromDatabase()
    }

    //Init with database method when Call Api and copyData
    init(arrSubjectDoanloaded : [Subject]) {
        super.init()
        
        self.arrSubjectDownloaded = arrSubjectDoanloaded
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        self.arrSubject = self.getOnlyIDsFromDatabase()
        
    }
    //Insert data
    func insertSubjectData() -> Bool{
        var isSuccess = false
        
        if arrSubject.count > 0{
            let arrOfNewSubject = self.compareSubject(arrSubjectDownload: arrSubjectDownloaded, arrSubjectApplication: arrSubject)
            
            if arrOfNewSubject.count > 0 {
                isSuccess = self.insertSubject(arrNewSubject: arrOfNewSubject)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }else{
            
            if arrSubjectDownloaded.count > 0 {
                isSuccess = self.insertSubject(arrNewSubject: arrSubjectDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }
        
        arrSubject.removeAll()
        arrSubjectDownloaded.removeAll()
        print("Subject Done")
        return isSuccess
        
    }
    
    //Download data and insert into model
    func setDataIntoSubjectModel() -> [Subject] {
        
        var arrSubjectTemp = [Subject]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Subject", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let SubjectId = sqlite3_column_int(queryStatement, 0)
                    let StandardId = sqlite3_column_int(queryStatement, 1)
                    let SubjectName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let SubjectAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let SubjectImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let IsActive = sqlite3_column_int(queryStatement, 9)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                    let IsDeleted = sqlite3_column_int(queryStatement, 14)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    
                    let objSubjectModel = Subject(SubjectId: SubjectId, StandardId: StandardId, SubjectName: SubjectName, SubjectAlias: SubjectAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, SubjectImage: SubjectImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                    arrSubjectTemp.append(objSubjectModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrSubjectTemp
        
    }
    
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrMediaTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Subject", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrMediaTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
    }
    
    // Check Subject is exist or not
    func compareSubject(arrSubjectDownload : [Subject] , arrSubjectApplication : [IDModel]) -> [Subject]{
        
        var arrNewSubject = [Subject]()
        if arrSubjectApplication.count > 0{
            for SubjectObj in arrSubjectDownload{
                let filterObj = arrSubjectApplication.filter({ $0.id == SubjectObj.SubjectId })
                if filterObj.count == 0{
                    arrNewSubject.append(SubjectObj)
                }
            }
        }else{
            arrNewSubject.append(contentsOf: arrSubjectDownload)
        }
        return arrNewSubject
    }
    
    //Insert Subject into database
    func insertSubject(arrNewSubject : [Subject]) -> Bool{
        var isSuccess = false
        for newSubject in arrNewSubject {
                let isInsert = self.sqliteManager.insertTable(query: self.makeInsertQuery(Subject: newSubject))
                if isInsert {
                    //print("Inserted Successfully")
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
            }
        return isSuccess
    }
    
    
    //Insert Query
    func makeInsertQuery(Subject : Subject) -> String {
        
        return "INSERT INTO Subject (SubjectId, StandardId, SubjectName, SubjectAlias, ShortDescription, LongDescription, ListOrder, SubjectImage, Remarks, IsActive, CreatedDate, UpdatedDate, CreatedUserId, UpdatedUserId, IsDeleted, DeletedUserId, DeletedDate) VALUES (\(Subject.SubjectId),\(Subject.StandardId),'\(Subject.SubjectName.replacingOccurrences(of: "\'", with: "\'\'"))','\(Subject.SubjectAlias)','\(Subject.ShortDescription)','\(Subject.LongDescription)',\(Subject.ListOrder),'\(Subject.SubjectImage)','\(Subject.Remarks)',\(Subject.IsActive),'\(Subject.CreatedDate)','\(Subject.UpdatedDate)',\(Subject.CreatedUserId),\(Subject.UpdatedUserId),\(Subject.IsDeleted),\(Subject.DeletedUserId),'\(Subject.DeletedDate)');"
        
    }

    //Delete Subject query From BLM
    func makeDeleteSubjectQuery(subjectid:Int) -> Bool{

        var issuccess = false
        if sqliteManager.openDatabase() != nil{
          issuccess = sqliteManager.delete(query:"delete from Subject where SubjectId = \(subjectid)")
        }
        return issuccess
    }
    
    //DELETE Query From Standard
    func makeDeleteSubjectFromStandard(StandardId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"delete from Subject where SubjectId = \(StandardId)")
        }
        return issuccess
    }

    //UPDATE -----------------------------------
    func searchSubjectData(SubjectId : Int32) -> Subject{
        
        var objSubject = Subject()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from Subject where SubjectId = \(SubjectId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let SubjectId = sqlite3_column_int(queryStatement, 0)
                    let StandardId = sqlite3_column_int(queryStatement, 1)
                    let SubjectName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let SubjectAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let SubjectImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let IsActive = sqlite3_column_int(queryStatement, 9)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                    let IsDeleted = sqlite3_column_int(queryStatement, 14)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    
                    objSubject = Subject(SubjectId: SubjectId, StandardId: StandardId, SubjectName: SubjectName, SubjectAlias: SubjectAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, SubjectImage: SubjectImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Something went wrong")
        }

        return objSubject

    }
    
    //Perform Update Operation
    func updateSubjectData(Subject : Subject) -> Bool{
        
        var isSuccess = false
        //Seach Subject
        let objSubject = searchSubjectData(SubjectId: Subject.SubjectId)
        
        if Subject.SubjectId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(Subject: Subject))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(Subject: Subject))
            //delete
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    
    //Make update query
    func makeUpdateQuery(Subject : Subject) -> String {
        
        return "UPDATE Subject SET StandardId = '\(Subject.StandardId)' ,  SubjectName = '\(Subject.SubjectName.replacingOccurrences(of: "\'", with: "\'\'"))' ,  SubjectAlias = '\(Subject.SubjectAlias.replacingOccurrences(of: "\'", with: "\'\'"))' ,  ShortDescription = '\(Subject.ShortDescription.replacingOccurrences(of: "\'", with: "\'\'"))' ,  LongDescription = '\(Subject.LongDescription.replacingOccurrences(of: "\'", with: "\'\'"))' ,  ListOrder = '\(Subject.ListOrder)' ,  SubjectImage = '\(Subject.SubjectImage.replacingOccurrences(of: "\'", with: "\'\'"))' ,  Remarks = '\(Subject.Remarks.replacingOccurrences(of: "\'", with: "\'\'"))' ,  IsActive = '\(Subject.IsActive)' ,  CreatedDate = '\(Subject.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  UpdatedDate = '\(Subject.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' ,  CreatedUserId = '\(Subject.CreatedUserId)' ,  UpdatedUserId = '\(Subject.UpdatedUserId)' ,  IsDeleted = '\(Subject.IsDeleted)' ,  DeletedUserId = '\(Subject.DeletedUserId)' ,  DeletedDate = '\(Subject.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' where SubjectId = '\(Subject.SubjectId)'"
        
    }
    
    //Get Subject from ID's
    func getSubjectDataFromID(SubjectId : Int32) -> Subject{
        
        var objSubject = Subject()
        
        if sqliteManager.openDatabase() != nil{
            
            sqliteManager.selectTable(query: "SELECT * FROM SUBJECT WHERE IsDeleted = 0 AND IsActive = 1 AND SubjectId = \(SubjectId) ORDER BY ListOrder, SubjectId", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let SubjectId = sqlite3_column_int(queryStatement, 0)
                    let StandardId = sqlite3_column_int(queryStatement, 1)
                    let SubjectName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let SubjectAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let SubjectImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let IsActive = sqlite3_column_int(queryStatement, 9)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                    let IsDeleted = sqlite3_column_int(queryStatement, 14)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    
                    objSubject = Subject(SubjectId: SubjectId, StandardId: StandardId, SubjectName: SubjectName, SubjectAlias: SubjectAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, SubjectImage: SubjectImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objSubject
        
    }

    //Get Subject from Standard ID
    func getSubjectDataFromStandardID(StandardID : Int32) -> [Subject]{
        
        var arrSubject = [Subject]()
        
        if sqliteManager.openDatabase() != nil{
            //----Query2: to get Subject of Standard
            sqliteManager.selectTable(query: "Select * From Subject Where StandardId = \(StandardID) And IsDeleted = 0", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let SubjectId = sqlite3_column_int(queryStatement, 0)
                    let StandardId = sqlite3_column_int(queryStatement, 1)
                    let SubjectName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let SubjectAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 3))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let ListOrder = sqlite3_column_int(queryStatement, 6)
                    let SubjectImage = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 8))
                    let IsActive = sqlite3_column_int(queryStatement, 9)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 11))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 12)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 13)
                    let IsDeleted = sqlite3_column_int(queryStatement, 14)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 15)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 16))
                    
                  let  objSubject = Subject(SubjectId: SubjectId, StandardId: StandardId, SubjectName: SubjectName, SubjectAlias: SubjectAlias, ShortDescription: ShortDescription, LongDescription: LongDescription, ListOrder: ListOrder, SubjectImage: SubjectImage, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                
                    arrSubject.append(objSubject)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return arrSubject
        
    }
}
