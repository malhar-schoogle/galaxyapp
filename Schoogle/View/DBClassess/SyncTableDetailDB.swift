//
//  SyncTableDetailDB.swift
//  Schoogle
//
//  Created by Malhar on 14/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class SyncTableDetailDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrSyncTableDetailDownloaded = [SyncTableDetail]()
    var arrSyncTableDetail = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    //Init with database method when Download database and copyData
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrSyncTableDetailDownloaded = self.setDataIntoSyncTableDetailModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrSyncTableDetail = self.getOnlyIDsFromDatabase()
        
    }
    
    
    //Init with database method when Call Api and copyData
    init(arrSyncTableDetailDoanloaded : [SyncTableDetail]) {
        super.init()
        
        arrSyncTableDetailDownloaded = arrSyncTableDetailDoanloaded
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrSyncTableDetail = self.getOnlyIDsFromDatabase()
        
    }
    
    //Insert data
    func insertSyncTableDetailData() -> Bool{
        var isSuccess = false
        
        if arrSyncTableDetail.count > 0 {
            let arrOfNewSyncTableDetail = self.compareSyncTableDetail(arrSyncTableDetailDownload: arrSyncTableDetailDownloaded, arrSyncTableDetailApplication: arrSyncTableDetail)
            
            if arrOfNewSyncTableDetail.count > 0 {
                isSuccess = self.insertSyncTableDetail(arrNewSyncTableDetail: arrOfNewSyncTableDetail)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }else{
            
            if arrSyncTableDetailDownloaded.count > 0 {
                isSuccess = self.insertSyncTableDetail(arrNewSyncTableDetail: arrSyncTableDetailDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }
        
        //        sqliteManager.closeDB()
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoSyncTableDetailModel() -> [SyncTableDetail] {
        
        var arrSyncTableDetailTemp = [SyncTableDetail]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from SyncTableDetail", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let TableId = sqlite3_column_int(queryStatement, 0)
                    let TableName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let SyncTableModeId = sqlite3_column_int(queryStatement, 2)
                    
                    let objSyncTableDetailModel = SyncTableDetail(TableId: TableId, TableName: TableName, SyncTableModeId: SyncTableModeId)
                    
                    arrSyncTableDetailTemp.append(objSyncTableDetailModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrSyncTableDetailTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrSyncTableDetailTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from SyncTableDetail", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrSyncTableDetailTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrSyncTableDetailTemp
    }
    
    // Check SyncTableDetail is exist or not
    func compareSyncTableDetail(arrSyncTableDetailDownload : [SyncTableDetail] , arrSyncTableDetailApplication : [IDModel]) -> [SyncTableDetail]{
        
        var arrNewSyncTableDetail = [SyncTableDetail]()
        if arrSyncTableDetailApplication.count > 0{
            for SyncTableDetailObj in arrSyncTableDetailDownload{
                let filterObj = arrSyncTableDetailApplication.filter({ $0.id == SyncTableDetailObj.TableId })
                if filterObj.count == 0{
                    arrNewSyncTableDetail.append(SyncTableDetailObj)
                }
            }
        }else{
            arrNewSyncTableDetail.append(contentsOf: arrSyncTableDetailDownload)
        }
        return arrNewSyncTableDetail
    }
    
    //Insert SyncTableDetail into database
    func insertSyncTableDetail(arrNewSyncTableDetail : [SyncTableDetail]) -> Bool{
        var isSuccess = false
        for newSyncTableDetail in arrNewSyncTableDetail {
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(SyncTableDetail: newSyncTableDetail))
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
                break
            }
        }
        arrSyncTableDetail.removeAll()
        arrSyncTableDetailDownloaded.removeAll()
        print("SyncTableDetail Done")
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(SyncTableDetail : SyncTableDetail) -> String {
        
        return "INSERT INTO SyncTableDetail(TableId,TableName,SyncTableModeId) VALUES (\(SyncTableDetail.TableId),'\(SyncTableDetail.TableName)',\(SyncTableDetail.SyncTableModeId));"
        
    }
    
    
    //UPDATE -----------------------------------
    func searchSyncTableDetailData(SyncTableDetailId : Int32) -> SyncTableDetail{
        
        var objSyncTableDetail = SyncTableDetail()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from SyncTableDetail where SyncTableDetailId = \(SyncTableDetailId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW)    {
                    
                    let TableId = sqlite3_column_int(queryStatement, 0)
                    let TableName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let SyncTableModeId = sqlite3_column_int(queryStatement, 2)
                    
                    objSyncTableDetail = SyncTableDetail(TableId: TableId, TableName: TableName, SyncTableModeId: SyncTableModeId)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objSyncTableDetail
        
    }
    
    //Perform Update Operation
    func updateSyncTableDetailData(SyncTableDetail : SyncTableDetail) -> Bool{
        
        var isSuccess = false
        //Seach SyncTableDetail
        let objSyncTableDetail = searchSyncTableDetailData(SyncTableDetailId: SyncTableDetail.TableId)
        
        if objSyncTableDetail.TableId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(SyncTableDetail: objSyncTableDetail))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(SyncTableDetail: SyncTableDetail))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery(SyncTableDetail : SyncTableDetail) -> String {
        
        return "UPDATE SyncTableDetail SET TableName = '\(SyncTableDetail.TableName)', SyncTableModeId = \(SyncTableDetail.SyncTableModeId) where SyncTableDetailId = \(SyncTableDetail.TableId)"
        
    }
    
}
