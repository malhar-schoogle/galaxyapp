//
//  ContentTypeDB.swift
//  Schoogle
//
//  Created by Malhar on 10/12/18.
//  Copyright © 2018 Malhar. All rights reserved.
//

import UIKit
import SQLite
class ContentTypeDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrContentTypeDownloaded = [ContentType]()
    var arrContentType = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    
    //Init with database method
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrContentTypeDownloaded = self.setDataIntoContentTypeModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrContentType = self.getOnlyIDsFromDatabase()
        
    }
    //Init with database method when Call Api and copyData
    init(arrContentTypeDoanloaded : [ContentType]) {
        super.init()
        
        self.arrContentTypeDownloaded = arrContentTypeDoanloaded
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        self.arrContentType = self.getOnlyIDsFromDatabase()
        
    }
    //Insert data
    func insertContentTypeData() -> Bool{
        var isSuccess = false
        
        if arrContentType.count > 0{
            let arrOfNewContentType = self.compareContentType(arrContentTypeDownload: arrContentTypeDownloaded, arrContentTypeApplication: arrContentType)
            
            if arrOfNewContentType.count > 0 {
                isSuccess = self.insertContentType(arrNewContentType: arrOfNewContentType)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }else{
            if arrContentTypeDownloaded.count > 0 {
                isSuccess = self.insertContentType(arrNewContentType: arrContentTypeDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }
        arrContentType.removeAll()
        arrContentTypeDownloaded.removeAll()
        print("ContentType Done")
        sqliteManager.closeDB()
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoContentTypeModel() -> [ContentType] {
        
        var arrContentTypeTemp = [ContentType]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from ContentType", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ContentTypeId = sqlite3_column_int(queryStatement, 0)
                    let ContentTypeName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let ContentTypeAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let StandardId = sqlite3_column_int(queryStatement, 3)
                    let ContentFiles = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let IsForTeacher = sqlite3_column_int(queryStatement, 7)
                    let IsForStudent = sqlite3_column_int(queryStatement, 8)
                    let ListOrder = sqlite3_column_int(queryStatement, 9)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    let SubjectId = sqlite3_column_int(queryStatement, 19)
                    
                    
                    let objContentTypeModel = ContentType(ContentTypeId: ContentTypeId, ContentTypeName: ContentTypeName, ContentTypeAlias: ContentTypeAlias, StandardId: StandardId, ContentFiles: ContentFiles, ShortDescription: ShortDescription, LongDescription: LongDescription, IsForTeacher: IsForTeacher, IsForStudent: IsForStudent, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, SubjectId: SubjectId)
                    
                    arrContentTypeTemp.append(objContentTypeModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrContentTypeTemp
    }
    
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrMediaTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from ContentType", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrMediaTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTemp
    }
    
    // Check ContentType is exist or not
    func compareContentType(arrContentTypeDownload : [ContentType] , arrContentTypeApplication : [IDModel]) -> [ContentType]{
        
        var arrNewContentType = [ContentType]()
        if arrContentTypeApplication.count > 0{
            for ContentTypeObj in arrContentTypeDownload{
                let filterObj = arrContentTypeApplication.filter({ $0.id == ContentTypeObj.ContentTypeId })
                if filterObj.count == 0{
                    arrNewContentType.append(ContentTypeObj)
                }
            }
        }else{
            arrNewContentType.append(contentsOf: arrContentTypeDownload)
        }
        return arrNewContentType
    }
    
    //Insert ContentType into database
    func insertContentType(arrNewContentType : [ContentType]) -> Bool{
        var isSuccess = false
        for newContentType in arrNewContentType {
                let isInsert = self.sqliteManager.insertTable(query: self.makeInsertQuery(ContentType: newContentType))
                if isInsert {
                    //print("Inserted Successfully")
                    isSuccess = true
                }else{
                    print("Not Inserted")
                    isSuccess = false
                }
            }
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(ContentType : ContentType) -> String {
        
        return "INSERT INTO ContentType(ContentTypeId, ContentTypeName, ContentTypeAlias,StandardId,ContentFiles,ShortDescription,LongDescription,IsForTeacher,IsForStudent,ListOrder,Remarks,IsActive,CreatedDate,UpdatedDate,CreatedUserId,UpdatedUserId,IsDeleted,DeletedUserId,DeletedDate,SubjectId) VALUES (\(ContentType.ContentTypeId), '\(ContentType.ContentTypeName.replacingOccurrences(of: "\'", with: "\'\'"))', '\(ContentType.ContentTypeAlias)', \(ContentType.StandardId), '\(ContentType.ContentFiles)', '\(ContentType.ShortDescription)', '\(ContentType.LongDescription)', \(ContentType.IsForTeacher), \(ContentType.IsForStudent), \(ContentType.ListOrder), '\(ContentType.Remarks)', \(ContentType.IsActive), '\(ContentType.CreatedDate)', '\(ContentType.UpdatedDate)', \(ContentType.CreatedUserId), \(ContentType.UpdatedUserId), \(ContentType.IsDeleted), \(ContentType.DeletedUserId), '\(ContentType.DeletedDate)', \(ContentType.SubjectId));"
        
    }
    
    
    //UPDATE -----------------------------------
    func searchContentTypeData(ContentTypeId : Int32) -> ContentType{
        
        var objContentType = ContentType()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from ContentType where ContentTypeId = \(ContentTypeId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ContentTypeId = sqlite3_column_int(queryStatement, 0)
                    let ContentTypeName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let ContentTypeAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let StandardId = sqlite3_column_int(queryStatement, 3)
                    let ContentFiles = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let IsForTeacher = sqlite3_column_int(queryStatement, 7)
                    let IsForStudent = sqlite3_column_int(queryStatement, 8)
                    let ListOrder = sqlite3_column_int(queryStatement, 9)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    let SubjectId = sqlite3_column_int(queryStatement, 19)
                    
                    
                    objContentType = ContentType(ContentTypeId: ContentTypeId, ContentTypeName: ContentTypeName, ContentTypeAlias: ContentTypeAlias, StandardId: StandardId, ContentFiles: ContentFiles, ShortDescription: ShortDescription, LongDescription: LongDescription, IsForTeacher: IsForTeacher, IsForStudent: IsForStudent, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, SubjectId: SubjectId)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objContentType
        
    }
    
    //Perform Update Operation
    func updateContentTypeData(ContentType : ContentType) -> Bool{
        
        var isSuccess = false
        //Seach ContentType
        let objContentType = searchContentTypeData(ContentTypeId: ContentType.ContentTypeId)
        
        if ContentType.ContentTypeId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(ContentType: ContentType))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(ContentType: ContentType))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
        }
        return isSuccess
    }
    
    //Delete Query From Content Media Table
    func makeDeleteContentType(StandardId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From ContentType Where StandardId = \(StandardId)")
        }
        return issuccess
    }
    
    
    //Delete Query From Content Media Table
    func makeDeleteContentTypeFromSubject(SubjectId:Int) -> Bool{
        
        var issuccess = false
        if sqliteManager.openDatabase() != nil{
            issuccess = sqliteManager.delete(query:"Delete From ContentType Where SubjectId = \(SubjectId)")
        }
        return issuccess
    }
    
    
    //Make update query
    func makeUpdateQuery(ContentType : ContentType) -> String {
        
        return "UPDATE ContentType SET ContentTypeName = '\(ContentType.ContentTypeName.replacingOccurrences(of: "\'", with: "\'\'"))' ,  ContentTypeAlias = '\(ContentType.ContentTypeAlias.replacingOccurrences(of: "\'", with: "\'\'"))' , StandardId = '\(ContentType.StandardId)' , ContentFiles = '\(ContentType.ContentFiles.replacingOccurrences(of: "\'", with: "\'\'"))' , ShortDescription = '\(ContentType.ShortDescription.replacingOccurrences(of: "\'", with: "\'\'"))' , LongDescription = '\(ContentType.LongDescription.replacingOccurrences(of: "\'", with: "\'\'"))' , IsForTeacher = '\(ContentType.IsForTeacher)' , IsForStudent = '\(ContentType.IsForStudent)' , ListOrder = '\(ContentType.ListOrder)' , Remarks = '\(ContentType.Remarks.replacingOccurrences(of: "\'", with: "\'\'"))' , IsActive = '\(ContentType.IsActive)' , CreatedDate = '\(ContentType.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , UpdatedDate = '\(ContentType.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , CreatedUserId = '\(ContentType.CreatedUserId)' , UpdatedUserId = '\(ContentType.UpdatedUserId)' , IsDeleted = '\(ContentType.IsDeleted)' , DeletedUserId = '\(ContentType.DeletedUserId)' , DeletedDate = '\(ContentType.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , SubjectId = '\(ContentType.SubjectId)'  where ContentTypeId = '\(ContentType.ContentTypeId)'"
        
    }
    
    func getContentTypeDataFromChapterId(chapterID : Int32) -> [ContentType]{
        
        var arrContentTypeTemp = [ContentType]()
//        SELECT DISTINCT CT.* FROM Chapter AS CH JOIN Topic AS T ON T.ChapterID = CH.ChapterId AND T.IsDeleted = 0 AND T.IsActive = 1 JOIN Content AS C ON C.TopicId = T.TopicID AND C.IsDeleted = 0 AND C.IsActive = 1 JOIN ContentType AS CT ON CT.ContentTypeID = C.ContentTypeId AND CT.IsDeleted = 0 AND CT.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = C.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 WHERE 1 = 1 AND CH.IsDeleted = 0 AND CH.IsActive = 1 AND CH.ChapterId = 560 ORDER BY CT.ListOrder, CT.ContentTypeID
        let Query_Get_Content_Menu = "SELECT DISTINCT CT.* FROM Chapter AS CH JOIN Topic AS T ON T.ChapterID = CH.ChapterId AND T.IsDeleted = 0 AND T.IsActive = 1 JOIN Content AS C ON C.TopicId = T.TopicID AND C.IsDeleted = 0 AND C.IsActive = 1 JOIN ContentType AS CT ON CT.ContentTypeID = C.ContentTypeId AND CT.IsDeleted = 0 AND CT.IsActive = 1 JOIN UserAllocation AS UA ON UA.ContentId = C.ContentId AND UA.IsDeleted = 0 AND UA.IsActive = 1 WHERE 1 = 1 AND CH.IsDeleted = 0 AND CH.IsActive = 1 AND CH.ChapterId = \(chapterID) ORDER BY CT.ListOrder, CT.ContentTypeID"
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: Query_Get_Content_Menu , success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let ContentTypeId = sqlite3_column_int(queryStatement, 0)
                    let ContentTypeName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let ContentTypeAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let StandardId = sqlite3_column_int(queryStatement, 3)
                    let ContentFiles = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                    let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let IsForTeacher = sqlite3_column_int(queryStatement, 7)
                    let IsForStudent = sqlite3_column_int(queryStatement, 8)
                    let ListOrder = sqlite3_column_int(queryStatement, 9)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                    let IsActive = sqlite3_column_int(queryStatement, 11)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                    let IsDeleted = sqlite3_column_int(queryStatement, 16)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                    let SubjectId = sqlite3_column_int(queryStatement, 19)
                    
                    
                    let objContentTypeModel = ContentType(ContentTypeId: ContentTypeId, ContentTypeName: ContentTypeName, ContentTypeAlias: ContentTypeAlias, StandardId: StandardId, ContentFiles: ContentFiles, ShortDescription: ShortDescription, LongDescription: LongDescription, IsForTeacher: IsForTeacher, IsForStudent: IsForStudent, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, SubjectId: SubjectId)
                    
                    arrContentTypeTemp.append(objContentTypeModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrContentTypeTemp
        
    }
    func getContentTypeData(query:String) -> [ContentType]{
            
            var arrContentTypeTemp = [ContentType]()
    let Query_Get_Content = query
            
            if sqliteManager.openDatabase() != nil{
                sqliteManager.selectTable(query: Query_Get_Content , success: { (queryStatement) in
                    while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                        
                        let ContentTypeId = sqlite3_column_int(queryStatement, 0)
                        let ContentTypeName = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                        let ContentTypeAlias = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                        let StandardId = sqlite3_column_int(queryStatement, 3)
                        let ContentFiles = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                        let ShortDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 5))
                        let LongDescription = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                        let IsForTeacher = sqlite3_column_int(queryStatement, 7)
                        let IsForStudent = sqlite3_column_int(queryStatement, 8)
                        let ListOrder = sqlite3_column_int(queryStatement, 9)
                        let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 10))
                        let IsActive = sqlite3_column_int(queryStatement, 11)
                        let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                        let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 13))
                        let CreatedUserId = sqlite3_column_int(queryStatement, 14)
                        let UpdatedUserId = sqlite3_column_int(queryStatement, 15)
                        let IsDeleted = sqlite3_column_int(queryStatement, 16)
                        let DeletedUserId = sqlite3_column_int(queryStatement, 17)
                        let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 18))
                        let SubjectId = sqlite3_column_int(queryStatement, 19)
                        
                        
                        let objContentTypeModel = ContentType(ContentTypeId: ContentTypeId, ContentTypeName: ContentTypeName, ContentTypeAlias: ContentTypeAlias, StandardId: StandardId, ContentFiles: ContentFiles, ShortDescription: ShortDescription, LongDescription: LongDescription, IsForTeacher: IsForTeacher, IsForStudent: IsForStudent, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate, SubjectId: SubjectId)
                        
                        arrContentTypeTemp.append(objContentTypeModel)
                    }
                    
                }){ (error) in
                    print(error as Any)
                }
            }else{
                print("Somethinf went wrong")
            }
            return arrContentTypeTemp
            
        }
    
}
