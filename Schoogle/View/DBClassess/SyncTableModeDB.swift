//  SyncTableModeDB.swift
//  Schoogle
//  Created by Malhar on 14/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit
import SQLite

class SyncTableModeDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrSyncTableModeDownloaded = [SyncTableMode]()
    var arrSyncTableMode = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    //Init with database method when Download database and copyData
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrSyncTableModeDownloaded = self.setDataIntoSyncTableModeModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrSyncTableMode = self.getOnlyIDsFromDatabase()
        
    }
    
    
    //Init with database method when Call Api and copyData
    init(arrSyncTableModeDoanloaded : [SyncTableMode]) {
        super.init()
        
        arrSyncTableModeDownloaded = arrSyncTableModeDoanloaded
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrSyncTableMode = self.getOnlyIDsFromDatabase()
        
    }
    
    //Insert data
    func insertSyncTableModeData() -> Bool{
        var isSuccess = false
        
        if arrSyncTableMode.count > 0 {
            
            let arrOfNewSyncTableMode = self.compareSyncTableMode(arrSyncTableModeDownload: arrSyncTableModeDownloaded, arrSyncTableModeApplication: arrSyncTableMode)
            
            if arrOfNewSyncTableMode.count > 0 {
                isSuccess = self.insertSyncTableMode(arrNewSyncTableMode: arrOfNewSyncTableMode)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }else{
            
            if arrSyncTableModeDownloaded.count > 0 {
                isSuccess = self.insertSyncTableMode(arrNewSyncTableMode: arrSyncTableModeDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }
        return isSuccess

    }
    
    //Download data and insert into model
    func setDataIntoSyncTableModeModel() -> [SyncTableMode] {
        
        var arrSyncTableModeTemp = [SyncTableMode]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from SyncTableMode", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let SyncTableModeId = sqlite3_column_int(queryStatement, 0)
                    let Mode = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                  
                    
                    let objSyncTableModeModel = SyncTableMode(SyncTableModeId: SyncTableModeId, Mode: Mode)
                    
                    arrSyncTableModeTemp.append(objSyncTableModeModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrSyncTableModeTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrSyncTableModeTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from SyncTableMode", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrSyncTableModeTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrSyncTableModeTemp
    }
    
    // Check SyncTableMode is exist or not
    func compareSyncTableMode(arrSyncTableModeDownload : [SyncTableMode] , arrSyncTableModeApplication : [IDModel]) -> [SyncTableMode]{
        
        var arrNewSyncTableMode = [SyncTableMode]()
        if arrSyncTableModeApplication.count > 0{
            for SyncTableModeObj in arrSyncTableModeDownload{
                let filterObj = arrSyncTableModeApplication.filter({ $0.id == SyncTableModeObj.SyncTableModeId })
                if filterObj.count == 0{
                    arrNewSyncTableMode.append(SyncTableModeObj)
                }
            }
        }else{
            arrNewSyncTableMode.append(contentsOf: arrSyncTableModeDownload)
        }
        return arrNewSyncTableMode
    }
    
    //Insert SyncTableMode into database
    func insertSyncTableMode(arrNewSyncTableMode : [SyncTableMode]) -> Bool{
        var isSuccess = false
        for newSyncTableMode in arrNewSyncTableMode {
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(SyncTableMode: newSyncTableMode))
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
                break
            }
        }
        arrSyncTableMode.removeAll()
        arrSyncTableModeDownloaded.removeAll()
        print("SyncTableMode Done")
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(SyncTableMode : SyncTableMode) -> String {
        
        return "INSERT INTO SyncTableMode(SyncTableModeId,Mode) VALUES (\(SyncTableMode.SyncTableModeId),'\(SyncTableMode.Mode)');"
        
    }
    
    
    //UPDATE -----------------------------------
    func searchSyncTableModeData(SyncTableModeId : Int32) -> SyncTableMode{
        
        var objSyncTableMode = SyncTableMode()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from SyncTableMode where SyncTableModeId = \(SyncTableModeId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW)    {
                    
                    let SyncTableModeId = sqlite3_column_int(queryStatement, 0)
                    let Mode = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    
                    
                    objSyncTableMode = SyncTableMode(SyncTableModeId: SyncTableModeId, Mode: Mode)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objSyncTableMode
        
    }
    
    //Perform Update Operation
    func updateSyncTableModeData(SyncTableMode : SyncTableMode) -> Bool{
        
        var isSuccess = false
        //Seach SyncTableMode
        let objSyncTableMode = searchSyncTableModeData(SyncTableModeId: SyncTableMode.SyncTableModeId)
        
        if objSyncTableMode.SyncTableModeId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(SyncTableMode: objSyncTableMode))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(SyncTableMode: SyncTableMode))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery(SyncTableMode : SyncTableMode) -> String {
        
        return "UPDATE SyncTableMode SET SyncTableMode = '\(SyncTableMode.Mode)' where SyncTableModeId = \(SyncTableMode.SyncTableModeId)"
        
    }
    
}
