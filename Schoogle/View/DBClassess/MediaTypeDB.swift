//  MediaTypeDB.swift
//  Schoogle
//  Created by Malhar on 11/12/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit
import SQLite
class MediaTypeDB: NSObject {
    
    var sqliteManager = SQLITEManager.shareInstanceSQLITEManager
    var arrMediaTypeDownloaded = [MediaType]()
    var arrMediaType = [IDModel]()
    
    // Init Method
    override init() {
        super.init()
    }
    
    //Init with database method when Download database and copyData
    init(dbName : String) {
        super.init()
        
        sqliteManager.pathToDatabase = sqliteManager.getDownloadedDatabasePath(dbName: dbName)
        sqliteManager.db = sqliteManager.openDatabase()
        arrMediaTypeDownloaded = self.setDataIntoMediaTypeModel()
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrMediaType = self.getOnlyIDsFromDatabase()
        
    }
    
    
    //Init with database method when Call Api and copyData
    init(arrMediaTypeDoanloaded : [MediaType]) {
        super.init()
        
        arrMediaTypeDownloaded = arrMediaTypeDoanloaded
        
        sqliteManager.pathToDatabase = sqliteManager.getApplicationDatabasePath()
        sqliteManager.db = sqliteManager.openDatabase()
        arrMediaType = self.getOnlyIDsFromDatabase()
        
    }
    
    //Insert data
    func insertMediaTypeData() -> Bool{
        var isSuccess = false
        
        if arrMediaType.count > 0 {
            let arrOfNewMediaType = self.compareMediaType(arrMediaTypeDownload: arrMediaTypeDownloaded, arrMediaTypeApplication: arrMediaType)
            
            if arrOfNewMediaType.count > 0 {
                isSuccess = self.insertMediaType(arrNewMediaType: arrOfNewMediaType)
            }else{
                print("No Any Update")
                isSuccess = true
            }
            
        }else{
            
            if arrMediaTypeDownloaded.count > 0 {
                isSuccess = self.insertMediaType(arrNewMediaType: arrMediaTypeDownloaded)
            }else{
                print("No Any Update")
                isSuccess = true
            }
        }

        //sqliteManager.closeDB()
        return isSuccess
    }
    
    //Download data and insert into model
    func setDataIntoMediaTypeModel() -> [MediaType] {
        
        var arrMediaTypeTemp = [MediaType]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from MediaType", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let MediaTypeId = sqlite3_column_int(queryStatement, 0)
                    let MediaType_FLD = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let FileExtnAllowed = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ListOrder = sqlite3_column_int(queryStatement, 3)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let IsActive = sqlite3_column_int(queryStatement, 5)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 8)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 9)
                    let IsDeleted = sqlite3_column_int(queryStatement, 10)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 11)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))

                    let objMediaTypeModel = MediaType(MediaTypeId: MediaTypeId, MediaType: MediaType_FLD, FileExtnAllowed: FileExtnAllowed, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                    arrMediaTypeTemp.append(objMediaTypeModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTypeTemp
        
    }
    
    func getOnlyIDsFromDatabase() -> [IDModel]{
        
        var arrMediaTypeTemp = [IDModel]()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from MediaType", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    
                    let id = sqlite3_column_int(queryStatement, 0)
                    
                    let objIDModel = IDModel(id: id)
                    
                    arrMediaTypeTemp.append(objIDModel)
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        return arrMediaTypeTemp
    }
    
    // Check MediaType is exist or not
    func compareMediaType(arrMediaTypeDownload : [MediaType] , arrMediaTypeApplication : [IDModel]) -> [MediaType]{
        
        var arrNewMediaType = [MediaType]()
        if arrMediaTypeApplication.count > 0{
            for MediaTypeObj in arrMediaTypeDownload{
                let filterObj = arrMediaTypeApplication.filter({ $0.id == MediaTypeObj.MediaTypeId })
                if filterObj.count == 0{
                    arrNewMediaType.append(MediaTypeObj)
                }
            }
        }else{
            arrNewMediaType.append(contentsOf: arrMediaTypeDownload)
        }
        return arrNewMediaType
    }
    
    //Insert MediaType into database
    func insertMediaType(arrNewMediaType : [MediaType]) -> Bool{
        var isSuccess = false
        for newMediaType in arrNewMediaType {
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(MediaType: newMediaType))
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
                break
            }
        }
        arrMediaType.removeAll()
        arrMediaTypeDownloaded.removeAll()
        print("MediaType Done")
        return isSuccess
    }
    
    //Insert Query
    func makeInsertQuery(MediaType : MediaType) -> String {
        
        return "INSERT INTO MEDIATYPE (MediaTypeId,MediaType,FileExtnAllowed,ListOrder,Remarks,IsActive,CreatedDate,UpdatedDate,CreatedUserId,UpdatedUserId,IsDeleted,DeletedUserId,DeletedDate) VALUES (\(MediaType.MediaTypeId),'\(MediaType.MediaType)' ,'\(MediaType.FileExtnAllowed)' ,\(MediaType.ListOrder),'\(MediaType.Remarks)' ,\(MediaType.IsActive),'\(MediaType.CreatedDate)' ,'\(MediaType.UpdatedDate)' ,\(MediaType.CreatedUserId),\(MediaType.UpdatedUserId),\(MediaType.IsDeleted),\(MediaType.DeletedUserId),'\(MediaType.DeletedDate)');"
        
    }
    
    
    //UPDATE -----------------------------------
    func searchMediaTypeData(MediaTypeId : Int32) -> MediaType{
        
        var objMediaType = MediaType()
        
        if sqliteManager.openDatabase() != nil{
            sqliteManager.selectTable(query: "select * from MediaType where MediaTypeId = \(MediaTypeId)", success: { (queryStatement) in
                while (sqlite3_step(queryStatement) == SQLITE_ROW)    {
                    
                    let MediaTypeId = sqlite3_column_int(queryStatement, 0)
                    let MediaType_FLD = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 1))
                    let FileExtnAllowed = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 2))
                    let ListOrder = sqlite3_column_int(queryStatement, 3)
                    let Remarks = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 4))
                    let IsActive = sqlite3_column_int(queryStatement, 5)
                    let CreatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 6))
                    let UpdatedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 7))
                    let CreatedUserId = sqlite3_column_int(queryStatement, 8)
                    let UpdatedUserId = sqlite3_column_int(queryStatement, 9)
                    let IsDeleted = sqlite3_column_int(queryStatement, 10)
                    let DeletedUserId = sqlite3_column_int(queryStatement, 11)
                    let DeletedDate = self.sqliteManager.handleStringValue(ptrString: sqlite3_column_text(queryStatement, 12))
                    
                    objMediaType = MediaType(MediaTypeId: MediaTypeId, MediaType: MediaType_FLD, FileExtnAllowed: FileExtnAllowed, ListOrder: ListOrder, Remarks: Remarks, IsActive: IsActive, CreatedDate: CreatedDate, UpdatedDate: UpdatedDate, CreatedUserId: CreatedUserId, UpdatedUserId: UpdatedUserId, IsDeleted: IsDeleted, DeletedUserId: DeletedUserId, DeletedDate: DeletedDate)
                    
                }
                
            }){ (error) in
                print(error as Any)
            }
        }else{
            print("Somethinf went wrong")
        }
        
        return objMediaType
        
    }
    
    //Perform Update Operation
    func updateMediaTypeData(MediaType : MediaType) -> Bool{
        
        var isSuccess = false
        //Seach MediaType
        let objMediaType = searchMediaTypeData(MediaTypeId: MediaType.MediaTypeId)
        
        if MediaType.MediaTypeId != 0{ // if exist
            //Perform Update
            let isUpdate = sqliteManager.updateTable(query: self.makeUpdateQuery(MediaType: MediaType))
            
            if isUpdate {
                print("Updated Successfully")
                isSuccess = true
            }else{
                print("Not Updated")
                isSuccess = false
            }
            
        }else{ // not exist
            //Perfor insert
            let isInsert = sqliteManager.insertTable(query: self.makeInsertQuery(MediaType: MediaType))
            
            if isInsert {
                //print("Inserted Successfully")
                isSuccess = true
            }else{
                print("Not Inserted")
                isSuccess = false
            }
            
        }
        
        return isSuccess
    }
    
    //Make update query
    func makeUpdateQuery(MediaType : MediaType) -> String {
        
        return "UPDATE MediaType SET MediaType = '\(MediaType.MediaType.replacingOccurrences(of: "\'", with: "\'\'"))' , FileExtnAllowed = '\(MediaType.FileExtnAllowed.replacingOccurrences(of: "\'", with: "\'\'"))' , ListOrder = '\(MediaType.ListOrder)' , Remarks = '\(MediaType.Remarks.replacingOccurrences(of: "\'", with: "\'\'"))' , IsActive = '\(MediaType.IsActive)' , CreatedDate = '\(MediaType.CreatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , UpdatedDate = '\(MediaType.UpdatedDate.replacingOccurrences(of: "\'", with: "\'\'"))' , CreatedUserId = '\(MediaType.CreatedUserId)' , UpdatedUserId = '\(MediaType.UpdatedUserId)' , IsDeleted = '\(MediaType.IsDeleted)' , DeletedUserId = '\(MediaType.DeletedUserId)' , DeletedDate = '\(MediaType.DeletedDate.replacingOccurrences(of: "\'", with: "\'\'"))' where MediaTypeId = '\(MediaType.MediaTypeId)'"
        
    }
    
}

