//  AppDelegate.swift
//  Schoogle
//  Created by Malhar on 23/10/18.
//  Copyright © 2018 Malhar. All rights reserved.

import UIKit
import CoreData
import IQKeyboardManagerSwift
import SideMenu
import UserNotifications
import Zip
import Alamofire
import Fabric
import Crashlytics
import MBProgressHUD
import Alamofire
import SQLite
import FirebaseAnalytics
import FirebaseCore
import FirebaseCrashlytics


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var shouldRotate = true
    var isSelectBoardIcon = false
    var arrRecentPageList = [Int]()
    var arrSidemenu = [MenuModel]()
    var mbHUD = MBProgressHUD()
    var arrGetMediaDelete = [Media]()
    var arrNotification = NSArray()
    var apnID = Int()
    var WorkType = Int()
    var sqlitemanager = SQLITEManager.shareInstanceSQLITEManager
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        self.updateUserPreference()
        self.checkuserRegistration()
        SQLITEManager.shareInstanceSQLITEManager.copynotificationTable()
        self.insertNewColumnInNotificationTable()
        
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        let device_id = UIDevice.current.identifierForVendor!.uuidString
        Constant.DEVICE_ID = device_id
        
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        self.registerForPushNotifications()
        
        self.checkIsFirstTimeSyncOrNot()
        self.statusBarBGColor()
        
        let strAssets =  UserDefaults.standard.bool(forKey: "assets")
        self.createContentFolder()
        //          if strAssets != true {
        //             self.copyFolders()
        //          }
        //---------------------------------------
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        let currentAppVersion = "\(appVersion!).\(buildVersion!)"
        print("CurrentVersion :\(currentAppVersion),OldVesrion : \(Constant.appOldVersion)")
        
        if MyVariables.appOldVersion != currentAppVersion {
            MyVariables.appOldVersion = currentAppVersion
            print(MyVariables.appOldVersion)
            self.copyFolders()
        }
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        //self.demoNotification()
        return true
    }
    
    //MARK: INSERT NEW COLUMN IN NOTIFICATION TABLE FOR FIRST TIME WHEN USER UPDATE APPLICATION
    func insertNewColumnInNotificationTable(){
        
        if UserDefaults.standard.bool(forKey: Constant.USER_IS_COLUMN) != true{
            
            sqlitemanager.pathToDatabase = sqlitemanager.getApplicationNotificationDatabasePath()
            sqlitemanager.db = sqlitemanager.openDatabase()
            if sqlitemanager.openDatabase() != nil{
                
                var issuccess = false
                if sqlitemanager.openDatabase() != nil{
                    issuccess = sqlitemanager.alterTable(query:"ALTER TABLE notificationTable ADD COLUMN workType INTEGER")
                    issuccess = sqlitemanager.alterTable(query:"ALTER TABLE notificationTable ADD COLUMN apnID INTEGER")
                    issuccess = sqlitemanager.alterTable(query:"ALTER TABLE notificationTable ADD COLUMN createdDate TEXT")
                }
                print("success is ====>",issuccess)
            }else{
                print("Something went wrong")
            }
            sqlitemanager.closeDB()
        }
        
        UserDefaults.standard.set(true, forKey:Constant.USER_IS_COLUMN)
        UserDefaults.standard.synchronize()
    }
    
    //MARK: Update User Preferance Value
    func updateUserPreference(){
        
        if UserDefaults.standard.bool(forKey: Constant.USER_IS_SNK) != true{
            
            let isSyncMain = ""
            let isSyncMedia = ""
            
            // print("Sync Url is ==========>",Constant.CONST_SYNC_URL)
            // print("Sync Media Url is ====>",Constant.MEDIA_URL_STATIC)
            
            if isSyncMain  == ""{
                UserDefaults.standard.set(Constant.CONST_SYNC_URL, forKey:Constant.USER_DEFAULT_SYNC_URL)
            }
            if isSyncMedia == ""{
                UserDefaults.standard.set(Constant.MEDIA_URL_STATIC, forKey: Constant.USER_DEFAULT_MEDIA_URL)
            }
            UserDefaults.standard.set(true, forKey:Constant.USER_IS_SNK)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    //MARK: CHECK USER REGISTRATION IS DONE OR NOT
    func checkuserRegistration(){
        
        if UserDefaults.standard.bool(forKey:Constant.USER_REGISTER) == true{
            
            if let deviceToken = UserDefaults.standard.value(forKey:Constant.USER_AUTHTOKEN) as? String{
                Constant.DEVICE_TOKEN = deviceToken
            }
            if let macID = UserDefaults.standard.value(forKey: Constant.USER_DEFAULT_MAC_ID) as? String{
                Constant.MACID = macID
                print("MACID is:- \(Constant.MACID)")
            }
            if let syncURL = UserDefaults.standard.value(forKey: Constant.USER_DEFAULT_SYNC_URL) as? String{
                Constant.SYNC_URL = syncURL
            }
        }else{
            Constant.SYNC_URL = Constant.CONST_SYNC_URL
        }
        
    }
    
    //MARK: SEND DEVICE TOKEN TO SERVER :
    func callDeviceTokenRegister(){
        
        let strAPIURL = "\(Constant.SYNC_URL)pushapi.ashx?macid=\(Constant.MACID)&devicetoken=\(Constant.DEVICE_TOKEN)&appversion=\(Constant.APP_VERSION ?? "")"
        
        APIManager.shared.fetchDataFromAPI(strUrl: strAPIURL , parameter: [:], header: [:], successResponse: {  (arrResponse) in
            
            if let _ = arrResponse{
                
                if let dictStatus = arrResponse?.object(at: 0) as? [String:Any]{
                    if let arrStatus = dictStatus["Status"] as? [[String : Any]]{
                        //print(arrStatus[0])
                        if let responseStatus = arrStatus[0]["status"] as? String{
                            if responseStatus == "Success"{
                                
                                UserDefaults.standard.set(true, forKey: Constant.USER_REGISTER)
                                UserDefaults.standard.set(Constant.DEVICE_TOKEN, forKey: Constant.USER_AUTHTOKEN)
                                UserDefaults.standard.synchronize()
                                
                            }else{
                                print(responseStatus)
                                return
                            }
                        }else{
                            
                        }
                        
                    }else{
                        print("No Any Status Array ")
                    }
                }else{
                    print("No Any Status ")
                }
                
            }
            
        }) { (error) in
            
        }
        
    }
    
    //MARK: SHOW CUSTOM LOADER
    func showCustomLoader(strMessage : String){
        
        if let windowObj = self.window{
            
            self.hideCustomLoader()
            self.mbHUD = MBProgressHUD.showAdded(to: windowObj, animated: true)
            self.mbHUD.animationType = .fade
            self.mbHUD.backgroundColor = UIColor.init(white: 0.1, alpha: 0.45)
            self.mbHUD.mode = .indeterminate
            self.mbHUD.label.numberOfLines = 0
            self.mbHUD.label.text = strMessage
        }
    }
    
    func showCustomProgress(strMessage : String){
        
        if let windowObj = self.window{
            
            self.hideCustomLoader()
            self.mbHUD = MBProgressHUD.showAdded(to: windowObj, animated: true)
            self.mbHUD.animationType = .fade
            self.mbHUD.backgroundColor = UIColor.init(white: 0.1, alpha: 0.45)
            self.mbHUD.mode = .annularDeterminate
            self.mbHUD.progress = 0.0
            self.mbHUD.label.numberOfLines = 0
            self.mbHUD.label.text = strMessage
        }
    }
    
    func updateProgress(progress : Float){
        if(self.mbHUD.isHidden){
            showCustomLoader(strMessage: "Downloading Media...")
        }
        self.mbHUD.progress = progress
    }
    
    func hideCustomLoader(){
        self.mbHUD.hide(animated: true)
    }
    
    //MARK: CREATE CONTENT FOLDER IN DOCUMENT DIRECTORY
    func createContentFolder(){
        
        let paths = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
        let dataPath = paths.appendingPathComponent("Content")
        
        if FileManager.default.fileExists(atPath: dataPath.path) == false{
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        
        let pathCSS = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
        let dataPathCSS = pathCSS.appendingPathComponent("theme")
        
        if FileManager.default.fileExists(atPath: dataPathCSS.path) == false{
            do {
                try FileManager.default.createDirectory(atPath: dataPathCSS.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        
        let paths1 = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
        let dataPath1 = paths1.appendingPathComponent("Database")
        
        if FileManager.default.fileExists(atPath: dataPath1.path) == false{
            do {
                try FileManager.default.createDirectory(atPath: dataPath1.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        
        let paths2 = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
        let dataPath2 = paths2.appendingPathComponent("Database").appendingPathComponent("ZIP")
        
        if FileManager.default.fileExists(atPath: dataPath2.path) == false{
            do {
                try FileManager.default.createDirectory(atPath: dataPath2.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        
        let paths3 = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
        let dataPath3 = paths3.appendingPathComponent("Database").appendingPathComponent("UNZIP")
        
        if FileManager.default.fileExists(atPath: dataPath3.path) == false{
            do{
                try FileManager.default.createDirectory(atPath: dataPath3.path, withIntermediateDirectories: false, attributes: nil)
            }catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    //MARK: REMOVE CONTENT FOLDER
    func removeContentFolder(){
        
        let paths = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
        let dataPath = paths.appendingPathComponent("Content")
        if FileManager.default.fileExists(atPath: dataPath.path) == true{
            do{
                try FileManager.default.removeItem(atPath: dataPath.path)
            }catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    //MARK: REMOVE THEME FOLDER
    func removeThemeFolder(){
        
        let pathCSS = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
        let dataPathCSS = pathCSS.appendingPathComponent("theme")
        
        if FileManager.default.fileExists(atPath: dataPathCSS.path) == true{
            do {
                try FileManager.default.removeItem(atPath: dataPathCSS.path)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    //MARK: REMOVE UMKNOWN ZIP FOLDER
    func removeUnkwnonZipFolder(){
        
        let fileManager = FileManager.default
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
        let documentsPath = documentsUrl.path
        
        do{
            if let documentPath = documentsPath
            {
                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache: \(fileNames)")
                for fileName in fileNames {
                    
                    if (fileName.hasSuffix(".zip")){
                        let filePathName = "\(documentPath)/\(fileName)"
                        try fileManager.removeItem(atPath: filePathName)
                    }
                }
                let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache after deleting images: \(files)")
            }
            
        } catch {
            print("Could not clear temp folder: \(error)")
        }
        
    }
    
    func statusBarBGColor() {
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = UIColor.init(red: 56/255.0, green: 42/255.0, blue: 94/255.0, alpha: 1.0)
            statusBar.frame.size.width = UIScreen.main.bounds.size.width
            UIApplication.shared.keyWindow?.addSubview(statusBar)
            
        }else{
            guard let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
                return
            }
            statusBarView.backgroundColor = UIColor.init(red: 56/255.0, green: 42/255.0, blue: 94/255.0, alpha: 1.0)
        }
        
    }
    
    //MARK: - PUSH NOTIFICATION DELEGATE METHOD
    func registerForPushNotifications() {
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            if let error = error {
                print("D'oh: \(error.localizedDescription)")
            }else{
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        Constant.DEVICE_TOKEN = token
        self.callDeviceTokenRegister()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        completionHandler([.alert,.sound])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // 1. Print out error if PNs registration not successful
        print("Failed to register for remote notifications with error: \(error)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("user info notification is ===>",response.notification.request.content.userInfo)
        self.pushNotificationRedirect(userInfo:response.notification.request.content.userInfo)
    }
    
    func demoNotification(){
        
        // 101 = For content and media sync
        // 102 =  only for media download
        // 103 = only content sync
        // 104 = only allocation sync
        // 105 =  only Icon Sync
        // 106 = only common media sync
        // 103 =  for clear all data...
        // 201 = delete grade content and media from table
        // 202 = delete subject content and media from table
        // 203 = delete BLM content and media from table
        // 207 = clear content and media
        // 301 = for check particular apns status
        
        
        //        [AnyHashable("aps"): {
        //            alert = "New BLM and Media";
        //            badge = 9;
        //            sound = default;
        //        }, AnyHashable("msg"): {
        //            apnid = 367;
        //            idlist =     (
        //                42
        //            );
        //            worktype = 107;
        //        }]
        
        let mediaziplist = [42]
        let worktype = 107
        let apnid = 367
        let userInfo = ["msg": ["apnid": apnid, "idlist": mediaziplist, "worktype" : worktype]]
        self.pushNotificationRedirect(userInfo: userInfo)
    }
    
    //MARK: REDIRECT TO NEXT SCREEN WHEN RECEIVE NOTIFICATION
    func pushNotificationRedirect(userInfo:[AnyHashable : Any]){
        
        UserDefaults.standard.set(true, forKey: "isPushReceived")
        if let aps = userInfo[AnyHashable("msg")] as? [String:Any]{
            
            self.arrNotification = aps["idlist"] as? NSArray ?? [0]
            // print("arr notfication is =====>",self.arrNotification)
            
            if let worktype = aps["worktype"] as? Int{
                self.WorkType = worktype
            }
            if let apnID = aps["apnid"] as? Int{
                self.apnID = apnID
            }
            
            var apnStatusID = 0
            
            if self.WorkType != 301{
                
                var isSuccess = false
                let notificationDB = NotificationDB(dbName:Constant.notificationDB)
                isSuccess = notificationDB.insertNotificationDataFromAppDelegate()
                print("success is ====>",isSuccess)
            }
            
            if let worktype = aps["worktype"] as? Int{
                UserDefaults.standard.set(worktype, forKey: "worktype")
            }
            if let apnID = aps["apnid"] as? Int{
                UserDefaults.standard.set(apnID, forKey:"apnid")
                apnStatusID = apnID
            }
            
        }
        
        UserDefaults.standard.synchronize()
        
        if let navController = self.window?.rootViewController as? UINavigationController{
            
            for standardvc in  navController.viewControllers{
                if let currentVC = standardvc as? StandardViewController{
                    Constant.APPDELEGATE?.showCustomLoader(strMessage: "SyncMain...")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        currentVC.redirectApnsMethod()
                    }
                    navController.popToViewController(currentVC, animated:true)
                }else{
                    print("check other view controller.........")
                }
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        
        if let data = text.data(using: .utf8) {
            do{
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // Common alertView
    func showAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        DispatchQueue.main.async {
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    static var visibleViewController: UIViewController? {
        
        var currentVc = UIApplication.shared.keyWindow?.rootViewController
        while let presentedVc = currentVc?.presentedViewController {
            if let navVc = (presentedVc as? UINavigationController)?.viewControllers.last {
                currentVc = navVc
            } else if let tabVc = (presentedVc as? UITabBarController)?.selectedViewController {
                currentVc = tabVc
            } else {
                currentVc = presentedVc
            }
        }
        return currentVc
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("App in BackGround...")
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Schoogle")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func setSideMenuItems(arrSideMenu : [MenuModel]){
        self.arrSidemenu = arrSideMenu
    }
    
    private func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> Int {
        if shouldRotate == true {
            return Int(UIInterfaceOrientationMask.portrait.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.landscapeLeft.rawValue)
        }
    }
    //MARK: CHECK SYNC OR NOT FIRSTTIME
    func checkIsFirstTimeSyncOrNot(){
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //---------- If Exam Running Re-Direc to ExamVC ------
       // UserDefaults.standard.set(false, forKey:Constant.Exam_Start)
        if UserDefaults.standard.bool(forKey: Constant.Exam_Start) == true {
           
          /*  let examVC = storyboard.instantiateViewController(withIdentifier: "ExamVC") as? ExamVC
            //examVC?.count = leftTime
            examVC?.isFromAppdelegate = true
            let navigationController = UINavigationController(rootViewController: examVC!)
            self.window?.rootViewController = navigationController*/
            let StandardViewController = storyboard.instantiateViewController(withIdentifier: "StandardViewController") as? StandardViewController
        
           // let AssessmentVC = storyboard.instantiateViewController(withIdentifier: "AssessmentVC") as? AssessmentVC
           // let AssessmentVC = storyboard.instantiateViewController(withIdentifier: "NewAssessmentVC") as? NewAssessmentVC

            let examVC = storyboard.instantiateViewController(withIdentifier: "ExamVC") as? ExamVC
            examVC?.isFromAppdelegate = true
            examVC?.isAppKill = true
           // let navigationController = UINavigationController(rootViewController: AssessmentVC!)
            let navigationController = UINavigationController(rootViewController: StandardViewController!)
            
            navigationController.navigationBar.isHidden = true
           // navigationController.viewControllers = [StandardViewController,AssessmentVC,examVC] as! [UIViewController]
            navigationController.viewControllers = [StandardViewController,examVC] as! [UIViewController]
            self.window?.rootViewController = navigationController
        
            
        }
        else {
            if let base_URL = UserDefaults.standard.value(forKey: "firstSyncFinish") as? Bool, base_URL == true{
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "StandardViewController") as? StandardViewController
                let navigationController = UINavigationController(rootViewController: initialViewController!)
                self.window?.rootViewController = navigationController
                
            }else{
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "SettingViewController") as? SettingViewController
                let navigationController = UINavigationController(rootViewController: initialViewController!)
                self.window?.rootViewController = navigationController
            }
        }
        self.window?.makeKeyAndVisible()
    }
    
    
  /*  func callAPIForGetCurrentDate() {
        
        Constant.APPDELEGATE?.showCustomLoader(strMessage: "")
        print("http://service.snk.schoogle.co.in/AndroidForStandardAndChapter/SyncGeneral.ashx?method=GetStandard")
        APIManager.shared.fetchDataFromAPI(strUrl:"http://service.snk.schoogle.co.in/AndroidForStandardAndChapter/SyncGeneral.ashx?method=GetStandard", parameter: [:], header: [:], successResponse: { (dictResponse) in
            
            if dictResponse != nil{
                
                if let dictStatus = dictResponse?.object(at: 0) as? [String:Any]{
                    
                    print((dictStatus["serverCurrentDate"] as? String)!)
                    
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                }else{
                    print("No Any Status ")
                    
                }
                Constant.APPDELEGATE?.hideCustomLoader()
                
            }else{
                Constant.APPDELEGATE?.hideCustomLoader()
            }
        }) { (Error) in
            print(Error?.localizedDescription ?? "")
            Constant.APPDELEGATE?.hideCustomLoader()
            
        }
    }
    */
    
    
    func getSaveFileUrl(fileName: String) -> URL {
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let nameUrl = URL(string: fileName)
        let fileURL = documentsURL.appendingPathComponent((nameUrl?.lastPathComponent)!)
        NSLog(fileURL.absoluteString)
        return fileURL;
    }
    
    func copyFolders() {
        let bundleURL = Bundle.main.url(forResource: "assets", withExtension: "zip")
        self.checkAndStoreMediaDownload(strFilePAth: bundleURL!)
    }
    
    func copyFiles(pathFromBundle : String, pathDestDocs: String) {
        let fileManagerIs = FileManager.default
        fileManagerIs.delegate = self as? FileManagerDelegate
        
        do {
            let filelist = try fileManagerIs.contentsOfDirectory(atPath: pathFromBundle)
            try? fileManagerIs.copyItem(atPath: pathFromBundle, toPath: pathDestDocs)
            
            for filename in filelist {
                try? fileManagerIs.copyItem(atPath: "\(pathFromBundle)/\(filename)", toPath: "\(pathDestDocs)/\(filename)")
            }
        } catch {
            print("\nError\n")
        }
    }
    
    //MARK: FOR DOWNLOADING MEDIA FILES AND STORES IN DIRECTORY
    func checkAndStoreMediaDownload(strFilePAth:URL){
        let docsDirURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            try FileManager.default.copyItem(at: strFilePAth, to:docsDirURL.appendingPathComponent("assets.zip"))
            self.extract(strUrl: docsDirURL.appendingPathComponent("assets.zip").path, destinationUrl: docsDirURL.path)
            
            let fileURLs = try FileManager.default.contentsOfDirectory(at: docsDirURL,includingPropertiesForKeys: nil,options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            
            for fileURL in fileURLs {
                if fileURL.pathExtension == "zip" {
                    try FileManager.default.removeItem(at: fileURL)
                }
            }
            UserDefaults.standard.set(true, forKey: "assets")
            UserDefaults.standard.synchronize()
            print("FILE SAVED!!")
        } catch let error as NSError {
            print("FILE FAILED SAVED!!  \(error.debugDescription)")
        }
    }
    
    //MARK: EXTRAT FILE
    func extract(strUrl : String, destinationUrl : String){
        
        do {
            let filePath = URL.init(string: strUrl)
            let documentsDirectory = URL(string: destinationUrl)
            if let filePath = filePath, let documentsDirectory = documentsDirectory{
                try Zip.unzipFile(filePath, destination: URL(fileURLWithPath: documentsDirectory.path), overwrite: true, password: "", progress: { (progress) -> () in
                    // print(progress)
                })
                print("FILE UNARCHIVED!!")
            }
        }catch {
            print("FILE FAILED UNARCHIVED!!")
        }
    }
    //MARK:- Orientation Setting
    
    var orientationLock = UIInterfaceOrientationMask.all
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
}

struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
}


